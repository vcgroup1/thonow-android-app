package dev.vcgroup.thonowandroidapp.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class Service {
    String id;
    String name;
    String unit;
    double price;
    int warrantyPeriod;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference serviceType;
}
