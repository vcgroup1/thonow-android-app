package dev.vcgroup.thonowandroidapp.data.model.fcm;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericFcmData<T> {
    private String type;
    private T object;
}
