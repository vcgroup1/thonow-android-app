package dev.vcgroup.thonowandroidapp.data.local.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification;

import static dev.vcgroup.thonowandroidapp.constant.LocalDatabaseConst.*;

@Dao
public interface NotificationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(FCMRemoteNotification notification);

    @Query("SELECT * FROM " + TBL_NOTIFICATION)
    List<FCMRemoteNotification> getAll();

    @Delete
    void delete(FCMRemoteNotification notification);
}
