package dev.vcgroup.thonowandroidapp.data.model.fcm;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.Address;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowandroidapp.data.model.OrderType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CallWorkerOrderPayload {
    OrderType type;
    Date workingDate;
    String note;
    double estimatedFee;
    String customer;
    Address workingAddress;
    List<OrderDetail> orderDetails;

    public static CallWorkerOrderPayload convertFrom(Order order) {
        if (order != null) {
            CallWorkerOrderPayload payload = new CallWorkerOrderPayload();
            payload.setType(order.getType());
            payload.setWorkingAddress(order.getWorkingAddress());
            payload.setNote(order.getNote());
            payload.setEstimatedFee(order.getEstimatedFee());
            payload.setCustomer(order.getCustomer().getId());
            payload.setWorkingDate(order.getWorkingDate());
            payload.setOrderDetails(order.getOrderDetails());
        }
        return null;
    }
}
