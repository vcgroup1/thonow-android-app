package dev.vcgroup.thonowandroidapp.data.model;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;

import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    DocumentReference from;
    DocumentReference order;
    double amount;
    Date timestamp;

    public static Transaction newObject(FirebaseFirestore db, double amount, DocumentReference order) {
        return new Transaction(CommonUtil.getCurrentUserRef(db),
                                order,
                                amount,
                Calendar.getInstance().getTime());
    }
}
