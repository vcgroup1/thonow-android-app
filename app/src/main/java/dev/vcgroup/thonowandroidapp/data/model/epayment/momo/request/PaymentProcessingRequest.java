package dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Map;

import dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters;
import dev.vcgroup.thonowandroidapp.data.model.PaymentMethod;
import dev.vcgroup.thonowandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static android.app.SearchManager.APP_DATA;
import static dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters.APP_PAY_TYPE;
import static dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters.MERCHANT_CODE;
import static dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters.ORDER_ID;
import static dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters.PHONE_NUMBER_V1;
import static dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters.VERSION;


/*
	Doc: https://developers.momo.vn/#/docs/app_in_app?id=http-request
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingRequest {
	String partnerCode;
	String customerNumber;
	String partnerRefId;
	String appData;
	String hash;
	double version;
	int payType;
	String description;
	String extra_data;

	public static PaymentProcessingRequest createFrom(Map<String, Object> eventValue) throws Exception {
		if (!eventValue.isEmpty()) {
			PaymentProcessingRequest request = new PaymentProcessingRequest();
			request.setPartnerCode((String) eventValue.get(MERCHANT_CODE));
			request.setPartnerRefId((String) eventValue.get(ORDER_ID));
			request.setCustomerNumber((String) eventValue.get(PHONE_NUMBER_V1));
			request.setAppData((String) eventValue.get(MoMoParameters.APP_DATA));
			String hash = EPaymentUtil.generateRSA(eventValue);
			request.setHash(hash);
			request.setVersion(VERSION);
			request.setPayType(APP_PAY_TYPE);
			request.setDescription("Thanh toán bằng thẻ");
			request.setExtra_data("");
			return request;
		}
		return null;
	}
}
