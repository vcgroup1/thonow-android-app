package dev.vcgroup.thonowandroidapp.data.model.chat;

public enum MessageType {
    TEXT,
    IMAGE
}
