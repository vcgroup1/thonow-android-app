package dev.vcgroup.thonowandroidapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryInfo {
    private String name;
    private String alp2Code;
    private String callingCode;
}
