package dev.vcgroup.thonowandroidapp.data.model;

import androidx.annotation.DrawableRes;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class ServiceType {
    String id;
    String name;
    String drawableName;
}
