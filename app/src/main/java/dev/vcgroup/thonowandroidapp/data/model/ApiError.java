package dev.vcgroup.thonowandroidapp.data.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {
    @SerializedName("statusCode")
    int statusCode;
    @SerializedName("message")
    String message;
}
