package dev.vcgroup.thonowandroidapp.data.remote.epaymentservice;

import dev.vcgroup.thonowandroidapp.data.model.TransactionMethod;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaymentRetrofitClientInstance {
    private static final String DOMAIN_MOMO_SANDBOX = "https://test-payment.momo.vn/";
    private static final String DOMAIN_MOMO_PRODUCTION = "https://payment.momo.vn/";

    private static final String DOMAIN_VNPAY_SANDBOX = "http://sandbox.vnpayment.vn/";

    private static final String DOMAIN_PUBLIC_IP_ADDRESS = "http://ip.jsontest.com";

    private Retrofit retrofit;
    private static PaymentRetrofitClientInstance mInstance;

    public PaymentRetrofitClientInstance(TransactionMethod method) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json; charset=UTF-8")
                    .build();
            return chain.proceed(newRequest);
        }).build();

        String baseUrl = "";
        if (method == null) {
            baseUrl = DOMAIN_PUBLIC_IP_ADDRESS;
        } else {
            if (method == TransactionMethod.ZALOPAY) {
                baseUrl = "";
            } else if (method == TransactionMethod.VNPAY) {
                baseUrl = DOMAIN_VNPAY_SANDBOX;
            } else if (method == TransactionMethod.MOMO) {
                baseUrl = DOMAIN_MOMO_SANDBOX;
            }
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static synchronized PaymentRetrofitClientInstance getInstance(TransactionMethod method) {
        if (mInstance == null) {
            mInstance = new PaymentRetrofitClientInstance(method);
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
