package dev.vcgroup.thonowandroidapp.data.model;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@NoArgsConstructor
public class OrderDetailPayload {
    double unitPrice;
    int quantity;
    String service;
    double subtotal;

    public static List<OrderDetailPayload> convertFrom(List<OrderDetail> orderDetails) {
        if (orderDetails != null && !orderDetails.isEmpty()) {
            List<OrderDetailPayload> orderDetailPayloads = new ArrayList<>();
            orderDetails.forEach(orderDetail -> {
                OrderDetailPayload payload = new OrderDetailPayload();
                payload.setUnitPrice(orderDetail.getUnitPrice());
                payload.setQuantity(orderDetail.getQuantity());
                payload.setService(orderDetail.getService().getId());
                payload.setSubtotal(orderDetail.getSubtotal());
                orderDetailPayloads.add(payload);
            });
            return orderDetailPayloads;
        }
        return null;
    }
}
