package dev.vcgroup.thonowandroidapp.data.model;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class Offer {
    String id;
    String title;
    String subtitle;
    String content;
    String thumbnail;
    Date createdAt;
    String code;
}
