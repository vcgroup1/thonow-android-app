package dev.vcgroup.thonowandroidapp.data.model.fcm;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FCMSendData {
    private String to;
    private Map<String, String> data;
}
