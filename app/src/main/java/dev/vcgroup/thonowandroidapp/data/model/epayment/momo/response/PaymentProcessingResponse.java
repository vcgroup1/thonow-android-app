package dev.vcgroup.thonowandroidapp.data.model.epayment.momo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
	Read more: https://developers.momo.vn/#/docs/app_in_app?id=http-response
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentProcessingResponse {
	int status;
	String message;
	String transid;
	long amount;
	String signature;
}
