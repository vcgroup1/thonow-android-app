package dev.vcgroup.thonowandroidapp.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentMethod {
    CASH("Tiền mặt"),
    CREDIT_CARD ("Thẻ thanh toán");

    String title;
}
