package dev.vcgroup.thonowandroidapp.data.remote.fcmservice;

import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSingleSendData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IFCMService {
    @POST("fcm/send")
    Call<FCMResponse> sendCallWorkerRequest(@Body FCMSendCallWorkerData body);

    @POST("fcm/send")
    Call<FCMResponse> sendFcmNotification(@Body FCMSingleSendData<String> body);
}
