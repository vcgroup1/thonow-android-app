package dev.vcgroup.thonowandroidapp.data.model.fcm;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class FCMMultipleSendData<T> {
    @NonNull
    @SerializedName("data")
    private GenericFcmData<T> data;

    @NonNull
    @SerializedName("registration_ids")
    private List<String> registration_ids;

    @SerializedName("notification")
    private FCMNotification notification;
}