package dev.vcgroup.thonowandroidapp.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Parcel
@AllArgsConstructor
@NoArgsConstructor
public class Worker {
    String id;
    String displayName;
    String email;
    String phoneNumber;
    String photoUrl;
    String promoCode;
    float rating;
    boolean isOnline;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> serviceTypeList;
}
