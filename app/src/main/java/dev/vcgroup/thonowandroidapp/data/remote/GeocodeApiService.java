package dev.vcgroup.thonowandroidapp.data.remote;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GeocodeApiService {
    String GEOCODE_API_URL = "geocode/json";
    String GEOCODE_BING_API_URL = "Locations/{latlng}";

    @GET(GEOCODE_API_URL)
    Call<JsonObject> getFormattedAddress(@Query("latlng") String latlng);

    @GET(GEOCODE_BING_API_URL)
    Call<JsonObject> getBingFormattedAddress(@Path("latlng") String latlng);
}
