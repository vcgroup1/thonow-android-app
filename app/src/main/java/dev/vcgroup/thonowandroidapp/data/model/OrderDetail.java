package dev.vcgroup.thonowandroidapp.data.model;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@NoArgsConstructor
public class OrderDetail {
    double unitPrice;
    int quantity;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference service;
    double subtotal;

    public OrderDetail(DocumentReference service, int quantity) {
        this.quantity = quantity;
        this.service = service;
        this.service.get()
                .addOnSuccessListener(documentSnapshot -> {
                    this.unitPrice = Objects.requireNonNull(documentSnapshot.toObject(Service.class)).getPrice();
                    this.setSubtotal(this.quantity * this.unitPrice);
                })
                .addOnFailureListener(e -> Log.d("order_detail", e.getMessage()));
    }
}
