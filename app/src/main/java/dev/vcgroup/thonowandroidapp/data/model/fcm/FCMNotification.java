package dev.vcgroup.thonowandroidapp.data.model.fcm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FCMNotification {
    String title;
    String body;
    String icon;
}
