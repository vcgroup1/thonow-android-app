package dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendIPNRequest {
	String partnerCode;
	String accessKey;
	long amount;
	String partnerRefId;
	String partnerTransId;
	String transType;
	String momoTransId;
	int status;
	String message;
	long responseTime;
	String storeId;
	String signature;

}
