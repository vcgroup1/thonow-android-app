package dev.vcgroup.thonowandroidapp.data.model.epayment.vnpay;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import dev.vcgroup.thonowandroidapp.util.SequenceGenerator;
import dev.vcgroup.thonowandroidapp.util.ewallet.EPaymentUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentRequest {
	@NonNull
	String vnp_Version;
	@NonNull
	String vnp_Command;
	@NonNull
	String vnp_TmnCode;
	@NonNull
	long vnp_Amount;
	String vnp_BankCode;
	@NonNull
	String vnp_CreateDate;
	@NonNull
	String vnp_CurrCode;
	@NonNull
	String vnp_IpAddr;
	@NonNull
	String vnp_Locale;
	@NonNull
	String vnp_OrderInfo;
	long vnp_OrderType;
	@NonNull
	String vnp_ReturnUrl;
	@NonNull
	String vnp_TxnRef;
	@NonNull
	String vnp_SecureHashType;
	@NonNull
	String vnp_SecureHash;

	@SneakyThrows
	public static CreatePaymentRequest createFrom(Context context, String orderId, long amount, String bankCode) throws UnknownHostException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
		CreatePaymentRequest request = new CreatePaymentRequest();

		request.setVnp_IpAddr("0.0.0.0");
		request.setVnp_Version("2");
		request.setVnp_Command("pay");
		request.setVnp_TmnCode(BuildConfig.VNP_TMNCODE);
		request.setVnp_Amount(amount * 100);
		request.setVnp_BankCode(bankCode);

		@SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		request.setVnp_CreateDate(formatter.format(new Date()));

		request.setVnp_CurrCode("VND");
		request.setVnp_Locale("vn");
		request.setVnp_OrderInfo(orderId);
		request.setVnp_OrderType(260000);
		request.setVnp_TxnRef(orderId);
		String returnUrl ="https://thonow.page.link/successful-payment";
		request.setVnp_ReturnUrl(returnUrl);
		String hash = EPaymentUtil.generateVnPaySecureHash(request);
		request.setVnp_SecureHash(hash);
		request.setVnp_SecureHashType("SHA256");
		return request;
	}
}
