package dev.vcgroup.thonowandroidapp.data.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.util.OrderHelper;
import dev.vcgroup.thonowandroidapp.util.OrderIdSequenceGenerator;
import dev.vcgroup.thonowandroidapp.util.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    String id;
    OrderType type;
    Date workingDate;
    Date finishDate;
    String note;
    double movingExpense;
    double estimatedFee;
    double overtimeFee;
    double otherExpense;
    double tip;
    double total;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference customer;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference worker;
    List<OrderDetail> orderDetails;
    OrderStatus status;
    Address workingAddress;
    List<MaterialCost> materialCosts;
    Feedback feedback;
    List<String> images;

    public Order(OrderType orderType, Date workingDate, List<OrderDetail> orderDetails, Address workingAddress, DocumentReference customerDocRef, OrderStatus orderStatus, double movingExpense, double overtimeFee, double estimatedFee) {
        this.id = new OrderIdSequenceGenerator().getOrderId();
        this.type = orderType;
        this.workingDate = workingDate;
        this.orderDetails = orderDetails;
        this.workingAddress = workingAddress;
        this.customer = customerDocRef;
        this.status = orderStatus;
        this.estimatedFee = estimatedFee;
        this.overtimeFee = overtimeFee;
        this.movingExpense = movingExpense;
        this.total = estimatedFee + overtimeFee + movingExpense;
        this.worker = FirebaseFirestore.getInstance().document("");
    }

    public static Order createNewOrder(OrderType orderType, Date workingDate, List<OrderDetail> orderDetails,
                                       Address workingAddress, double movingExpense,
                                       double overtimeFee, double estimatedFee) {
        DocumentReference customerDocRef = FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_USER).document(
                Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        return new Order(orderType, workingDate, orderDetails, workingAddress, customerDocRef,
                OrderStatus.ALLOCATING, movingExpense, overtimeFee, estimatedFee);
    }

    public Order progressing() {
        if(status == null)
            this.status = OrderStatus.ALLOCATING;
        this.status = status.nextStatus();
        return this;
    }

    public Order cancel() {
        this.status = OrderStatus.CANCELED;
        return this;
    }
}
