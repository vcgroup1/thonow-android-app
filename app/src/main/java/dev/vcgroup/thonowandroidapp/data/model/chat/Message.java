package dev.vcgroup.thonowandroidapp.data.model.chat;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;

import dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    String id;
    String content;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference from;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference order;
    Date timestamp;
    MessageType type;

    public static Message createNewMessage(FirebaseFirestore db, String content, DocumentReference order, MessageType messageType) {
        Message messageResponse = new Message();
        messageResponse.setOrder(order);
        messageResponse.setContent(content);
        messageResponse.setType(messageType);
        messageResponse.setTimestamp(new Date());
        messageResponse.setFrom(CommonUtil.getCurrentUserRef(db));
        return messageResponse;
    }
}