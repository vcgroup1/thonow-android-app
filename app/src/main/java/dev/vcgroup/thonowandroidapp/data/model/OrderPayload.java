package dev.vcgroup.thonowandroidapp.data.model;

import com.google.android.gms.maps.model.LatLng;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPayload {
    String id;
    OrderType type;
    long workingDate;
    String note;
    double estimatedFee;
    double overtimeFee;
    String customer;
    List<OrderDetailPayload> orderDetails;
    Address workingAddress;
    OrderStatus status;

   public static OrderPayload convertFrom(Order order) {
       OrderPayload orderPayload = new OrderPayload();
       orderPayload.setId(order.getId());
       orderPayload.setType(order.getType());
       orderPayload.setWorkingDate(order.getWorkingDate() != null ? order.getWorkingDate().getTime() : 0);
       orderPayload.setNote(order.getNote());
       orderPayload.setEstimatedFee(order.getEstimatedFee());
       orderPayload.setOvertimeFee(order.getOvertimeFee());
       orderPayload.setCustomer(order.getCustomer() != null ? order.getCustomer().getId() : null);
       orderPayload.setOrderDetails(OrderDetailPayload.convertFrom(order.getOrderDetails()));
       orderPayload.setWorkingAddress(order.getWorkingAddress());
       orderPayload.setStatus(order.getStatus());
       return orderPayload;
   }
}
