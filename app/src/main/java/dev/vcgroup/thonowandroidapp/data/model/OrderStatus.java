package dev.vcgroup.thonowandroidapp.data.model;

import android.text.TextUtils;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {
    ALLOCATING ("Đang tìm thợ", "#3298dc") {
        @Override
        public OrderStatus nextStatus() {
                return OrderStatus.WAITING_FOR_WORK;
        }
    },
    WAITING_FOR_WORK ("Đang đợi làm việc", "#3298dc") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.ON_THE_MOVE;
        }
    },
    ON_THE_MOVE ("Đang di chuyển", "#17a2b8") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.ARRIVED;
        }
    },
    ARRIVED ("Đã đến nơi", "#FFA900") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.INVESTIGATING;
        }
    },
    INVESTIGATING ("Đang khảo sát", "#39C0ED") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.WAIT_FOR_ORDER_CONFIRMATION;
        }
    },
    WAIT_FOR_ORDER_CONFIRMATION ("Chờ xác nhận đơn", "#E65100") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.CONFIRMED_ORDER;
        }
    },
    CONFIRMED_ORDER ("Đơn đã xác nhận", "#827717") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.WORKING;
        }
    },
    WORKING ("Đang làm việc", "#006064") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.WAIT_FOR_CONFIRMATION_TO_COMPLETE;
        }
    },
    WAIT_FOR_CONFIRMATION_TO_COMPLETE ("Chờ xác nhận hoàn thành", "#1A237E"){
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.WAIT_FOR_CONFIRMATION_TO_PAY;
        }
    },
    WAIT_FOR_CONFIRMATION_TO_PAY ("Chờ xác nhận thanh toán", "#0091EA") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.COMPLETED;
        }
    },
    COMPLETED ("Đã hoàn thành", "#48c774") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.COMPLETED;
        }
    },
    CANCELED ("Đơn đã từ chối", "#f14668") {
        @Override
        public OrderStatus nextStatus() {
            return OrderStatus.CANCELED;
        }
    };

    private String title;
    private String color;

    public abstract OrderStatus nextStatus();

    public static OrderStatus getOrderStatusByTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            for (OrderStatus orderStatus : OrderStatus.values()) {
                if (orderStatus.title.equals(title)) {
                    return orderStatus;
                }
            }
        }
        return null;
    }
}
