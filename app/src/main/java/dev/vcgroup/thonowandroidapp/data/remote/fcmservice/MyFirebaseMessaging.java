package dev.vcgroup.thonowandroidapp.data.remote.fcmservice;

import android.app.ActivityOptions;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.local.database.MyDatabase;
import dev.vcgroup.thonowandroidapp.data.local.database.dao.NotificationDao;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification;
import dev.vcgroup.thonowandroidapp.data.model.fcm.GenericFcmData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.ui.audiocall.incomingcall.IncomingCallActivity;
import dev.vcgroup.thonowandroidapp.ui.callworker.foundwoker.FoundWorkerActivity;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.ChatActivity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static dev.vcgroup.thonowandroidapp.constant.AudioCallConst.INCOMING_CALL;
import static dev.vcgroup.thonowandroidapp.constant.AudioCallConst.NEW_MESSAGE;
import static dev.vcgroup.thonowandroidapp.constant.CallWorkerConst.RECEIVED_CALL_WORKER_REQUEST_CODE;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessaging.class.getSimpleName();
    private static FirebaseFirestore db;
    private static FirebaseAuth mAuth;
    static {
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }
    private NotificationDao notificationDao;
    @Getter
    @AllArgsConstructor
    private enum NotificationType {
        RECEIVE_CALL_WORKER_NOTIFICATION(501),
        NEW_MESSAGE_NOTIFICATION(502),
        NEW_AUDIO_CALL_NOTIFICATION(503);

        private int id;
    }

    @Override
    public void onNewToken(@NonNull @NotNull String token) {
        super.onNewToken(token);
        if (mAuth.getCurrentUser() != null) {
            Log.d(TAG, "refreshed token: " + token);
            Token tokenObj = new Token(token);
            db.collection(CollectionConst.COLLECTION_TOKEN)
                    .document(mAuth.getCurrentUser().getUid())
                    .set(tokenObj);
        }
    }

    @Override
    public void onMessageReceived(@NonNull @NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Gson gson = new Gson();
            GenericFcmData<String> data = gson.fromJson(remoteMessage.getData().toString(), GenericFcmData.class);

            // Check if message contains a data payload.
            if (data != null) {
                switch (data.getType()) {
                    case RECEIVED_CALL_WORKER_REQUEST_CODE:
                        Intent intent = new Intent(getApplicationContext(), FoundWorkerActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("orderId", data.getObject());
                        startActivity(intent, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                        break;
                    case INCOMING_CALL:
                        Intent intent1 = new Intent(getApplicationContext(), IncomingCallActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent1.putExtra("workerId", data.getObject());
                        startActivity(intent1, ActivityOptions.makeCustomAnimation(getApplicationContext(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                        break;
                }
            }

            // Check if message contains a notification payload.
            RemoteMessage.Notification notification = remoteMessage.getNotification();
            if (notification != null) {
                sendNotification(data, notification);
                FCMRemoteNotification remoteNotification = new FCMRemoteNotification(new Date(),
                        notification.getTitle(), notification.getBody());
                new SaveNotification().execute(remoteNotification);
                Log.d(TAG, "notification: " + notification.toString());
            }

        }
    }

    private void sendNotification(GenericFcmData<String> data, RemoteMessage.Notification notification) {
        String image = notification.getIcon();
        if (image != null && !image.isEmpty()) {
            Glide.with(this)
                    .asBitmap()
                    .load(image)
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull @NotNull Bitmap resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Bitmap> transition) {
                            createNewMessageNotification(notification, data, resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable @org.jetbrains.annotations.Nullable Drawable placeholder) {
                        }
                    });
        }
    }

    private void createNewMessageNotification(RemoteMessage.Notification notificationPayload, GenericFcmData<String> data, Bitmap resource) {
        Intent intent = null;
        NotificationType notificationType = null;
        // Create an explicit intent for an Activity in your app
        switch (data.getType()) {
            case RECEIVED_CALL_WORKER_REQUEST_CODE:
                notificationType = NotificationType.RECEIVE_CALL_WORKER_NOTIFICATION;
                intent = new Intent(getApplicationContext(), FoundWorkerActivity.class);
                intent.putExtra("orderId", data.getObject());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case INCOMING_CALL:
                notificationType = NotificationType.NEW_AUDIO_CALL_NOTIFICATION;
                intent = new Intent(getApplicationContext(), IncomingCallActivity.class);
                intent.putExtra("workerId", data.getObject());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case NEW_MESSAGE:
                String[] datas = data.getObject().split(";");
                notificationType = NotificationType.NEW_MESSAGE_NOTIFICATION;
                intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra("workerId", datas[0]);
                intent.putExtra("orderId", datas[1]);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            default:
                break;
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification notification = new Notification.Builder(this)
                .setContentTitle(notificationPayload.getTitle())
                .setContentText(notificationPayload.getBody())
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(resource)
                .setStyle(new Notification.BigTextStyle().bigText(notificationPayload.getTitle()))
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // notificationId is a unique int for each notification that you must define
        if (notificationManager != null && notificationType != null) {
            notificationManager.notify(getNotificationId(notificationType), notification);
        }
    }

    private int getNotificationId(NotificationType notificationType) {
        return Integer.parseInt(notificationType.getId() + "" + new Date().getTime());
    }

    class SaveNotification extends AsyncTask<FCMRemoteNotification, Void, Void> {

        @Override
        protected Void doInBackground(FCMRemoteNotification... fcmRemoteNotifications) {
            if (notificationDao == null) {
                notificationDao = MyDatabase.getDatabase(getApplicationContext()).notificationDao();
            }
            notificationDao.insert(fcmRemoteNotifications[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
        }
    }
}

