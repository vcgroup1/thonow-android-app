package dev.vcgroup.thonowandroidapp.data.model.fcm;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FCMSendCallWorkerData {
    @SerializedName("data")
    OrderPayload data;
    @SerializedName("registration_ids")
    List<String> registration_ids;

    public static FCMSendCallWorkerData createSendCallWorkerData(Order data, List<String> registration_ids) {
        return new FCMSendCallWorkerData(OrderPayload.convertFrom(data), registration_ids);
    }
}
