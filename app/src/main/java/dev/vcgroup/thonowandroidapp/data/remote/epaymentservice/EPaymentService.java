package dev.vcgroup.thonowandroidapp.data.remote.epaymentservice;

import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request.PaymentProcessingRequest;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request.TransactionConfirmationRequest;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.response.TransactionConfirmationResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface EPaymentService {

    @POST("pay/app")
    Call<PaymentProcessingResponse> processPayment(@Body PaymentProcessingRequest request);

    @POST("pay/confirm")
    Call<TransactionConfirmationResponse> processPaymentConfirmation(@Body TransactionConfirmationRequest request);

    @GET("/")
    Call<String> getPublicIpAddress();


}
