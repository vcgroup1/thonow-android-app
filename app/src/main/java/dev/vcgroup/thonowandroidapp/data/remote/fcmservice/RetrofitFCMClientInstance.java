package dev.vcgroup.thonowandroidapp.data.remote.fcmservice;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFCMClientInstance {
    private static final String BASE_URL = "https://fcm.googleapis.com/";
    private Retrofit retrofit;
    private static RetrofitFCMClientInstance mInstance;

    public RetrofitFCMClientInstance() {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", String.format("key=%s", BuildConfig.FCM_SERVER_KEY))
                    .build();
            return chain.proceed(newRequest);
        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitFCMClientInstance getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitFCMClientInstance();
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
