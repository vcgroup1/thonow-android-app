package dev.vcgroup.thonowandroidapp.data.remote;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    //http://dev.virtualearth.net/REST/v1/Locations/10.7910008,106.6820004?includeEntityTypes=Address&key=Ammvr2G3oo_Jylu_L0sdyU8EkbXC5IY21ey9AOHaJWKA-97dAJi7vzrnAlugi9w7
    //private static final String BASE_MAP_URL = "https://maps.googleapis.com/maps/api/";
    private static final String BASE_MAP_URL = "http://dev.virtualearth.net/REST/v1/";
    private Retrofit retrofit;
    private static RetrofitClientInstance mInstance;

    public RetrofitClientInstance() {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("key", BuildConfig.BING_MAP_API_KEY)
                    .addQueryParameter("includeEntityTypes", "Address")
                    .build();
            return chain.proceed(original.newBuilder().url(url).build());
        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static synchronized RetrofitClientInstance getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClientInstance();
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
