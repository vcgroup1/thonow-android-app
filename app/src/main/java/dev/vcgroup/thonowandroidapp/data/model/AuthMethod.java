package dev.vcgroup.thonowandroidapp.data.model;

public enum AuthMethod {
    PHONE,
    GOOGLE,
    FACEBOOK
}
