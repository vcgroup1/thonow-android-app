package dev.vcgroup.thonowandroidapp.ui.callworker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Visibility;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCallWorkerBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.CallWorkerFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation.DefineMyLocationFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation.picklocationinmap.PickLocationInMapFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.SelectServiceTypeFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice.SelectServiceFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;

public class CallWorkerActivity extends AppCompatActivity {
    private CallWorkerViewModel mViewModel;
    private Toolbar toolbar;
    private ActivityCallWorkerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_call_worker);

        mViewModel = ViewModelProviders.of(this).get(CallWorkerViewModel.class);
        if (getIntent() != null) {
            Service selectedService = Parcels.unwrap(getIntent().getParcelableExtra("selectedService"));
            if (selectedService != null) {
                mViewModel.setSelectedService(selectedService);
            }
        }

        FragmentUtil.replaceFragment(this, R.id.call_worker_fragment_container, new CallWorkerFragment(), null, CallWorkerFragment.class.getSimpleName(), false);
    }

    private void setupToolbar() {
        toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Gọi thợ");
        getSupportActionBar().show();

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            Fragment currentFragment = FragmentUtil.getCurrentFragment(CallWorkerActivity.this, R.id.call_worker_fragment_container);
            Log.d("currentFragment", currentFragment.toString());
            if (currentFragment instanceof CallWorkerFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Gọi thợ");
            } else if (currentFragment instanceof DefineMyLocationFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn địa điểm");
            } else if (currentFragment instanceof SelectServiceFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn dịch vụ");
            } else if (currentFragment instanceof SelectServiceTypeFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn loại dịch vụ");
            } else if (currentFragment instanceof PickLocationInMapFragment) {
                binding.topAppBar.tvTitleToolbar.setText("Chọn vị trí trên bản đồ");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupToolbar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}