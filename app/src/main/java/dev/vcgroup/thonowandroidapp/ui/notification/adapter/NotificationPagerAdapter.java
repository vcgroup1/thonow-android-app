package dev.vcgroup.thonowandroidapp.ui.notification.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import dev.vcgroup.thonowandroidapp.ui.notification.NotificationFragment;
import dev.vcgroup.thonowandroidapp.ui.notification.conservation.ConservationListFragment;
import dev.vcgroup.thonowandroidapp.ui.notification.remotenotification.NotificationListFragment;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbycalendar.TaskByCalendarFragment;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbylist.TaskByListFragment;

public class NotificationPagerAdapter extends FragmentStateAdapter {

    public NotificationPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1: return new NotificationListFragment();
            case 0:
            default: return new ConservationListFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
