package dev.vcgroup.thonowandroidapp.ui.taskmanagement;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.Order;

public class TaskManagementViewModel extends ViewModel {
    private MutableLiveData<List<Order>> orders = new MutableLiveData<>(new ArrayList<>());

    public MutableLiveData<List<Order>> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders.setValue(orders);
    }
}