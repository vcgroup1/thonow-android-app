package dev.vcgroup.thonowandroidapp.ui.audiocall.incomingcall;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;

import org.parceler.Parcels;

import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowandroidapp.databinding.ActivityIncomingCallBinding;
import dev.vcgroup.thonowandroidapp.ui.audiocall.BaseSinchActivity;
import dev.vcgroup.thonowandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowandroidapp.util.AudioPlayer;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;

import static dev.vcgroup.thonowandroidapp.constant.CommonConst.CALL_PERMISSIONS;

public class IncomingCallActivity extends BaseSinchActivity implements SinchService.StartFailedListener {
    private static final String TAG = IncomingCallActivity.class.getSimpleName();
    private ActivityIncomingCallBinding binding;
    private String mCallId;
    private AudioPlayer mAudioPlayer;
    private Worker caller;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_incoming_call);

        if (getIntent().getExtras() != null) {
            String workerId = getIntent().getStringExtra(SinchService.REMOTE_USER_ID);
            if (workerId != null && !workerId.isEmpty()) {
                db.collection(CollectionConst.COLLECTION_WORKER)
                        .document(workerId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            caller = documentSnapshot.toObject(Worker.class);
                            if (caller != null) {
                                binding.setWorker(caller);
                            }
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            }
        }

        mAudioPlayer = new AudioPlayer(this);
        mAudioPlayer.playRingtone();
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);

        binding.btnAcceptCall.setOnClickListener(v -> {
            if (!TedPermission.isGranted(this, CALL_PERMISSIONS)) {
                TedPermission.with(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                answerPhone();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(CALL_PERMISSIONS)
                        .check();
            } else {
                answerPhone();
            }
        });
        binding.ibnDeclineCall.setOnClickListener(v -> declinePhone());
    }

    @Override
    protected void onServiceConnected() {
        // startSinchService
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(CommonUtil.currentUserUid);
        }

        getSinchServiceInterface().setStartListener(this);

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            displayRemoteUserInfo(call.getRemoteUserId());
        } else {
            Log.d(TAG, "Started with invalid callId, aborting");
            finish();
        }
    }

    private void answerPhone() {
        mAudioPlayer.stopRingtone();
        Log.d(TAG, "mCallId: " + mCallId);
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.answer();
            Intent intent = new Intent(this, CallScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("worker", Parcels.wrap(caller));
            intent.putExtra(SinchService.CALL_ID, mCallId);
            startActivity(intent);
        }
        finish();
    }

    private void declinePhone() {
        mAudioPlayer.stopRingtone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private void displayRemoteUserInfo(String remoteUserId) {
        Log.d("remoteUserId", remoteUserId);
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {

    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
        }

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended, cause: " + cause.toString());
            mAudioPlayer.stopRingtone();
            finish();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> list) {
            // Send a push through your push provider here, e.g. GCM
        }
    }
}
