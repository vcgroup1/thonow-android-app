package dev.vcgroup.thonowandroidapp.ui.callworker.callworker.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.card.MaterialCardView;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomWorkerInfowindowBinding;
import dev.vcgroup.thonowandroidapp.util.ImageLoadingUtil;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context mContext;
    private ItemCustomWorkerInfowindowBinding binding;

    public CustomInfoWindowAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_custom_worker_infowindow, null, false);
        Worker worker = (Worker) marker.getTag();
        if (worker != null) {
            binding.tvName.setText(worker.getDisplayName());
            ImageLoadingUtil.displayImage(binding.avatar, worker.getPhotoUrl());
            binding.ratingBar.setRating(worker.getRating());
        }
        return binding.getRoot();
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
