package dev.vcgroup.thonowandroidapp.ui.profile.transactionhistory;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.Transaction;
import dev.vcgroup.thonowandroidapp.databinding.ItemTransactionBinding;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.TransactionViewHolder> {
	private static final String TAG = TransactionHistoryAdapter.class.getSimpleName();
	private List<Transaction> transactions;
	private AsyncListDiffer<Transaction> mDiffer;
	private DiffUtil.ItemCallback<Transaction> mCallback = new DiffUtil.ItemCallback<Transaction>() {
		@Override
		public boolean areItemsTheSame(@NonNull @NotNull Transaction oldItem, @NonNull @NotNull Transaction newItem) {
			return oldItem.equals(newItem);
		}

		@Override
		public boolean areContentsTheSame(@NonNull @NotNull Transaction oldItem, @NonNull @NotNull Transaction newItem) {
			return oldItem.getTimestamp().equals(newItem.getTimestamp());
		}
	};

	public TransactionHistoryAdapter() {
		this.transactions = new ArrayList<>();
		this.mDiffer = new AsyncListDiffer<>(this, mCallback);
	}


	@NonNull
	@NotNull
	@Override
	public TransactionViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
		return new TransactionViewHolder(ItemTransactionBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull @NotNull TransactionHistoryAdapter.TransactionViewHolder holder, int position) {
		Transaction transaction = getItem(position);
		holder.bindItem(transaction);
	}

	@Override
	public int getItemCount() {
		return mDiffer.getCurrentList().size();
	}

	public void update(List<Transaction> newList) {
		mDiffer.submitList(newList);
	}

	public Transaction getItem(int position) {
		return mDiffer.getCurrentList().get(position);
	}

	public static class TransactionViewHolder extends RecyclerView.ViewHolder {
		private ItemTransactionBinding binding;

		public TransactionViewHolder(@NonNull @NotNull ItemTransactionBinding binding) {
			super(binding.getRoot());
			this.binding = binding;
		}

		public void bindItem(Transaction transaction) {
			binding.setTransaction(transaction);
			binding.executePendingBindings();
		}

	}
}