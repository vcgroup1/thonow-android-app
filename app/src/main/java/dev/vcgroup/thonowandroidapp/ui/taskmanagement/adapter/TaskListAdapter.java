package dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ItemByCalendarTaskBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemTaskBinding;

import static dev.vcgroup.thonowandroidapp.data.model.OrderStatus.WAIT_FOR_ORDER_CONFIRMATION;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskViewHolder> implements Filterable {
    public enum TaskAdapterType {
        TASK_BY_LIST_ITEM,
        TASK_BY_CALENDAR_ITEM
    }
    private static final String TAG = TaskListAdapter.class.getSimpleName();
    private List<Order> orders;
    private TaskAdapterType type;
    private OnTaskListener onTaskListener;
    private ViewDataBinding binding;
    private AsyncListDiffer<Order> mDiffer;
    private DiffUtil.ItemCallback<Order> itemCallback = new DiffUtil.ItemCallback<Order>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull Order oldItem, @NonNull @NotNull Order newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull Order oldItem, @NonNull @NotNull Order newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };

    public TaskListAdapter(List<Order> orders, TaskAdapterType taskAdapterType, OnTaskListener onTaskListener) {
        this.orders = orders;
        this.type = taskAdapterType;
        this.onTaskListener = onTaskListener;
        this.mDiffer = new AsyncListDiffer<>(this, itemCallback);
    }

    @NonNull
    @NotNull
    @Override
    public TaskListAdapter.TaskViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (type.equals(TaskAdapterType.TASK_BY_LIST_ITEM)) {
            this.binding = ItemTaskBinding.inflate(inflater, parent, false);
        } else {
            this.binding = ItemByCalendarTaskBinding.inflate(inflater, parent, false);
        }
        return new TaskViewHolder(binding, onTaskListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TaskListAdapter.TaskViewHolder holder, int position) {
        Order order = getItem(position);

        holder.bindOrder(binding, order);

        order.getWorker()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    holder.bindWorker(binding, worker);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        order.getOrderDetails().get(0).getService().get()
                .addOnSuccessListener(documentSnapshot -> {
                    Service service = documentSnapshot.toObject(Service.class);

                    Objects.requireNonNull(service).getServiceType().get()
                            .addOnSuccessListener(documentSnapshot1 -> {
                                ServiceType serviceType = documentSnapshot1.toObject(ServiceType.class);
                                holder.bindServiceType(binding, serviceType);
                            })
                            .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Order getItem(int pos) {
        return mDiffer.getCurrentList().get(pos);
    }

    public void update(List<Order> newList) {
        mDiffer.submitList(newList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.count = orders.size();
                    filterResults.values = orders;
                } else {
                    String keyword = charSequence.toString();

                    if (keyword.equals("Tất cả")) {
                        filterResults.count = orders.size();
                        filterResults.values = orders;
                    } else {
                        OrderStatus orderStatus = OrderStatus.getOrderStatusByTitle(keyword);
                        List<Order> resultList = new ArrayList<>();
                        orders.forEach(order -> {
                            if (order.getStatus().equals(orderStatus)) {
                                resultList.add(order);
                            }
                        });
                        filterResults.count = resultList.size();
                        filterResults.values = resultList;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                update((List<Order>) results.values);
            }
        };
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        private ItemByCalendarTaskBinding binding;
        private ItemTaskBinding itemTaskBinding;
        private OnTaskListener onTaskListener;

        private View.OnClickListener onTaskClickedListener = v -> {
            if (this.onTaskListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    this.onTaskListener.onTaskClickedListener(position);
                }
            }
        };

        public TaskViewHolder(ViewDataBinding binding, OnTaskListener onTaskListener) {
            super(binding.getRoot());
            this.onTaskListener = onTaskListener;

            if (binding instanceof ItemTaskBinding) {
                this.itemTaskBinding = (ItemTaskBinding) binding;
            } else {
                this.binding = (ItemByCalendarTaskBinding) binding;

                ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onTaskCancelClickedListener(position);
                        }
                    }
                });

                ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setOnClickListener(onTaskClickedListener);

                ((ItemByCalendarTaskBinding) binding).ibnChat.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onChatClickedListener(position);
                        }
                    }
                });

                ((ItemByCalendarTaskBinding) binding).ibnCall.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onAudioCallClickedListener(position);
                        }
                    }
                });

                ((ItemByCalendarTaskBinding) binding).btnConfirmToComplete.setOnClickListener(v -> {
                    if (this.onTaskListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            this.onTaskListener.onConfirmToCompleterOrder(position);
                        }
                    }
                });
            }
            binding.getRoot().setOnClickListener(onTaskClickedListener);
        }

        public void bindOrder(ViewDataBinding binding, Order order) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setOrder(order);

                switch (order.getStatus()) {
                    case WAITING_FOR_WORK:
                    case ON_THE_MOVE:
                    case ARRIVED:
                    case INVESTIGATING:
                        ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setVisibility(View.VISIBLE);
                        ((ItemByCalendarTaskBinding) binding).cardview.setClickable(false);
                        break;
                    case WAIT_FOR_ORDER_CONFIRMATION:
                        ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setVisibility(View.VISIBLE);
                        ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setVisibility(View.VISIBLE);
                        ((ItemByCalendarTaskBinding) binding).cardview.setClickable(true);
                        break;
                    case CONFIRMED_ORDER:
                    case WORKING:
                        ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setVisibility(View.VISIBLE);
                        ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).cardview.setClickable(true);
                        break;
                    case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
                        ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).btnConfirmToComplete.setVisibility(View.VISIBLE);
                        ((ItemByCalendarTaskBinding) binding).cardview.setClickable(true);
                        break;
                    case WAIT_FOR_CONFIRMATION_TO_PAY:
                    case COMPLETED:
                    case CANCELED:
                        ((ItemByCalendarTaskBinding) binding).btnSeeDetailOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).btnCancelOrder.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).btnConfirmToComplete.setVisibility(View.GONE);
                        ((ItemByCalendarTaskBinding) binding).cardview.setClickable(true);
                        break;
                }

                this.binding.executePendingBindings();
            } else {
                this.itemTaskBinding.setOrder(order);

                switch (order.getStatus()) {
                    case WAITING_FOR_WORK:
                    case ON_THE_MOVE:
                    case ARRIVED:
                    case INVESTIGATING:
                        ((ItemTaskBinding) binding).cardview.setClickable(false);
                        break;
                    case WAIT_FOR_ORDER_CONFIRMATION:
                    case CONFIRMED_ORDER:
                    case WORKING:
                    case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
                    case WAIT_FOR_CONFIRMATION_TO_PAY:
                    case COMPLETED:
                    case CANCELED:
                        ((ItemTaskBinding) binding).cardview.setClickable(true);
                        break;
                }

                this.itemTaskBinding.executePendingBindings();
            }
        }


        public void bindWorker(ViewDataBinding binding, Worker worker) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setWorker(worker);
                this.binding.executePendingBindings();
            } else {
                this.itemTaskBinding.setWorker(worker);
                this.itemTaskBinding.executePendingBindings();
            }
        }

        public void bindServiceType(ViewDataBinding binding, ServiceType serviceType) {
            if (binding instanceof ItemByCalendarTaskBinding) {
                this.binding.setServiceType(serviceType);
                this.binding.executePendingBindings();
            } else {
                this.itemTaskBinding.setServiceType(serviceType);
                this.itemTaskBinding.executePendingBindings();
            }
        }
    }

    public interface OnTaskListener {
        void onTaskClickedListener(int position);
        void onTaskCancelClickedListener(int position);
        void onChatClickedListener(int position);
        void onAudioCallClickedListener(int position);
        void onConfirmToCompleterOrder(int position);
    }

}
