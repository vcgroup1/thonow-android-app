package dev.vcgroup.thonowandroidapp.ui.welcomescreen;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.constant.CommonConst;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.data.model.AuthMethod;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.databinding.ActivityWelcomeBinding;
import dev.vcgroup.thonowandroidapp.databinding.DialogAskPermissionBinding;
import dev.vcgroup.thonowandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowandroidapp.ui.askpermission.AskPermissionActivity;
import dev.vcgroup.thonowandroidapp.ui.signinscreen.SignInActivity;
import lombok.SneakyThrows;

import static dev.vcgroup.thonowandroidapp.constant.AuthenticationConst.RESULT_CODE_GOOGLE_SIGN_IN;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = WelcomeActivity.class.getSimpleName();
    private ActivityWelcomeBinding binding;
    private BottomSheetDialog changeLanguageBsd;
    private RadioButton rbVietnamese, rbEnglish;
    private FirebaseAuth mAuth;

    // Google Sign In Section
    private SignInButton signInButton;
    private GoogleSignInClient googleSignInClient;

    // Facebook Sign In Section
    private CallbackManager mCallbackManager;
    private LoginButton facebookLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        binding.setHandlers(this);
        askForPermission();
        mAuth = FirebaseAuth.getInstance();
        setupFacebookSignIn();
        setupGoogleSignIn();
    }

    private void askForPermission() {
        // show ask permission dialog
        if (!TedPermission.isGranted(this, CommonConst.REQUIRED_PERMISSION)) {
            DialogAskPermissionBinding askPermissionBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_ask_permission, (ViewGroup) binding.getRoot(), false);
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this, R.style.MaterialAlertDialog_Rounded);
            builder.setView(askPermissionBinding.getRoot());
            AlertDialog dialog = builder.create();
            dialog.show();


            askPermissionBinding.btnAccept.setOnClickListener(v -> {
                Intent intent = new Intent(this, AskPermissionActivity.class);
                startActivityForResult(intent, RequestCodeConst.ASK_PERMISSION_CODE);
                dialog.dismiss();
            });

            askPermissionBinding.btnIgnore.setOnClickListener(v -> dialog.dismiss());
        }
    }

    public void showChangeLanguage(View view) {
        View changeLanguageView = getLayoutInflater().inflate(R.layout.bsd_change_language, null);
        changeLanguageBsd = new BottomSheetDialog(this);
        changeLanguageBsd.setContentView(changeLanguageView);
        changeLanguageBsd.show();

        LinearLayout btnVietnamese = changeLanguageView.findViewById(R.id.btn_vietnamese);
        LinearLayout btnEnglish = changeLanguageView.findViewById(R.id.btn_english);

        rbVietnamese = changeLanguageView.findViewById(R.id.rd_btn_vietnamese);
        rbEnglish = changeLanguageView.findViewById(R.id.rd_btn_en);

        btnVietnamese.setOnClickListener(this);
        btnEnglish.setOnClickListener(this);
        rbVietnamese.setOnClickListener(this);
        rbEnglish.setOnClickListener(this);
    }

    public void onLoginClick(View view) {
        navigateToVerifyPhoneNumber(null);
    }

    private void setupGoogleSignIn() {
        GoogleSignInOptions gso = new
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        binding.btnGoogleSignIn.setOnClickListener(view -> {
            Intent singInIntent = googleSignInClient.getSignInIntent();
            startActivityForResult(singInIntent, RESULT_CODE_GOOGLE_SIGN_IN);
        });
    }

    private void setupFacebookSignIn() {
        AppEventsLogger.activateApp(getApplication());
        mCallbackManager = CallbackManager.Factory.create();
        binding.btnFacebookSignIn.setOnClickListener((View.OnClickListener) view -> facebookLoginButton.performClick());
        facebookLoginButton = binding.facebookLoginBtn;
        facebookLoginButton.setPermissions("public_profile", "email", "user_birthday");

        facebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "onSuccess: logged in successfully");

                handleFacebookAccessToken(loginResult.getAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i(TAG, "onCompleted: response: " + response.toString());
                        try {
                            String email = object.getString("email");
                            String birthday = object.getString("birthday");

                            Log.i(TAG, "onCompleted: Email: " + email);
                            Log.i(TAG, "onCompleted: Birthday: " + birthday);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i(TAG, "onCompleted: JSON exception");
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            checkExistsUser(Objects.requireNonNull(Objects.requireNonNull(task.getResult()).getUser()), AuthMethod.FACEBOOK);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(binding.welcomeContainer, "Đăng nhập Facebook xảy ra lỗi!", BaseTransientBottomBar.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESULT_CODE_GOOGLE_SIGN_IN) {
            if (resultCode == RESULT_OK && data != null) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }

        } else {
            if (resultCode == RESULT_OK && data != null) {
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            handleFirebaseGoogleAuth(account);
        } catch (ApiException e) {
            e.printStackTrace();
            Log.d(WelcomeActivity.class.getSimpleName(), "signInResult:failed code=" + e.getStatusCode());
            handleFirebaseGoogleAuth(null);
        }
    }

    private void handleFirebaseGoogleAuth(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    checkExistsUser(task.getResult().getUser(), AuthMethod.GOOGLE);
                } else {
                    Snackbar.make(binding.welcomeContainer, "Đăng nhập Google xảy ra lỗi!", BaseTransientBottomBar.LENGTH_SHORT);
                }
            }
        });
    }

    private void checkExistsUser(FirebaseUser user, AuthMethod authMethod) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(CollectionConst.COLLECTION_USER)
                .whereEqualTo("email", user.getEmail())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<User> result = queryDocumentSnapshots.toObjects(User.class);
                    if (!result.isEmpty()) {
                        navigateToMainScreen();
                    } else {
                        navigateToVerifyPhoneNumber(authMethod);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(this, MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        finish();
    }

    private void navigateToVerifyPhoneNumber(AuthMethod authMethod) {
        Intent intent = new Intent(this, SignInActivity.class);
        if (authMethod != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("authMethod", authMethod);
            intent.putExtras(bundle);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
    }

    @SneakyThrows
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_vietnamese:
            case R.id.rd_btn_vietnamese:
                rbVietnamese.setChecked(true);
                rbEnglish.setChecked(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeLanguageBsd.dismiss();
                    }
                }, 1000);

                break;
            case R.id.btn_english:
            case R.id.rd_btn_en:
                rbVietnamese.setChecked(false);
                rbEnglish.setChecked(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        changeLanguageBsd.dismiss();
                    }
                }, 1000);
                break;
        }
    }
}