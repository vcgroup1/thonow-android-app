package dev.vcgroup.thonowandroidapp.ui.walkthroughscreen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import lombok.AllArgsConstructor;
import lombok.Data;

public class WalkthroughSliderAdapter extends RecyclerView.Adapter<WalkthroughSliderAdapter.WalkthroughSliderVH> {
    private List<WalkthroughSliderItem> sliderItemList;

    public WalkthroughSliderAdapter() {
        sliderItemList = new ArrayList<>();
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider1,
                "Hàng ngàn thợ có tay nghề và uy tín",
                "Tiết kiệm thời gian, dẹp bỏ mọi lo lắng. Không những vậy, bạn còn có thể đặt lịch sửa chữa cho bất kỳ bạn bè, người thân trong gia đình."));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider2,
                "Hỗ trợ mọi lúc mọi nơi",
                "Chỉ với hai bước, mọi nhu cầu về sửa chữa, bảo dưỡng nhà cửa và đồ đạc gia dụng của bạn sẽ được ThoNOW tiếp nhận"));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider3,
                "Hỗ trợ tìm kiếm thợ gần nhà",
                "ThoNOW cung cấp dịch vụ tìm kiếm thợ an toàn, giá rẻ và nhanh chóng cho khách hàng"));
        sliderItemList.add(new WalkthroughSliderItem(R.drawable.ic_slider4,
                "Đưa sự lựa chọn tốt nhất cho khách hàng",
                "Bạn có thể dễ dàng tìm kiếm thợ phù hợp với bạn và để lại đánh giá giúp ích cho cộng đồng"));
    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public WalkthroughSliderAdapter.WalkthroughSliderVH onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        return new WalkthroughSliderVH(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_in_app_welcome_slider, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull WalkthroughSliderAdapter.WalkthroughSliderVH holder, int position) {
        WalkthroughSliderItem item = sliderItemList.get(position);
        holder.setWelcomeSliderContent(item);
    }

    @Override
    public int getItemCount() {
        return sliderItemList == null ? 0 : sliderItemList.size();
    }

    public class WalkthroughSliderVH extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title;
        private TextView description;

        public WalkthroughSliderVH(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.welcome_item_thumbnail);
            title = itemView.findViewById(R.id.welcome_item_title);
            description = itemView.findViewById(R.id.welcome_item_desc);
        }

        public void setWelcomeSliderContent(WalkthroughSliderItem item) {
            image.setImageResource(item.getImage());
            title.setText(item.getTitle());
            description.setText(item.getDescription());
        }
    }

    @Data
    @AllArgsConstructor
    public class WalkthroughSliderItem {
        private int image;
        private String title;
        private String description;
    }
}
