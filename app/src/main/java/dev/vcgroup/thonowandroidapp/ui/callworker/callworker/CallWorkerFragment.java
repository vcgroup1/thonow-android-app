package dev.vcgroup.thonowandroidapp.ui.callworker.callworker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.koalap.geofirestore.GeoFire;
import com.koalap.geofirestore.GeoLocation;
import com.koalap.geofirestore.GeoQuery;
import com.koalap.geofirestore.GeoQueryEventListener;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.data.model.Address;
import dev.vcgroup.thonowandroidapp.data.model.ApiError;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.remote.GeocodeApiService;
import dev.vcgroup.thonowandroidapp.data.remote.RetrofitClientInstance;
import dev.vcgroup.thonowandroidapp.databinding.FragmentCallWorkerBinding;
import dev.vcgroup.thonowandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.adapter.CustomInfoWindowAdapter;
import dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation.DefineMyLocationFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice.SelectServiceFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;
import dev.vcgroup.thonowandroidapp.ui.profile.favoriteworkerlist.FavoriteWorkerListActivity;
import dev.vcgroup.thonowandroidapp.util.ApiErrorUtil;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.ConnectionDetector;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowandroidapp.util.GPSTracker;
import dev.vcgroup.thonowandroidapp.util.GeoFirestoreCallback;

import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowandroidapp.util.OrderHelper;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;
import dev.vcgroup.thonowandroidapp.util.VassCustomToast;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static dev.vcgroup.thonowandroidapp.constant.CallWorkerConst.RADIUS_1;
import static dev.vcgroup.thonowandroidapp.constant.CallWorkerConst.RADIUS_2;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.SELECT_FAVORITE_WORKER_REQ_CODE;

public class CallWorkerFragment extends Fragment implements
		GoogleMap.OnMapLongClickListener, OnMapReadyCallback, LocationSource.OnLocationChangedListener, GoogleMap.OnMarkerClickListener {
	private static final String TAG = CallWorkerFragment.class.getSimpleName();
	private static final String KEY_LOCATION = "location";
	private FragmentCallWorkerBinding binding;
	private CallWorkerViewModel mViewModel;
	private BottomSheetBehavior bottomSheetBehavior, confirmBsdBehavior;

	// Google Maps
	private SupportMapFragment mapFragment;
	private GoogleMap map;
	private GoogleMap.OnCameraIdleListener onCameraIdleListener;
	private Location mCurrentLocation;
	private LocationRequest mLocationRequest;
	private long UPDATE_INTERVAL = 180000;  /* 3 minutes */
	private long FASTEST_INTERVAL = 300000; /* 5 minutes */
	private CustomInfoWindowAdapter customInfoWindowAdapter;

	// GPS Tracker
	private GPSTracker gps;

	// Connection Detector
	private ConnectionDetector connectionDetector;
	private Boolean isInternetPresent = false;

	// FirebaseAuth, FirebaseFirestore
	private static FirebaseAuth mAuth;
	private static FirebaseFirestore db;
	private static String uid;
	private static GeoFire geoFire, workerGeoFire;
	private static BitmapDescriptor customMarker, checkedCustomMarker;

	static {
		db = FirebaseFirestore.getInstance();
		mAuth = FirebaseAuth.getInstance();

		uid = mAuth.getCurrentUser().getUid();

		geoFire = new GeoFire(db.collection(CollectionConst.COLLECTION_USER_GEOLOCATION));
		workerGeoFire = new GeoFire(db.collection(CollectionConst.COLLECTION_WORKER_GEOLOCATION));
	}

	private VCLoadingDialog loading;
	private Set<Marker> nearbyWorkerMarkers = new HashSet<>();
	private GeocodeApiService geocodeApiService;
	private Marker myMarker;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_call_worker, container, false);
		mViewModel = ViewModelProviders.of(getActivity()).get(CallWorkerViewModel.class);
		binding.setLifecycleOwner(this);
		binding.setItem(mViewModel);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (mapFragment == null) {
			if (loading == null) {
				loading = VCLoadingDialog.create(requireActivity())
						.setStyle(VCLoadingDialog.LoadingStyle.WITH_TEXT);
			}

			loading.show();

			new Handler().postDelayed(() -> {
				loading.setMessage("Đang cài đặt bản đồ...");
			}, 1000);

			new Handler().postDelayed(() -> {
				loading.setMessage("Xác định vị trí của bạn...");
			}, 5000);

			new Handler().postDelayed(() -> {
				loading.setMessage("Đang tìm thợ phù hợp...");
			}, 8000);

			new Handler().postDelayed(() -> {
				loading.dismiss();
			}, 10000);

		}

		setupBsdCallWorker();

		if (TextUtils.isEmpty(BuildConfig.MAPS_API_KEY)) {
			throw new IllegalStateException("You forgot to supply a Google Maps API key");
		}

		// Check if internet present and show alert if not
		connectionDetector = new ConnectionDetector(getContext());
		isInternetPresent = connectionDetector.isConnectingToInternet();
		if (!isInternetPresent) {

		}

		gps = new GPSTracker(requireActivity());

		setupMapIfNeeded();

		registerClickListener();

		mViewModel.getCurrentAddress().observe(requireActivity(), address -> {
			binding.callWorkerInformation.btnCallWorkerNext.setEnabled(address != null);
		});
	}

	private void setupBsdCallWorkerConfirmation() {
		confirmBsdBehavior = BottomSheetBehavior.from(binding.callWorkerConfirmInformation.confirmCallWorkerBottomSheet);
		DocumentReference serviceDocRef = db.collection(CollectionConst.COLLECTION_SERVICE).document(Objects.requireNonNull(mViewModel.getSelectedService().getValue()).getId());
		OrderDetail orderDetail = new OrderDetail(serviceDocRef, mViewModel.getJobQuatitiy().getValue());

		Date startDate = Objects.requireNonNull(mViewModel.getStartDate().getValue()).getTime();

		AtomicReference<Double> movingExpense = new AtomicReference<>((double) 0);
		AtomicReference<Double> estimatedFee = new AtomicReference<>((double) 0);
		AtomicReference<Double> overtimeFee = new AtomicReference<>((double) 0);
		new Handler().postDelayed(() -> {
			movingExpense.set(OrderHelper.calculateMovingExpense(mViewModel.getRadius().getValue()));
			Log.d(TAG, "movingExpense: " + movingExpense);

			estimatedFee.set(orderDetail.getSubtotal());
			Log.d(TAG, "estimatedFee: " + estimatedFee);

			overtimeFee.set(OrderHelper.calculateOvertimeFee(estimatedFee.get(), startDate));
			Log.d(TAG, "overtimeFee: " + overtimeFee);

			double tempTotal = estimatedFee.get() + overtimeFee.get() + movingExpense.get();
			Log.d(TAG, "tempTotal: " + tempTotal);
			binding.callWorkerConfirmInformation.tvEstimatedFee.setText(CommonUtil.convertCurrencyText(tempTotal) + " đ");
		}, 1000);

		confirmBsdBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

		binding.callWorkerConfirmInformation.btnFindWorker.setOnClickListener(v -> {

			Order newOrder = Order.createNewOrder(mViewModel.getCurrentOrderType().getValue(),
					startDate, Collections.singletonList(orderDetail),
					mViewModel.getCurrentAddress().getValue(), movingExpense.get(), overtimeFee.get(), estimatedFee.get());

			String note = CommonUtil.getInputText(binding.callWorkerConfirmInformation.tipNote);
			if (!note.isEmpty()) {
				newOrder.setNote(note);
			}

			List<Worker> requestWorkerList = new ArrayList<>();
			if (mViewModel.getSelectedWorkers().getValue() != null
					&& !mViewModel.getSelectedWorkers().getValue().isEmpty()) {
				requestWorkerList.addAll(mViewModel.getSelectedWorkers().getValue());
			} else {
				requestWorkerList.addAll(Objects.requireNonNull(mViewModel.getNearbyWorkers().getValue()));
			}

			if (!requestWorkerList.isEmpty()) {
				if (loading == null) {
					loading = VCLoadingDialog.create(requireActivity());
				}
				loading.show();
				Intent intent = new Intent(requireActivity(), MainScreenActivity.class);
				intent.putExtra("requestWorkerList", Parcels.wrap(requestWorkerList));
				intent.putExtra("order", Parcels.wrap(newOrder));
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
				loading.dismiss();
			} else {
				// TODO: Thông báo ko thể tìm worker
			}

		});

		binding.callWorkerConfirmInformation.btnConfirmRequestCallBack.setOnClickListener(v -> {
			if (confirmBsdBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
				bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
				binding.callWorkerConfirmInformation.getRoot().setVisibility(View.GONE);
			}
		});
	}

	private void registerClickListener() {
		binding.callWorkerInformation.btnPickLocation.setOnClickListener(v -> {
			FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
					new DefineMyLocationFragment(), null, DefineMyLocationFragment.class.getSimpleName(), true);
		});

		binding.callWorkerInformation.btnSelectService.setOnClickListener(v -> {
			FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
					new SelectServiceFragment(), null, SelectServiceFragment.class.getSimpleName(),
					true);
		});

		binding.callWorkerInformation.btnSelectFavoriteWorker.setOnClickListener(v -> {
			Intent intent = new Intent(getActivity(), FavoriteWorkerListActivity.class);
			intent.putExtra("selectedFavoriteWorker", Parcels.wrap(mViewModel.getFavoriteWorker().getValue()));
			intent.putExtra("selectedServiceTypeId",
					Objects.requireNonNull(mViewModel.getSelectedServiceType().getValue()).getId());
			startActivityForResult(intent, SELECT_FAVORITE_WORKER_REQ_CODE);
		});

		binding.callWorkerInformation.btnCallWorkerNext.setOnClickListener(v -> {
			// hide call worker information bottom sheet dialog
			bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
			// show call worker confirm request bottom sheet dialog
			setupBsdCallWorkerConfirmation();
			binding.callWorkerConfirmInformation.getRoot().setVisibility(View.VISIBLE);
		});

		binding.btnExtendScope.setOnClickListener(v -> {
			if (mViewModel.getRadius().getValue() != null && mViewModel.getRadius().getValue() == RADIUS_1) {
				mViewModel.setRadius(RADIUS_2);

				VassCustomToast.makeText(requireActivity(), "Đã mở rộng phạm vi lên 10km",
						Toast.LENGTH_LONG, VassCustomToast.ToastType.SUCCESS)
						.show();
			} else {
				VassCustomToast.makeText(requireActivity(), "Đã đạt tối đa phạm vi tìm kiếm thợ " +
								"là" +
								" " +
								"10km",
						Toast.LENGTH_LONG, VassCustomToast.ToastType.INFO)
				.show();
			}
		});
	}

	protected void setupMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mapFragment == null) {
			mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
			// Check if we were successful in obtaining the map.
			if (mapFragment != null) {
				mapFragment.getMapAsync(this);
			} else {
				Log.d(TAG, "Error - Map Fragment was null!!");
			}
		}
	}

	// The Map is verified. It is now safe to manipulate the map.
	private void loadMap(GoogleMap googleMap) {
		map = googleMap;
		if (map != null) {
			Log.d(TAG, "Map Fragment was loaded properly!");
			// Map is ready
			TedPermission.with(requireActivity())
					.setPermissionListener(new PermissionListener() {
						@SuppressLint("MissingPermission")
						@Override
						public void onPermissionGranted() {
							getMyLocation();
							startLocationUpdates();
							map.setOnMapLongClickListener(CallWorkerFragment.this);

							// Check if GPS location can get
							if (gps.canGetLocation()) {
								LatLng latLng = new LatLng(gps.getLatitude(), gps.getLongitude());

								CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18.0f).build();
								CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
								map.animateCamera(cameraUpdate);
							} else {
								//Toast.makeText(getActivity(), "Không thể xác định vị trí hiện tại, vui lòng bật GPS!", Toast.LENGTH_SHORT).show();
								return;
							}
						}

						@Override
						public void onPermissionDenied(List<String> deniedPermissions) {
							Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
						}
					})
					.setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
					.setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
					.check();
		} else {
			Log.d(TAG, "Error - Map was null!!");
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RequestCodeConst.SELECT_FAVORITE_WORKER_REQ_CODE && resultCode == RESULT_OK && data != null) {
			Worker selectedWorker = Parcels.unwrap(data.getParcelableExtra("selectedFavoriteWorker"));
			Log.d("selectedWorker", selectedWorker.toString());
			if (!Objects.requireNonNull(mViewModel.getSelectedWorkers().getValue()).contains(selectedWorker)) {
				MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
				dialog.setTitle("Thông báo")
						.setMessage("Thợ này hiện đang không trong phạm vi khả dụng.\nRất tiếc " +
								"bạn không thể chọn họ")
						.setPositiveButton("Đã hiểu", v -> dialog.dismiss())
						.show();
			} else {
				mViewModel.setFavoriteWorker(selectedWorker);
				mViewModel.setSelectedWorkers(Arrays.asList(selectedWorker));
			}

		}
	}

	@SuppressLint("MissingPermission")
	private void startLocationUpdates() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
		builder.addLocationRequest(mLocationRequest);
		LocationSettingsRequest locationSettingsRequest = builder.build();

		SettingsClient settingsClient = LocationServices.getSettingsClient(requireActivity());
		settingsClient.checkLocationSettings(locationSettingsRequest);

		// no inspection MissingPermission
		LocationServices.getFusedLocationProviderClient(requireActivity())
				.requestLocationUpdates(mLocationRequest, new LocationCallback() {
							@Override
							public void onLocationResult(LocationResult locationResult) {
								onLocationChanged(locationResult.getLastLocation());
							}
						},
						Looper.myLooper());
	}

	@SuppressLint("MissingPermission")
	private void getMyLocation() {
		map.setMyLocationEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setCompassEnabled(true);
		map.getUiSettings().setRotateGesturesEnabled(true);

		customInfoWindowAdapter = new CustomInfoWindowAdapter(getContext());
		map.setInfoWindowAdapter(customInfoWindowAdapter);

		map.setOnInfoWindowClickListener(marker -> {
			Worker worker = (Worker) marker.getTag();
			Boolean result = mViewModel.addOrRemoveSelectedWorker(worker);
			if (result != null) {
				nearbyWorkerMarkers.remove(marker);
				if (result.equals(Boolean.TRUE)) {
					if (checkedCustomMarker == null) {
						checkedCustomMarker = BitmapDescriptorFactory.fromBitmap(CommonUtil.resizeMapIcons(requireContext(), R.drawable.custom_vc_map_marker_checked, 120, 167));
					}
					marker.setIcon(checkedCustomMarker);
				} else {
					marker.setIcon(customMarker);
				}
				nearbyWorkerMarkers.add(marker);
			}
			marker.hideInfoWindow();
			;
		});

		FusedLocationProviderClient locationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());
		locationProviderClient.getLastLocation()
				.addOnSuccessListener(location -> {
					if (location != null) {
						onLocationChanged(location);
					}
				})
				.addOnFailureListener(e -> {
					Log.d(TAG, "Error trying to get last GPS location");
					e.printStackTrace();
				});
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	private void setupBsdCallWorker() {
		bottomSheetBehavior = BottomSheetBehavior.from(binding.callWorkerInformation.callWorkerBottomSheet);
		bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
	}

	@Override
	public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
		outState.putParcelable(KEY_LOCATION, mCurrentLocation);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onMapLongClick(LatLng latLng) {
		Toast.makeText(getActivity(), "Long Press", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		loadMap(googleMap);
		map.setOnCameraIdleListener(onCameraIdleListener);
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location == null) {
			return;
		}

		if (location != mCurrentLocation) {
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
			CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
			map.animateCamera(cameraUpdate);

			MarkerOptions markerOptions = new MarkerOptions()
					.position(latLng);
			myMarker = map.addMarker(markerOptions);

			if (Objects.equals(mViewModel.getIsFromPickLocation().getValue(), Boolean.FALSE)) {
				updateCurrentAddress(location);

//				// Bypass reverse-geocoding if the Geocoder service is not available on the
//				// device. The isPresent() convenient method is only available on Gingerbread or above.
//				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent()) {
//					// Since the geocoding API is synchronous and may take a while.  You don't want to lock
//					// up the UI thread.  Invoking reverse geocoding in an AsyncTask.
//					(new ReverseGeocodingTask(requireActivity())).execute(new Location[]{location});
//				}
			}

			// update GeoLocation To Firebase
			GeoLocation currentGeoLocation = new GeoLocation(location.getLatitude(), location.getLongitude());
			geoFire.setLocation(uid, currentGeoLocation, (key, exception) -> {
				if (exception != null) {
					Log.d(TAG, "There was an error saving the location to Geofire: " + exception.toString());
				} else {
					Log.d(TAG, "Location saved on server successfully!");
					loadNearbyWorkerPlaces(currentGeoLocation);
				}
			});
		}

		mCurrentLocation = location;
	}

	private void loadNearbyWorkerPlaces(GeoLocation currentGeoLocation) {
		onLoadNearbyWorkerPlaces(currentGeoLocation, new GeoFirestoreCallback() {
			@Override
			public void onKeyEnteredResponse(String key, GeoLocation location) {

				LatLng latLng = new LatLng(location.latitude, location.longitude);
				if (nearbyWorkerMarkers != null) {
					if (nearbyWorkerMarkers.isEmpty()) {
						searchAndAddMarker(key, latLng);
					} else {
						Log.d(TAG, "nearbyworkermarkers: " + nearbyWorkerMarkers.size());
						nearbyWorkerMarkers.forEach(marker -> {
							Worker markerWorker = (Worker) marker.getTag();
							if (!(marker.getPosition().equals(latLng) || Objects.requireNonNull(Objects.requireNonNull(markerWorker).getId()).equals(key))) {
								searchAndAddMarker(key, latLng);
							}
						});
					}
				}
			}

			@Override
			public void onKeyExitedResponse(String key) {
				nearbyWorkerMarkers.forEach(marker -> {
					String workerId = ((Worker) Objects.requireNonNull(marker.getTag())).getId();
					if (workerId.equals(key)) {
						nearbyWorkerMarkers.remove(marker);
						marker.remove();
					}
				});
			}
		});
	}

	private void searchAndAddMarker(String key, LatLng latLng) {
		DocumentReference serviceTypeDocRef = db.collection(CollectionConst.COLLECTION_SERVICE_TYPE).document(Objects.requireNonNull(mViewModel.getSelectedServiceType().getValue()).getId());
		db.collection(CollectionConst.COLLECTION_WORKER)
				.whereEqualTo("id", key)
				.whereNotEqualTo("active", false)
				.whereArrayContains("serviceTypeList", Objects.requireNonNull(serviceTypeDocRef))
				.get()
				.addOnSuccessListener(queryDocumentSnapshots -> {
					List<Worker> workers = queryDocumentSnapshots.toObjects(Worker.class);
					if (!workers.isEmpty()) {
						Worker worker = workers.get(0);
						addWorkerMarker(worker, latLng);
					}
				})
				.addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
	}

	private GeoQueryEventListener getGeoQueryEventListener(GeoFirestoreCallback mCallback) {
		return new GeoQueryEventListener() {
			@Override
			public void onKeyEntered(String key, GeoLocation location) {
				mCallback.onKeyEnteredResponse(key, location);
			}

			@Override
			public void onKeyExited(String key) {
				mCallback.onKeyExitedResponse(key);
			}

			@Override
			public void onKeyMoved(String key, GeoLocation location) {

			}

			@Override
			public void onGeoQueryReady() {
				Log.d(TAG, "onLoadNearbyWorkerLocation:: is Listening!");
			}

			@Override
			public void onGeoQueryError(Exception error) {
				Log.d(TAG, "onLoadNearbyWorkerLocation::" + error.getMessage());
			}
		};
	}

	private GeoQuery geoQuery;
	private double currentRadius = 0;

	private void onLoadNearbyWorkerPlaces(GeoLocation currentGeoLocation, GeoFirestoreCallback mCallback) {
		mViewModel.setRadius(RADIUS_1);
		mViewModel.getRadius().observe(requireActivity(), radius -> {
			Log.d(TAG, "radius: " + radius);
			if (radius != null && radius != currentRadius) {
				if (radius > RADIUS_1) {
					geoQuery.removeAllListeners();
				}
				geoQuery = workerGeoFire.queryAtLocation(currentGeoLocation, radius);
				geoQuery.addGeoQueryEventListener(getGeoQueryEventListener(mCallback));
				currentRadius = radius;
			}
		});
	}

	private void addWorkerMarker(Worker worker, LatLng latLng) {
		if (customMarker == null) {
			customMarker = BitmapDescriptorFactory.fromBitmap(CommonUtil.resizeMapIcons(requireContext(), R.drawable.custom_vc_map_marker, 120, 157));
		}
		MarkerOptions markerOption = new MarkerOptions()
				.icon(customMarker)
				.position(latLng);
		Marker marker = map.addMarker(markerOption);
		marker.setTag(worker);
		marker.showInfoWindow();
		nearbyWorkerMarkers.add(marker);
		mViewModel.addNearbyWorker(worker);
	}

	// AsyncTask encapsulating the reverse-geocoding API. Since the geocoder API is blocked,
	private class ReverseGeocodingTask extends AsyncTask<Location, Void, Void> {
		Context mContext;

		public ReverseGeocodingTask(Context context) {
			super();
			mContext = context;
		}

		@Override
		protected Void doInBackground(Location... params) {
			Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

			Location loc = params[0];
			List<android.location.Address> addresses = null;
			try {
				// Call the synchronous getFromLocation() method by passing in the lat/long values.
				addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
			} catch (IOException e) {
				Log.d(TAG, "reverseGeocoding: " + e.getLocalizedMessage());
				updateCurrentAddress(loc);
			}
			if (addresses != null && addresses.size() > 0) {
				android.location.Address address = addresses.get(0);
				// Format the first line of address (if available), city, and country name.
				String addressText = String.format("%s, %s, %s",
						address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
						address.getLocality(),
						address.getCountryName());
				// Update current Address
				mViewModel.setCurrentAddress(new Address(loc.getLatitude(), loc.getLongitude(),
						addressText));
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void unused) {
			super.onPostExecute(unused);
		}
	}


	private void updateCurrentAddress(Location location) {
		if (geocodeApiService == null) {
			geocodeApiService = RetrofitClientInstance.getInstance().getRetrofit().create(GeocodeApiService.class);
		}
		Call<JsonObject> call =
				geocodeApiService.getBingFormattedAddress(CommonUtil.getFormattedLatlng(new LatLng(location.getLatitude(), location.getLongitude())));
		call.enqueue(new Callback<JsonObject>() {
			@SneakyThrows
			@Override
			public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
				if (response.isSuccessful()) {
					if (response.body() != null) {
						JsonObject respBody = response.body();
						if (respBody.get("statusCode").getAsInt() == 200) {
							JsonObject result = respBody.getAsJsonArray("resourceSets").get(0).getAsJsonObject().getAsJsonArray("resources").get(0).getAsJsonObject();
							String fullAddress = result.get("name").getAsString();

							Log.d(TAG, "fullAddress: " + fullAddress);
							Address newAddress = new Address(location.getLatitude(), location.getLongitude(), fullAddress);
							mViewModel.setCurrentAddress(newAddress);
						} else {
							showLocateAddressFailed();
						}
					} else {
						showLocateAddressFailed();
					}
				} else {
					ApiError apiError = ApiErrorUtil.parseError(getActivity(), response);
					Log.d(TAG, "getAddress::onError " + apiError.toString());

					showLocateAddressFailed();
				}
			}

			@Override
			public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
				Log.d(TAG, "getAddress::onFailure" + t.getMessage());
				showLocateAddressFailed();
			}
		});
	}

	MyCustomAlertDialog locateFailedDialog;

	private void showLocateAddressFailed() {
		if (locateFailedDialog == null) {
			locateFailedDialog = MyCustomAlertDialog.create(requireActivity());
		}

		if (!locateFailedDialog.isShowing()) {
			locateFailedDialog.setTitle("Thông báo")
					.setMessage("Không thể xác định vị trí của bạn. Bật GPS và thử lại " +
							"hoặc nhập địa chỉ thủ công.")
					.setPositiveButton("Đã hiểu", v -> {
						locateFailedDialog.dismiss();
					})
					.show();
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		if (marker.equals(myMarker)) {
			marker.hideInfoWindow();
			return true;
		}
		return false;
	}

	@Override
	public void onStop() {
		super.onStop();
		if (loading != null) {
			loading.dismiss();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (loading != null) {
			loading.dismiss();
		}
	}
}
