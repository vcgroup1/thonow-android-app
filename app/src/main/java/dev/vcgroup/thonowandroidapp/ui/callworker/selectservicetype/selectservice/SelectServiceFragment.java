package dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import org.jetbrains.annotations.NotNull;


import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.CallWorkerFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.SelectServiceTypeFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

public class SelectServiceFragment extends Fragment implements ServiceAdapter.OnServiceListener {
    private ActivitySelectServiceBinding binding;
    private ServiceAdapter adapter;
    private CallWorkerViewModel mViewModel;


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_select_service, container, false);
        binding.setLifecycleOwner(getActivity());
        mViewModel = ViewModelProviders.of(requireActivity()).get(CallWorkerViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupServiceRecyclerView();
        setupServiceSearchView();
    }

    private void setupServiceRecyclerView() {
        adapter = new ServiceAdapter(this, mViewModel.getSelectedServiceType().getValue(), mViewModel.getSelectedService().getValue() != null ? mViewModel.getSelectedService().getValue().getId() : null);
        binding.rvService.setAdapter(adapter);
        binding.rvService.addItemDecoration(new SpacesItemDecoration(15));
    }

    private void setupServiceSearchView() {
        binding.svService.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

    }

    @Override
    public void onServiceClickedListener(int position) {
        mViewModel.setSelectedService(adapter.getItem(position));
        FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
                FragmentUtil.getFragmentByTag((AppCompatActivity) getActivity(), CallWorkerFragment.class.getSimpleName()), null, CallWorkerFragment.class.getSimpleName(), true);
        FragmentUtil.removeFragmentByTag((AppCompatActivity) getActivity(), SelectServiceFragment.class.getSimpleName());
        FragmentUtil.removeFragmentByTag((AppCompatActivity) getActivity(), SelectServiceTypeFragment.class.getSimpleName());
    }
}
