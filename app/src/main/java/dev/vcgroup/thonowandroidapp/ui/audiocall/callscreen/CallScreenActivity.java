package dev.vcgroup.thonowandroidapp.ui.audiocall.callscreen;

import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;
import com.sinch.android.rtc.calling.CallState;

import org.parceler.Parcels;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMNotification;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSingleSendData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.GenericFcmData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.RetrofitFCMClientInstance;
import dev.vcgroup.thonowandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOutgoingCallBinding;
import dev.vcgroup.thonowandroidapp.ui.audiocall.BaseSinchActivity;
import dev.vcgroup.thonowandroidapp.util.AudioPlayer;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import retrofit2.Callback;
import retrofit2.Response;

import static dev.vcgroup.thonowandroidapp.constant.AudioCallConst.INCOMING_CALL;
import static dev.vcgroup.thonowandroidapp.constant.AudioCallConst.OUT_GOING_CALL;

public class CallScreenActivity extends BaseSinchActivity implements SinchService.StartFailedListener {
    private static final String TAG = CallScreenActivity.class.getSimpleName();
    private ActivityOutgoingCallBinding binding;
    private Worker recipient;
    private boolean mIsOutgoingCall;
    private AudioPlayer mAudioPlayer;
    private Call mCall;
    private Timer mTimer;
    private CallScreenActivity.UpdateCallDurationTask mDurationTask;
    private long mCallStart = 0;
    private String mCallId;
    private IFCMService ifcmService;
    private static FirebaseFirestore db;

    static {
        db = FirebaseFirestore.getInstance();
    }

    private Snackbar mSnackbar;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(CallScreenActivity.this::updateCallDuration);
        }
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0 && mCall != null && mCall.getState() == CallState.ESTABLISHED) {
            binding.tvTime.setVisibility(View.VISIBLE);
            binding.tvTime.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_outgoing_call);

        mAudioPlayer = new AudioPlayer(this);
        if (getIntent().getExtras() != null) {
            recipient = Parcels.unwrap(getIntent().getParcelableExtra("worker"));
            if (recipient != null) {
                binding.setWorker(recipient);
            }

            mIsOutgoingCall = getIntent().getBooleanExtra(OUT_GOING_CALL, false);

            mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
            Log.d(TAG, "mCallId: " + mCallId);
        }

        binding.ibnFinish.setOnClickListener(v -> {
            endCall();
        });
        mCallStart = System.currentTimeMillis();

        mSnackbar = Snackbar.make(binding.getRoot(), "Nhấn hai lần để kết thúc cuộc gọi", Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        doCall();
    }

    private void doCall() {
        if (mIsOutgoingCall) {
            Call call = getSinchServiceInterface().callUser(recipient.getId());
            mCallId = call.getCallId();
            if (mIsOutgoingCall) {
                mAudioPlayer.playOutgoingTone();
            }
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mIsOutgoingCall = false;
            mCall = call;
            displayRemoteUserInfo(call.getRemoteUserId());
            call.addCallListener(new CallScreenActivity.SinchCallListener());
            binding.tvCallState.setText("Đang kết nối");
        } else {
            if (!mIsOutgoingCall) {
                Log.d(TAG, "Started with invalid callId, aborting.");
                finish();
            }
        }
    }

    @Override
    protected void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(CommonUtil.currentUserUid);
            showAndSetCallState("Đang kết nối");
        } else {
            doCall();
        }
        getSinchServiceInterface().setStartListener(this);
    }

    private void showAndSetCallState(String stateString) {
        binding.tvCallState.setVisibility(View.VISIBLE);
        binding.tvCallState.setText(stateString);
    }

    private void displayRemoteUserInfo(String remoteUserId) {
        Log.d(TAG, remoteUserId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new CallScreenActivity.UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
        if (mSnackbar.isShown()) {
            super.onBackPressed();
        } else {
            mSnackbar.show();
        }
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private class SinchCallListener implements CallListener {
        @Override
        public void onCallProgressing(Call progressingCall) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
            Log.d(TAG, "Call established");
            mAudioPlayer.stopProgressTone();
            binding.tvCallState.setVisibility(View.GONE);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            mCallStart = System.currentTimeMillis();
        }

        @Override
        public void onCallEnded(Call endedCall) {
            CallEndCause callEndCause = endedCall.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + callEndCause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);

            String endMsg = "Call ended: " + endedCall.getDetails().toString();
            Log.d(TAG, endMsg);

            binding.tvCallState.setVisibility(View.VISIBLE);
            binding.tvCallState.setText("Kết thúc");

            endCall();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> list) {
            // Send a push through your push provider here, e.g. GCM
            if (mIsOutgoingCall) {
                db.collection(CollectionConst.COLLECTION_TOKEN)
                        .document(recipient.getId())
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            Token token = documentSnapshot.toObject(Token.class);

                            if (token != null) {
                                if (ifcmService == null) {
                                    ifcmService = RetrofitFCMClientInstance.getInstance().getRetrofit().create(IFCMService.class);
                                }

                                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                                FCMSingleSendData<String> data = null;
                                if (currentUser != null) {
                                    data = new FCMSingleSendData<>(
                                            new GenericFcmData<>(INCOMING_CALL, CommonUtil.currentUserUid), token.getToken(),
                                            new FCMNotification("KH. " + currentUser.getDisplayName(),
                                                    "Đang gọi đến", Objects.requireNonNull(currentUser.getPhotoUrl()).toString()));
                                };

                                Log.d(TAG, "vào đây");
                                ifcmService.sendFcmNotification(data)
                                        .enqueue(new Callback<FCMResponse>() {
                                            @Override
                                            public void onResponse(retrofit2.Call<FCMResponse> call, Response<FCMResponse> response) {
                                                FCMResponse fcmResponse = response.body();
                                                if (fcmResponse != null) {
                                                    if (response.isSuccessful() && fcmResponse.getSuccess() > 0) {
                                                        Log.d(TAG, "send notification: succeed");
                                                    } else {
                                                        Log.d(TAG, fcmResponse.getResults().get(0).getError());
                                                        showAndSetCallState("Xảy ra lỗi");
                                                        finish();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(retrofit2.Call<FCMResponse> call, Throwable t) {
                                                Log.d(TAG, t.getMessage());
                                                showAndSetCallState("Xảy ra lỗi");
                                                finish();
                                            }
                                        });
                            }

                        });
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDurationTask.cancel();
        mTimer.cancel();
    }
}
