package dev.vcgroup.thonowandroidapp.ui.profile.favoriteworkerlist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityFavoriteWorkerListBinding;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;

public class FavoriteWorkerListActivity extends AppCompatActivity implements FavoriteWorkerAdapter.OnFavoriteWorkerListener {
    private ActivityFavoriteWorkerListBinding binding;
    private FavoriteWorkerAdapter adapter;
    private FavoriteWorkerViewModel mViewModel;
    private Worker selectedWorker;
    private FirebaseFirestore db;
    private List<Worker> workerList;
    private String selectedServiceTypeId;
    private VCLoadingDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favorite_worker_list);
        mViewModel = ViewModelProviders.of(this).get(FavoriteWorkerViewModel.class);
        binding.setLifecycleOwner(this);

        db = FirebaseFirestore.getInstance();

        if (getIntent() != null) {
            selectedWorker = Parcels.unwrap(getIntent().getParcelableExtra("selectedFavoriteWorker"));
            if (selectedWorker == null) {
                selectedWorker = new Worker();
            }

            selectedServiceTypeId = getIntent().getStringExtra("selectedServiceTypeId");
        }

        setupToolbar();
        setupFavoriteWorkerList();
    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        binding.topAppBar.tvTitleToolbar.setText("Danh sách thợ yêu thích");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite_worker, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_favorite_worker_helper:
                Toast.makeText(this, "dasdas", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_favorite_worker_plumber: break;
            case android.R.id.home:
                finish();
                onBackPressed();
                break;
        }
        return true;
    }


    DocumentReference selectedServiceTypeDocRef = null;
    private void loadData() {

        if (selectedServiceTypeId == null || selectedServiceTypeId.isEmpty()) {
            selectedServiceTypeId = "";
        } else {
            selectedServiceTypeDocRef =
                    db.collection(CollectionConst.COLLECTION_SERVICE_TYPE).document(selectedServiceTypeId);
        }


        db.collection(CollectionConst.COLLECTION_USER)
                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    List<DocumentReference> list = Objects.requireNonNull(documentSnapshot.toObject(User.class)).getFavoriteWorkerList();
                    if (list != null && !list.isEmpty()) {
                        list.forEach(docRef -> {
                            docRef.get()
                                    .addOnSuccessListener(documentSnapshot1 -> {
                                        Worker worker = documentSnapshot1.toObject(Worker.class);
                                        if (selectedServiceTypeDocRef != null) {
                                            if (Objects.requireNonNull(worker).getServiceTypeList().contains(selectedServiceTypeDocRef)) {
                                                workerList.add(worker);
                                            }
                                        } else {
                                            workerList.add(worker);
                                        }
                                        adapter.update(workerList);
                                    });
                        });

                    }

                });
    }

    private void setupFavoriteWorkerList() {
        workerList = new ArrayList<>();
        adapter = new FavoriteWorkerAdapter(workerList, this);
        binding.rvFavoriteWorker.setAdapter(adapter);
        binding.rvFavoriteWorker.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        binding.rvFavoriteWorker.addItemDecoration(new SpacesItemDecoration(10));

        loadData();
    }

    @Override
    public void onFavoriteWorkerClickedListener(int position) {
        if (selectedWorker != null) {
            if (loading == null) {
                loading = VCLoadingDialog.create(this);
            }
            loading.show();
            Intent returnIntent = new Intent();
            selectedWorker = adapter.getItem(position);
            returnIntent.putExtra("selectedFavoriteWorker", Parcels.wrap(selectedWorker));
            setResult(RESULT_OK, returnIntent);
            finish();
            loading.dismiss();
        }
    }
}