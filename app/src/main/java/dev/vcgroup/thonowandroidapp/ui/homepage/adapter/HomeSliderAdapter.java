package dev.vcgroup.thonowandroidapp.ui.homepage.adapter;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.databinding.ItemSlideContainerBinding;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

public class HomeSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.SliderViewHolder> {
    private List<SliderItem> sliderItems;

    public HomeSliderAdapter() {
        this.sliderItems = new ArrayList<>();
        this.populateData();
    }

    private void populateData() {
        sliderItems.add(new SliderItem(R.drawable.slider_1, "Thỉnh thoảng nhà có hư hỏng nhưng mình là phụ nữ nên cũng đành chịu, rất may là có dịch vụ của Gọi Thợ, dù chỉ hỏng vòi nước nhưng vẫn nhiệt tình đến sửa", "Chị Khánh Vân"));
        sliderItems.add(new SliderItem(R.drawable.slider_2, "\uD83D\uDC90 Dịch vụ Gọi thợ sửa chữa của công ty rất uy tín. Sau khi sửa chữa dùng rất ổn định. Cảm ơn Gọi Thợ đã giúp cho mình tiết kiệm được nhiều thời gian và chi phí", "Anh Bảo Hoàng"));
        sliderItems.add(new SliderItem(R.drawable.slider_3, "\tGọi Thợ là một địa chỉ uy tín cung cấp các dịch vụ gọi thợ sửa chữa. Tôi và gia đình hoàn toàn hài lòng với đội thợ sửa chữa nhà sau quá trình sử dụng.", "Chị Lan Khuê"));
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SliderViewHolder(ItemSlideContainerBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        SliderItem item = sliderItems.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return sliderItems.size();
    }

    @Getter
    class SliderViewHolder extends RecyclerView.ViewHolder{
        private ItemSlideContainerBinding binding;

        public SliderViewHolder(@NonNull ItemSlideContainerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(SliderItem item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class SliderItem {
        private int image;
        private String feedback;
        private String owner;
    }

}
