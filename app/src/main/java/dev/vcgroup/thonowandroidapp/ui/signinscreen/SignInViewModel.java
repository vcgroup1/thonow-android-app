package dev.vcgroup.thonowandroidapp.ui.signinscreen;

import android.text.Html;
import android.text.Spanned;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.auth.api.Auth;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import dev.vcgroup.thonowandroidapp.data.model.AuthMethod;
import dev.vcgroup.thonowandroidapp.data.model.CountryInfo;
import lombok.SneakyThrows;

public class SignInViewModel extends ViewModel {
    private MutableLiveData<String> phoneNumber = new MutableLiveData<>();
    public Spanned acceptTerms = Html.fromHtml("<strong>Bằng việc sử dụng ứng dụng. Tôi đồng ý " +
            "với <span style=\"text-decoration: underline; color: #0000ff;\">Điều khoản sử dụng</span> của aihDoctor</strong>");
    public final ObservableField<String> errPhoneNumber = new ObservableField<>();
    public MutableLiveData<CountryInfo> selectedCountryCode = new MutableLiveData<CountryInfo>();

    public void setPhoneNumber(String phoneNum) {
        phoneNumber.setValue(phoneNum);
    }

    public void setSelectedCountryCode(CountryInfo countryInfo) {
        selectedCountryCode.setValue(countryInfo);
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }
}
