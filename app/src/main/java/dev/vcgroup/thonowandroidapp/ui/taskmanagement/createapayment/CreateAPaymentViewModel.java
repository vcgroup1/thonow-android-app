package dev.vcgroup.thonowandroidapp.ui.taskmanagement.createapayment;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import dev.vcgroup.thonowandroidapp.data.model.TransactionMethod;
import dev.vcgroup.thonowandroidapp.data.model.Worker;


public class CreateAPaymentViewModel extends ViewModel {
    private MutableLiveData<Long> amount = new MutableLiveData<>( (long) 0);
    private MutableLiveData<TransactionMethod> transactionMethod =
            new MutableLiveData<>(TransactionMethod.MOMO);
    private MutableLiveData<String> orderId = new MutableLiveData<>();
    private MutableLiveData<Worker> currentWorker = new MutableLiveData<>();

    public MutableLiveData<TransactionMethod> getTransactionMethod() {
        return transactionMethod;
    }

    public void setTransactionMethod(TransactionMethod transactionMethod) {
        this.transactionMethod.setValue(transactionMethod);
    }

    public MutableLiveData<Long> getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount.setValue(amount);
    }

    public MutableLiveData<String> getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId.setValue(orderId);
    }

    public MutableLiveData<Worker> getCurrentWorker() {
        return currentWorker;
    }

    public void setCurrentWorker(Worker currentWorker) {
        this.currentWorker.setValue(currentWorker);
    }
}
