package dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskdetail;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOrderDetailBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemByCalendarTaskBinding;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter.ImageAdapter;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter.OrderDetailsAdapter;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.CANCEL_ORDER_REQ_CODE;

public class TaskDetailActivity extends AppCompatActivity implements ImageAdapter.OnImageListener {
    private static final String TAG = TaskDetailActivity.class.getSimpleName();
    private ActivityOrderDetailBinding binding;
    private Order order;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);

        setupToolbar();

        if (getIntent() != null) {
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));
            if (order != null) {
                populateAndBindData();
            }

        }

        binding.btnAcceptOrder.setOnClickListener(v -> {
            // chuyển status tiếp theo
            if (db == null) {
                this.db = FirebaseFirestore.getInstance();
            }

            OrderStatus nextStatus = OrderStatus.CONFIRMED_ORDER;
            this.db.collection(CollectionConst.COLLECTION_ORDER)
                    .document(order.getId())
                    .update(
                            "status", nextStatus
                    )
                    .addOnSuccessListener(unused -> {
                        order.setStatus(nextStatus);
                        binding.setOrder(order);
                        Toast.makeText(this, "Đã xác nhận đơn hàng", Toast.LENGTH_SHORT).show();
                        binding.btnAcceptOrder.setVisibility(View.GONE);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        });

        binding.btnCancelOrder.setOnClickListener(v -> {
            Intent intent = new Intent(this, CancelOrderActivity.class);
            intent.putExtra("deleteOrderId", Parcels.wrap(order.getId()));
            startActivityForResult(intent, CANCEL_ORDER_REQ_CODE);
        });
    }

    private void populateAndBindData() {
        // bind order
        binding.setOrder(order);    
        
        // bind worker
        order.getWorker()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    binding.setWorker(worker);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        // bind service type
        order.getOrderDetails().get(0).getService().get()
                .addOnSuccessListener(documentSnapshot -> {
                    Service service = documentSnapshot.toObject(Service.class);
                    binding.setService(service);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

        // setup Order Details RecyclerView
        OrderDetailsAdapter orderDetailsAdapter = new OrderDetailsAdapter(order.getOrderDetails());
        binding.rvOrderDetails.setAdapter(orderDetailsAdapter);
        binding.rvOrderDetails.addItemDecoration(new SpacesItemDecoration(10));

        // setup image recyclerview
        ImageAdapter imageAdapter = new ImageAdapter(order.getImages(), this);
        binding.rvOrderImages.setAdapter(imageAdapter);
        binding.rvOrderImages.addItemDecoration(new SpacesItemDecoration(10));

        switch (order.getStatus()) {
            case WAIT_FOR_ORDER_CONFIRMATION:
                binding.btnAcceptOrder.setVisibility(View.VISIBLE);
                binding.btnCancelOrder.setVisibility(View.VISIBLE);
                break;
            case CONFIRMED_ORDER:
            case WORKING:
            case WAIT_FOR_CONFIRMATION_TO_COMPLETE:
            case WAIT_FOR_CONFIRMATION_TO_PAY:
            case COMPLETED:
            case CANCELED:
                binding.btnAcceptOrder.setVisibility(View.GONE);
                binding.btnCancelOrder.setVisibility(View.GONE);
                break;
        }
    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Chi tiết đơn hàng");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_chat) {
            if (order.getStatus() != OrderStatus.CANCELED) {
                String orderId = order.getId();
                Intent intent = new Intent(this, ChatActivity.class);
                intent.putExtra("orderId", orderId);
                intent.putExtra("workerId", order.getWorker().getId());
                startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            } else {
                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                dialog.setTitle("Thông báo")
                        .setMessage("Đơn trong trạng thái đã bị từ chối. Bạn không thể liên hệ với thợ.")
                        .setPositiveButton("Quay lại", v -> dialog.dismiss())
                        .show();
            }
            return true;
        } else if (item.getItemId() == R.id.menu_call) {
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_OK);
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onImageClickListener(int position) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CANCEL_ORDER_REQ_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
