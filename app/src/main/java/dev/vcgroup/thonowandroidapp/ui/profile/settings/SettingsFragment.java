package dev.vcgroup.thonowandroidapp.ui.profile.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.firebase.auth.FirebaseAuth;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity;

public class SettingsFragment extends PreferenceFragmentCompat {
    private FirebaseAuth zcAuth;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings, rootKey);
    }

    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case "settings_sign_out":
                return logoutCurrentUser();
            case "settings_delete_account":
                return deleteCurrentAccount();
            case "settings_change_password":
                return navigateToChangePassword();
        }
        return false;
    }

    private boolean navigateToChangePassword() {
//        FragmentUtil.replaceFragment(getActivity(), R.id.more_container, new ChangePasswordFragment(), null, ChangePasswordFragment.class.getSimpleName(), true);
        return true;
    }

    private boolean deleteCurrentAccount() {
        return false;
    }

    private boolean logoutCurrentUser() {
        zcAuth = FirebaseAuth.getInstance();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Thông báo")
                .setMessage("Bạn có chắn chắn muốn thoát?")
                .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        zcAuth.signOut();
//                        new SessionManager(getActivity()).logout();
                        Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
        return true;
    }
}