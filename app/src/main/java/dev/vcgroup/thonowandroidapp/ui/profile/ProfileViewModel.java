package dev.vcgroup.thonowandroidapp.ui.profile;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowandroidapp.data.model.User;

public class ProfileViewModel extends ViewModel {
    private static final String TAG = ProfileViewModel.class.getSimpleName();
    private MutableLiveData<User> currentUser = new MutableLiveData<>();
    private MutableLiveData<CountryInfo> selectedCountryCode = new MutableLiveData<>();

    public MutableLiveData<User> getCurrentUser() {
        if (this.currentUser.getValue() == null) {
            setCurrentUser(User.convertFrom(FirebaseAuth.getInstance().getCurrentUser()));
        }
        return currentUser;
    }

    public void setCurrentUser(User user) {
        this.currentUser.setValue(user);
        getCountryInfoFromPhone(user.getPhoneNumber());
    }

    public MutableLiveData<CountryInfo> getSelectedCountryCode() {
        return selectedCountryCode;
    }

    public void setSelectedCountryCode(CountryInfo selectedCountryCode) {
        this.selectedCountryCode.setValue(selectedCountryCode);
    }

    private void getCountryInfoFromPhone(String phoneNumber) {
        String callingCode = phoneNumber.substring(1,3);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(CollectionConst.LIB_COUNTRY_CODE)
                .whereEqualTo("callingCode", callingCode)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    CountryInfo countryInfo = queryDocumentSnapshots.toObjects(CountryInfo.class).get(0);
                    this.setSelectedCountryCode(countryInfo);
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }
}