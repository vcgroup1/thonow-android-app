package dev.vcgroup.thonowandroidapp.ui.signinscreen;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.dialog.MaterialDialogs;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySignInScreenBinding;
import dev.vcgroup.thonowandroidapp.databinding.DialogAskPermissionBinding;
import dev.vcgroup.thonowandroidapp.ui.askpermission.AskPermissionActivity;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.CallWorkerFragment;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;

public class SignInActivity extends AppCompatActivity {
    private static final String SIGN_IN_STEP_ONE_TAG = SignInStepOneFragment.class.getSimpleName();
    private ActivitySignInScreenBinding binding;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in_screen);
        Bundle bundle = null;
        if (getIntent() != null) {
            bundle = getIntent().getExtras();
        }
        setupToolbar();
        FragmentUtil.replaceFragment(this, R.id.register_fragment_container, new SignInStepOneFragment(),
                bundle, SIGN_IN_STEP_ONE_TAG, false);

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.mToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sign_in_guideline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_ask_question_guide) {
            showSignInGuide();
            return true;
        }
        return false;
    }

    private void showSignInGuide() {
    }
}