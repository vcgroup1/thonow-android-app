package dev.vcgroup.thonowandroidapp.ui.taskmanagement.createapayment;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;


import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.constant.epayment.MoMoParameters;
import dev.vcgroup.thonowandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowandroidapp.data.model.Transaction;
import dev.vcgroup.thonowandroidapp.data.model.TransactionMethod;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request.PaymentProcessingRequest;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.request.TransactionConfirmationRequest;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.response.PaymentProcessingResponse;
import dev.vcgroup.thonowandroidapp.data.model.epayment.momo.response.TransactionConfirmationResponse;
import dev.vcgroup.thonowandroidapp.data.model.epayment.vnpay.CreatePaymentRequest;
import dev.vcgroup.thonowandroidapp.data.remote.epaymentservice.EPaymentService;
import dev.vcgroup.thonowandroidapp.data.remote.epaymentservice.PaymentRetrofitClientInstance;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCreateAPaymentBinding;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.thankyou.ThankForYourOrderActivity;
import dev.vcgroup.thonowandroidapp.util.GenericWebview;
import dev.vcgroup.thonowandroidapp.util.ewallet.EPaymentUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.momo.momo_partner.AppMoMoLib;

public class CreateAPaymentActivity extends AppCompatActivity implements View.OnClickListener {
	private static final String TAG = CreateAPaymentActivity.class.getSimpleName();
	private ActivityCreateAPaymentBinding binding;
	private CreateAPaymentViewModel mViewModel;
	private static EPaymentService ePaymentService;
	private static FirebaseFirestore db;
	static {
		ePaymentService =
				PaymentRetrofitClientInstance.getInstance(TransactionMethod.MOMO).getRetrofit().create(EPaymentService.class);
		db = FirebaseFirestore.getInstance();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_create_a_payment);
		mViewModel = ViewModelProviders.of(this).get(CreateAPaymentViewModel.class);

		if (getIntent().getExtras() != null) {
			long amount = getIntent().getLongExtra("amount", 0);
			Log.d(TAG, "amount: " + amount	);
			mViewModel.setAmount(amount);

			String orderId = getIntent().getStringExtra("orderId");
			mViewModel.setOrderId(orderId);

			Worker worker = Parcels.unwrap(getIntent().getParcelableExtra("worker"));
			mViewModel.setCurrentWorker(worker);
		}

		binding.rdBtnMomo.setOnClickListener(this);
		binding.rdBtnZaloPay.setOnClickListener(this);
		binding.btnMomo.setOnClickListener(this);
		binding.btnZaloPay.setOnClickListener(this);
		binding.btnVnpay.setOnClickListener(this);
		binding.rdBtnVnpay.setOnClickListener(this);
	}

	Fragment currentFragment;

	private void setupToolbar() {
		Toolbar toolbar = binding.topAppBar.mToolbar;
		setSupportActionBar(toolbar);
		Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
		setTitle(null);
		binding.topAppBar.tvTitleToolbar.setText("Chọn phương thức giao dịch");
		getSupportActionBar().show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setupToolbar();
	}

	@Override
	protected void onStop() {
		super.onStop();
		Objects.requireNonNull(getSupportActionBar()).hide();
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_zalo_pay:
			case R.id.rd_btn_zalo_pay:
				binding.rdBtnMomo.setChecked(false);
				binding.rdBtnVnpay.setChecked(false);
				binding.rdBtnZaloPay.setChecked(true);
				new Handler().postDelayed(() -> {

				}, 500);

				break;
			case R.id.btn_momo:
			case R.id.rd_btn_momo:
				binding.rdBtnZaloPay.setChecked(false);
				binding.rdBtnVnpay.setChecked(false);
				binding.rdBtnMomo.setChecked(true);
				new Handler().postDelayed(this::createPaymentThroughMomo, 500);

				break;
			case R.id.btn_vnpay:
			case R.id.rd_btn_vnpay:
				binding.rdBtnZaloPay.setChecked(false);
				binding.rdBtnMomo.setChecked(false);
				binding.rdBtnVnpay.setChecked(true);
				new Handler().postDelayed(this::createPaymentThroughVnPay, 500);

				break;
		}
	}

	private void createPaymentThroughVnPay() {
		try {
			CreatePaymentRequest vnPaymentRequest =
					CreatePaymentRequest.createFrom(this, mViewModel.getOrderId().getValue(),
							mViewModel.getAmount().getValue(), null);
			String paymentUrl = EPaymentUtil.getPaymentUrl(vnPaymentRequest);
			Log.d(TAG, paymentUrl);

			GenericWebview.create(binding.wvVnpay, paymentUrl, () -> {
				binding.wvVnpay.setVisibility(View.GONE);
				// updateBalanceMoney(vnPaymentRequest.getVnp_TxnRef());
				// chuyển sang thank you
				updateNextOrderStatus();
			});

			binding.wvVnpay.setVisibility(View.VISIBLE);
		} catch (UnknownHostException | NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	private void navigateToThankYouScreen() {
		Intent intent = new Intent(this, ThankForYourOrderActivity.class);
		intent.putExtra("orderId", mViewModel.getOrderId().getValue());
		intent.putExtra("worker", Parcels.wrap(mViewModel.getCurrentWorker().getValue()));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
		setResult(RESULT_OK);
		finish();
	}

	private Map<String, Object> eventValue;

	private void createPaymentThroughMomo() {
		AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT);
		AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
		AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);
		eventValue = EPaymentUtil.getMomoData(mViewModel.getOrderId().getValue(), mViewModel.getAmount().getValue());
		Log.d(TAG, "eventValue: " + eventValue.toString());
		AppMoMoLib.getInstance().requestMoMoCallBack(this, eventValue);
	}

	@SneakyThrows
	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == Activity.RESULT_OK) {
			if (data != null) {
				EPaymentUtil.MomoResponse response = EPaymentUtil.getResponse(data);

				if (response.getStatus() == 0) { // User xác nhận thanh toán thành công
					String token = response.getData();
					if (token != null && !token.isEmpty()) {
						eventValue.put(MoMoParameters.PHONE_NUMBER_V1, response.getPhoneNumber());
						eventValue.put(MoMoParameters.APP_DATA, response.getData());
						processMomoPayment();
					} else {
						Log.d(TAG, "token = null");
						Toast.makeText(this, this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
					}
				} else if (response.getStatus() == 5) { //Hết thời gian thực hiện giao dịch (Timeout transaction)
					Toast.makeText(this, "Hết thời gian thực hiện giao dịch", Toast.LENGTH_SHORT).show();
					Log.d(TAG, response.getMessage());
				} else { // User huỷ giao dịch
					Log.d(TAG, response.getMessage());
				}
			} else {
				Log.d(TAG, "data == null");
				Toast.makeText(this, this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
			}
		} else {
			Log.d(TAG, "không chuyển tiếp");
			Toast.makeText(this, this.getString(R.string.not_receive_info), Toast.LENGTH_SHORT).show();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void processMomoPayment() throws Exception {
		PaymentProcessingRequest request = PaymentProcessingRequest.createFrom(eventValue);
		Log.d(TAG, "request: " + request.toString());
		ePaymentService.processPayment(request)
				.enqueue(new Callback<PaymentProcessingResponse>() {
					@SneakyThrows
					@Override
					public void onResponse(Call<PaymentProcessingResponse> call, Response<PaymentProcessingResponse> response) {
						if (response.isSuccessful()) {
							PaymentProcessingResponse rsp = response.body();
							if (rsp != null) {
								Log.d(TAG, "rsp: " + rsp.toString());
								if (rsp.getStatus() == 0) { // Giao dịch thành công
									TransactionConfirmationRequest confirmationRequest = TransactionConfirmationRequest.createFrom(request,
											rsp);
									processTransactionConfirmation(confirmationRequest);
								} else {
									Log.d(TAG, "processPayment: " + rsp.getMessage());
									//Toast.makeText(CreateAPaymentActivity.this, rsp.getMessage(), Toast.LENGTH_SHORT).show();
								}
							}

						} else {
							Toast.makeText(CreateAPaymentActivity.this, "Không thể thực hiện giao dịch. Vui" +
											" lòng thử lại!",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onFailure(Call<PaymentProcessingResponse> call, Throwable t) {
						Log.d(TAG, t.getMessage());
						Toast.makeText(CreateAPaymentActivity.this, "Đã xảy ra lỗi gì đó. Xin hãy thử lại một lần nữa!", Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void processTransactionConfirmation(TransactionConfirmationRequest confirmationRequest) {
		ePaymentService.processPaymentConfirmation(confirmationRequest)
				.enqueue(new Callback<TransactionConfirmationResponse>() {
					@Override
					public void onResponse(Call<TransactionConfirmationResponse> call, Response<TransactionConfirmationResponse> response) {
						if (response.isSuccessful()) {
							TransactionConfirmationResponse confirmationResponse = response.body();
							if (confirmationResponse != null) {
								Log.d(TAG, confirmationResponse.toString());

								if (confirmationResponse.getStatus() == 0) {
									// update balance_money
									// updateBalanceMoney(confirmationResponse.getData().getMomoTransId());
									// show success screen
									updateNextOrderStatus();
								} else {
									Toast.makeText(CreateAPaymentActivity.this, "Giao dịch xử lý thất bại!",
											Toast.LENGTH_SHORT).show();
								}
							} else {
								Toast.makeText(CreateAPaymentActivity.this, "Xác nhận giao dịch xảy ra lỗi!", Toast.LENGTH_SHORT).show();
							}
						} else {
							Toast.makeText(CreateAPaymentActivity.this, "Oops! Dường như đã có lỗi xảy ra. " +
											"Hãy thử lại!",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onFailure(Call<TransactionConfirmationResponse> call, Throwable t) {
						Log.d(TAG, t.getMessage());
						Toast.makeText(CreateAPaymentActivity.this, "Oops! Dường như đã có lỗi xảy ra. " +
										"Hãy thử lại!",
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	private void updateNextOrderStatus() {
		db.collection(CollectionConst.COLLECTION_ORDER)
				.document(Objects.requireNonNull(mViewModel.getOrderId().getValue()))
				.update(
						"status", OrderStatus.WAIT_FOR_CONFIRMATION_TO_PAY
				)
				.addOnSuccessListener(unused -> {
					insertTransactionHistory();
					navigateToThankYouScreen();
				})
				.addOnFailureListener(e -> {
					Log.d(TAG, e.getLocalizedMessage());
					Toast.makeText(this, "Kiểm tra lại đường truyền Internet để tiếp tục", Toast.LENGTH_SHORT).show();
				});
	}


	private void insertTransactionHistory() {
		DocumentReference orderDocRef = db.collection(CollectionConst.COLLECTION_ORDER).document(Objects.requireNonNull(mViewModel.getOrderId().getValue()));
		Transaction transaction = Transaction.newObject(db, mViewModel.getAmount().getValue(), orderDocRef);
		db.collection(CollectionConst.COLLECTION_CUSTOMER_TRANSACTION_HISTORY)
				.add(transaction);
	}
}