package dev.vcgroup.thonowandroidapp.ui.notification.remotenotification;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.local.database.MyDatabase;
import dev.vcgroup.thonowandroidapp.data.local.database.dao.NotificationDao;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification;
import dev.vcgroup.thonowandroidapp.databinding.FragmentNotificationListBinding;
import dev.vcgroup.thonowandroidapp.ui.notification.adapter.NotificationListAdapter;

public class NotificationListFragment extends Fragment implements NotificationListAdapter.OnNotificationItemListener {
    private FragmentNotificationListBinding binding;
    private NotificationListAdapter adapter;
    private NotificationDao notificationDao;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_list,
                container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        adapter = new NotificationListAdapter(new ArrayList<>(), this);
        binding.rvNotification.setAdapter(adapter);
        populateData();
    }

    private void populateData() {
        if (notificationDao == null) {
            notificationDao = MyDatabase.getDatabase(requireContext()).notificationDao();
        }

        class PopulateData extends AsyncTask<Void, Void, List<FCMRemoteNotification>> {

            @Override
            protected List<FCMRemoteNotification> doInBackground(Void... voids) {
                List<FCMRemoteNotification> notifications = notificationDao.getAll();
                return notifications;
            }

            @Override
            protected void onPostExecute(List<FCMRemoteNotification> fcmRemoteNotifications) {
                super.onPostExecute(fcmRemoteNotifications);
                if (fcmRemoteNotifications != null && !fcmRemoteNotifications.isEmpty()) {
                    adapter.update(fcmRemoteNotifications);
                    binding.rvNotification.setVisibility(View.VISIBLE);
                    binding.emptySection.setVisibility(View.INVISIBLE);
                } else {
                    binding.rvNotification.setVisibility(View.INVISIBLE);
                    binding.emptySection.setVisibility(View.VISIBLE);
                }
            }
        }

        new PopulateData().execute();
    }

    @Override
    public void onNotificationItemClick(int position) {

    }

    @Override
    public void onNotificationItemLongClick(int position) {

    }
}