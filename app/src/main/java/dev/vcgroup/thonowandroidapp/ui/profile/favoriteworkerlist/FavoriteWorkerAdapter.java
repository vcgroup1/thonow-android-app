package dev.vcgroup.thonowandroidapp.ui.profile.favoriteworkerlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ItemFavoriteWorkerBinding;

public class FavoriteWorkerAdapter extends RecyclerView.Adapter<FavoriteWorkerAdapter.FavoriteWorkerVH> {
    private List<Worker> workerList;
    private OnFavoriteWorkerListener onFavoriteWorkerListener;
    private AsyncListDiffer<Worker> mDiffer;
    private DiffUtil.ItemCallback<Worker> mCallback = new DiffUtil.ItemCallback<Worker>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull Worker oldItem, @NonNull @NotNull Worker newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull Worker oldItem, @NonNull @NotNull Worker newItem) {
            return oldItem.getPhoneNumber().equals(newItem.getPhoneNumber());
        }
    };

    public FavoriteWorkerAdapter(List<Worker> workerList, OnFavoriteWorkerListener onFavoriteWorkerListener) {
        this.workerList = workerList;
        this.onFavoriteWorkerListener = onFavoriteWorkerListener;
        this.mDiffer = new AsyncListDiffer<Worker>(this, mCallback);
    }

    @NonNull
    @NotNull
    @Override
    public FavoriteWorkerVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new FavoriteWorkerVH(ItemFavoriteWorkerBinding.inflate(inflater, parent, false), onFavoriteWorkerListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull FavoriteWorkerAdapter.FavoriteWorkerVH holder, int position) {
        Worker worker = getItem(position);
        holder.bind(worker);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Worker getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<Worker> newList) {
        mDiffer.submitList(newList == null ? null : new ArrayList<>(newList));
    }

    public class FavoriteWorkerVH extends RecyclerView.ViewHolder {
        private ItemFavoriteWorkerBinding binding;
        private OnFavoriteWorkerListener onFavoriteWorkerListener;

        public FavoriteWorkerVH(ItemFavoriteWorkerBinding binding, OnFavoriteWorkerListener onFavoriteWorkerListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onFavoriteWorkerListener = onFavoriteWorkerListener;

            binding.getRoot().setOnClickListener(v -> {
                if (onFavoriteWorkerListener != null) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        onFavoriteWorkerListener.onFavoriteWorkerClickedListener(pos);
                    }
                }
            });
        }

        public void bind(Worker worker) {
            binding.setItem(worker);
            binding.executePendingBindings();
        }
    }

    public interface OnFavoriteWorkerListener {
        void onFavoriteWorkerClickedListener(int position);
    }
}
