package dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbylist;

import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.parceler.Parcels;

import java.util.ArrayList;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.databinding.BsdTaskFilterBinding;
import dev.vcgroup.thonowandroidapp.databinding.TaskByListFragmentBinding;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.TaskManagementViewModel;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter.TaskListAdapter;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.confirmtocompleteorder.ConfirmToCompleteOrderActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.CANCEL_ORDER_REQ_CODE;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.CONFIRM_TO_COMPLETE_ORDER_REQ_CODE;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.SEE_DETAIL_ORDER_REQ_CODE;

public class TaskByListFragment extends Fragment implements TaskListAdapter.OnTaskListener {
    private static final String TAG = TaskByListFragment.class.getSimpleName();
    private TaskByListFragmentBinding binding;
    private TaskListAdapter adapter;
    private BottomSheetDialog bsdTaskFilter;
    private TaskManagementViewModel mViewModel;

    public static TaskByListFragment newInstance() {
        return new TaskByListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.task_by_list_fragment, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(TaskManagementViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupRvTaskList();

        binding.btnTaskFilter.setOnClickListener(v -> {
            BsdTaskFilterBinding taskFilterBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_task_filter, (ViewGroup) v.getRootView(), false);
            bsdTaskFilter = new BottomSheetDialog(requireActivity());
            bsdTaskFilter.setContentView(taskFilterBinding.getRoot());

            bsdTaskFilter.show();

            taskFilterBinding.lvOrderStatus.setOnItemClickListener((parent, view, position, id) -> {
                String query = (String) taskFilterBinding.lvOrderStatus.getItemAtPosition(position);
                binding.tvTaskFilter.setText(query);

                adapter.getFilter().filter(query, count -> {
                    if (count != 0) {
                        binding.rvTaskList.setVisibility(View.VISIBLE);
                        binding.noTaskSchedule.getRoot().setVisibility(View.GONE);
                    } else {
                        binding.rvTaskList.setVisibility(View.GONE);
                        binding.noTaskSchedule.getRoot().setVisibility(View.VISIBLE);
                    }
                });

                bsdTaskFilter.dismiss();
            });


        });
    }

    private void setupRvTaskList() {
        mViewModel.getOrders().observe(requireActivity(), orders -> {
            adapter = new TaskListAdapter(orders, TaskListAdapter.TaskAdapterType.TASK_BY_LIST_ITEM, this);
            binding.rvTaskList.setAdapter(adapter);
            binding.rvTaskList.addItemDecoration(new SpacesItemDecoration(20));
            adapter.update(orders);
        });
    }

    @Override
    public void onTaskClickedListener(int position) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(requireActivity(), TaskDetailActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        startActivity(intent);
    }

    @Override
    public void onTaskCancelClickedListener(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(requireActivity(), CancelOrderActivity.class);
        intent.putExtra("deleteOrderId", Parcels.wrap(orderId));
        startActivityForResult(intent, CANCEL_ORDER_REQ_CODE);
    }

    @Override
    public void onChatClickedListener(int position) {

    }

    @Override
    public void onAudioCallClickedListener(int position) {

    }

    @Override
    public void onConfirmToCompleterOrder(int position) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(requireActivity(), ConfirmToCompleteOrderActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        startActivityForResult(intent, CONFIRM_TO_COMPLETE_ORDER_REQ_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CANCEL_ORDER_REQ_CODE && resultCode == Activity.RESULT_OK) {
            adapter.notifyDataSetChanged();
        } else if (requestCode == SEE_DETAIL_ORDER_REQ_CODE && resultCode == Activity.RESULT_OK) {
            adapter.notifyDataSetChanged();
        }
    }
}