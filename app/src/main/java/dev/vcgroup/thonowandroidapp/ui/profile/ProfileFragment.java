package dev.vcgroup.thonowandroidapp.ui.profile;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.databinding.BsdInviteFriendDialogBinding;
import dev.vcgroup.thonowandroidapp.databinding.FragmentProfileBinding;
import dev.vcgroup.thonowandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowandroidapp.ui.profile.favoriteworkerlist.FavoriteWorkerListActivity;
import dev.vcgroup.thonowandroidapp.ui.profile.jobhistory.JobHistoryActivity;
import dev.vcgroup.thonowandroidapp.ui.profile.transactionhistory.TransactionHistoryActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.EDIT_PROFILE_REQ_CODE;

public class ProfileFragment extends Fragment {
    private ProfileViewModel mViewModel;
    private FragmentProfileBinding binding;
    private Toolbar mToolbar;
    private BottomSheetDialog bsdInviteFriend;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        binding.setItem(mViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel.setCurrentUser(User.convertFrom(FirebaseAuth.getInstance().getCurrentUser()));

        setHasOptionsMenu(true);
        setupToolbar();

        binding.jobHistorySection.setOnClickListener(v -> {
            startActivity(new Intent(requireActivity(), JobHistoryActivity.class));
        });

        binding.transactionHistorySection.setOnClickListener(v -> {
            startActivity(new Intent(requireActivity(), TransactionHistoryActivity.class));
        });

        binding.favoriteWorkerSection.setOnClickListener(v -> {
            startActivity(new Intent(requireActivity(), FavoriteWorkerListActivity.class));
        });

        binding.ratingSection.setOnClickListener(v -> {
            BsdInviteFriendDialogBinding inviteBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_invite_friend_dialog, (ViewGroup) v.getRootView(), false);
            bsdInviteFriend = new BottomSheetDialog(requireActivity());
            bsdInviteFriend.setContentView(inviteBinding.getRoot());

            String id = Objects.requireNonNull(mViewModel.getCurrentUser().getValue()).getUid();

            inviteBinding.txtGuiLink.setOnClickListener(v1 -> {
                generateContentLink(id);
            });

            inviteBinding.txtGuiMa.setOnClickListener(v1 -> {
                String contentShare = "Mã giới thiệu khi cài đặt ứng dụng ThoNOW Dành cho thợ: " + id;
                shareLink(contentShare);
            });

            inviteBinding.txtBack.setOnClickListener(v1 -> {
                bsdInviteFriend.dismiss();
            });

            bsdInviteFriend.show();
        });

        binding.goToEditProfile.setOnClickListener(v -> navigateToEditProfile());
    }

    private void setupToolbar() {
        mToolbar = binding.topAppBar.mToolbar;
        mToolbar.setNavigationIcon(null);
        ((MainScreenActivity) requireActivity()).setSupportActionBar(mToolbar);
        Objects.requireNonNull(((MainScreenActivity) requireActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        Objects.requireNonNull(((MainScreenActivity) requireActivity()).getSupportActionBar()).setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Hồ sơ của bạn");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.profile_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_profile);
        menuItem.setVisible(false);
	    binding.fragmentProfileScrollview.scrollTo(0,0);
        binding.fragmentProfileScrollview.setOnScrollChangeListener((View.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int actionBarHeight = CommonUtil.getActionBarHeight(getActivity());
            if(scrollY >= actionBarHeight) {
                menuItem.setVisible(true);
            } else {
                menuItem.setVisible(false);
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_profile) {
            navigateToEditProfile();
            return true;
        }
        return false;
    }

    private void navigateToEditProfile() {
        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
        intent.putExtra("currentUser", Parcels.wrap(mViewModel.getCurrentUser().getValue()));
        startActivityForResult(intent, EDIT_PROFILE_REQ_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_PROFILE_REQ_CODE && resultCode == Activity.RESULT_OK && data != null) {
	       mViewModel.setCurrentUser(Parcels.unwrap(data.getParcelableExtra("user")));
        }
    }

    private void generateContentLink(String id) {
        String sharelinktext  = "https://thonow.page.link?"+
                "link=https://www.thonow.com/invite-friend/" + id + "/" +
                "&apn=" + requireActivity().getPackageName()+
                "&st=" + "Link-gioi-thieu-cua-toi";

        FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setLongLink(Uri.parse(sharelinktext))
                .buildShortDynamicLink()
                .addOnSuccessListener(shortDynamicLink -> {
                    // Short link created
                    Uri shortLink = shortDynamicLink.getShortLink();
                    Uri flowchartLink = shortDynamicLink.getPreviewLink();

                    if (shortLink != null) {
                        String contentShare = "Mời bạn sử dụng ứng dụng nhận gọi thợ ThoNOW App\n"
                                + "Link download ứng dụng:\n"
                                + shortLink.toString();

                        shareLink(contentShare);
                    }

                })
                .addOnFailureListener(e -> {
                    Log.d("dynamicLink", e.getMessage());
                    bsdInviteFriend.dismiss();
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
                    dialog.setTitle("Thông báo")
                            .setMessage("Tạo link giới thiệu xảy ra lỗi. Vui lòng kiểm tra internet và thử lại!")
                            .setPositiveButton("Đã hiểu", v -> dialog.dismiss())
                            .show();
                });
    }

    private void shareLink(String contentShare) {
        // Send app dialog
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, contentShare);
        startActivity(Intent.createChooser(intent, "Mời bạn bè sử dụng"));
        bsdInviteFriend.dismiss();
    }
}