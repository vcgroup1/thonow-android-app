package dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeCardviewBinding;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> implements Filterable {
    private static final String TAG = ServiceAdapter.class.getSimpleName();
    private List<Service> list;
    private List<Service> filteredList;
    private OnServiceListener onServiceListener;
    private int lastCheckedPosition = -1;
    private FirebaseFirestore db;
    private String selectedServiceId;

    public ServiceAdapter(OnServiceListener onServiceListener, @Nullable ServiceType selectedServiceType, @Nullable String selectedServiceId) {
        this.onServiceListener = onServiceListener;
        this.list = new ArrayList<>();
        this.db = FirebaseFirestore.getInstance();
        this.selectedServiceId = selectedServiceId;
        this.populateData(selectedServiceType);
    }

    private void populateData(ServiceType selectedServiceType) {
        if (selectedServiceType != null) {
            DocumentReference serviceTypeDocRef = db.collection(CollectionConst.COLLECTION_SERVICE_TYPE).document(selectedServiceType.getId());
            db.collection(CollectionConst.COLLECTION_SERVICE)
                    .whereEqualTo("serviceType", serviceTypeDocRef)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        List<Service> result = queryDocumentSnapshots.toObjects(Service.class);
                        refreshData(result);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        } else {
            db.collection(CollectionConst.COLLECTION_SERVICE)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        List<Service> result = queryDocumentSnapshots.toObjects(Service.class);
                        refreshData(result);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        }
    }

    private void refreshData(List<Service> result) {
        this.list = result;
        this.filteredList = result;
        notifyDataSetChanged();
        if (selectedServiceId != null) {
            list.forEach(sv -> {
                if (sv.getId().equals(selectedServiceId)) {
                    lastCheckedPosition = list.indexOf(sv);
                }
            });
        }
    }

    @NonNull
    @NotNull
    @Override
    public ServiceAdapter.ServiceViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ServiceViewHolder(ItemServiceBinding.inflate(inflater, parent, false), onServiceListener);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    filterResults.values = filteredList;
                    filterResults.count = filteredList.size();
                } else {
                    List<Service> resultList = new ArrayList<>();
                    if (resultList.isEmpty()) {
                        filteredList.forEach(service -> {
                            if (service.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                resultList.add(service);
                            }
                        });
                    }
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<Service>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ServiceAdapter.ServiceViewHolder holder, int position) {
        Service service = list.get(position);
        holder.bind(service);
        holder.binding.cvService.setChecked(position == lastCheckedPosition);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public Service getItem(int position) {
        return list.get(position);
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemServiceBinding binding;
        private OnServiceListener onServiceListener;

        public ServiceViewHolder(@NonNull @NotNull ItemServiceBinding binding, OnServiceListener onServiceListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onServiceListener = onServiceListener;
            binding.getRoot().setOnClickListener(this);
        }


        public void bind(Service item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onServiceListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onServiceListener.onServiceClickedListener(position);
                    if (binding != null) {
                        binding.cvService.setChecked(!binding.cvService.isChecked());
                        int copyOfLastCheckedPosition = lastCheckedPosition;
                        lastCheckedPosition = getAdapterPosition();
                        notifyItemChanged(copyOfLastCheckedPosition);
                        notifyItemChanged(lastCheckedPosition);
                    }
                }
            }
        }
    }

    public interface OnServiceListener {
        void onServiceClickedListener(int position);
    }
}
