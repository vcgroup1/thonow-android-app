package dev.vcgroup.thonowandroidapp.ui.taskmanagement.confirmtocompleteorder;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.PaymentMethod;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityConfirmationOfPaymentBinding;
import dev.vcgroup.thonowandroidapp.databinding.DialogEnterTipMoneyBinding;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.createapayment.CreateAPaymentActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.thankyou.ThankForYourOrderActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;
import dev.vcgroup.thonowandroidapp.util.VassCustomToast;


public class ConfirmToCompleteOrderActivity extends AppCompatActivity {
    private static final String TAG = ConfirmToCompleteOrderActivity.class.getSimpleName();
    private ActivityConfirmationOfPaymentBinding binding;
    private Order order;
    private Worker worker;
    private FirebaseFirestore db;
    private PaymentMethod selectedPaymentMethod;
    private VCLoadingDialog loading;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation_of_payment);
        db = FirebaseFirestore.getInstance();
        loading = VCLoadingDialog.create(this);

        if (getIntent().getExtras() != null) {
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));
            if (order != null) {
                binding.setOrder(order);

                order.getWorker().get()
                        .addOnSuccessListener(documentSnapshot -> {
                            worker = documentSnapshot.toObject(Worker.class);
                            binding.setWorker(worker);
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

                // bind service type
                order.getOrderDetails().get(0).getService().get()
                        .addOnSuccessListener(documentSnapshot -> {
                            Service service = documentSnapshot.toObject(Service.class);
                            binding.setService(service);
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            }

            selectedPaymentMethod = PaymentMethod.valueOf(getIntent().getStringExtra("paymentMethod"));
            binding.paymentMethod.setText(selectedPaymentMethod.getTitle());
        }

        binding.btnMakeAPayment.setOnClickListener(v -> {
            if (selectedPaymentMethod == PaymentMethod.CASH) {
                db.collection(CollectionConst.COLLECTION_ORDER)
                        .document(order.getId())
                        .update(
                                "status", order.getStatus().nextStatus()
                        )
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Intent intent = new Intent(this, ThankForYourOrderActivity.class);
                                intent.putExtra("orderId", order.getId());
                                intent.putExtra("worker", Parcels.wrap(worker));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                                dialog.setTitle("Thông báo")
                                        .setMessage("Có gì đó không ổn. Vui lòng thử lại!")
                                        .setPositiveButton("Thử lại", v1 -> {
                                            dialog.dismiss();
                                            binding.btnMakeAPayment.performClick();
                                        })
                                        .setNegativeButton("Huỷ", v1 -> dialog.dismiss());
                            }
                        });
            } else {
                Intent intent = new Intent(this, CreateAPaymentActivity.class);
                intent.putExtra("amount", Math.round(order.getTotal()));
                intent.putExtra("orderId", order.getId());
                intent.putExtra("worker", Parcels.wrap(worker));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                setResult(RESULT_OK);
                finish();
            }
        });

        binding.btnTipMoney.setOnClickListener(v -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            DialogEnterTipMoneyBinding tipMoneyBinding = DialogEnterTipMoneyBinding.inflate(LayoutInflater.from(this));
            builder.setView(tipMoneyBinding.getRoot());

            AlertDialog dialog = builder.create();
            dialog.show();

            tipMoneyBinding.tipAdd.setOnClickListener(v1 -> {
                String amountStr = CommonUtil.getInputText(tipMoneyBinding.tipAmount);
                if (amountStr.isEmpty()) {
                    return;
                } else {
                    double amount = Double.parseDouble(amountStr);
                    if (amount < 1000 || amount > 10000000) {
                        VassCustomToast.makeText(this,
                                "Số tiền nhập vào không hợp lệ!",
                                Toast.LENGTH_SHORT,
                                VassCustomToast.ToastType.ERROR)
                                .show();
                        return;
                    }
                }
                updateOrderTipMoney(amountStr);
                dialog.dismiss();
            });

            tipMoneyBinding.tipCancel.setOnClickListener(v1 -> {
                dialog.dismiss();

            });

            tipMoneyBinding.bsdClose.setOnClickListener(v1 -> {
                dialog.dismiss();
            });

        });

    }

    private void updateOrderTipMoney(String amount) {
        loading.show();
        double amt = Double.parseDouble(amount);
        order.setTip(amt);
        binding.btnTipMoney.setText(amt + "đ");
//        if (order.getTip() == 0) {
//            order.setTotal(order.getTotal() + amt);
//        } else {
//            order.setTotal(order.getTotal() - order.getTip() + amt);
//        }

        db.collection(CollectionConst.COLLECTION_ORDER)
                .document(order.getId())
                .update("tip", amt,
                "total", order.getTip() == 0 ? (order.getTotal() + amt) : (order.getTotal() - order.getTip() + amt))
                .addOnSuccessListener(command -> {
                    loading.dismiss();
                })
                .addOnFailureListener(command -> {
                    VassCustomToast.makeText(ConfirmToCompleteOrderActivity.this, "Vui lòng xem lại đường truyền Internet!", Toast.LENGTH_SHORT, VassCustomToast.ToastType.ERROR).show();
                    loading.dismiss();
                });

    }
}
