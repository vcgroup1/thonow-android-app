package dev.vcgroup.thonowandroidapp.ui.homepage.offerdetail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.parceler.Parcels;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Offer;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOfferDetailBinding;
import dev.vcgroup.thonowandroidapp.ui.homepage.newsdetail.NewsDetailActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;

public class OfferDetailActivity extends AppCompatActivity {
    private static final String TAG = OfferDetailActivity.class.getSimpleName();
    private ActivityOfferDetailBinding binding;
    private Offer offer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offer_detail);
        setupToolbar();
        if(getIntent().getExtras() != null) {
            offer = Parcels.unwrap(getIntent().getParcelableExtra("offer"));
            if(offer != null) {
                binding.setItem(offer);
            }
        }

        binding.newsDetailScrollView.setOnScrollChangeListener((View.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY > 250) {
                CommonUtil.animateFade(binding.newsDetailBottomSection, View.VISIBLE);
            } else {
                CommonUtil.animateFade(binding.newsDetailBottomSection, View.GONE);
            }
        });

        binding.btnCopyCode.setOnClickListener(v -> {
            String code = offer.getCode();
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText(code, code);
            clipboardManager.setPrimaryClip(clipData);
            Snackbar.make(binding.getRoot(), "Đã copy mã khuyến mãi", BaseTransientBottomBar.LENGTH_SHORT).show();
        });
    }

    private void setupToolbar() {
        setSupportActionBar(binding.mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }
}