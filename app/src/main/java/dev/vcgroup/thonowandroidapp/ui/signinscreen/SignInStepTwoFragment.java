package dev.vcgroup.thonowandroidapp.ui.signinscreen;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.kusu.loadingbutton.LoadingButton;
import com.mukesh.OtpView;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.AuthMethod;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.databinding.FragmentSignInStepTwoBinding;
import dev.vcgroup.thonowandroidapp.ui.MainScreenActivity;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;
import lombok.SneakyThrows;

public class SignInStepTwoFragment extends Fragment {
    private static final String TAG = SignInStepTwoFragment.class.getSimpleName();
    private static final String KEY_VERIFICATION_ID = "key_verification_id";
    private String strVerificationId;
    private CountDownTimer countDownTimer;
    private FragmentSignInStepTwoBinding binding;
    private SignInViewModel signInViewModel;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private LoadingButton btnResendCode;
    private String phoneNumber;
    private OtpView inputOtp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_step_two, container, false);
        signInViewModel = ViewModelProviders.of(requireActivity()).get(SignInViewModel.class);
        binding.setSignInViewModel(signInViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @SneakyThrows
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFirebase();

        inputOtp = view.findViewById(R.id.input_otp);
        phoneNumber = signInViewModel.getPhoneNumber().getValue();
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phonenumberProto = phoneNumberUtil.parse(phoneNumber, "VN");
        binding.tvPhoneNumber.setText(PhoneNumberUtil.getInstance().format(phonenumberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL));
        sendVerificationCode(phoneNumber);

        btnResendCode = binding.btnResendCode;
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String secondString = "";
                long second = millisUntilFinished / 1000;
                if (second < 10) {
                    secondString = "0" + second;
                } else {
                    secondString = second + "";
                }
                binding.tvCountDownTimer.setText("0:" + secondString);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
                btnResendCode.setEnabled(true);
                btnResendCode.setBackgroundColor(getResources().getColor(R.color.strong_primary_red, requireActivity().getTheme()));
                btnResendCode.setTextColor(requireActivity().getColor(android.R.color.white));
                binding.tvRegisterHelper.setText("Bạn không nhận được mã?");
                binding.tvCountDownTimer.setVisibility(View.GONE);
                binding.tvWrongPhoneNumber.setVisibility(View.VISIBLE);
            }
        };
        countDownTimer.start();

        btnResendCode.setOnClickListener(view1 -> {
            btnResendCode.showLoading();
            sendVerificationCode(phoneNumber);
            binding.tvRegisterHelper.setText("Gửi lại mã sau: ");
            binding.tvCountDownTimer.setVisibility(View.VISIBLE);
            btnResendCode.setEnabled(false);
            btnResendCode.setBackgroundColor(Objects.requireNonNull(getActivity()).getColor(R.color.light_gray));
            btnResendCode.setTextColor(getActivity().getColor(R.color.txt_medium_gray));
            binding.tvWrongPhoneNumber.setVisibility(View.GONE);
            countDownTimer.start();
            btnResendCode.hideLoading();
        });

        binding.ibnEditPhoneNumber.setOnClickListener(view1 -> {
            requireActivity().onBackPressed();
        });

        inputOtp.setOtpCompletionListener(this::verifyVerificationCode);
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(strVerificationId == null && savedInstanceState != null) {
            onViewStateRestored(savedInstanceState);
        }
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    private void createUserProfile(FirebaseUser currentUser) {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            navigateToMainScreen();
                        } else {
                            db.collection(CollectionConst.COLLECTION_USER)
                                    .document(currentUser.getUid())
                                    .set(User.convertFrom(currentUser))
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            Log.d(TAG, "createUserPro5::onSuccess::" + "Thành công");
                                            navigateToMainScreen();
                                        } else {
                                            Log.d(TAG, "createUserPro5::onFailure:: " + "Thất bại");
                                            Snackbar.make(binding.tvPhoneNumber.getRootView(), "Đăng nhập xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                });
    }

    private void navigateToMainScreen() {
        VCLoadingDialog loading = VCLoadingDialog.create(getActivity());
        loading.show();
        Intent intent = new Intent(getActivity(), MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        requireActivity().finish();
        loading.dismiss();
    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthOptions phoneAuthOptions = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber("+84" + phoneNumber)
                .setTimeout(30L, TimeUnit.SECONDS)
                .setActivity(requireActivity())
                .setCallbacks(phoneCallbacks)
                .requireSmsValidation(false)
                .build();
        PhoneAuthProvider.verifyPhoneNumber(phoneAuthOptions);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks phoneCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                inputOtp.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            setErrorText("Mã pin không chính xác");
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            strVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        try {
            String phoneNumber = "+84968958053";
            String smsCode = "190599";
            String phoneNumber2 = "+84986660762";

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);
            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber2, smsCode);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);

            Bundle bundle = getArguments();
            if (bundle != null) {
                Log.d(TAG, bundle.toString());
                Serializable socialAuthMethod = bundle.getSerializable("authMethod");
                if (socialAuthMethod.equals(AuthMethod.FACEBOOK) || socialAuthMethod.equals(AuthMethod.GOOGLE)) {
                    Log.d(TAG, "link user");
                    linkWithPhoneAuthCredential(credential);
                }
            } else {
                signInWithPhoneAuthCredential(credential);
            }
        } catch (Exception e) {
            Log.d("VerifyVerificationCode", e.getMessage());
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        countDownTimer.cancel();
                        createUserProfile(Objects.requireNonNull(mAuth.getCurrentUser()));
                    } else {
                        String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            setErrorText("Mã xác nhận không hợp lệ!");
                        } else {
                            setErrorText(message);
                        }
                    }
                });
    }

    private void linkWithPhoneAuthCredential(PhoneAuthCredential credential) {
        Objects.requireNonNull(mAuth.getCurrentUser()).linkWithCredential(credential)
                .addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "linkWithCredential:success");
                        FirebaseUser user = Objects.requireNonNull(task.getResult()).getUser();
                        countDownTimer.cancel();
                        createUserProfile(Objects.requireNonNull(user));
                    } else {
                        Log.w(TAG, "linkWithCredential:failure", task.getException());
                        Snackbar.make(binding.tvPhoneNumber.getRootView(), "Đăng nhập xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                });

    }

    private void setErrorText(String text) {
        binding.tvRegisterHint.setText(text);
        binding.tvRegisterHint.setTextColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
    }

    @Override
    public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_VERIFICATION_ID, strVerificationId);
    }

    @Override
    public void onViewStateRestored(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            strVerificationId = savedInstanceState.getString(KEY_VERIFICATION_ID);
        }
    }
}