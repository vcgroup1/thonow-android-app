package dev.vcgroup.thonowandroidapp.ui.callworker.foundwoker;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CallWorkerConst;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityFoundWorkerBinding;
import dev.vcgroup.thonowandroidapp.ui.MainScreenActivity;

public class FoundWorkerActivity extends AppCompatActivity {
    private static final String TAG = FoundWorkerActivity.class.getSimpleName();
    private ActivityFoundWorkerBinding binding;
    private Order order;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_found_worker);

        if (getIntent().getExtras() != null) {
            String orderId = getIntent().getStringExtra("orderId");

            if (!orderId.isEmpty()) {
                db.collection(CollectionConst.COLLECTION_ORDER)
                        .document(orderId)
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            if (documentSnapshot.exists()) {
                                order = documentSnapshot.toObject(Order.class);

                                if (order != null) {
                                    setupUI();
                                }
                            }
                        })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
            }
        }

        binding.btnGoToTaskManagement.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(CallWorkerConst.IS_FROM_FOUND_WORKER_REQUEST_CODE, true);
            startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            finish();
        });
    }

    private void setupUI() {
        binding.setOrder(order);

        order.getWorker()
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    binding.setWorker(worker);
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }
}
