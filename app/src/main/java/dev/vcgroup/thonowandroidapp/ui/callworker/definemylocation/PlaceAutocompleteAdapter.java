package dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import dev.vcgroup.thonowandroidapp.R;

public class PlaceAutocompleteAdapter extends RecyclerView.Adapter<PlaceAutocompleteAdapter.PlaceViewholder> implements Filterable {
    private static final String TAG = PlaceAutocompleteAdapter.class.getSimpleName();
    private Context mContext;
    private List<AutocompletePrediction> mResults;
    private PlacesClient placesClient;
    private OnPlaceAutocompleteListener onPlaceAutocompleteListener;
    public static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    static RectangularBounds vnLatlngBounds;

    static {
        vnLatlngBounds = RectangularBounds.newInstance(new LatLng(102.170435826, 8.59975962975), new LatLng(109.33526981, 23.3520633001));
    }

    public PlaceAutocompleteAdapter(Context mContext, OnPlaceAutocompleteListener onPlaceAutocompleteListener) {
        this.mContext = mContext;
        this.mResults = new ArrayList<>();
        this.onPlaceAutocompleteListener = onPlaceAutocompleteListener;
        String apiKey = BuildConfig.MAPS_API_KEY;
        if (!Places.isInitialized()) {
            Places.initialize(mContext, apiKey);
        }
        this.placesClient = Places.createClient(mContext);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence keyword) {
                FilterResults filterResults = new FilterResults();
                if (keyword != null && keyword.length() > 0) {
                    String keywordStr = keyword.toString().toLowerCase();
                    mResults = getPredictions(keywordStr);
                    if (mResults != null) {
                        filterResults.values = mResults;
                        filterResults.count = mResults.size();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResults = (List<AutocompletePrediction>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private List<AutocompletePrediction> getPredictions(String keyword) {
        // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
        // and once again when the user makes a selection (for example when calling fetchPlace()).
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();


        // Use the builder to create a FindAutocompletePredictionsRequest.
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setLocationBias(vnLatlngBounds)
                .setCountry("vn")
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(keyword)
                .build();

        Task<FindAutocompletePredictionsResponse> autocompletePredictions = placesClient.findAutocompletePredictions(request);

        // This method should have been called off the main UI thread. Block and wait for at most
        // 60s for a result from the API.
        try {
            Tasks.await(autocompletePredictions, 60, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }

        if (autocompletePredictions.isSuccessful()) {
            FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = autocompletePredictions.getResult();
            if (findAutocompletePredictionsResponse != null)
                return findAutocompletePredictionsResponse.getAutocompletePredictions();
        }
        return null;
    }

    @NonNull
    @NotNull
    @Override
    public PlaceAutocompleteAdapter.PlaceViewholder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new PlaceViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place_search_result, parent, false), onPlaceAutocompleteListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PlaceAutocompleteAdapter.PlaceViewholder holder, int position) {
        holder.tvAddress.setText(getItem(position).getFullText(STYLE_BOLD));
    }

    @Override
    public int getItemCount() {
        return mResults == null ? 0 : mResults.size();
    }

    public AutocompletePrediction getItem(int position) {
        return mResults.get(position);
    }

    public static class PlaceViewholder extends RecyclerView.ViewHolder {
        private TextView tvAddress;
        private OnPlaceAutocompleteListener placeAutocompleteListener;

        public PlaceViewholder(@NonNull @NotNull View itemView, OnPlaceAutocompleteListener onPlaceAutocompleteListener) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tv_place_search_result);
            this.placeAutocompleteListener = onPlaceAutocompleteListener;
            itemView.setOnClickListener(v -> {
                if (placeAutocompleteListener != null) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        placeAutocompleteListener.onPlaceAutocompleteClickedListener(pos);
                    }
                }
            });
        }
    }

    public interface OnPlaceAutocompleteListener {
        void onPlaceAutocompleteClickedListener(int pos);
    }
}
