package dev.vcgroup.thonowandroidapp.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CallWorkerConst;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.local.preference.SessionManager;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.RetrofitFCMClientInstance;
import dev.vcgroup.thonowandroidapp.databinding.ActivityMainScreenBinding;
import dev.vcgroup.thonowandroidapp.ui.audiocall.BaseSinchActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainScreenActivity extends BaseSinchActivity  {
    private static final String TAG = MainScreenActivity.class.getSimpleName();
    private ActivityMainScreenBinding binding;
    public BottomNavigationView navView;
    private IFCMService ifcmService;
    private static FirebaseFirestore db;
    private BottomSheetBehavior bottomSheetBehavior;
    private Order order;
    private int senderCount = 0;

    static {
        db = FirebaseFirestore.getInstance();
    }

    private List<String> registration_ids;
    private List<Worker> requestWorkerList;
    private SessionManager sessionManager;
    private VCLoadingDialog loading;

    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loading = VCLoadingDialog.create(this);
        loading.show();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_screen);
        navView = findViewById(R.id.nav_view);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        createOrUpdateFcmToken();

        if (getIntent().getExtras() != null) {

            if (getIntent().getBooleanExtra(CallWorkerConst.IS_FROM_FOUND_WORKER_REQUEST_CODE, false)) {
                navView.setSelectedItemId(R.id.navigation_task_management);
                loading.dismiss();
                return;
            }

            requestWorkerList = Parcels.unwrap(getIntent().getParcelableExtra("requestWorkerList"));
            order = Parcels.unwrap(getIntent().getParcelableExtra("order"));

            if (order != null && requestWorkerList != null && !requestWorkerList.isEmpty()) {
                Log.d(TAG, order.toString());
                if (sessionManager == null) {
                    sessionManager = new SessionManager(this);
                }
                sessionManager.saveIsFindingWorker(true);

                if (registration_ids == null) {
                    registration_ids = new ArrayList<>();
                }

                requestWorkerList.forEach(worker -> {
                    generateTokenReceivers(worker.getId());
                });
            }

            View partialFindingWorker = binding.partialMinimizeFindingWorker.getRoot();
            partialFindingWorker.setVisibility(View.VISIBLE);
            bottomSheetBehavior = BottomSheetBehavior.from(partialFindingWorker);
            bottomSheetBehavior.setHideable(true);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            CommonUtil.animateToggle(navView, View.GONE);

            binding.partialMinimizeFindingWorker.cancelCallWorker.setOnClickListener(v -> {
                sessionManager.saveIsFindingWorker(false);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                CommonUtil.animateToggle(navView, View.VISIBLE);
            });

            binding.partialMinimizeFindingWorker.minimizeFindingWorker.ibnCancelCallWorker.setOnClickListener(v -> {
                sessionManager.saveIsFindingWorker(false);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                CommonUtil.animateToggle(navView, View.VISIBLE);
            });

            bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull @NotNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        binding.partialMinimizeFindingWorker.minimizeFindingWorker.getRoot().setVisibility(View.VISIBLE);
                        CommonUtil.animateToggle(navView, View.VISIBLE);
                    } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        binding.partialMinimizeFindingWorker.minimizeFindingWorker.getRoot().setVisibility(View.INVISIBLE);
                        CommonUtil.animateToggle(navView, View.GONE);
                    }
                }

                @Override
                public void onSlide(@NonNull @NotNull View bottomSheet, float slideOffset) {


                }
            });

            binding.partialMinimizeFindingWorker.btnMinimizeFindingWorker.setOnClickListener(v -> {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            });

            binding.partialMinimizeFindingWorker.getRoot().setOnClickListener(v -> {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });
        }
        loading.dismiss();
    }

    private void generateTokenReceivers(String workerId) {
        senderCount++;
        db.collection(CollectionConst.COLLECTION_TOKEN)
                .document(workerId)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Token token = documentSnapshot.toObject(Token.class);
                    if (token != null) {
                        registration_ids.add(token.getToken());
                        Log.d(TAG, registration_ids.toString());
                    }

                    if (senderCount == requestWorkerList.size()) {
                        sendCallWorkerRequestToWorkers();
                    }
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void sendCallWorkerRequestToWorkers() {
        if (ifcmService == null) {
            ifcmService = RetrofitFCMClientInstance.getInstance().getRetrofit().create(IFCMService.class);
        }

        ifcmService.sendCallWorkerRequest(FCMSendCallWorkerData.createSendCallWorkerData(order, registration_ids))
                .enqueue(new Callback<FCMResponse>() {
                    @Override
                    public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(TAG, "Call Worker: Succeeded!!");
                        } else {
                            if (response.errorBody() != null) {
                                Log.d(TAG, response.errorBody().toString());
                            }
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//
//                            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(MainScreenActivity.this);
//                            dialog.setTitle("Yêu cầu gọi thợ")
//                                    .setMessage("Gửi yêu cầu gọi thợ xảy ra lỗi. Xin hãy thử lại")
//                                    .setNegativeButton("Thử lại", v -> {
//                                        sendCallWorkerRequestToWorkers();
//                                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                                    })
//                                    .setNegativeButton("Huỷ", v -> {
//                                        dialog.dismiss();
//                                    })
//                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<FCMResponse> call, Throwable t) {
                        Log.d(TAG, t.getMessage());
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(MainScreenActivity.this);
                        dialog.setTitle("Yêu cầu gọi thợ")
                                .setMessage("Gửi yêu cầu gọi thợ xảy ra lỗi. Chúng tôi sẽ cố gắng sửa lỗi sớm nhất!")
                                .setNegativeButton("Đã hiểu", v -> {
                                    dialog.dismiss();
                                })
                                .show();
                    }
                });
    }


    private void createOrUpdateFcmToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Token tokenObj = new Token(task.getResult());
                        FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_TOKEN)
                                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                                .set(tokenObj)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "updated new token");
                                })
                                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

                    } else {
                        Log.d(TAG, "create or update token cause errors!");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}