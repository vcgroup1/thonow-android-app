package dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceTypeBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice.SelectServiceActivity;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.ServiceTypeAdapter;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

// Gọi từ HomeFragment
public class SelectServiceTypeActivity extends AppCompatActivity implements ServiceTypeAdapter.OnServiceTypeListener {
    private ActivitySelectServiceTypeBinding binding;
    private ServiceTypeAdapter adapter;
    private Service selectedService;
    private ServiceType selectedServiceType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_service_type);
        binding.setLifecycleOwner(this);

        if (getIntent().getExtras() != null) {
            selectedService = Parcels.unwrap(getIntent().getParcelableExtra("selectedService"));
            selectedServiceType = Parcels.unwrap(getIntent().getParcelableExtra("selectedServiceType"));
        }
        
        setupToolbar();
        setupServiceTypeRv();
        setupSearchView();
    }

    private void setupServiceTypeRv() {
        adapter = new ServiceTypeAdapter(this, ServiceTypeAdapter.ServiceTypeEnum.CARD_VIEW, selectedServiceType != null ?  selectedServiceType.getId() : null);
        binding.rvServiceType.setAdapter(adapter);
        binding.rvServiceType.addItemDecoration(new SpacesItemDecoration(20));
    }

    private void setupSearchView() {
        binding.svServiceType.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Dịch vụ");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceTypeClickedListener(int position) {
        selectedServiceType = adapter.getItem(position);
        Intent intent = new Intent(this, SelectServiceActivity.class);
        intent.putExtra("selectedServiceType", Parcels.wrap(selectedServiceType));
        intent.putExtra("selectedService", Parcels.wrap(selectedService));
        startActivity(intent);
    }
}