package dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.Collections;
import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageMediaBinding;
import dev.vcgroup.thonowandroidapp.util.VCFileUploader;
import lombok.Getter;

import static dev.vcgroup.thonowandroidapp.constant.StorageConst.FOLDER_CONSERVATION_PHOTO;

@Getter
public class ImageMessageViewHolder extends RecyclerView.ViewHolder {
    private ItemChatMessageMediaBinding binding;
    private OnMessageClickListener onMessageClickListener;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    private View.OnClickListener onImageMessageClickListener = v -> {
        if (onMessageClickListener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onMessageClickListener.onMessageItemClickListener(position);
            }
        }
    };

    private View.OnLongClickListener onImageMessageLongClickListener = v -> {
        if (onMessageClickListener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onMessageClickListener.onMessageItemLongClickListener(v, position);
                return true;
            }
        }
        return false;
    };

    public ImageMessageViewHolder(@NonNull ItemChatMessageMediaBinding binding, OnMessageClickListener onMessageClickListener) {
        super(binding.getRoot());
        this.onMessageClickListener = onMessageClickListener;

        this.binding = binding;
        this.binding.itemChatLeftMedia.mediaMessageItemImageview.setOnClickListener(onImageMessageClickListener);
        this.binding.itemChatRightMedia.mediaMessageItemImageview.setOnClickListener(onImageMessageClickListener);
        this.binding.itemChatLeftMedia.mediaMessageItemImageview.setOnLongClickListener(onImageMessageLongClickListener);
        this.binding.itemChatRightMedia.mediaMessageItemImageview.setOnLongClickListener(onImageMessageLongClickListener);
    }

    public void bindMessage(Message message, boolean fromLeft, boolean isBreakTimeStamp) {
        RoundedImageView ivContent;
        LinearProgressIndicator progressIndicator;
        if (fromLeft) {
            this.binding.itemChatLeftMedia.getRoot().setVisibility(View.VISIBLE);
            this.binding.itemChatLeftMedia.breakTimestamp.setVisibility(isBreakTimeStamp ? View.VISIBLE : View.GONE);
            ivContent = this.binding.itemChatLeftMedia.mediaMessageItemImageview;
            progressIndicator = this.binding.itemChatLeftMedia.mediaImageProgress;
        } else {
            this.binding.itemChatRightMedia.getRoot().setVisibility(View.VISIBLE);
            this.binding.itemChatRightMedia.breakTimestamp.setVisibility(isBreakTimeStamp ? View.VISIBLE : View.GONE);
            ivContent = this.binding.itemChatRightMedia.mediaMessageItemImageview;
            progressIndicator = this.binding.itemChatRightMedia.mediaImageProgress;
        }

        String imageUrl = message.getContent();
        Context context = this.binding.getRoot().getContext();
        if (!imageUrl.contains(context.getString(R.string.google_storage_bucket))) {
            progressIndicator.setVisibility(View.VISIBLE);
            ivContent.setAlpha(.3f);

            VCFileUploader fileUploader = new VCFileUploader(context);
            fileUploader.uploadFiles(FOLDER_CONSERVATION_PHOTO, Collections.singletonList(Uri.parse(imageUrl)), new VCFileUploader.FileUploaderCallback() {
                @Override
                public void onError(Exception e) {
                    Log.d("upload image message", "::onError: " + e.getMessage());
                }

                @Override
                public void onFinish(List<Uri> responses) {
                    progressIndicator.setVisibility(View.GONE);
                    ivContent.setAlpha(1f);

                    if (responses != null && !responses.isEmpty()) {
                        String uploadedUrl = responses.get(0).toString();
                        db.collection(CollectionConst.COLLECTION_MESSAGE)
                                .document(message.getId())
                                .update("content", uploadedUrl)
                                .addOnSuccessListener(aVoid -> {
                                    message.setContent(uploadedUrl);
                                    binding.setMessage(message);
                                    binding.executePendingBindings();
                                })
                                .addOnFailureListener(e -> {
                                    Log.d("updImageChat", e.getMessage());
                                });
                    }
                }

                @Override
                public void onProgressUpdate(long currentpercent, long totalpercent, long filenumber) {
                    progressIndicator.setProgress((int) currentpercent);
                }
            });
        } else {
            Log.d("load_mess", message.toString() + " vô đây");
            binding.setMessage(message);
            binding.executePendingBindings();
        }
    }

}
