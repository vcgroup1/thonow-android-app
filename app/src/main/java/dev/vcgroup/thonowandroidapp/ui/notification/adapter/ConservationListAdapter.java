package dev.vcgroup.thonowandroidapp.ui.notification.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowandroidapp.databinding.ItemConservationBinding;

public class ConservationListAdapter extends RecyclerView.Adapter<ConservationListAdapter.ConservationViewHolder> {
	private List<Message> conservationList;
	private OnConservationItemListener onConservationItemListener;
	private AsyncListDiffer<Message> mDiffer;
	private DiffUtil.ItemCallback<Message> mCallback = new DiffUtil.ItemCallback<Message>() {
		@Override
		public boolean areItemsTheSame(@NonNull @NotNull Message oldItem, @NonNull @NotNull Message newItem) {
			return oldItem.equals(newItem);
		}

		@Override
		public boolean areContentsTheSame(@NonNull @NotNull Message oldItem, @NonNull @NotNull Message newItem) {
			return oldItem.getContent().equals(newItem.getContent());
		}
	};

	public ConservationListAdapter(List<Message> conservationList, OnConservationItemListener onConservationItemListener) {
		this.conservationList = conservationList;
		this.onConservationItemListener = onConservationItemListener;
		this.mDiffer = new AsyncListDiffer<>(this, mCallback);
	}

	@NonNull
	@NotNull
	@Override
	public ConservationViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
		return new ConservationViewHolder(ItemConservationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), onConservationItemListener);
	}

	@Override
	public void onBindViewHolder(@NonNull @NotNull ConservationListAdapter.ConservationViewHolder holder, int position) {
		Message message = getItem(position);
		holder.bindItem(message);
		message.getOrder().get().addOnSuccessListener(documentSnapshot -> {
			Objects.requireNonNull(documentSnapshot.toObject(Order.class)).getWorker().get()
					.addOnSuccessListener(documentSnapshot1 -> {
						holder.bindWorker(documentSnapshot1.toObject(Worker.class));
					});
		});
	}

	@Override
	public int getItemCount() {
		return mDiffer.getCurrentList().size();
	}

	public void update(List<Message> newConservationList) {
		mDiffer.submitList(newConservationList);
	}

	public Message getItem(int position) {
		return mDiffer.getCurrentList().get(position);
	}

	public void removeItem(int position) {
		conservationList.remove(position);
		update(conservationList);
		notifyItemRemoved(position);
	}

	public interface OnConservationItemListener {
		void onConservationItemClick(int position);

		void onConservationItemLongClick(int position);
	}

	public static class ConservationViewHolder extends RecyclerView.ViewHolder {
		private ItemConservationBinding binding;
		private OnConservationItemListener onConservationItemListener;

		public ConservationViewHolder(@NonNull @NotNull ItemConservationBinding binding, OnConservationItemListener onConservationItemListener) {
			super(binding.getRoot());
			this.binding = binding;
			this.onConservationItemListener = onConservationItemListener;

			this.binding.getRoot().setOnClickListener(v -> {
				if (onConservationItemListener != null) {
					int position = getAdapterPosition();
					if (position != RecyclerView.NO_POSITION) {
						this.onConservationItemListener.onConservationItemClick(position);
					}
				}
			});

		}

		public void bindItem(Message message) {
			binding.setMessage(message);
			binding.executePendingBindings();
		}

		public void bindWorker(Worker worker) {
			binding.setWorker(worker);
			binding.executePendingBindings();
		}

	}
}