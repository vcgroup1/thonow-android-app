package dev.vcgroup.thonowandroidapp.ui.audiocall.outgoingcall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;

import org.parceler.Parcels;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import dev.vcgroup.thonowandroidapp.BuildConfig;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOutgoingCallBinding;
import dev.vcgroup.thonowandroidapp.ui.audiocall.BaseSinchActivity;
import dev.vcgroup.thonowandroidapp.util.AudioPlayer;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;

public class OutgoingCallActivity extends BaseSinchActivity {
    private static final String TAG = OutgoingCallActivity.class.getSimpleName();
    private ActivityOutgoingCallBinding binding;
    private Worker recipient;

    // Sinch
    private Call call;
    private SinchClient sinchClient;
    private long count = 0;

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;
    private long mCallStart = 0;
    private String mCallId;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            OutgoingCallActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_outgoing_call);

//        if (getIntent().getExtras() != null) {
//            recipient = Parcels.unwrap(getIntent().getParcelableExtra("worker"));
//            if (recipient != null) {
//                binding.setWorker(recipient);
//            }
//        }

        mAudioPlayer = new AudioPlayer(this);

        //setupSinchClient();

        binding.ibnFinish.setOnClickListener(v -> {
            endCall();
        });
        mCallStart = System.currentTimeMillis();
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
    }

    @Override
    protected void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            displayRemoteUserInfo(call.getRemoteUserId());
            call.addCallListener(new SinchCallListener());
            //call.addCallListener(new SinchCallLis);
            binding.tvCallState.setText(call.getState().toString());
        } else {
            Log.d(TAG, "Started with invalid callId, aborting.");
            finish();
        }
    }

    private void displayRemoteUserInfo(String remoteUserId) {
        Log.d(TAG, remoteUserId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private void setupSinchClient() {
        sinchClient = Sinch.getSinchClientBuilder()
                .context(this)
                .userId(CommonUtil.currentUserUid)
                .applicationKey(BuildConfig.SINCH_APP_KEY)
                .applicationSecret(BuildConfig.SINCH_APP_SECRET)
                .environmentHost(BuildConfig.SINCH_HOST_NAME)
                .build();

        sinchClient.setSupportCalling(true);
        sinchClient.startListeningOnActiveConnection();
        sinchClient.start();

        call = sinchClient.getCallClient().callUser(recipient.getId());

        call.addCallListener(new CallListener() {
            @Override
            public void onCallProgressing(Call progressingCall) {
                binding.tvCallState.setVisibility(View.GONE);
                binding.tvTime.setVisibility(View.VISIBLE);
                //progressingCall.getDetails().getDuration()
                Timer timer = new Timer();

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(() -> {
                            binding.tvTime.setText(CommonUtil.formatToDigitalClock(count));
                            count++;
                        });
                    }
                }, 1000, 1000);
            }

            @Override
            public void onCallEstablished(Call establishedCall) {
                binding.tvCallState.setText("Đã kết nối");
                setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            }

            @Override
            public void onCallEnded(Call endedCall) {
                // set Kết thúc
                call = null;
                SinchError sinchError = endedCall.getDetails().getError();
                if (sinchError != null) {
                    Log.d(TAG, sinchError.getMessage());
                    Toast.makeText(OutgoingCallActivity.this, sinchError.getMessage(), Toast.LENGTH_SHORT).show();
                }
                binding.tvTime.setVisibility(View.GONE);
                binding.tvCallState.setText("Kết thúc");
                binding.tvCallState.setVisibility(View.VISIBLE);
                setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);

                // finish activity này
                new Handler().postDelayed(OutgoingCallActivity.this::finish, 500);
            }

            @Override
            public void onShouldSendPushNotification(Call call, List<PushPair> list) {

            }
        });
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            binding.tvTime.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    private class SinchCallListener implements CallListener {
        @Override
        public void onCallProgressing(Call progressingCall) {
//            binding.tvCallState.setVisibility(View.GONE);
//            binding.tvTime.setVisibility(View.VISIBLE);
//            //progressingCall.getDetails().getDuration()
//            Timer timer = new Timer();
//
//            timer.scheduleAtFixedRate(new TimerTask() {
//                @Override
//                public void run() {
//                    runOnUiThread(() -> {
//                        binding.tvTime.setText(CommonUtil.formatToDigitalClock(count));
//                        count++;
//                    });
//                }
//            }, 1000, 1000);

            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
//            binding.tvCallState.setText("Đã kết nối");
//            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            Log.d(TAG, "Call established");
            mAudioPlayer.stopProgressTone();
            binding.tvCallState.setText(establishedCall.getState().toString());
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            mCallStart = System.currentTimeMillis();
        }

        @Override
        public void onCallEnded(Call endedCall) {
            // set Kết thúc
//            call = null;
//            SinchError sinchError = endedCall.getDetails().getError();
//            if (sinchError != null) {
//                Log.d(TAG, sinchError.getMessage());
//                Toast.makeText(OutgoingCallActivity.this, sinchError.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//            binding.tvTime.setVisibility(View.GONE);
//            binding.tvCallState.setText("Kết thúc");
//            binding.tvCallState.setVisibility(View.VISIBLE);
//            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
//
//            // finish activity này
//            new Handler().postDelayed(OutgoingCallActivity.this::finish, 500);
            CallEndCause callEndCause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + callEndCause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);

            String endMsg = "Call ended: " + call.getDetails().toString();
            Toast.makeText(OutgoingCallActivity.this, endMsg, Toast.LENGTH_LONG).show();
            endCall();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> list) {
            // Send a push through your push provider here, e.g. GCM
        }
    }
}