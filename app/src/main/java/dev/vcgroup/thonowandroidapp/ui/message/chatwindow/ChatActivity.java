package dev.vcgroup.thonowandroidapp.ui.message.chatwindow;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sj.emoji.EmojiBean;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.constant.VCKeyboardConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowandroidapp.data.model.chat.MessageType;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMNotification;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSingleSendData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.GenericFcmData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.data.remote.RetrofitClientInstance;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowandroidapp.databinding.ActivityChatBinding;
import dev.vcgroup.thonowandroidapp.databinding.DialogSampleMessageListBinding;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder.OnMessageClickListener;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.keyboard.VCEmoticonsKeyBoard;
import dev.vcgroup.thonowandroidapp.util.keyboard.VCKeyboardUtils;
import gun0912.tedbottompicker.TedBottomPicker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.EmoticonsEditText;
import sj.keyboard.widget.FuncLayout;

public class ChatActivity extends AppCompatActivity implements
        VCEmoticonsKeyBoard.OnChatKeyboardListener,
        OnMessageClickListener,
        FuncLayout.OnFuncKeyBoardListener {

    private static final String TAG = ChatActivity.class.getSimpleName();
    private ActivityChatBinding binding;
    private static FirebaseFirestore db;
    private static IFCMService ifcmService;
    static {
        db = FirebaseFirestore.getInstance();
        ifcmService = RetrofitClientInstance.getInstance().getRetrofit().create(IFCMService.class);
    }
    private DocumentReference orderDocRef, workerDocRef;
    private VCEmoticonsKeyBoard chatKeyBoard;
    private EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
        @Override
        public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
            if (isDelBtn) {
                int action = KeyEvent.ACTION_DOWN;
                int code = KeyEvent.KEYCODE_DEL;
                KeyEvent event = new KeyEvent(action, code);
                chatKeyBoard.getEtChat().onKeyDown(KeyEvent.KEYCODE_DEL, event);
            } else {
                if (o == null) {
                    return;
                }

                if (actionType == VCKeyboardConst.EMOTICON_CLICK_BIGIMAGE) {
                    //onSendSticker(((EmoticonEntity) o).getIconUri());
                } else {
                    String content = null;
                    if (o instanceof EmojiBean) {
                        content = ((EmojiBean) o).emoji;
                    }

                    if (TextUtils.isEmpty(content)) {
                        return;
                    }

                    int index = chatKeyBoard.getEtChat().getSelectionStart();
                    Editable editable = chatKeyBoard.getEtChat().getText();
                    editable.insert(index, content);
                }
            }
        }
    };
    private MessageAdapter messageAdapter;
    private ListenerRegistration chatListenerRegistration;
    private ArrayList<Message> messages;
    private RecyclerView rvChatList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        setupToolbar();

        if (getIntent().getExtras() != null) {
            String orderId = getIntent().getStringExtra("orderId");
            if (orderId != null && !orderId.isEmpty()) {
                orderDocRef = db.collection(CollectionConst.COLLECTION_ORDER).document(orderId);
            }

            String workerId = getIntent().getStringExtra("workerId");
            if (workerId != null && !workerId.isEmpty()) {
                workerDocRef = db.collection(CollectionConst.COLLECTION_WORKER).document(workerId);
                getWorker();
            } else {
                orderDocRef.get().addOnSuccessListener(documentSnapshot -> {
                    Order order = documentSnapshot.toObject(Order.class);
                    if (order != null) {
                        workerDocRef = order.getWorker();
                    }
                    getWorker();
                });
            }
        }

        setupKeyboard();
        setupChatList();
    }

    private void getWorker() {
        workerDocRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    Worker worker = documentSnapshot.toObject(Worker.class);
                    if (worker != null) {
                        binding.setWorker(worker);
                    }
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void setupChatList() {
        messages = new ArrayList<>();
        rvChatList = binding.rvChatList;
        messageAdapter = new MessageAdapter(this, messages, this);
        rvChatList.setAdapter(messageAdapter);
        rvChatList.setHasFixedSize(true);
        rvChatList.setItemAnimator(null);
        rvChatList.setItemViewCacheSize(50);
        if (orderDocRef != null) {
            listenForChatMessages();
        }
    }

    private void listenForChatMessages() {
        chatListenerRegistration = db.collection(CollectionConst.COLLECTION_MESSAGE)
                .whereEqualTo("order", orderDocRef)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed.", error);
                        return;
                    }

                    if (value != null) {
                        messages.clear();
                        for (DocumentSnapshot docSnap : value.getDocuments()) {
                            Message messageResponse = docSnap.toObject(Message.class);
                            if (messageResponse != null) {
                                messageResponse.setId(docSnap.getId());

                                switch (messageResponse.getType()) {
                                    case TEXT:
                                        messages.add(messageResponse);
                                        break;
                                    case IMAGE:
                                        if (messageResponse.getFrom().equals(CommonUtil.getCurrentUserRef(db))) {
                                            messages.add(messageResponse);
                                        } else {
                                            if (messageResponse.getContent().contains(getResources().getString(R.string.google_storage_bucket))) {
                                                messages.add(messageResponse);
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        messages.sort(Comparator.comparing(Message::getTimestamp));
                        messageAdapter.submitList(messages);
                        scrollToBottom();
                    }
                });
    }

    private void scrollToBottom() {
        rvChatList.requestLayout();
        rvChatList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int count = rvChatList.getAdapter().getItemCount();
                if (count > 0) {
                    rvChatList.smoothScrollToPosition(count - 1);
                }
            }
        });
    }

    private void setupKeyboard() {
        chatKeyBoard = binding.chatKeyboard;
        VCKeyboardUtils.initEmotionsEditText(chatKeyBoard.getEtChat());
        chatKeyBoard.setAdapter(VCKeyboardUtils.getCommonAdapter(this, emoticonClickListener));
        chatKeyBoard.setOnChatKeyboardListener(this);
        chatKeyBoard.addOnFuncKeyBoardListener(this);

        chatKeyBoard.getEtChat().setOnSizeChangedListener(new EmoticonsEditText.OnSizeChangedListener() {
            @Override
            public void onSizeChanged(int w, int h, int oldw, int oldh) {

            }
        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (EmoticonsKeyboardUtils.isFullScreen(this)) {
            boolean isConsum = chatKeyBoard.dispatchKeyEventInFullScreen(event);
            return isConsum ? isConsum : super.dispatchKeyEvent(event);
        }
        return super.dispatchKeyEvent(event);
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.menu_call) {
            return  true;
        }
        return false;
    }

    @Override
    public void onMessageItemLongClickListener(View view, int position) {

    }

    @Override
    public void onTrySendMessageAgain(int position) {

    }

    @Override
    public void onMessageItemClickListener(int position) {

    }

    @Override
    public void onChatImageClickListener(View view) {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showImagePicker();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(ChatActivity.this, "Cấp quyền bị từ chối\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối, bạn sẽ có thể không thể sử dụng dịch vụ này\n\nXin hãy cấp quyền tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    private void showImagePicker() {
        chatKeyBoard.reset();
        TedBottomPicker.with(this)
                .setTitle("Chọn ảnh")
                .setPeekHeight(1500)
                .setCompleteButtonText("Xong")
                .setEmptySelectionText("Chưa chọn ảnh")
                .show(uri -> {
                    doSendMessage(uri.toString(), MessageType.IMAGE);
                });
    }

    @Override
    public void onSendButtonClickListener(View view) {
        String message = chatKeyBoard.getEtChat().getText().toString().trim();

        if (!TextUtils.isEmpty(message)) {
            doSendMessage(message, MessageType.TEXT);
        }
    }

    private void doSendMessage(String message, MessageType messageType) {
        Message sendMessageRequest = Message.createNewMessage(db, message, orderDocRef, messageType);
        db.collection(CollectionConst.COLLECTION_MESSAGE)
                .add(sendMessageRequest)
                .addOnSuccessListener(documentReference -> {
                    scrollToBottom();
                    chatKeyBoard.getEtChat().setText("");
                    saveLastMessage(sendMessageRequest);
                })
                .addOnFailureListener(e -> Log.d(TAG, "sendMessage::onFailure: " + e.getMessage()));
    }

    private void saveLastMessage(Message sendMessageRequest) {
        db.collection(CollectionConst.COLLECTION_LAST_MESSAGE)
                .document(orderDocRef.getId())
                .set(sendMessageRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "updateLastMessage::onSuccess");
                    sendNewMessageNotification(sendMessageRequest);
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "updateLastMessage::onFailure: " + e.getMessage());
                });
    }

    private void sendNewMessageNotification(Message message) {
        String content = message.getType() == MessageType.IMAGE ? "[Hình ảnh]" :
                message.getContent();
        Worker worker = binding.getWorker();
        if (worker != null) {
            db.collection(CollectionConst.COLLECTION_TOKEN)
                    .document(worker.getId())
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        String token = Objects.requireNonNull(Objects.requireNonNull(documentSnapshot.toObject(Token.class))).getToken();

                        if (!token.isEmpty()) {
                            FCMSingleSendData<String> data = new FCMSingleSendData<>(
                                    new GenericFcmData<>("chat_notification", "notification"),
                                    token,
                                    new FCMNotification(worker.getDisplayName(), content,
                                            worker.getPhotoUrl())
                            );


                            ifcmService.sendFcmNotification(data)
                                    .enqueue(new Callback<FCMResponse>() {
                                        @Override
                                        public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                            if (response.isSuccessful()) {
                                                Log.d(TAG, "Send New Message Noti: Succeeded!!");
                                            } else {
                                                if (response.errorBody() != null) {
                                                    Log.d(TAG, response.errorBody().toString());
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<FCMResponse> call, Throwable t) {
                                            Log.d(TAG, t.getMessage());
                                        }
                                    });
                        }
                    });
        }
    }

    @Override
    public void onSampleMessageClickListener(View view) {
        DialogSampleMessageListBinding sampleMessageListBinding = DialogSampleMessageListBinding.inflate(LayoutInflater.from(this), (ViewGroup) this.getWindow().getDecorView(), false);
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setView(sampleMessageListBinding.getRoot());

        AlertDialog dialog = builder.create();
        dialog.show();

        sampleMessageListBinding.lvSampleMessage.setOnItemClickListener((parent, view1, position, id) -> {
            String message = (String) sampleMessageListBinding.lvSampleMessage.getItemAtPosition(position);
            doSendMessage(message, MessageType.TEXT);
            dialog.dismiss();
            chatKeyBoard.reset();
        });

        sampleMessageListBinding.btnCancel.setOnClickListener(v -> {
            dialog.dismiss();
            chatKeyBoard.reset();
        });

        dialog.setOnDismissListener(dialog1 -> chatKeyBoard.reset());
        dialog.setOnCancelListener(dialog1 -> chatKeyBoard.reset());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_window, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onAudioChatClickListener(View view) {

    }

    @Override
    public void OnFuncPop(int i) {

    }

    @Override
    public void OnFuncClose() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        chatKeyBoard.reset();
    }
}