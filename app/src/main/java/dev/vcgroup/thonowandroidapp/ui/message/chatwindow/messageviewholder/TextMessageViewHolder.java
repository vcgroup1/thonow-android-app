package dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import dev.vcgroup.thonowandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftTextBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightTextBinding;
import lombok.Getter;

@Getter
public class TextMessageViewHolder extends RecyclerView.ViewHolder {
    private ItemChatMessageLeftTextBinding leftTextBinding;
    private ItemChatMessageRightTextBinding rightTextBinding;
    private OnMessageClickListener onMessageClickListener;

    public TextMessageViewHolder(@NonNull ViewDataBinding binding, OnMessageClickListener onChatItemListener) {
        super(binding.getRoot());
        this.onMessageClickListener = onChatItemListener;

        if (binding instanceof ItemChatMessageLeftTextBinding) {
            this.leftTextBinding = (ItemChatMessageLeftTextBinding) binding;
        } else {
            this.rightTextBinding = (ItemChatMessageRightTextBinding) binding;
        }

        binding.getRoot().setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(view, position);
                    return true;
                }
            }
            return false;
        });

        binding.getRoot().setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });
    }

    public void bindMessage(Message message, boolean fromLeft, boolean isBreakTimeStamp) {
        if (fromLeft) {
            this.leftTextBinding.setMessage(message);
            this.leftTextBinding.breakTimestamp.setVisibility(isBreakTimeStamp ? View.VISIBLE : View.GONE);
            this.leftTextBinding.executePendingBindings();
        } else {
            this.rightTextBinding.setMessage(message);
            this.rightTextBinding.breakTimestamp.setVisibility(isBreakTimeStamp ? View.VISIBLE : View.GONE);
            this.rightTextBinding.executePendingBindings();
        }
    }
}
