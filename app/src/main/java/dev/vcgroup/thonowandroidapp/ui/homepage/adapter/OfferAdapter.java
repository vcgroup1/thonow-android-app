package dev.vcgroup.thonowandroidapp.ui.homepage.adapter;

import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.News;
import dev.vcgroup.thonowandroidapp.data.model.Offer;
import dev.vcgroup.thonowandroidapp.databinding.ItemOfferBinding;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.NewsOfferItemViewHolder> {
    private static final String TAG = OfferAdapter.class.getSimpleName();
    private List<Offer> list;
    private OnOfferListener onOfferListener;
    private FirebaseFirestore db;
    private AsyncListDiffer<Offer> mDiffer;
    private DiffUtil.ItemCallback<Offer> diffCallback = new DiffUtil.ItemCallback<Offer>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull Offer oldItem, @NonNull @NotNull Offer newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull Offer oldItem, @NonNull @NotNull Offer newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };

    public OfferAdapter(OnOfferListener onOfferListener) {
        this.list = new ArrayList<>();
        this.mDiffer = new AsyncListDiffer<>(this, diffCallback);
        this.onOfferListener = onOfferListener;
        this.db = FirebaseFirestore.getInstance();
        this.populateData();
    }

    private void populateData() {
        db.collection(CollectionConst.COLLECTION_OFFER)
                .addSnapshotListener((value, error) -> {
                    if(error != null) {
                        Log.d(TAG, "get offers cause error!");
                        return;
                    }

                    if(value != null) {
                        this.list = value.toObjects(Offer.class);
                        this.update(list);
                    }
                });
    }

    @NonNull
    @NotNull
    @Override
    public NewsOfferItemViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new NewsOfferItemViewHolder(ItemOfferBinding.inflate(inflater, parent, false), onOfferListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OfferAdapter.NewsOfferItemViewHolder holder, int position) {
        Offer item = list.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Offer getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<Offer> newList) {
        mDiffer.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    public class NewsOfferItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemOfferBinding binding;
        private OnOfferListener onOfferListener;

        public NewsOfferItemViewHolder(@NonNull @NotNull ItemOfferBinding binding, OnOfferListener onOfferListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onOfferListener = onOfferListener;
            binding.newsCardView.setOnClickListener(this);
            binding.btnOfferDetail.setOnClickListener(this);
        }

        public void bind(Offer item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onOfferListener != null) {
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION) {
                    onOfferListener.onOfferClickedListener(position);
                }
            }
        }
    }

    public interface OnOfferListener {
        void onOfferClickedListener(int position);
    }
}
