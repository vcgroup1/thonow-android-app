package dev.vcgroup.thonowandroidapp.ui.profile.transactionhistory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Transaction;
import dev.vcgroup.thonowandroidapp.databinding.ActivityTransactionHistoryBinding;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

public class TransactionHistoryActivity extends AppCompatActivity {
    private static final String TAG = TransactionHistoryActivity.class.getSimpleName();
    private ActivityTransactionHistoryBinding binding;
    private TransactionHistoryAdapter adapter;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_transaction_history);

        setupRvTransactionHistory();
    }

    private void setupRvTransactionHistory() {
        adapter = new TransactionHistoryAdapter();
        binding.rvTransactionHistory.setAdapter(adapter);
        binding.rvTransactionHistory.addItemDecoration(new SpacesItemDecoration(20));

        populateData();
    }

    private void populateData() {
        db.collection(CollectionConst.COLLECTION_CUSTOMER_TRANSACTION_HISTORY)
                .whereEqualTo("from", CommonUtil.getCurrentUserRef(db))
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed!", error);
                        return;
                    }

                    if (value != null) {
                        adapter.update(value.toObjects(Transaction.class));
                        checkEmpty();
                    }
                });
    }

    private void checkEmpty() {
        if (adapter.getItemCount() == 0) {
            binding.rvTransactionHistory.setVisibility(View.INVISIBLE);
            binding.emptySection.setVisibility(View.VISIBLE);
        } else {
            binding.rvTransactionHistory.setVisibility(View.VISIBLE);
            binding.emptySection.setVisibility(View.INVISIBLE);
        }
    }
}