package dev.vcgroup.thonowandroidapp.ui.homepage;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.databinding.FragmentHomePageBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.CallWorkerActivity;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.CallWorkerFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice.SelectServiceActivity;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.HomeSliderAdapter;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.NewsAdapter;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.OfferAdapter;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.ServiceTypeAdapter;
import dev.vcgroup.thonowandroidapp.ui.homepage.newsdetail.NewsDetailActivity;
import dev.vcgroup.thonowandroidapp.ui.homepage.offerdetail.OfferDetailActivity;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

public class HomePageFragment extends Fragment implements ServiceTypeAdapter.OnServiceTypeListener, OfferAdapter.OnOfferListener, NewsAdapter.OnNewsListener {
    private FragmentHomePageBinding binding;

    // Service Type Section
    private ServiceTypeAdapter serviceTypeAdapter;

    // Offer Section
    private OfferAdapter offerAdapter;

    // News Adapter
    private NewsAdapter newsAdapter;

    public static HomePageFragment newInstance() {
        return new HomePageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_page, container, false);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupHomeSlider();
        setupServiceTypeRecyclerview();
        setupOfferSection();
        setupNewsSection();
    }

    private void setupNewsSection() {
        newsAdapter = new NewsAdapter(this, NewsAdapter.NewsAdapterType.NEWS_ITEM);
        ViewPager2 vpNews = binding.newsSection.vpNews;
        vpNews.setAdapter(newsAdapter);
        vpNews.setClipToPadding(false);
        vpNews.setClipChildren(false);
        vpNews.setOffscreenPageLimit(3);
        vpNews.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

        binding.newsSection.newsIndicator.setViewPager2(vpNews);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(10));
        vpNews.setPageTransformer(compositePageTransformer);

        binding.newsSection.btnSeeDetail.setOnClickListener(v -> {
            binding.newsSection.vpNews.performClick();
        });
    }

    private void setupOfferSection() {
        offerAdapter = new OfferAdapter(this);
        ViewPager2 vpOffer = binding.offerSection.vpOffer;
        vpOffer.setAdapter(offerAdapter);
        vpOffer.setClipToPadding(false);
        vpOffer.setClipChildren(false);
        vpOffer.setOffscreenPageLimit(2);
        vpOffer.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

        binding.offerSection.offerIndicator.setViewPager2(vpOffer);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(20));
        vpOffer.setPageTransformer(compositePageTransformer);
    }

    private void setupServiceTypeRecyclerview() {
        serviceTypeAdapter = new ServiceTypeAdapter(this, ServiceTypeAdapter.ServiceTypeEnum.BASIC, null);
        binding.serviceTypeSection.rvService.setAdapter(serviceTypeAdapter);
        binding.serviceTypeSection.rvService.addItemDecoration(new SpacesItemDecoration(30));
    }

    private void setupHomeSlider() {
        HomeSliderAdapter homeSliderAdapter = new HomeSliderAdapter();
        ViewPager2 vpImageSlider = binding.imageSliderSection.vpImageSlide;
        vpImageSlider.setAdapter(homeSliderAdapter);
        vpImageSlider.setClipToPadding(false);
        vpImageSlider.setClipChildren(false);
        vpImageSlider.setOffscreenPageLimit(1);
        vpImageSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(50));
        vpImageSlider.setPageTransformer(compositePageTransformer);

        binding.imageSliderSection.sliderIndicator.setViewPager2(vpImageSlider);

        Handler sliderHandler = new Handler();
        Runnable sliderRunnable = () -> {
            int index = vpImageSlider.getCurrentItem();
            if (index == (homeSliderAdapter.getItemCount() - 1))
                vpImageSlider.setCurrentItem(0);
            else vpImageSlider.setCurrentItem(vpImageSlider.getCurrentItem() + 1);
        };

        vpImageSlider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 3000);
            }
        });
    }


    @Override
    public void onServiceTypeClickedListener(int position) {
        ServiceType serviceType = serviceTypeAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), SelectServiceActivity.class);
        intent.putExtra("selectedServiceType", Parcels.wrap(serviceType));
        startActivity(intent);
    }

    @Override
    public void onOfferClickedListener(int position) {
        Intent intent = new Intent(getActivity(), OfferDetailActivity.class);
        intent.putExtra("offer", Parcels.wrap(offerAdapter.getItem(position)));
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
    }

    @Override
    public void onNewsClickedListener(int position) {
        Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
        intent.putExtra("news", Parcels.wrap(newsAdapter.getItem(position)));
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
    }
}