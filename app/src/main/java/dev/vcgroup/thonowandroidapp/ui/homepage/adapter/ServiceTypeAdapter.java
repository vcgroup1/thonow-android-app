package dev.vcgroup.thonowandroidapp.ui.homepage.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeBinding;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeCardviewBinding;

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.ServiceTypeViewHolder> implements Filterable {
    public enum ServiceTypeEnum {
        BASIC,
        CARD_VIEW
    }

    private List<ServiceType> list;
    private List<ServiceType> filteredList;
    private OnServiceTypeListener onServiceTypeListener;
    private ServiceTypeEnum serviceTypeEnum;
    private int lastCheckedPosition = -1;
    private String selectedServiceTypeId;

    public ServiceTypeAdapter(OnServiceTypeListener onServiceTypeListener, @NonNull ServiceTypeEnum serviceTypeEnum, @Nullable String selectedServiceTypeId) {
        this.onServiceTypeListener = onServiceTypeListener;
        this.list = new ArrayList<>();
        this.selectedServiceTypeId = selectedServiceTypeId;
        this.populateData();
        this.serviceTypeEnum = serviceTypeEnum;
    }

    private void populateData() {
        list.add(new ServiceType("1", "Sửa chữa nước", "ic_tho_sua_chua_nuoc"));
        list.add(new ServiceType("2", "Sửa máy tính", "ic_tho_sua_may_tinh"));
        list.add(new ServiceType("3", "Sửa điện gia dụng", "ic_tho_sua_dien_gia_dung"));
        list.add(new ServiceType("4", "Người giúp việc", "ic_nguoi_giup_viec"));
        list.add(new ServiceType("5", "Sửa điện lạnh", "ic_tho_sua_dien_lanh"));
        list.add(new ServiceType("6", "Sửa thông hút cống", "ic_tho_thong_hut_cong"));
        this.filteredList = list;

        if (selectedServiceTypeId != null) {
            list.forEach(sv -> {
                if (sv.getId().equals(selectedServiceTypeId)) {
                    lastCheckedPosition = list.indexOf(sv);
                }
            });
        }
    }

    @NonNull
    @NotNull
    @Override
    public ServiceTypeViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (serviceTypeEnum.equals(ServiceTypeEnum.BASIC)) {
            return new ServiceTypeViewHolder(ItemServiceTypeBinding.inflate(inflater, parent, false), onServiceTypeListener);
        }
        return new ServiceTypeViewHolder(ItemServiceTypeCardviewBinding.inflate(inflater, parent, false), onServiceTypeListener);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    filterResults.values = filteredList;
                    filterResults.count = filteredList.size();
                } else {
                    List<ServiceType> resultList = new ArrayList<>();
                    if (resultList.isEmpty()) {
                        filteredList.forEach(serviceType -> {
                            if (serviceType.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                resultList.add(serviceType);
                            }
                        });
                    }
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (List<ServiceType>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ServiceTypeAdapter.ServiceTypeViewHolder holder, int position) {
        ServiceType serviceType = getItem(position);

        if (serviceTypeEnum.equals(ServiceTypeEnum.CARD_VIEW)) {
            if (lastCheckedPosition != -1) {
                holder.cardviewBinding.rbServiceTypeCheck.setChecked(position == lastCheckedPosition);
            }
            holder.bindCardView(serviceType);
        } else {
            holder.bind(serviceType);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ServiceType getItem(int position) {
        return list.get(position);
    }

    public class ServiceTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemServiceTypeBinding binding;
        private ItemServiceTypeCardviewBinding cardviewBinding;
        private OnServiceTypeListener onServiceTypeListener;

        public ServiceTypeViewHolder(@NonNull @NotNull ItemServiceTypeCardviewBinding binding, OnServiceTypeListener onServiceTypeListener) {
            super(binding.getRoot());
            this.cardviewBinding = binding;
            this.onServiceTypeListener = onServiceTypeListener;
            cardviewBinding.getRoot().setOnClickListener(this);
        }

        public ServiceTypeViewHolder(@NonNull @NotNull ItemServiceTypeBinding binding, OnServiceTypeListener onServiceTypeListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onServiceTypeListener = onServiceTypeListener;
            binding.getRoot().setOnClickListener(this);
        }


        public void bind(ServiceType item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }

        public void bindCardView(ServiceType item) {
            cardviewBinding.setItem(item);
            cardviewBinding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onServiceTypeListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onServiceTypeListener.onServiceTypeClickedListener(position);
                    if (cardviewBinding != null) {
                        cardviewBinding.rbServiceTypeCheck.setChecked(!cardviewBinding.rbServiceTypeCheck.isChecked());
                        int copyOfLastCheckedPosition = lastCheckedPosition;
                        lastCheckedPosition = getAdapterPosition();
                        notifyItemChanged(copyOfLastCheckedPosition);
                        notifyItemChanged(lastCheckedPosition);
                    }
                }
            }
        }
    }

    public interface OnServiceTypeListener {
        void onServiceTypeClickedListener(int position);
    }
}
