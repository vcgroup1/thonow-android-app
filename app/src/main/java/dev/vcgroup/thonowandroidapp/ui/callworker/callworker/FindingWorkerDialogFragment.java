package dev.vcgroup.thonowandroidapp.ui.callworker.callworker;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;

/**
 * Cơ chế tìm thợ:
 * - Nếu có pick thợ trước đó:
 * + Thì gửi thông báo đến các thợ đã chọn
 * - Nếu không pick trước ai cả:
 * + Trong bán kính 5km, get danh sách các nearby worker
 * + Thông báo đến các thợ trong phạm vi đó.
 */
public class FindingWorkerDialogFragment extends BottomSheetDialogFragment {
    private static final String TAG = FindingWorkerDialogFragment.class.getSimpleName();
    private CallWorkerViewModel mViewModel;
    private List<Worker> requestWorkerList = new ArrayList<>();
    private IFCMService IFCMService;
    private static FirebaseFirestore db;
    private BottomSheetBehavior bottomSheetBehavior;
    private View view;

    static {
        db = FirebaseFirestore.getInstance();
    }

    public FindingWorkerDialogFragment() {

    }

    public static FindingWorkerDialogFragment display(FragmentManager fragmentManager) {
        FindingWorkerDialogFragment dialog = new FindingWorkerDialogFragment();
        dialog.show(fragmentManager, TAG);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.ThoNOWTheme_FullScreenDialog);
        mViewModel = ViewModelProviders.of(this).get(CallWorkerViewModel.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.ThoNOWTheme_Slide);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mViewModel.getSelectedWorkers().getValue() != null
                && !mViewModel.getSelectedWorkers().getValue().isEmpty()) {
            requestWorkerList.addAll(mViewModel.getSelectedWorkers().getValue());
        } else {
            requestWorkerList.addAll(Objects.requireNonNull(mViewModel.getNearbyWorkers().getValue()));
        }

        if (!requestWorkerList.isEmpty()) {
            requestWorkerList.forEach(worker -> {
                sendRequestToWorker(worker.getId());
            });
        }
    }

    private void sendRequestToWorker(String workerId) {
        db.collection(CollectionConst.COLLECTION_TOKEN)
                .document(workerId)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Token token = documentSnapshot.toObject(Token.class);

                    if (token != null) {
                        // convert current_address --> json by Gson
                        String address_json = new Gson().toJson(mViewModel.getCurrentAddress().getValue());
                        Map<String, String> content = new HashMap<>();
                        content.put("ThoNOWApp", address_json);

                        FCMSendData data = new FCMSendData(token.getToken(), content);
                    }

                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_finding_worker, container, false);
        bottomSheetBehavior = BottomSheetBehavior.from(view);

        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull @NotNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    view.findViewById(R.id.minimize_finding_worker).setVisibility(View.VISIBLE);
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    view.findViewById(R.id.minimize_finding_worker).setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull @NotNull View bottomSheet, float slideOffset) {
                bottomSheet.animate().y(slideOffset <= 0 ?
                        bottomSheet.getY() + bottomSheetBehavior.getPeekHeight() - bottomSheet.getHeight() :
                        view.getHeight() - bottomSheet.getHeight()).setDuration(0).start();

            }
        });

        view.findViewById(R.id.btn_minimize_finding_worker).setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });
        return view;
    }
}
