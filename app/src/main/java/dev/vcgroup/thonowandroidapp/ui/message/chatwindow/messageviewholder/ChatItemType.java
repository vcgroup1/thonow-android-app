package dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder;

import android.os.Build;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ChatItemType {
    CHAT_ITEM_RIGHT_TEXT(1),
    CHAT_ITEM_RIGHT_IMAGE(2),
    CHAT_ITEM_LEFT_TEXT(3),
    CHAT_ITEM_LEFT_IMAGE(4);

    private int typeId;

    public static ChatItemType getChatItemType(final int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Stream.of(ChatItemType.values())
                    .filter(targetStatus -> targetStatus.getTypeId() == id).findFirst().get();
        } else {
            for (ChatItemType chatItemType : ChatItemType.values()) {
                if (chatItemType.getTypeId() == id)
                    return chatItemType;
            }
        }
        return null;
    }
}
