package dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.CallWorkerActivity;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.ServiceTypeAdapter;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;
import dev.vcgroup.thonowandroidapp.util.VCLoadingDialog;

public class SelectServiceActivity extends AppCompatActivity implements ServiceAdapter.OnServiceListener {
    private ActivitySelectServiceBinding binding;
    private ServiceAdapter adapter;
    private ServiceType selectedServiceType;
    private Service selectedSerivce;
    private VCLoadingDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_service);
        binding.setLifecycleOwner(this);

        if (getIntent() != null) {
            selectedServiceType = Parcels.unwrap(getIntent().getParcelableExtra("selectedServiceType"));
            selectedSerivce = Parcels.unwrap(getIntent().getParcelableExtra("selectedService"));
        }

        setupToolbar();
        setupServiceRecyclerView();
        setupServiceSearchView();
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Dịch vụ");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupServiceSearchView() {
        binding.svService.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private void setupServiceRecyclerView() {
        adapter = new ServiceAdapter(this, selectedServiceType, selectedSerivce != null ? selectedSerivce.getId() : null);
        binding.rvService.setAdapter(adapter);
        binding.rvService.addItemDecoration(new SpacesItemDecoration(15));
    }

    @Override
    public void onServiceClickedListener(int position) {
        Service service = adapter.getItem(position);
        if (service != null) {
            if (loading == null) {
                loading = VCLoadingDialog.create(this);
            }
            loading.show();
            Intent intent =  new Intent(this, CallWorkerActivity.class);
            intent.putExtra("selectedService", Parcels.wrap(service));
            loading.dismiss();
            startActivity(intent, ActivityOptions.makeCustomAnimation(this,
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        }
    }
}