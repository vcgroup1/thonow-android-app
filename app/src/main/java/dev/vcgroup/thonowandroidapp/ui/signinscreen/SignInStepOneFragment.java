package dev.vcgroup.thonowandroidapp.ui.signinscreen;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.kusu.loadingbutton.LoadingButton;


import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowandroidapp.databinding.BsdChooseCountryCodeBinding;
import dev.vcgroup.thonowandroidapp.databinding.FragmentSignInStepOneBinding;
import dev.vcgroup.thonowandroidapp.ui.signinscreen.adapters.CountryCodeAdapter;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import lombok.SneakyThrows;

public class SignInStepOneFragment extends Fragment implements CountryCodeAdapter.OnCountryCodeListener {
    private static final String REGISTER_STEP_TWO_TAG = SignInStepTwoFragment.class.getSimpleName();
    private static final String TAG = SignInStepOneFragment.class.getSimpleName();
    private SignInViewModel signInViewModel;
    private FragmentSignInStepOneBinding binding;
    private BottomSheetDialog changeCountryBsd;

    private CountryCodeAdapter countryCodeAdapter;
    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_step_one, container, false);
        signInViewModel = ViewModelProviders.of(requireActivity()).get(SignInViewModel.class);
        binding.setSignInViewModel(signInViewModel);
        binding.setLifecycleOwner(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFirebase();

        signInViewModel.setSelectedCountryCode(new CountryInfo("Vietnam", "VN", "84"));
        LoadingButton btnResendCode = binding.btnResendCode;

        btnResendCode.setOnClickListener(v -> {
            btnResendCode.showLoading();
            String phoneNumber = signInViewModel.getPhoneNumber().getValue();
            if (validateForm(phoneNumber)) {
                FragmentUtil.replaceFragment(getActivity(), R.id.register_fragment_container,
                        new SignInStepTwoFragment(), getArguments(), REGISTER_STEP_TWO_TAG, true);
            }
            btnResendCode.hideLoading();
        });

        binding.dropDownCountry.setOnClickListener(v -> {
            BsdChooseCountryCodeBinding chooseCountryCodeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.bsd_choose_country_code, (ViewGroup) v.getRootView(), false);
            changeCountryBsd = new BottomSheetDialog(requireActivity());
            changeCountryBsd.setContentView(chooseCountryCodeBinding.getRoot());

            countryCodeAdapter = new CountryCodeAdapter(db, this);
            chooseCountryCodeBinding.rvCountries.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
            chooseCountryCodeBinding.rvCountries.setItemAnimator(new DefaultItemAnimator());
            chooseCountryCodeBinding.rvCountries.setAdapter(countryCodeAdapter);
            changeCountryBsd.show();

            chooseCountryCodeBinding.svCountryCode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    countryCodeAdapter.getFilter().filter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    countryCodeAdapter.getFilter().filter(newText);
                    return true;
                }
            });
        });
    }

    private void initFirebase() {
        db = FirebaseFirestore.getInstance();
    }

    @SneakyThrows
    private boolean validateForm(String phoneNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            signInViewModel.errPhoneNumber.set("Vui lòng nhập số điện thoại");
        } else {
            if (phoneNumberUtil.isValidNumberForRegion(phoneNumberUtil.parse(phoneNumber, "VN"), "VN")) {
                return true;
            } else {
                signInViewModel.errPhoneNumber.set("Vui lòng nhập số điện thoại hợp lệ");
            }
        }
        return false;
    }

    @Override
    public void onCountryCodeClickedListener(int position) {
        signInViewModel.setSelectedCountryCode(countryCodeAdapter.getItem(position));
        new Handler().postDelayed(() -> {
            changeCountryBsd.cancel();
        }, 1000);
    }
}
