package dev.vcgroup.thonowandroidapp.ui.profile.jobhistory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.databinding.ActivityJobHistoryBinding;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter.TaskListAdapter;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

public class JobHistoryActivity extends AppCompatActivity implements TaskListAdapter.OnTaskListener {
    private static final String TAG = JobHistoryActivity.class.getSimpleName();
    private ActivityJobHistoryBinding binding;
    private TaskListAdapter adapter;
    private static FirebaseFirestore db;
    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_history);
        setupToolbar();
        setupRvJobHistory();
        setupSearchView();
    }

    private void setupSearchView() {
        binding.svJobHistory.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query, count -> onFilterListener(count));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText, count -> onFilterListener(count));
                return true;
            }
        });
    }

    private void onFilterListener(int count) {
        if (count == 0) {
            binding.noTaskSchedule.getRoot().setVisibility(View.VISIBLE);
            binding.rvJobHistory.setVisibility(View.GONE);
        } else {
            binding.noTaskSchedule.getRoot().setVisibility(View.GONE);
            binding.rvJobHistory.setVisibility(View.VISIBLE);
        }
    }

    private void setupRvJobHistory() {
        adapter = new TaskListAdapter(new ArrayList<>(), TaskListAdapter.TaskAdapterType.TASK_BY_CALENDAR_ITEM, this);
        binding.rvJobHistory.setAdapter(adapter);
        binding.rvJobHistory.addItemDecoration(new SpacesItemDecoration(10));
        populateData();
    }

    private void populateData() {
        DocumentReference customerDocRef = CommonUtil.getCurrentUserRef(db);
        db.collection(CollectionConst.COLLECTION_ORDER)
                .whereEqualTo("customer", customerDocRef)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.d(TAG, error.getMessage());
                        return;
                    }

                    if (value != null) {
                        Log.d(TAG, "value: " + value.size());
                        adapter.update(value.toObjects(Order.class));
                    }
                });
    }

    private void setupToolbar() {
        Toolbar toolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle(null);
        binding.topAppBar.tvTitleToolbar.setText("Lịch sử công việc");
        getSupportActionBar().show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskClickedListener(int position) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(this, TaskDetailActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        startActivity(intent);
    }

    @Override
    public void onTaskCancelClickedListener(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(this, CancelOrderActivity.class);
        intent.putExtra("deleteOrderId", Parcels.wrap(orderId));
        startActivity(intent);
    }

    @Override
    public void onChatClickedListener(int position) {

    }

    @Override
    public void onAudioCallClickedListener(int position) {

    }

    @Override
    public void onConfirmToCompleterOrder(int position) {

    }
}