package dev.vcgroup.thonowandroidapp.ui.taskmanagement.ratingworker;

import android.os.Bundle;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.Parcels;

import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Feedback;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityRatingWorkerBinding;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;

public class RatingWorkerActivity extends AppCompatActivity {
    private static final String TAG = RatingWorkerActivity.class.getSimpleName();
    private ActivityRatingWorkerBinding binding;
    private Worker worker;
    private String orderId;
    private static FirebaseFirestore db;

    static {
        db = FirebaseFirestore.getInstance();
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rating_worker);

        if (getIntent().getExtras() != null) {
            worker = Parcels.unwrap(getIntent().getParcelableExtra("worker"));
            orderId = getIntent().getStringExtra("orderId");

            if (worker != null) {
                binding.setWorker(worker);
            }
        }

        binding.ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
            dialog.setTitle("Xác nhận")
                    .setMessage("Bạn có chắc chắn đánh giá thợ này chứ?")
                    .setPositiveButton("Chắc chắn", v -> {
                        dialog.dismiss();
                        String review = CommonUtil.getInputText(binding.tipReview);
                        createOrUpdateFeedback(rating, review);
                    })
                    .setNegativeButton("Huỷ", v -> {
                        dialog.dismiss();
                    })
                    .show();
        });

        binding.btnAddFavoriteWorker.setOnClickListener(v -> {
            addWorkerToFavoriteList();
        });

        binding.btnIgnoreAddFavoriteWorker.setOnClickListener(v -> {
            finish();
        });
    }

    private void addWorkerToFavoriteList() {
        DocumentReference workerRef = db.collection(CollectionConst.COLLECTION_WORKER).document(worker.getId());
        DocumentReference currentUserDocRef = CommonUtil.getCurrentUserRef(db);
        currentUserDocRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    List<DocumentReference> favoriteWorkerList = documentSnapshot.toObject(User.class).getFavoriteWorkerList();

                    if (favoriteWorkerList != null) {
                        if (!favoriteWorkerList.contains(workerRef)) {
                            favoriteWorkerList.add(workerRef);
                        }
                    }
                    
                    currentUserDocRef.update(
                            "favoriteWorkerList", favoriteWorkerList
                    ).addOnSuccessListener(unused -> {
                        Toast.makeText(this, "Đã thêm vào danh sách thợ yêu thích của bạn", Toast.LENGTH_SHORT).show();
                        finish();
                    }).addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void createOrUpdateFeedback(float rating, String review) {
        DocumentReference orderRef = db.collection(CollectionConst.COLLECTION_ORDER)
                .document(orderId);

        orderRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Feedback oldFeedback = Objects.requireNonNull(documentSnapshot.toObject(Order.class)).getFeedback();

                        float oldRating = 0;
                        if (oldFeedback == null) {
                            oldFeedback = new Feedback();
                        } else {
                            oldRating = oldFeedback.getRating();
                        }

                        if (rating > 0) {
                            oldFeedback.setRating(rating);
                        }

                        if (review != null && !review.isEmpty()) {
                            oldFeedback.setReview(review);
                        }

                        float finalRating = oldFeedback.getRating();
                        float finalOldRating = oldRating;
                        orderRef.update("feedback", oldFeedback)
                                .addOnSuccessListener(unused -> {
                                    updateWorkerRating(finalRating, finalOldRating);
                                })
                                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                    }
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private void updateWorkerRating(float rating, float oldRating) {
        // get all feedback --> size
        // get average worker's rating

        DocumentReference workerRef = db.collection(CollectionConst.COLLECTION_WORKER).document(worker.getId());
        db.collection(CollectionConst.COLLECTION_ORDER)
                .whereEqualTo("worker", workerRef)
                .whereNotEqualTo("feedback", null)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    int size = queryDocumentSnapshots.size();
                    float currentRating = worker.getRating();
                    float newRating = calculateRating(size, currentRating, rating, oldRating);

                    workerRef.update("rating", newRating)
                            .addOnSuccessListener(unused -> {
                                Toast.makeText(RatingWorkerActivity.this, "Cảm ơn bạn đã đánh giá!", Toast.LENGTH_SHORT).show();
                            })
                            .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));

                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    private float calculateRating(int size, float currentRating, float rating, float oldRating) {
        return ((currentRating * size - oldRating) + rating) / (oldRating > 0 ? size : (size + 1));
    }
}

