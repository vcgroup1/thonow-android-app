package dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation;

import android.os.Bundle;
import android.text.style.CharacterStyle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koalap.geofirestore.GeoFire;
import com.koalap.geofirestore.GeoLocation;
import com.koalap.geofirestore.LocationCallback;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.Address;
import dev.vcgroup.thonowandroidapp.data.model.ApiError;
import dev.vcgroup.thonowandroidapp.data.remote.GeocodeApiService;
import dev.vcgroup.thonowandroidapp.data.remote.RetrofitClientInstance;
import dev.vcgroup.thonowandroidapp.databinding.FragmentDefineMyLocationBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.callworker.CallWorkerFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.definemylocation.picklocationinmap.PickLocationInMapFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;
import dev.vcgroup.thonowandroidapp.util.ApiErrorUtil;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefineMyLocationFragment extends Fragment implements PlaceAutocompleteAdapter.OnPlaceAutocompleteListener {
    private static final String TAG = DefineMyLocationFragment.class.getSimpleName();
    private FragmentDefineMyLocationBinding binding;
    private CallWorkerViewModel callWorkerViewModel;
    private BottomSheetDialog extraAddressInfoBsd;
    private PlaceAutocompleteAdapter adapter;
    private Address oldAddress;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_define_my_location, container, false);
        callWorkerViewModel = ViewModelProviders.of(requireActivity()).get(CallWorkerViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForClickListener();

        callWorkerViewModel.getCurrentAddress().observe(requireActivity(), new Observer<Address>() {
            @Override
            public void onChanged(Address address) {
                oldAddress = address;
                if (oldAddress != null) {
                    binding.tvUpdateLocation.setText(oldAddress.getAddress());
                }
            }
        });

        setupAutocompleteSearchPlaces();
    }

    private void setupAutocompleteSearchPlaces() {
        adapter = new PlaceAutocompleteAdapter(requireActivity(), this);
        binding.rvPlaceSearch.setAdapter(adapter);

        binding.svPlace.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearchPlace(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                doSearchPlace(newText);
                return true;
            }
        });
    }

    private void doSearchPlace(String queryText) {
        if (queryText.isEmpty()) {
            binding.btnAddExtraInfo.setVisibility(View.VISIBLE);
            binding.btnPickLocationInMap.setVisibility(View.VISIBLE);
            binding.rvPlaceSearch.setVisibility(View.GONE);
        } else {
            binding.btnAddExtraInfo.setVisibility(View.GONE);
            binding.btnPickLocationInMap.setVisibility(View.GONE);
            binding.rvPlaceSearch.setVisibility(View.VISIBLE);
            adapter.getFilter().filter(queryText);
        }
    }

    private void registerForClickListener() {
        binding.btnPickLocationInMap.setOnClickListener(v -> {
            FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
                    new PickLocationInMapFragment(), null, PickLocationInMapFragment.class.getSimpleName(), true);
        });

        binding.btnAddExtraInfo.setOnClickListener(v -> {
            showAddExtraInfoBsd();
        });
    }

    private void showAddExtraInfoBsd() {
        View addExtraInfoView = getLayoutInflater().inflate(R.layout.bsd_define_my_location_extra_info, null);
        extraAddressInfoBsd = new BottomSheetDialog(getContext());
        extraAddressInfoBsd.setContentView(addExtraInfoView);
        extraAddressInfoBsd.show();

        TextInputLayout tipExtraInfo = addExtraInfoView.findViewById(R.id.tip_extra_info);

        TextView tvCurrentAddress = addExtraInfoView.findViewById(R.id.tvCurrentAddress);

        if (oldAddress != null) {
            tvCurrentAddress.setText(oldAddress.getAddress());
        }

        addExtraInfoView.findViewById(R.id.extra_info_cancel).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });

        addExtraInfoView.findViewById(R.id.bsd_close).setOnClickListener(v -> {
            extraAddressInfoBsd.cancel();
        });

        addExtraInfoView.findViewById(R.id.extra_info_add).setOnClickListener(v -> {
            String extraInfoStr = CommonUtil.getInputText(tipExtraInfo);
            if (!extraInfoStr.trim().isEmpty()) {
                String currentAddress = extraInfoStr + " " + tvCurrentAddress.getText();
                Address newAddress = new Address(oldAddress.getLatitude(), oldAddress.getLongtitude(), currentAddress);
                callWorkerViewModel.setCurrentAddress(newAddress);
            }
            extraAddressInfoBsd.cancel();
            backToCallWorkerFragment();
        });
    }

    private void backToCallWorkerFragment() {
        callWorkerViewModel.setIsFromPickLocation(Boolean.TRUE);
        FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
                FragmentUtil.getFragmentByTag((AppCompatActivity) requireActivity(), CallWorkerFragment.class.getSimpleName()), null, CallWorkerFragment.class.getSimpleName(), true);
        FragmentUtil.removeFragmentByTag((AppCompatActivity) requireActivity(), DefineMyLocationFragment.class.getSimpleName());
    }

    @Override
    public void onPlaceAutocompleteClickedListener(int pos) {
        String address = adapter.getItem(pos).getFullText(null).toString();
        Address newAddress = new Address(oldAddress.getLatitude(), oldAddress.getLongtitude(), address);
        callWorkerViewModel.setCurrentAddress(newAddress);
        showAddExtraInfoBsd();
    }
}
