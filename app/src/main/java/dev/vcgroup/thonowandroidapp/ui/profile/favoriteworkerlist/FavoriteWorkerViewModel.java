package dev.vcgroup.thonowandroidapp.ui.profile.favoriteworkerlist;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.data.model.Worker;

public class FavoriteWorkerViewModel extends ViewModel {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private MutableLiveData<List<Worker>> favoriteWorkerList = new MutableLiveData<>();

    public MutableLiveData<List<Worker>> getFavoriteWorkerList() {
        return favoriteWorkerList;
    }

    public void setFavoriteWorkerList(List<Worker> favoriteWorkerList) {
        this.favoriteWorkerList.setValue(favoriteWorkerList);
    }
}
