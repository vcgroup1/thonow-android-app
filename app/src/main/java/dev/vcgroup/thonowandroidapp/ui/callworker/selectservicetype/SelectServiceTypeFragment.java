package dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceTypeBinding;
import dev.vcgroup.thonowandroidapp.ui.callworker.selectservicetype.selectservice.SelectServiceFragment;
import dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel;
import dev.vcgroup.thonowandroidapp.ui.homepage.adapter.ServiceTypeAdapter;
import dev.vcgroup.thonowandroidapp.util.FragmentUtil;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;

public class SelectServiceTypeFragment extends Fragment implements ServiceTypeAdapter.OnServiceTypeListener{
    private ActivitySelectServiceTypeBinding binding;
    private ServiceTypeAdapter adapter;
    private CallWorkerViewModel mViewModel;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_select_service_type, container, false);
        binding.setLifecycleOwner(getActivity());
        mViewModel = ViewModelProviders.of(requireActivity()).get(CallWorkerViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupServiceTypeRv();
        setupSearchView();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void setupServiceTypeRv() {
        adapter = new ServiceTypeAdapter(this, ServiceTypeAdapter.ServiceTypeEnum.CARD_VIEW, mViewModel.getSelectedServiceType() != null ?  Objects.requireNonNull(mViewModel.getSelectedServiceType().getValue()).getId() : null);
        binding.rvServiceType.setAdapter(adapter);
        binding.rvServiceType.addItemDecoration(new SpacesItemDecoration(20));
    }

    private void setupSearchView() {
        binding.svServiceType.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });

    }

    @Override
    public void onServiceTypeClickedListener(int position) {
        mViewModel.setSelectedServiceType(adapter.getItem(position));
        FragmentUtil.replaceFragment(getActivity(), R.id.call_worker_fragment_container,
                new SelectServiceFragment(), null, SelectServiceFragment.class.getSimpleName(), true);
    }
}
