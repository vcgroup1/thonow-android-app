package dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbycalendar.TaskByCalendarFragment;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbylist.TaskByListFragment;

public class TaskPagerAdapter extends FragmentStateAdapter {

    public TaskPagerAdapter(@NonNull @NotNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1: return new TaskByListFragment();
            case 0:
            default: return new TaskByCalendarFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
