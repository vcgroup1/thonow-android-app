package dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.location.Location;
import android.view.View;
import android.widget.RadioGroup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CallWorkerConst;
import dev.vcgroup.thonowandroidapp.data.model.Address;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.OrderType;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.data.model.User;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;

import static dev.vcgroup.thonowandroidapp.constant.CallWorkerConst.RADIUS_1;

public class CallWorkerViewModel extends ViewModel {
    private static final String TAG = CallWorkerViewModel.class.getSimpleName();
    private MutableLiveData<Address> currentAddress = new MutableLiveData<>();
    private MutableLiveData<Service> selectedService = new MutableLiveData<>();
    private MutableLiveData<ServiceType> selectedServiceType = new MutableLiveData<>();
    private MutableLiveData<Integer> jobQuatitiy = new MutableLiveData<>(1);
    private MutableLiveData<Worker> favoriteWorker = new MutableLiveData<>();
    private MutableLiveData<OrderType> currentOrderType = new MutableLiveData<>(OrderType.QUICK_CALL);
    private MutableLiveData<Calendar> startDate = new MutableLiveData<>();
    private MutableLiveData<Order> currentOrder = new MutableLiveData<>();
    private MutableLiveData<List<Worker>> selectedWorkers = new MutableLiveData<>(new ArrayList<>());
    private MutableLiveData<List<Worker>> nearbyWorkers = new MutableLiveData<>(new ArrayList<>());
    private MutableLiveData<Boolean> isFromPickLocation = new MutableLiveData<>(Boolean.FALSE);
    private MutableLiveData<User> customer = new MutableLiveData<>();
    private MutableLiveData<String> note = new MutableLiveData<>();
    private MutableLiveData<Double> radius = new MutableLiveData<>(RADIUS_1);
    private MutableLiveData<Location> currentLocation = new MutableLiveData<>();

    public MutableLiveData<Location> getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation.setValue(currentLocation);
    }

    public MutableLiveData<Double> getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius.setValue(radius);
    }

    public MutableLiveData<String> getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note.setValue(note);
    }

    public MutableLiveData<User> getCustomer() {
        if (customer.getValue() == null) {
            User user = User.convertFrom(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
            customer.setValue(user);
        }
        return customer;
    }

    public RadioGroup.OnCheckedChangeListener rgCheckedChangeListener = (group, checkedId) -> {
        if (checkedId == R.id.rb_quick_call) {
            setCurrentOrderType(OrderType.QUICK_CALL);
        } else {
            setCurrentOrderType(OrderType.MAKE_AN_APPOINTMENT);
        }
    };

    private DatePickerDialog startDatePicker;
    private static int year, month, day;
    private static Calendar currentCalendar = Calendar.getInstance();
    private Calendar pickedCalendar = Calendar.getInstance();

    static {
        year = currentCalendar.get(Calendar.YEAR);
        month = currentCalendar.get(Calendar.MONTH);
        day = currentCalendar.get(Calendar.DATE);
    }

    public View.OnClickListener onStartDateClickListener = v -> {
        if (startDatePicker == null) {
            startDatePicker = new DatePickerDialog(v.getContext());
            startDatePicker.getDatePicker().setMinDate(currentCalendar.getTimeInMillis());
            startDatePicker.updateDate(year, month,day);
            startDatePicker.setOnDateSetListener((view, yyyy, mm, dd) -> {
                year = yyyy;
                month = mm;
                day = dd;
                pickedCalendar.set(year, month, day);
                setStartDate(pickedCalendar);
            });
        }
        startDatePicker.updateDate(year, month, day);
        startDatePicker.show();
    };

    private TimePickerDialog startTimePicker;
    private static int hourOfDay, minute;
    static {
        hourOfDay = currentCalendar.get(Calendar.HOUR_OF_DAY);
        minute = currentCalendar.get(Calendar.MINUTE);
    }

    public View.OnClickListener onStartTimeClickListener = v -> {
        if (startTimePicker == null) {
            startTimePicker = new TimePickerDialog(v.getContext(), (view, hod, mm) -> {
                if (hod > currentCalendar.get(Calendar.HOUR_OF_DAY)) {
                    hourOfDay = hod;
                    minute = mm;
                } else {
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(v.getContext());
                    dialog
                            .setTitle("Thông báo")
                            .setMessage("Giờ hẹn phải sau ít nhất 1 tiếng để đảm bảo chất lượng.")
                            .setPositiveButton("Đã hiểu", v1 -> {
                                dialog.dismiss();
                            })
                            .show();
                    return;
                }
                pickedCalendar.set(year, month, day, hourOfDay, minute);
                setStartDate(pickedCalendar);
            }, hourOfDay, minute, true);
        }


        startTimePicker.updateTime(hourOfDay, minute);
        startTimePicker.show();
    };

    public MutableLiveData<Address> getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(Address currentAddress) {
        this.currentAddress.setValue(currentAddress);
    }

    public MutableLiveData<Service> getSelectedService() {
        return selectedService;
    }

    public void setSelectedService(Service selectedService) {
        this.selectedService.setValue(selectedService);
        selectedService.getServiceType().get().addOnSuccessListener(documentSnapshot -> {
            setSelectedServiceType(documentSnapshot.toObject(ServiceType.class));
        });
    }

    public MutableLiveData<Integer> getJobQuatitiy() {
        return jobQuatitiy;
    }

    public void setJobQuatitiy(Integer jobQuatitiy) {
        this.jobQuatitiy.setValue(jobQuatitiy);
    }

    public MutableLiveData<Worker> getFavoriteWorker() {
        return favoriteWorker;
    }

    public void setFavoriteWorker(Worker favoriteWorker) {
        this.favoriteWorker.setValue(favoriteWorker);
    }

    public MutableLiveData<OrderType> getCurrentOrderType() {
        return currentOrderType;
    }

    public void setCurrentOrderType(OrderType currentOrderType) {
        this.currentOrderType.setValue(currentOrderType);
    }

    public int getCurrentOrderTypeId() {
        if (getCurrentOrderType().getValue().equals(OrderType.QUICK_CALL)) {
            return R.id.rb_quick_call;
        }
        return R.id.rb_make_an_appointment;
    }

    public MutableLiveData<ServiceType> getSelectedServiceType() {
        return selectedServiceType;
    }

    public void setSelectedServiceType(ServiceType selectedServiceType) {
        this.selectedServiceType.setValue(selectedServiceType);
    }

    public MutableLiveData<Calendar> getStartDate() {
        if (startDate.getValue() == null) {
            setStartDate(Calendar.getInstance());
        }
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate.setValue(startDate);
    }

    public MutableLiveData<Order> getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder.setValue(currentOrder);
    }

    public MutableLiveData<List<Worker>> getSelectedWorkers() {
        return selectedWorkers;
    }

    public void setSelectedWorkers(List<Worker> selectedWorkers) {
        this.selectedWorkers.setValue(selectedWorkers);
    }

    public Boolean addOrRemoveSelectedWorker(Worker worker) {
        List<Worker> currentList = this.selectedWorkers.getValue();
        if (currentList != null && worker != null) {
            if (!selectedWorkers.getValue().contains(worker)) {
                currentList.add(worker);
                setSelectedWorkers(currentList);
                return true;
            } else {
                currentList.remove(worker);
                setSelectedWorkers(currentList);
                return false;
            }
        }
        return null;
    }

    public MutableLiveData<Boolean> getIsFromPickLocation() {
        return isFromPickLocation;
    }

    public void setIsFromPickLocation(Boolean isFromPickLocation) {
        this.isFromPickLocation.setValue(isFromPickLocation);
    }

    public MutableLiveData<List<Worker>> getNearbyWorkers() {
        return nearbyWorkers;
    }

    public void setNearbyWorkers(List<Worker> nearbyWorkers) {
        this.nearbyWorkers.setValue(nearbyWorkers);
    }

    public void addNearbyWorker(Worker worker) {
        List<Worker> currentList = this.nearbyWorkers.getValue();
        if (currentList != null && worker != null) {
            if (!currentList.contains(worker)) {
                currentList.add(worker);
                setNearbyWorkers(currentList);
            }
        }
    }
}
