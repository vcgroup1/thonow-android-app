package dev.vcgroup.thonowandroidapp.ui.signinscreen.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.CountryInfo;
import dev.vcgroup.thonowandroidapp.databinding.ItemCountryCodeBinding;
import lombok.AllArgsConstructor;

public class CountryCodeAdapter extends RecyclerView.Adapter<CountryCodeAdapter.CountryCodeVH> implements Filterable {
    private static final String TAG = CountryCodeAdapter.class.getSimpleName();
    private List<CountryInfo> countryInfos;
    private List<CountryInfo> filteredCountryInfos;
    private AsyncListDiffer<CountryInfo> mDiffer;
    private OnCountryCodeListener onCountryCodeListener;
    private DiffUtil.ItemCallback<CountryInfo> diffCallback = new DiffUtil.ItemCallback<CountryInfo>() {
        @Override
        public boolean areItemsTheSame(@NonNull @NotNull CountryInfo oldItem, @NonNull @NotNull CountryInfo newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull @NotNull CountryInfo oldItem, @NonNull @NotNull CountryInfo newItem) {
            return oldItem.getName().equals(newItem.getName());
        }
    };

    public CountryCodeAdapter(FirebaseFirestore db, OnCountryCodeListener onCountryCodeListener) {
        this.countryInfos = new ArrayList<>();
        this.mDiffer = new AsyncListDiffer<>(this, diffCallback);
        this.onCountryCodeListener = onCountryCodeListener;
        this.populateData(db);
    }

    private void populateData(FirebaseFirestore db) {
        db.collection(CollectionConst.LIB_COUNTRY_CODE)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    countryInfos = queryDocumentSnapshots.toObjects(CountryInfo.class);
                    this.update(countryInfos);
                    this.filteredCountryInfos = countryInfos;
                })
                .addOnFailureListener(e -> Log.d(TAG, "getCountryInfo::" + e.getMessage()));
    }

    @NonNull
    @NotNull
    @Override
    public CountryCodeVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new CountryCodeVH(ItemCountryCodeBinding.inflate(layoutInflater, parent, false), onCountryCodeListener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CountryCodeAdapter.CountryCodeVH holder, int position) {
        CountryInfo countryInfo = getItem(position);
        holder.bind(countryInfo);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public CountryInfo getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<CountryInfo> newList) {
        mDiffer.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if(charSequence == null || charSequence.length() == 0) {
                    filterResults.count = filteredCountryInfos.size();
                    filterResults.values = filteredCountryInfos;
                } else {
                    String keyword = charSequence.toString().toLowerCase();
                    List<CountryInfo> resultList = new ArrayList<>();
                    if(resultList.isEmpty()) {
                        filteredCountryInfos.forEach(countryInfo -> {
                            if(countryInfo.getName().toLowerCase().contains(keyword) ||
                                countryInfo.getCallingCode().toLowerCase().contains(keyword)) {
                                resultList.add(countryInfo);
                            }
                        });
                    }
                    filterResults.count = resultList.size();
                    filterResults.values = resultList;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                countryInfos = (List<CountryInfo>) results.values;
                update(countryInfos);
            }
        };
    }

    public class CountryCodeVH extends RecyclerView.ViewHolder {
        private final ItemCountryCodeBinding binding;
        private OnCountryCodeListener onCountryCodeListener;

        public CountryCodeVH(ItemCountryCodeBinding binding, OnCountryCodeListener onCountryCodeListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onCountryCodeListener = onCountryCodeListener;

            binding.getRoot().setOnClickListener(v -> {
                if (onCountryCodeListener != null) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION) {
                        onCountryCodeListener.onCountryCodeClickedListener(position);
                    }
                }
            });
        }

        public void bind(CountryInfo countryInfo) {
            binding.setItem(countryInfo);
            binding.executePendingBindings();
        }
    }

    public interface OnCountryCodeListener {
        void onCountryCodeClickedListener(int position);
    }
}
