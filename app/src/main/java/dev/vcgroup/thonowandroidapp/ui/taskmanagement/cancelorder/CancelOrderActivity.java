package dev.vcgroup.thonowandroidapp.ui.taskmanagement.cancelorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.data.model.OrderStatus;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCancelOrderBinding;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;

public class CancelOrderActivity extends AppCompatActivity {
    private static final String TAG = CancelOrderActivity.class.getSimpleName();
    private ActivityCancelOrderBinding binding;
    private String deleteOrderId;
    private FirebaseFirestore db;
    private String reasonStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cancel_order);

        setupToolbar();

        if (getIntent().getExtras() != null) {
            deleteOrderId = getIntent().getStringExtra("deleteOrderId");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.cancel_order_reasons));
            binding.tipCancelReason.setAdapter(adapter);
            binding.tipCancelReason.setOnItemClickListener((parent, view, position, id) -> reasonStr = (String) adapter.getItem(position));

            if (deleteOrderId != null && !deleteOrderId.isEmpty()) {
                binding.btnCancelOrder.setOnClickListener(v -> {
                    MyCustomAlertDialog dialog = MyCustomAlertDialog.create(this);
                    dialog
                            .setTitle("Xác nhận huỷ đơn hàng")
                            .setMessage("Bạn có chắc muốn huỷ đơn hàng này?")
                            .setPositiveButton("Huỷ đơn", v1 -> {
                                if (db == null) {
                                    this.db = FirebaseFirestore.getInstance();
                                }

                                this.db.collection(CollectionConst.COLLECTION_ORDER)
                                        .document(deleteOrderId)
                                        .update(
                                                "status", OrderStatus.CANCELED,
                                                "note", reasonStr
                                        )
                                        .addOnSuccessListener(unused -> {
                                            setResult(RESULT_OK);
                                            finish();
                                        })
                                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                            })
                            .setNegativeButton("Quay lại", v1 -> {
                                dialog.dismiss();
                            })
                            .show();
                });
            }
        }

    }

    private void setupToolbar() {
        Toolbar mToolbar = binding.topAppBar.mToolbar;
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}