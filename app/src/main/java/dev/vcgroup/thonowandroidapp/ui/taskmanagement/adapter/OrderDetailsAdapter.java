package dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import dev.vcgroup.thonowandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.databinding.ItemOrderDetailServiceBinding;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.OrderDetailVH> {
    private static final String TAG = OrderDetailsAdapter.class.getSimpleName();
    private List<OrderDetail> orderDetails;

    @NonNull
    @NotNull
    @Override
    public OrderDetailVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new OrderDetailVH(ItemOrderDetailServiceBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OrderDetailsAdapter.OrderDetailVH holder, int position) {
        OrderDetail orderDetail = orderDetails.get(position);
        if (orderDetail != null) {
            orderDetail.getService()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        Service service = documentSnapshot.toObject(Service.class);
                        holder.bindItem(orderDetail, service);
                    })
                    .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
        }
    }

    @Override
    public int getItemCount() {
        return orderDetails == null ? 0 : orderDetails.size();
    }

    public OrderDetail getItem(int position) {
        return orderDetails.get(position);
    }

    public static class OrderDetailVH extends RecyclerView.ViewHolder {
        private ItemOrderDetailServiceBinding binding;

        public OrderDetailVH(@NonNull @NotNull ItemOrderDetailServiceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindItem(OrderDetail orderDetail, Service service) {
            binding.setOrderDetail(orderDetail);
            binding.setService(service);
            binding.executePendingBindings();
        }
    }
}
