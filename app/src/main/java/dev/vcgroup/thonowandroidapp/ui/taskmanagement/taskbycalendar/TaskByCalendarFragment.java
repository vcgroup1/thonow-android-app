package dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskbycalendar;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.AudioCallConst;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import dev.vcgroup.thonowandroidapp.constant.CommonConst;
import dev.vcgroup.thonowandroidapp.constant.RequestCodeConst;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.PaymentMethod;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMResponse;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSingleSendData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.GenericFcmData;
import dev.vcgroup.thonowandroidapp.data.model.fcm.Token;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.IFCMService;
import dev.vcgroup.thonowandroidapp.data.remote.fcmservice.RetrofitFCMClientInstance;
import dev.vcgroup.thonowandroidapp.data.remote.sinchservice.SinchService;
import dev.vcgroup.thonowandroidapp.databinding.DialogChangeProfilePictureBinding;
import dev.vcgroup.thonowandroidapp.databinding.DialogChoosePaymentMethodBinding;
import dev.vcgroup.thonowandroidapp.databinding.TaskByCalendarFragmentBinding;
import dev.vcgroup.thonowandroidapp.ui.audiocall.callscreen.CallScreenActivity;
import dev.vcgroup.thonowandroidapp.ui.audiocall.incomingcall.IncomingCallActivity;
import dev.vcgroup.thonowandroidapp.ui.audiocall.outgoingcall.OutgoingCallActivity;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.ChatActivity;
import dev.vcgroup.thonowandroidapp.ui.profile.editprofile.EditProfileActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.TaskManagementViewModel;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.cancelorder.CancelOrderActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.adapter.TaskListAdapter;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.confirmtocompleteorder.ConfirmToCompleteOrderActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.taskdetail.TaskDetailActivity;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.thankyou.ThankForYourOrderActivity;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.MyCustomAlertDialog;
import dev.vcgroup.thonowandroidapp.util.SpacesItemDecoration;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static dev.vcgroup.thonowandroidapp.constant.AudioCallConst.OUT_GOING_CALL;
import static dev.vcgroup.thonowandroidapp.constant.CommonConst.CALL_PERMISSIONS;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.CANCEL_ORDER_REQ_CODE;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.CONFIRM_TO_COMPLETE_ORDER_REQ_CODE;
import static dev.vcgroup.thonowandroidapp.constant.RequestCodeConst.SEE_DETAIL_ORDER_REQ_CODE;

public class TaskByCalendarFragment extends Fragment implements TaskListAdapter.OnTaskListener {
    private static final String TAG = TaskByCalendarFragment.class.getSimpleName();
    private TaskByCalendarFragmentBinding binding;
    private CalendarView calendarView;
    private TaskListAdapter adapter;
    private TaskManagementViewModel mViewModel;
    private Calendar selectedCalendar;
    private static FirebaseFirestore db;

    static {
        db = FirebaseFirestore.getInstance();
    }

    public static TaskByCalendarFragment newInstance() {
        return new TaskByCalendarFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.task_by_calendar_fragment, container, false);
        mViewModel = ViewModelProviders.of(requireActivity()).get(TaskManagementViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRvTaskList();
        setupCalendarView();
    }

    private void setupRvTaskList() {
        adapter = new TaskListAdapter(new ArrayList<>(), TaskListAdapter.TaskAdapterType.TASK_BY_CALENDAR_ITEM, this);
        binding.rvTaskListByCalendar.setAdapter(adapter);
        binding.rvTaskListByCalendar.addItemDecoration(new SpacesItemDecoration(20));
    }

    @SneakyThrows
    private void setupCalendarView() {
        calendarView = binding.calendarView;
        calendarView.performClick();

        // set current date
        selectedCalendar = Calendar.getInstance();
        calendarView.setDate(selectedCalendar);
        onTaskFilterByDate(selectedCalendar.getTime());

        calendarView.setOnDayClickListener(eventDay -> {
            selectedCalendar = eventDay.getCalendar();
            onTaskFilterByDate(selectedCalendar.getTime());
        });
        calendarView.showCurrentMonthPage();

    }

    private void onTaskFilterByDate(Date date) {
        List<EventDay> events = new ArrayList<>();
        mViewModel.getOrders().observe(requireActivity(), orders -> {
            List<Order> results = new ArrayList<>();
            if (orders != null && !orders.isEmpty()) {
                orders.forEach(order -> {
                    if (CommonUtil.isSameDay(date, order.getWorkingDate())) {
                        results.add(order);
                    }

                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(order.getWorkingDate());
                    events.add(new EventDay(calendar1, R.drawable.ic_repair_event));
                });
            }
            adapter.update(results);
            calendarView.setEvents(events);
            onFilterListener(results.size());
        });
    }

    private void onFilterListener(int count) {
        if (count == 0) {
            binding.noTaskSchedule.getRoot().setVisibility(View.VISIBLE);
            binding.rvTaskListByCalendar.setVisibility(View.GONE);
        } else {
            binding.noTaskSchedule.getRoot().setVisibility(View.GONE);
            binding.rvTaskListByCalendar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onTaskClickedListener(int position) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(requireActivity(), TaskDetailActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        startActivityForResult(intent, SEE_DETAIL_ORDER_REQ_CODE);
    }

    @Override
    public void onTaskCancelClickedListener(int position) {
        String orderId = adapter.getItem(position).getId();
        Intent intent = new Intent(requireActivity(), CancelOrderActivity.class);
        intent.putExtra("deleteOrderId", orderId);
        startActivityForResult(intent, CANCEL_ORDER_REQ_CODE);
    }

    @Override
    public void onChatClickedListener(int position) {
        Order order = adapter.getItem(position);
        String orderId = order.getId();
        Intent intent = new Intent(requireActivity(), ChatActivity.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra("workerId", order.getWorker().getId());
        startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
    }

    @Override
    public void onAudioCallClickedListener(int position) {
        Order order = adapter.getItem(position);

        if (!TedPermission.isGranted(requireActivity(), CALL_PERMISSIONS)) {
            TedPermission.with(requireActivity())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            callToWorker(order);
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Snackbar.make(binding.getRoot(), "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(CALL_PERMISSIONS)
                    .check();
        } else {
            callToWorker(order);
        }
    }

    @Override
    public void onConfirmToCompleterOrder(int position) {
        showChoosePaymentMethod(position);
    }

    private void showChoosePaymentMethod(int position) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        DialogChoosePaymentMethodBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_choose_payment_method, (ViewGroup) requireActivity().getWindow().getDecorView().getRootView(), false);

        builder.setView(binding.getRoot());
        AlertDialog dialog = builder.create();
        dialog.show();

        binding.cashPaymentMethod.setOnClickListener(view -> navigateToConfirmToCompleteOrder(position, PaymentMethod.CASH));

        binding.creditCardPaymentMethod.setOnClickListener(view -> {
            navigateToConfirmToCompleteOrder(position, PaymentMethod.CREDIT_CARD);
            dialog.dismiss();
        });
    }

    private void navigateToConfirmToCompleteOrder(int position, PaymentMethod method) {
        Order order = adapter.getItem(position);
        Intent intent = new Intent(requireActivity(), ConfirmToCompleteOrderActivity.class);
        intent.putExtra("order", Parcels.wrap(order));
        intent.putExtra("paymentMethod", method.name());
        startActivityForResult(intent, CONFIRM_TO_COMPLETE_ORDER_REQ_CODE);
    }

    private void callToWorker(Order order) {
        order.getWorker()
                .get()
                .addOnSuccessListener(documentSnapshot1 -> {
                    Intent intent = new Intent(requireActivity(), CallScreenActivity.class);
                    intent.putExtra("worker", Parcels.wrap(documentSnapshot1.toObject(Worker.class)));
                    intent.putExtra(OUT_GOING_CALL, true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent, ActivityOptions.makeCustomAnimation(getActivity(), android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                    notifyFailedAudioCall();
                });

    }

    private void notifyFailedAudioCall() {
        MyCustomAlertDialog dialog = MyCustomAlertDialog.create(requireActivity());
        dialog.setTitle("Thông báo")
                .setMessage("Gọi thoại cho thợ xảy ra lỗi. Hãy thử lại xem sao.")
                .setPositiveButton("Thử lại", v -> {
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            onTaskFilterByDate(selectedCalendar.getTime());
        }
    }

}