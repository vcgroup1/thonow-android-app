package dev.vcgroup.thonowandroidapp.ui.taskmanagement.thankyou;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import org.parceler.Parcels;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.Order;
import dev.vcgroup.thonowandroidapp.data.model.Worker;
import dev.vcgroup.thonowandroidapp.databinding.ActivityThankYouForOrderBinding;
import dev.vcgroup.thonowandroidapp.ui.taskmanagement.ratingworker.RatingWorkerActivity;

public class ThankForYourOrderActivity extends AppCompatActivity {
    private ActivityThankYouForOrderBinding binding;
    private String orderId;
    private Worker worker;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thank_you_for_order);

        if (getIntent().getExtras() != null) {
            orderId = getIntent().getStringExtra("orderId");
            worker = Parcels.unwrap(getIntent().getParcelableExtra("worker"));
        }

        binding.btnRatingWorker.setOnClickListener(v -> {
            Intent intent = new Intent(this, RatingWorkerActivity.class);
            intent.putExtra("worker", Parcels.wrap(worker));
            intent.putExtra("orderId", orderId);
            startActivity(intent, ActivityOptions.makeCustomAnimation(this, android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
            finish();
        });
    }
}
