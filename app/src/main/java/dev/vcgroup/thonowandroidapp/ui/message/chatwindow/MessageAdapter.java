package dev.vcgroup.thonowandroidapp.ui.message.chatwindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.chat.Message;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageMediaBinding;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder.ChatItemType;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder.ImageMessageViewHolder;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder.OnMessageClickListener;
import dev.vcgroup.thonowandroidapp.ui.message.chatwindow.messageviewholder.TextMessageViewHolder;
import lombok.SneakyThrows;

import static dev.vcgroup.thonowandroidapp.util.CommonUtil.getDateDiff;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MessageAdapter.class.getSimpleName();
    private Context context;
    private List<Message> messageList;
    private OnMessageClickListener onChatItemListener;
    private AsyncListDiffer<Message> mDiffer;
    private String currentUserUid;
    private StorageReference mStorageRef;
    private FirebaseFirestore db;
    private DiffUtil.ItemCallback<Message> itemCallback = new DiffUtil.ItemCallback<Message>() {
        @Override
        public boolean areItemsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };

    public MessageAdapter(Context context, List<Message> messageList, OnMessageClickListener onChatItemListener) {
        this.context = context;
        this.messageList = messageList;
        this.currentUserUid = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
        this.mDiffer = new AsyncListDiffer<>(this, itemCallback);
        this.onChatItemListener = onChatItemListener;
        this.db = FirebaseFirestore.getInstance();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = getItem(position);

        if (message.getFrom().getId().equals(currentUserUid)) { // right
            switch (message.getType()) {
                case TEXT:
                    return ChatItemType.CHAT_ITEM_RIGHT_TEXT.getTypeId();
                case IMAGE:
                    return ChatItemType.CHAT_ITEM_RIGHT_IMAGE.getTypeId();
            }
        } else { // left
            switch (message.getType()) {
                case TEXT:
                    return ChatItemType.CHAT_ITEM_LEFT_TEXT.getTypeId();
                case IMAGE:
                    return ChatItemType.CHAT_ITEM_LEFT_IMAGE.getTypeId();
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding;
        ChatItemType chatItemType = ChatItemType.getChatItemType(viewType);
        switch (Objects.requireNonNull(chatItemType)) {
            case CHAT_ITEM_RIGHT_TEXT:
                binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chat_message_right_text, parent, false);
                return new TextMessageViewHolder(binding, onChatItemListener);
            case CHAT_ITEM_LEFT_TEXT:
                binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chat_message_left_text, parent, false);
                return new TextMessageViewHolder(binding, onChatItemListener);
            case CHAT_ITEM_RIGHT_IMAGE:
            case CHAT_ITEM_LEFT_IMAGE:
                return new ImageMessageViewHolder(ItemChatMessageMediaBinding.inflate(layoutInflater, parent, false), onChatItemListener);
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
    }

    @SneakyThrows
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message messageItem = getItem(position);
        ChatItemType chatItemType = ChatItemType.getChatItemType(getItemViewType(position));
        boolean isBreakTimestamp = false;

        if (getItemCount() > 1 && position > 0) {
            Message previousMessage = getItem(position - 1);
            if (getDateDiff(previousMessage.getTimestamp(), messageItem.getTimestamp(), TimeUnit.DAYS) >= 1) {
                isBreakTimestamp = true;
            }
        } else if (position == 0) {
            isBreakTimestamp = true;
        }

        switch (Objects.requireNonNull(chatItemType)) {
            case CHAT_ITEM_RIGHT_TEXT:
                ((TextMessageViewHolder) holder).bindMessage(messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_TEXT:
                ((TextMessageViewHolder) holder).bindMessage(messageItem, true, isBreakTimestamp);
                break;
            case CHAT_ITEM_RIGHT_IMAGE:
                ((ImageMessageViewHolder) holder).bindMessage(messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_IMAGE:
                ((ImageMessageViewHolder) holder).bindMessage(messageItem, true, isBreakTimestamp);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + chatItemType);
        }
        //holder.setIsRecyclable(false);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        int position = holder.getAdapterPosition();
        super.onViewRecycled(holder);
    }

    public void addNewMessage(@NonNull Message chatMessage) {
        messageList.add(chatMessage);
        submitList(messageList);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Message getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void submitList(List<Message> newList) {
        mDiffer.submitList(newList);
    }
}
