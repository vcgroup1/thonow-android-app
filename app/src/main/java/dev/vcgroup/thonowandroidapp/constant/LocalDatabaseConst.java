package dev.vcgroup.thonowandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LocalDatabaseConst {
    public static final String DATABASE_NAME = "THONOW_DATABASE";
    public static final int DATABASE_VERSION = 1;
    public static final String TBL_NOTIFICATION = "TBL_NOTIFICATION";
    public static final String KEY_NOTIFICATION_ID = "id";
    public static final String KEY_NOTIFICATION_TITLE = "title";
    public static final String KEY_NOTIFICATION_TEXT = "text";
    public static final String KEY_NOTIFICATION_TIMESTAMP = "timestamp";
}
