package dev.vcgroup.thonowandroidapp.constant;

import android.Manifest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RequestCodeConst {
    public static final int TAKE_PHOTO_CODE = 111;
    public static final int ASK_PERMISSION_CODE = 300;
    public static final int SELECT_SERVICE_TYPE_REQ_CODE = 400;
    public static final int SELECT_SERVICE_REQ_CODE = 401;
    public static final int SELECT_FAVORITE_WORKER_REQ_CODE = 401;
    public static final int CANCEL_ORDER_REQ_CODE = 402;
    public static final int SEE_DETAIL_ORDER_REQ_CODE = 403;
    public static final int CONFIRM_TO_COMPLETE_ORDER_REQ_CODE = 404;
    public static final int THANK_FOR_YOUR_ORDER_REQ_CODE = 405;
    public static final int EDIT_PROFILE_REQ_CODE = 201;
}
