package dev.vcgroup.thonowandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StorageConst {
    public static final String AVATAR_FOLDER = "images_avatar";
    public static final String CHAT_FOLDER = "images_chat";
    public static final String FOLDER_CONSERVATION_PHOTO = "images_convervation";
}
