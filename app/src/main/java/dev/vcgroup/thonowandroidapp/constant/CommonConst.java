package dev.vcgroup.thonowandroidapp.constant;

import android.Manifest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CommonConst {
    public static final String[] REQUIRED_PERMISSION = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_PHONE_STATE};
    public static final String[] LOCATION_PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static final String[] CALL_PERMISSIONS = {android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_PHONE_STATE};

}
