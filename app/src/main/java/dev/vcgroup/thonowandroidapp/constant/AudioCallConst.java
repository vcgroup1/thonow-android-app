package dev.vcgroup.thonowandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AudioCallConst {
    public static final String INCOMING_CALL = "incoming_call";
    public static final String OUT_GOING_CALL = "outgoing_call";
    public static final String NEW_MESSAGE = "new_message";
}
