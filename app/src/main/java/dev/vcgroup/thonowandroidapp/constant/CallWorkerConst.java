package dev.vcgroup.thonowandroidapp.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CallWorkerConst {
    public static final String RECEIVED_CALL_WORKER_REQUEST_CODE = "received_call_worker_request";
    public static final String IS_FROM_FOUND_WORKER_REQUEST_CODE = "isFromFoundWorkerRequest";
    public static final double RADIUS_1 = 6;
    public static final double RADIUS_2 = 10;
}
