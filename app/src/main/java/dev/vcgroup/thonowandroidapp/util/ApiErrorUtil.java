package dev.vcgroup.thonowandroidapp.util;

import android.content.Context;

import java.io.IOException;
import java.lang.annotation.Annotation;

import dev.vcgroup.thonowandroidapp.data.model.ApiError;
import dev.vcgroup.thonowandroidapp.data.remote.RetrofitClientInstance;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiErrorUtil {
    public static ApiError parseError(Context context, Response<?> response) {
        Converter<ResponseBody, ApiError> converter = RetrofitClientInstance.getInstance()
                .getRetrofit().responseBodyConverter(ApiError.class, new Annotation[0]);

        ApiError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ApiError();
        }
        return error;
    }
}
