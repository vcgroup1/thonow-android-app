package dev.vcgroup.thonowandroidapp.util;

import com.google.firebase.firestore.FirebaseFirestore;

import dev.vcgroup.thonowandroidapp.constant.CollectionConst;

public class OrderIdSequenceGenerator extends SequenceGenerator {
    private FirebaseFirestore db;
    private String orderId;
    private boolean isOverlap;

    public OrderIdSequenceGenerator(int nodeId) {
        super(nodeId);
        this.db = FirebaseFirestore.getInstance();
        this.isOverlap = false;
    }

    public OrderIdSequenceGenerator() {
        this.db = FirebaseFirestore.getInstance();
        this.isOverlap = false;
    }

    @Override
    public synchronized long nextId() {
        return super.nextId();
    }

    public synchronized String getOrderId() {
        orderId = "OD" + super.nextId();
        while (isOverlap) {
            db.collection(CollectionConst.COLLECTION_ORDER)
                    .document(orderId)
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        isOverlap = documentSnapshot.exists();
                    });
        }
        return orderId;
    }
}
