package dev.vcgroup.thonowandroidapp.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.transition.Fade;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.applandeo.materialcalendarview.utils.DateUtils;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.koalap.geofirestore.GeoLocation;

import org.ocpsoft.prettytime.PrettyTime;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.constant.CollectionConst;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CommonUtil {
    private static PrettyTime prettyTime;
    private static DateFormat targetDateFormat;
    private static DateFormat originalDateFormat;
    private static DecimalFormat currencyFormat;
    private static SimpleDateFormat chatDateTimeFormat;
    private static SimpleDateFormat chatTimeFormat;
    static {
        targetDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        originalDateFormat = new SimpleDateFormat("dd MM, yyyy");
        currencyFormat = new DecimalFormat("###,###.##");
        chatDateTimeFormat = new SimpleDateFormat("HH:mm a, dd/MM/yyyy");
        chatTimeFormat = new SimpleDateFormat("HH:mm");
    }
    public static String currentUserUid;
    static {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
        }
    }

    public static String convertCurrencyText(double number) {
        return currencyFormat.format(number);
    }

    public static double revertCurrencyText(String numberString) throws ParseException {
        return Objects.requireNonNull(currencyFormat.parse(numberString)).doubleValue();
    }

    public static String getInputText(TextInputLayout tip) {
        return tip.getEditText().getText().toString();
    }

    public static String getInputText(TextInputEditText tip) {
        return tip.getText().toString();
    }

    public static void setInputText(TextInputLayout tip, String content) {
        tip.getEditText().setText(content);
    }

    public static Locale getCurrentLocale(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            return context.getResources().getConfiguration().locale;
        }
    }

    public static String getDayGapString(Context context, Date date) {
        if(prettyTime == null) {
            prettyTime = new PrettyTime(getCurrentLocale(context));
        }
        return prettyTime.format(date);
    }

    public static void animateFade(View view, int visibility) {
        Transition transition = new Fade();
        transition.setDuration(500);
        transition.addTarget(view.getId());

        TransitionManager.beginDelayedTransition((ViewGroup) view.getParent(), transition);
        view.setVisibility(visibility);
    }

    public static void animateToggle(View view, int visibility) {
        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(100);
        transition.addTarget(view.getId());

        TransitionManager.beginDelayedTransition((ViewGroup) view.getParent(), transition);
        view.setVisibility(visibility);
    }

    public static int getActionBarHeight(Activity activity) {
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (activity.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public static Bitmap resizeMapIcons(Context context, @DrawableRes int iconResId, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), iconResId);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static Drawable getDrawableByName(Context context, String resName) {
        int drawableRes;
        if (resName != null && !resName.isEmpty()) {
            drawableRes = context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
            if (drawableRes == 0) {
                throw new RuntimeException("Can't find drawable with name: " + resName);
            }
        } else {
            drawableRes = R.drawable.ic_tho_sua_chua_nuoc;
        }
        return ResourcesCompat.getDrawable(context.getResources(), drawableRes, context.getTheme());
    }

    public static Date convertDatePickerTextToFormattedDate(String datePickerText) throws ParseException {
        datePickerText = datePickerText.replace("thg ", "");
        return originalDateFormat.parse(datePickerText);
    }

    public static String convertDateToString(Date date) {
        return targetDateFormat.format(date);
    }

    @SneakyThrows
    public static Date fromDateStringToDate(String dateStr){
        if(!dateStr.isEmpty())
            return targetDateFormat.parse(dateStr);
        return null;
    }

    public static String getFullAddressFromGeoLocation(Context context, GeoLocation geoLocation) throws IOException {
        Geocoder geocoder = new Geocoder(context, new Locale("vi", "VN"));
        List<Address> addressList = geocoder.getFromLocation(geoLocation.latitude, geoLocation.longitude, 1);
        if (addressList != null && addressList.size() > 0) {
            String address = addressList.get(0).getAddressLine(0);
            String city = addressList.get(0).getLocality();
            String country = addressList.get(0).getCountryName();
            return address + ", " + city + ", " + country;
        }
        return null;
    }

    public static String getFormattedLatlng(LatLng latLng) {
        return latLng.latitude + "," + latLng.longitude;
    }

    public static boolean isSameDay(Date date1, Date date2) {
        return targetDateFormat.format(date1).equals(targetDateFormat.format(date2));
    }

    public static String formatChatDateTime(Date date) {
        if (date != null) {
            return chatDateTimeFormat.format(date);
        }
        return "";
    }

    public static String formatChatTime(Date date) {
        if (date != null) {
            return chatTimeFormat.format(date);
        }
        return "";
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static DocumentReference getCurrentUserRef(FirebaseFirestore db) {
        return db.collection(CollectionConst.COLLECTION_USER).document(currentUserUid);
    }

    public static String formatToDigitalClock(long miliSeconds) {
        long hours = TimeUnit.MILLISECONDS.toHours(miliSeconds) % 24;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(miliSeconds) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(miliSeconds) % 60;
        if (hours > 0) {
            return String.format(Locale.getDefault(), "%d:%02d:%02d", hours, minutes, seconds);
        } else if (minutes > 0) {
            return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        } else if (seconds > 0) {
            return String.format(Locale.getDefault(), "00:%02d", seconds);
        }
        return "00:00";
    }
}

