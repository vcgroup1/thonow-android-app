package dev.vcgroup.thonowandroidapp.util.databinding.adapters;

import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;

@InverseBindingMethods({
        @InverseBindingMethod(type = RadioButton.class, attribute = "android:checkedButton", method = "getCheckedButtonId"),
})
public class RadioGroupBindingAdapter {

    @BindingAdapter("android:checkedButton")
    public static void setCheckedButton(RadioGroup view, int id) {
        if (view.getCheckedRadioButtonId() != id) {
            view.check(id);
        }
    }

    @BindingAdapter(value = {"android:onCheckedChanged", "android:checkedButtonAttrChanged"}, requireAll = false)
    public static void setListeners (RadioGroup view, final RadioGroup.OnCheckedChangeListener listener,
                                     final InverseBindingListener attrChange) {
        if (attrChange == null) {
            view.setOnCheckedChangeListener(listener);
        } else {
            view.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (listener != null) {
                        listener.onCheckedChanged(group, checkedId);
                    }

                    attrChange.onChange();
                }
            });
        }
    }
}
