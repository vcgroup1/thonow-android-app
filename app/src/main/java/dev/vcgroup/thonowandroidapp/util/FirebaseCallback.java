package dev.vcgroup.thonowandroidapp.util;

public interface FirebaseCallback {
    <T> void onResponse(T object);
}
