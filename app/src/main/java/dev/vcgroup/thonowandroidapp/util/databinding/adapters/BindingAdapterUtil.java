package dev.vcgroup.thonowandroidapp.util.databinding.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.databinding.BindingAdapter;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.data.model.OrderDetail;
import dev.vcgroup.thonowandroidapp.data.model.Service;
import dev.vcgroup.thonowandroidapp.data.model.ServiceType;
import dev.vcgroup.thonowandroidapp.util.CommonUtil;
import dev.vcgroup.thonowandroidapp.util.ImageLoadingUtil;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BindingAdapterUtil {
    private static PrettyTime prettyTime;
    private static DateFormat targetDateFormat;
    private static DateFormat hourDateFormat;
    private static DateFormat startDateFormat;
    static {
         targetDateFormat = new SimpleDateFormat("dd/MM/yyyy");
         hourDateFormat = new SimpleDateFormat("hh:mm aa");
         startDateFormat = new SimpleDateFormat("dd/MM/yyyy - hh:mm aa");
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String url) {
        if (url != null && !url.isEmpty()) {
            ImageLoadingUtil.displayImage(view, url);
        } else {
            view.setImageResource(R.drawable.logo_place_holder);
        }
    }

    @BindingAdapter({"displayBlurImage"})
    public static void loadBlurImage(ImageView view, String url) {
        ImageLoadingUtil.displayImage(view, url);
    }

    @BindingAdapter({"drawableSrc"})
    public static void loadDrawable(ImageView view, @DrawableRes int resId) {
        view.setImageResource(resId);
    }

    @BindingAdapter({"drawableNameSrc"})
    public static void loadDrawableFromName(ImageView view, String drawableName) {
        view.setImageDrawable(CommonUtil.getDrawableByName(view.getContext(), drawableName));
    }

    @BindingAdapter({"drawableSrcFromServiceType"})
    public static void loadDrawableFromServiceType(ImageView view, DocumentReference serviceTypeDocRef) {
        serviceTypeDocRef
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    ServiceType serviceType = documentSnapshot.toObject(ServiceType.class);
                    if (serviceType != null) {
                        view.setImageDrawable(CommonUtil.getDrawableByName(view.getContext(), serviceType.getDrawableName()));
                    }
                });
    }

    @BindingAdapter({"countryIconUrl"})
    public static void loadCountryIcon(ImageView view, String alp2code) {
        String url = "gs://" + view.getContext().getString(R.string.google_storage_bucket) + "/res/country_flags/" + alp2code + ".png";
        StorageReference gsReference = FirebaseStorage.getInstance().getReferenceFromUrl(url);
        gsReference.getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    ImageLoadingUtil.displayImage(view, uri.toString());
                })
                .addOnFailureListener(e -> Log.d("loadCountryIcon", e.getMessage()));
    }

    @BindingAdapter({"dayGapString"})
    public static void calculateDayGap(TextView textView, Date date) {
        textView.setText(CommonUtil.getDayGapString(textView.getContext(), date));
    }

    @BindingAdapter({"htmlText"})
    public static void setHtmlText(TextView textView, String text) {
        textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
    }

    @BindingAdapter({"workerServiceTypeText"})
    public static void setWorkerServiceTypeText(TextView textView, List<DocumentReference> serviceTypeList) {
        if (serviceTypeList != null && !serviceTypeList.isEmpty()) {
            serviceTypeList.forEach(st -> {
                st.get().addOnSuccessListener(documentSnapshot -> {
                    ServiceType serviceType = documentSnapshot.toObject(ServiceType.class);
                    if (serviceType != null) {
                        textView.setText(serviceType.getName());
                    }
                })
                        .addOnFailureListener(e -> Log.d("setWorkerServiceTypeText", e.getMessage()));
            });
        }
    }

    @BindingAdapter({"setDate"})
    public static void convertDateToString(TextInputEditText et, Calendar calendar) {
        et.setText(targetDateFormat.format(calendar.getTime()));
    }

    @BindingAdapter({"setTime"})
    public static void convertHourToString(TextInputEditText et, Calendar calendar) {
        et.setText(hourDateFormat.format(calendar.getTime()));
    }

    @BindingAdapter({"formatStartDate"})
    public static void formatStartDate(TextView textView, Date startDate) {
        textView.setText(startDateFormat.format(startDate));
    }

    @BindingAdapter({"formatStartDate2"})
    public static void formatStartDate2(TextView textView, Date startDate) {
        textView.setText("Ngày bắt đầu : " + startDateFormat.format(startDate));
    }

    @BindingAdapter({"parseColor"})
    public static void parseColor(View view, String colorStr) {
        view.setBackgroundColor(Color.parseColor(colorStr));
    }

    @BindingAdapter({"setStrokeColor"})
    public static void setStrokeColor(MaterialCardView cardView, String colorStr) {
        cardView.setStrokeColor(Color.parseColor(colorStr));
    }

    @BindingAdapter({"setBackgroundTintList"})
    public static void setBackgroundTintList(View view, String colorStr) {
        view.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(colorStr)));
    }

    @BindingAdapter({"setOrderServiceList"})
    public static void setOrderServiceList(TextView textView, List<OrderDetail> orderDetails) {
        StringBuilder stringBuilder = new StringBuilder();
        if (orderDetails != null && !orderDetails.isEmpty()) {
            orderDetails.forEach(orderDetail -> {
                orderDetail.getService()
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            stringBuilder.append(Objects.requireNonNull(documentSnapshot.toObject(Service.class)).getName()).append("\n");
                        });
            });
        }
        textView.setText(stringBuilder.toString());
    }

    @BindingAdapter({"setDateGap"})
    public static void setDateGap(TextView textView, Date date) {
        textView.setText(CommonUtil.getDayGapString(textView.getContext(), date));
    }

    @BindingAdapter({"formatLastMessage"})
    public static void formatLastMessageContent(TextView textView, String content) {
        Context context = textView.getContext();
        if (content.contains(context.getString(R.string.google_storage_bucket))) {
            textView.setText("[Hình ảnh]");
        } else {
            textView.setText(content);
        }
    }

    @BindingAdapter({"setTint"})
    public static void setTint(View view, String colorStr) {
        if (view instanceof ImageButton) {
            Drawable drawable = ((ImageButton) view).getDrawable();
            drawable.setTint(Color.parseColor(colorStr));
            ((ImageButton) view).setImageDrawable(drawable);
        }
    }
}
