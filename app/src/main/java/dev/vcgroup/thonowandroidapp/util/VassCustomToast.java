/*
 * Copyright  © VASS. All rights reserved
 * Author: chumin99 on 6/2/21, 2:43 PM
 */

package dev.vcgroup.thonowandroidapp.util;

import android.app.Activity;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.databinding.DataBindingUtil;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomToastBinding;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

public class VassCustomToast {
    private static Toast mToast;

    public static Toast makeText(Activity activity, String msg, int duration, ToastType type) {
        if (mToast == null) {
            mToast = new Toast(activity);
        }
        mToast.setDuration(duration);

        ItemCustomToastBinding binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.item_custom_toast, (ViewGroup) activity.getWindow().getDecorView(), false);
        ToastItem toastItem = new ToastItem(msg, type);
        binding.setToastItem(toastItem);
        binding.ibnCloseCustomToast.setOnClickListener(v -> mToast.cancel());
        binding.txtMessCustomToast.setCompoundDrawablesWithIntrinsicBounds(type.resIconId, 0, 0, 0);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            mToast.setView(binding.getRoot());
        } else {
            // show Snackbar instead
            VassCustomSnackbar.create(activity.getWindow().getDecorView())
                    .setMessage(msg)
                    .setDuration(duration)
                    .show();
        }
        mToast.setGravity(Gravity.TOP, 0, 200);

        return mToast;
    }

    @Getter
    @AllArgsConstructor
    public enum ToastType {
        INFO("#2196f3", "#e9f5fe", R.drawable.ic_info_circle_solid),
        SUCCESS("#4caf50", "#edfae1", R.drawable.ic_check_circle_solid),
        WARNING("#ffb300", "#fff9e6", R.drawable.ic_warning),
        ERROR("#f44336", "#fde9ef", R.drawable.ic_error_solid);

        String primaryColor;
        String secondaryColor;
        @DrawableRes int resIconId;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ToastItem {
        private String msg;
        private ToastType type;
    }
}
