package dev.vcgroup.thonowandroidapp.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class GenericWebview {
    private WebView mWebView;
    private OnPaymentCompletedListener onPaymentCompletedListener;


    private GenericWebview(WebView mWebView, String url) {
        this.mWebView = mWebView;
        settingWebView(url);
    }

    private GenericWebview(WebView mWebView,
                           String url, OnPaymentCompletedListener onPaymentCompletedListener) {
        this.mWebView = mWebView;
        this.onPaymentCompletedListener = onPaymentCompletedListener;
        settingWebView(url);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void settingWebView(String url) {
        // Configure related browser settings
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        // Enable responsive layout
        mWebView.getSettings().setUseWideViewPort(true);
        // Zoom out if the content width is greater than the width of the viewport
        mWebView.getSettings().setLoadWithOverviewMode(true);

        // Configure the client to use when opening URLs
        mWebView.setWebViewClient(new GenericWebviewClient(mWebView.getContext(), onPaymentCompletedListener));

        mWebView.loadUrl(url);
    }

    public static GenericWebview create(WebView webView, String url, OnPaymentCompletedListener onPaymentCompletedListener) {
        return new GenericWebview(webView, url, onPaymentCompletedListener);
    }

    public static GenericWebview create(WebView webView, String url) {
        return new GenericWebview(webView, url);
    }

    public static class GenericWebviewClient extends WebViewClient {
        private VCLoadingDialog loading;
        private OnPaymentCompletedListener onPaymentCompletedListener;

        public GenericWebviewClient(Context context, OnPaymentCompletedListener onPaymentCompletedListener) {
            this.loading = VCLoadingDialog.create(context);
            this.onPaymentCompletedListener = onPaymentCompletedListener;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (url.startsWith("https://thonow.page.link/successful-payment")) {
                if (onPaymentCompletedListener != null) {
                    loading.dismiss();
                    onPaymentCompletedListener.onPaymentCompleted();
                }
            } else {
                loading.show();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            loading.dismiss();
        }
    }

    public interface OnPaymentCompletedListener {
        void onPaymentCompleted();
    }
}
