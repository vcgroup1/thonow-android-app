/*
 * Copyright  © VASS. All rights reserved
 * Author: chumin99 on 6/3/21, 3:12 PM
 */

package dev.vcgroup.thonowandroidapp.util;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomSnackbarBinding;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

public class VassCustomSnackbar {
    private View rootView;
    private Snackbar mSnackbar;
    private SnackbarType snackbarType;
    private String message;
    private ItemCustomSnackbarBinding binding;
    private @BaseTransientBottomBar.Duration
    int duration;

    public VassCustomSnackbar(View rootView) {
        this.rootView = rootView;
        this.snackbarType = SnackbarType.INFO;
        this.duration = Snackbar.LENGTH_SHORT;
    }

    public VassCustomSnackbar setStyle(SnackbarType snackbarType) {
        if (snackbarType != null) {
            this.snackbarType = snackbarType;
        }
        return this;
    }

    public VassCustomSnackbar setMessage(@NonNull String message) {
        if (!message.isEmpty()) {
            this.message = message;
        }
        return this;
    }

    public VassCustomSnackbar setDuration(@Nullable @BaseTransientBottomBar.Duration int duration) {
        this.duration = duration;
        return this;
    }

    private String actionText;
    private View.OnClickListener onActionClickListener;

    public VassCustomSnackbar setAction(String actionText, @Nullable View.OnClickListener onClickListener) {
        if (!actionText.isEmpty()) {
            this.actionText = actionText;
        }
        this.onActionClickListener = onClickListener;
        return this;
    }

    public static VassCustomSnackbar create(View rootView) {
        return new VassCustomSnackbar(rootView);
    }

    private void initVassCustomSnackbar() {
        if (mSnackbar == null) {
            mSnackbar = Snackbar.make(rootView, "", BaseTransientBottomBar.LENGTH_SHORT);
        }
        ItemCustomSnackbarBinding binding = DataBindingUtil.inflate(LayoutInflater.from(rootView.getContext()), R.layout.item_custom_snackbar, (ViewGroup) rootView, false);
        SnackbarItem snackbarItem = new SnackbarItem(message, snackbarType);
        binding.setSnackBarItem(snackbarItem);
        binding.txtMess.setCompoundDrawablesWithIntrinsicBounds(snackbarType.resIconId, 0, 0, 0);

        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) mSnackbar.getView();
        snackbarLayout.setPadding(0, 0, 0, 0);
        snackbarLayout.setBackgroundColor(Color.parseColor(snackbarType.secondaryColor));
        snackbarLayout.addView(binding.getRoot());

        if (actionText != null && !actionText.isEmpty()) {
            binding.tvAction.setText(actionText);
            binding.tvAction.setTextColor(Color.parseColor(snackbarType.primaryColor));
            binding.tvAction.setOnClickListener(onActionClickListener);
        }
    }

    public VassCustomSnackbar show() {
        initVassCustomSnackbar();
        mSnackbar.show();
        return this;
    }

    @Getter
    @AllArgsConstructor
    public enum SnackbarType {
        INFO("#2196f3", "#e9f5fe", R.drawable.ic_info_circle_solid),
        SUCCESS("#4caf50", "#edfae1", R.drawable.ic_check_circle_solid),
        WARNING("#ffb300", "#fff9e6", R.drawable.ic_warning),
        ERROR("#f44336", "#fde9ef", R.drawable.ic_error_solid);

        String primaryColor;
        String secondaryColor;
        @DrawableRes
        int resIconId;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SnackbarItem {
        private String msg;
        private SnackbarType type;
    }
}
