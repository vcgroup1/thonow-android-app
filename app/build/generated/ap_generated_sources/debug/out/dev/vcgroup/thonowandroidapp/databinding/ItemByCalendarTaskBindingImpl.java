package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemByCalendarTaskBindingImpl extends ItemByCalendarTaskBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linearLayout5, 11);
        sViewsWithIds.put(R.id.ibnChat, 12);
        sViewsWithIds.put(R.id.ibnCall, 13);
        sViewsWithIds.put(R.id.divider, 14);
        sViewsWithIds.put(R.id.specialty_item, 15);
        sViewsWithIds.put(R.id.textView9, 16);
        sViewsWithIds.put(R.id.linearLayout8, 17);
        sViewsWithIds.put(R.id.btn_see_detail_order, 18);
        sViewsWithIds.put(R.id.btn_confirm_to_complete, 19);
        sViewsWithIds.put(R.id.btnCancelOrder, 20);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView10;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemByCalendarTaskBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private ItemByCalendarTaskBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[20]
            , (com.google.android.material.button.MaterialButton) bindings[19]
            , (com.google.android.material.button.MaterialButton) bindings[18]
            , (com.google.android.material.card.MaterialCardView) bindings[0]
            , (android.view.View) bindings[14]
            , (android.widget.ImageButton) bindings[13]
            , (android.widget.ImageButton) bindings[12]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.RatingBar) bindings[6]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[4]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[7]
            , (com.google.android.material.card.MaterialCardView) bindings[15]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[16]
            );
        this.cardview.setTag(null);
        this.mboundView1 = (android.widget.FrameLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (android.widget.TextView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.ratingBar.setTag(null);
        this.roundedImageView5.setTag(null);
        this.specialtyIcon.setTag(null);
        this.specialtyName.setTag(null);
        this.textView10.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else if (BR.serviceType == variableId) {
            setServiceType((dev.vcgroup.thonowandroidapp.data.model.ServiceType) variable);
        }
        else if (BR.order == variableId) {
            setOrder((dev.vcgroup.thonowandroidapp.data.model.Order) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }
    public void setServiceType(@Nullable dev.vcgroup.thonowandroidapp.data.model.ServiceType ServiceType) {
        this.mServiceType = ServiceType;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.serviceType);
        super.requestRebind();
    }
    public void setOrder(@Nullable dev.vcgroup.thonowandroidapp.data.model.Order Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        dev.vcgroup.thonowandroidapp.data.model.OrderStatus orderStatus = null;
        java.lang.String orderWorkingAddressAddress = null;
        java.lang.String orderStatusTitle = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String orderId = null;
        float workerRating = 0f;
        java.lang.String serviceTypeDrawableName = null;
        java.lang.String javaLangStringOrderId = null;
        dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType = mServiceType;
        java.lang.String serviceTypeName = null;
        java.lang.String workerDisplayName = null;
        java.util.Date orderWorkingDate = null;
        dev.vcgroup.thonowandroidapp.data.model.Address orderWorkingAddress = null;
        dev.vcgroup.thonowandroidapp.data.model.Order order = mOrder;
        java.lang.String orderStatusColor = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.rating
                    workerRating = worker.getRating();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        if ((dirtyFlags & 0xaL) != 0) {



                if (serviceType != null) {
                    // read serviceType.drawableName
                    serviceTypeDrawableName = serviceType.getDrawableName();
                    // read serviceType.name
                    serviceTypeName = serviceType.getName();
                }
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (order != null) {
                    // read order.status
                    orderStatus = order.getStatus();
                    // read order.id
                    orderId = order.getId();
                    // read order.workingDate
                    orderWorkingDate = order.getWorkingDate();
                    // read order.workingAddress
                    orderWorkingAddress = order.getWorkingAddress();
                }


                if (orderStatus != null) {
                    // read order.status.title
                    orderStatusTitle = orderStatus.getTitle();
                    // read order.status.color
                    orderStatusColor = orderStatus.getColor();
                }
                // read ("#") + (order.id)
                javaLangStringOrderId = ("#") + (orderId);
                if (orderWorkingAddress != null) {
                    // read order.workingAddress.address
                    orderWorkingAddressAddress = orderWorkingAddress.getAddress();
                }
        }
        // batch finished
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setStrokeColor(this.cardview, orderStatusColor);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.parseColor(this.mboundView1, orderStatusColor);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.formatStartDate(this.mboundView10, orderWorkingDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, orderStatusTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, javaLangStringOrderId);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView10, orderWorkingAddressAddress);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, workerDisplayName);
            androidx.databinding.adapters.RatingBarBindingAdapter.setRating(this.ratingBar, workerRating);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.roundedImageView5, workerPhotoUrl);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadDrawableFromName(this.specialtyIcon, serviceTypeDrawableName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.specialtyName, serviceTypeName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): serviceType
        flag 2 (0x3L): order
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}