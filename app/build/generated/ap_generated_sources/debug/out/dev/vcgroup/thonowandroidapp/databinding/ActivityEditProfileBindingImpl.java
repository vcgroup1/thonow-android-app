package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityEditProfileBindingImpl extends ActivityEditProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.profile_default_cover, 7);
        sViewsWithIds.put(R.id.btnChangeProfilePicture, 8);
        sViewsWithIds.put(R.id.materialCardView, 9);
        sViewsWithIds.put(R.id.linearLayout3, 10);
        sViewsWithIds.put(R.id.tipName, 11);
        sViewsWithIds.put(R.id.btnGoToLogin, 12);
        sViewsWithIds.put(R.id.tipAddress, 13);
        sViewsWithIds.put(R.id.drop_down_country, 14);
        sViewsWithIds.put(R.id.tipEmail, 15);
        sViewsWithIds.put(R.id.appbar, 16);
        sViewsWithIds.put(R.id.mToolbar, 17);
        sViewsWithIds.put(R.id.btnSaveProfile, 18);
    }
    // views
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    @NonNull
    private final android.widget.ImageView mboundView4;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of item.currentUser.getValue().phoneNumber
            //         is item.currentUser.getValue().setPhoneNumber((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // item.currentUser.getValue()
            dev.vcgroup.thonowandroidapp.data.model.User itemCurrentUserGetValue = null;
            // item.currentUser.getValue().phoneNumber
            java.lang.String itemCurrentUserPhoneNumber = null;
            // item.currentUser.getValue() != null
            boolean itemCurrentUserGetValueJavaLangObjectNull = false;
            // item
            dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel item = mItem;
            // item.currentUser != null
            boolean itemCurrentUserJavaLangObjectNull = false;
            // item.currentUser
            androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> itemCurrentUser = null;
            // item != null
            boolean itemJavaLangObjectNull = false;



            itemJavaLangObjectNull = (item) != (null);
            if (itemJavaLangObjectNull) {


                itemCurrentUser = item.getCurrentUser();

                itemCurrentUserJavaLangObjectNull = (itemCurrentUser) != (null);
                if (itemCurrentUserJavaLangObjectNull) {


                    itemCurrentUserGetValue = itemCurrentUser.getValue();

                    itemCurrentUserGetValueJavaLangObjectNull = (itemCurrentUserGetValue) != (null);
                    if (itemCurrentUserGetValueJavaLangObjectNull) {




                        itemCurrentUserGetValue.setPhoneNumber(((java.lang.String) (callbackArg_0)));
                    }
                }
            }
        }
    };

    public ActivityEditProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private ActivityEditProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (com.google.android.material.appbar.AppBarLayout) bindings[16]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.FrameLayout) bindings[12]
            , (com.google.android.material.button.MaterialButton) bindings[18]
            , (android.widget.LinearLayout) bindings[14]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.LinearLayout) bindings[10]
            , (androidx.appcompat.widget.Toolbar) bindings[17]
            , (com.google.android.material.card.MaterialCardView) bindings[9]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[7]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (android.widget.TextView) bindings[5]
            );
        this.editProfileContainer.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.ImageView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (com.google.android.material.textfield.TextInputEditText) bindings[6];
        this.mboundView6.setTag(null);
        this.profileAvatar.setTag(null);
        this.tvPrefix.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemCurrentUser((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User>) object, fieldId);
            case 1 :
                return onChangeItemSelectedCountryCode((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemCurrentUser(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> ItemCurrentUser, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemSelectedCountryCode(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo> ItemSelectedCountryCode, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel item = mItem;
        java.lang.String charItemSelectedCountryCodeCallingCode = null;
        dev.vcgroup.thonowandroidapp.data.model.User itemCurrentUserGetValue = null;
        java.lang.String itemSelectedCountryCodeAlp2Code = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> itemCurrentUser = null;
        java.lang.String itemSelectedCountryCodeCallingCode = null;
        java.lang.String itemCurrentUserPhoneNumber = null;
        java.lang.String itemCurrentUserEmail = null;
        java.lang.String itemCurrentUserDisplayName = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo> itemSelectedCountryCode = null;
        dev.vcgroup.thonowandroidapp.data.model.CountryInfo itemSelectedCountryCodeGetValue = null;
        java.lang.String itemCurrentUserPhotoUrl = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (item != null) {
                        // read item.currentUser
                        itemCurrentUser = item.getCurrentUser();
                    }
                    updateLiveDataRegistration(0, itemCurrentUser);


                    if (itemCurrentUser != null) {
                        // read item.currentUser.getValue()
                        itemCurrentUserGetValue = itemCurrentUser.getValue();
                    }


                    if (itemCurrentUserGetValue != null) {
                        // read item.currentUser.getValue().phoneNumber
                        itemCurrentUserPhoneNumber = itemCurrentUserGetValue.getPhoneNumber();
                        // read item.currentUser.getValue().email
                        itemCurrentUserEmail = itemCurrentUserGetValue.getEmail();
                        // read item.currentUser.getValue().displayName
                        itemCurrentUserDisplayName = itemCurrentUserGetValue.getDisplayName();
                        // read item.currentUser.getValue().photoUrl
                        itemCurrentUserPhotoUrl = itemCurrentUserGetValue.getPhotoUrl();
                    }
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (item != null) {
                        // read item.selectedCountryCode
                        itemSelectedCountryCode = item.getSelectedCountryCode();
                    }
                    updateLiveDataRegistration(1, itemSelectedCountryCode);


                    if (itemSelectedCountryCode != null) {
                        // read item.selectedCountryCode.getValue()
                        itemSelectedCountryCodeGetValue = itemSelectedCountryCode.getValue();
                    }


                    if (itemSelectedCountryCodeGetValue != null) {
                        // read item.selectedCountryCode.getValue().alp2Code
                        itemSelectedCountryCodeAlp2Code = itemSelectedCountryCodeGetValue.getAlp2Code();
                        // read item.selectedCountryCode.getValue().callingCode
                        itemSelectedCountryCodeCallingCode = itemSelectedCountryCodeGetValue.getCallingCode();
                    }


                    // read ('+') + (item.selectedCountryCode.getValue().callingCode)
                    charItemSelectedCountryCodeCallingCode = ('+') + (itemSelectedCountryCodeCallingCode);
            }
        }
        // batch finished
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, itemCurrentUserDisplayName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, itemCurrentUserPhoneNumber);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, itemCurrentUserEmail);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.profileAvatar, itemCurrentUserPhotoUrl);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadCountryIcon(this.mboundView4, itemSelectedCountryCodeAlp2Code);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPrefix, charItemSelectedCountryCodeCallingCode);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item.currentUser
        flag 1 (0x2L): item.selectedCountryCode
        flag 2 (0x3L): item
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}