package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileBindingImpl extends FragmentProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.top_app_bar, 4);
        sViewsWithIds.put(R.id.fragment_profile_scrollview, 5);
        sViewsWithIds.put(R.id.goToEditProfile, 6);
        sViewsWithIds.put(R.id.imageView2, 7);
        sViewsWithIds.put(R.id.job_history_section, 8);
        sViewsWithIds.put(R.id.transaction_history_section, 9);
        sViewsWithIds.put(R.id.favorite_worker_section, 10);
        sViewsWithIds.put(R.id.rating_section, 11);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.cardview.widget.CardView) bindings[10]
            , (androidx.core.widget.NestedScrollView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (android.widget.ImageView) bindings[7]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[2]
            , (androidx.cardview.widget.CardView) bindings[8]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (bindings[4] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialToolbarBinding.bind((android.view.View) bindings[4]) : null
            , (androidx.cardview.widget.CardView) bindings[9]
            , (android.widget.TextView) bindings[3]
            );
        this.ivAvatar.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.tvDisplayName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemCurrentUser((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemCurrentUser(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> ItemCurrentUser, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.ui.profile.ProfileViewModel item = mItem;
        boolean itemCurrentUserDisplayNameEmpty = false;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> itemCurrentUser = null;
        dev.vcgroup.thonowandroidapp.data.model.User itemCurrentUserGetValue = null;
        java.lang.String itemCurrentUserPhoneNumber = null;
        java.lang.String itemCurrentUserDisplayNameEmptyItemCurrentUserPhoneNumberItemCurrentUserDisplayName = null;
        java.lang.String itemCurrentUserDisplayName = null;
        java.lang.String itemCurrentUserPhotoUrl = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (item != null) {
                    // read item.currentUser
                    itemCurrentUser = item.getCurrentUser();
                }
                updateLiveDataRegistration(0, itemCurrentUser);


                if (itemCurrentUser != null) {
                    // read item.currentUser.getValue()
                    itemCurrentUserGetValue = itemCurrentUser.getValue();
                }


                if (itemCurrentUserGetValue != null) {
                    // read item.currentUser.getValue().displayName
                    itemCurrentUserDisplayName = itemCurrentUserGetValue.getDisplayName();
                    // read item.currentUser.getValue().photoUrl
                    itemCurrentUserPhotoUrl = itemCurrentUserGetValue.getPhotoUrl();
                }


                if (itemCurrentUserDisplayName != null) {
                    // read item.currentUser.getValue().displayName.empty
                    itemCurrentUserDisplayNameEmpty = itemCurrentUserDisplayName.isEmpty();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(itemCurrentUserDisplayNameEmpty) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x10L) != 0) {

                if (itemCurrentUserGetValue != null) {
                    // read item.currentUser.getValue().phoneNumber
                    itemCurrentUserPhoneNumber = itemCurrentUserGetValue.getPhoneNumber();
                }
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read item.currentUser.getValue().displayName.empty ? item.currentUser.getValue().phoneNumber : item.currentUser.getValue().displayName
                itemCurrentUserDisplayNameEmptyItemCurrentUserPhoneNumberItemCurrentUserDisplayName = ((itemCurrentUserDisplayNameEmpty) ? (itemCurrentUserPhoneNumber) : (itemCurrentUserDisplayName));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.ivAvatar, itemCurrentUserPhotoUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDisplayName, itemCurrentUserDisplayNameEmptyItemCurrentUserPhoneNumberItemCurrentUserDisplayName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item.currentUser
        flag 1 (0x2L): item
        flag 2 (0x3L): null
        flag 3 (0x4L): item.currentUser.getValue().displayName.empty ? item.currentUser.getValue().phoneNumber : item.currentUser.getValue().displayName
        flag 4 (0x5L): item.currentUser.getValue().displayName.empty ? item.currentUser.getValue().phoneNumber : item.currentUser.getValue().displayName
    flag mapping end*/
    //end
}