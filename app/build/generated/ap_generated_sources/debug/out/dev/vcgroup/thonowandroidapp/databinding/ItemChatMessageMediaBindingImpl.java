package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemChatMessageMediaBindingImpl extends ItemChatMessageMediaBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(3);
        sIncludes.setIncludes(0, 
            new String[] {"item_chat_message_left_media", "item_chat_message_right_media"},
            new int[] {1, 2},
            new int[] {dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_left_media,
                dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_right_media});
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemChatMessageMediaBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemChatMessageMediaBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftMediaBinding) bindings[1]
            , (dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightMediaBinding) bindings[2]
            );
        setContainedBinding(this.itemChatLeftMedia);
        setContainedBinding(this.itemChatRightMedia);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        itemChatLeftMedia.invalidateAll();
        itemChatRightMedia.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (itemChatLeftMedia.hasPendingBindings()) {
            return true;
        }
        if (itemChatRightMedia.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.message == variableId) {
            setMessage((dev.vcgroup.thonowandroidapp.data.model.chat.Message) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMessage(@Nullable dev.vcgroup.thonowandroidapp.data.model.chat.Message Message) {
        this.mMessage = Message;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.message);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        itemChatLeftMedia.setLifecycleOwner(lifecycleOwner);
        itemChatRightMedia.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemChatLeftMedia((dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftMediaBinding) object, fieldId);
            case 1 :
                return onChangeItemChatRightMedia((dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightMediaBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemChatLeftMedia(dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftMediaBinding ItemChatLeftMedia, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemChatRightMedia(dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightMediaBinding ItemChatRightMedia, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.data.model.chat.Message message = mMessage;

        if ((dirtyFlags & 0xcL) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            this.itemChatLeftMedia.setMessage(message);
            this.itemChatRightMedia.setMessage(message);
        }
        executeBindingsOn(itemChatLeftMedia);
        executeBindingsOn(itemChatRightMedia);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemChatLeftMedia
        flag 1 (0x2L): itemChatRightMedia
        flag 2 (0x3L): message
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}