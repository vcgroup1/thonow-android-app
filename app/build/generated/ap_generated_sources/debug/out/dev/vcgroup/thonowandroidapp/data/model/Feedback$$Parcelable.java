
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Feedback$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.Feedback>
{

    private dev.vcgroup.thonowandroidapp.data.model.Feedback feedback$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Feedback$$Parcelable>CREATOR = new Creator<Feedback$$Parcelable>() {


        @Override
        public Feedback$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Feedback$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Feedback$$Parcelable[] newArray(int size) {
            return new Feedback$$Parcelable[size] ;
        }

    }
    ;

    public Feedback$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.Feedback feedback$$2) {
        feedback$$0 = feedback$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(feedback$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.Feedback feedback$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(feedback$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(feedback$$1));
            parcel$$1 .writeString(feedback$$1 .review);
            parcel$$1 .writeFloat(feedback$$1 .rating);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.Feedback getParcel() {
        return feedback$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.Feedback read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.Feedback feedback$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            feedback$$4 = new dev.vcgroup.thonowandroidapp.data.model.Feedback();
            identityMap$$1 .put(reservation$$0, feedback$$4);
            feedback$$4 .review = parcel$$3 .readString();
            feedback$$4 .rating = parcel$$3 .readFloat();
            dev.vcgroup.thonowandroidapp.data.model.Feedback feedback$$3 = feedback$$4;
            identityMap$$1 .put(identity$$1, feedback$$3);
            return feedback$$3;
        }
    }

}
