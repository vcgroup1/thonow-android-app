package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemOrderDetailServiceBindingImpl extends ItemOrderDetailServiceBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemOrderDetailServiceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ItemOrderDetailServiceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[1]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.textView14.setTag(null);
        this.textView8.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.orderDetail == variableId) {
            setOrderDetail((dev.vcgroup.thonowandroidapp.data.model.OrderDetail) variable);
        }
        else if (BR.service == variableId) {
            setService((dev.vcgroup.thonowandroidapp.data.model.Service) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOrderDetail(@Nullable dev.vcgroup.thonowandroidapp.data.model.OrderDetail OrderDetail) {
        this.mOrderDetail = OrderDetail;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.orderDetail);
        super.requestRebind();
    }
    public void setService(@Nullable dev.vcgroup.thonowandroidapp.data.model.Service Service) {
        this.mService = Service;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.service);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String orderDetailQuantityJavaLangString = null;
        double orderDetailUnitPrice = 0.0;
        dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail = mOrderDetail;
        java.lang.String commonUtilConvertCurrencyTextOrderDetailUnitPriceJavaLangString = null;
        java.lang.String serviceUnit = null;
        java.lang.String serviceName = null;
        java.lang.String orderDetailQuantityJavaLangStringServiceUnit = null;
        int orderDetailQuantity = 0;
        dev.vcgroup.thonowandroidapp.data.model.Service service = mService;
        java.lang.String commonUtilConvertCurrencyTextOrderDetailUnitPrice = null;

        if ((dirtyFlags & 0x7L) != 0) {


            if ((dirtyFlags & 0x5L) != 0) {

                    if (orderDetail != null) {
                        // read orderDetail.unitPrice
                        orderDetailUnitPrice = orderDetail.getUnitPrice();
                    }


                    // read CommonUtil.convertCurrencyText(orderDetail.unitPrice)
                    commonUtilConvertCurrencyTextOrderDetailUnitPrice = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderDetailUnitPrice);


                    // read (CommonUtil.convertCurrencyText(orderDetail.unitPrice)) + (" đ")
                    commonUtilConvertCurrencyTextOrderDetailUnitPriceJavaLangString = (commonUtilConvertCurrencyTextOrderDetailUnitPrice) + (" đ");
            }

                if (orderDetail != null) {
                    // read orderDetail.quantity
                    orderDetailQuantity = orderDetail.getQuantity();
                }
                if (service != null) {
                    // read service.unit
                    serviceUnit = service.getUnit();
                }


                // read (orderDetail.quantity) + (" ")
                orderDetailQuantityJavaLangString = (orderDetailQuantity) + (" ");


                // read ((orderDetail.quantity) + (" ")) + (service.unit)
                orderDetailQuantityJavaLangStringServiceUnit = (orderDetailQuantityJavaLangString) + (serviceUnit);
            if ((dirtyFlags & 0x6L) != 0) {

                    if (service != null) {
                        // read service.name
                        serviceName = service.getName();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, orderDetailQuantityJavaLangStringServiceUnit);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView14, commonUtilConvertCurrencyTextOrderDetailUnitPriceJavaLangString);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView8, serviceName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): orderDetail
        flag 1 (0x2L): service
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}