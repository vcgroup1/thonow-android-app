package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityOrderDetailBindingImpl extends ActivityOrderDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.top_app_bar, 15);
        sViewsWithIds.put(R.id.view6, 16);
        sViewsWithIds.put(R.id.textView8, 17);
        sViewsWithIds.put(R.id.rv_order_images, 18);
        sViewsWithIds.put(R.id.rv_order_details, 19);
        sViewsWithIds.put(R.id.btn_accept_order, 20);
        sViewsWithIds.put(R.id.btn_cancel_order, 21);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView10;
    @NonNull
    private final android.widget.TextView mboundView11;
    @NonNull
    private final android.widget.TextView mboundView12;
    @NonNull
    private final android.widget.TextView mboundView13;
    @NonNull
    private final android.widget.TextView mboundView14;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    @NonNull
    private final android.widget.TextView mboundView6;
    @NonNull
    private final android.widget.TextView mboundView7;
    @NonNull
    private final android.widget.TextView mboundView8;
    @NonNull
    private final android.widget.TextView mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityOrderDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private ActivityOrderDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[20]
            , (com.google.android.material.button.MaterialButton) bindings[21]
            , (androidx.recyclerview.widget.RecyclerView) bindings[19]
            , (androidx.recyclerview.widget.RecyclerView) bindings[18]
            , (android.widget.TextView) bindings[17]
            , (bindings[15] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialToolbarBinding.bind((android.view.View) bindings[15]) : null
            , (android.view.View) bindings[16]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (android.widget.TextView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView11 = (android.widget.TextView) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.widget.TextView) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView13 = (android.widget.TextView) bindings[13];
        this.mboundView13.setTag(null);
        this.mboundView14 = (android.widget.TextView) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (android.widget.TextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.TextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.TextView) bindings[9];
        this.mboundView9.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else if (BR.service == variableId) {
            setService((dev.vcgroup.thonowandroidapp.data.model.Service) variable);
        }
        else if (BR.order == variableId) {
            setOrder((dev.vcgroup.thonowandroidapp.data.model.Order) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }
    public void setService(@Nullable dev.vcgroup.thonowandroidapp.data.model.Service Service) {
        this.mService = Service;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.service);
        super.requestRebind();
    }
    public void setOrder(@Nullable dev.vcgroup.thonowandroidapp.data.model.Order Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.data.model.OrderStatus orderStatus = null;
        double orderMovingExpense = 0.0;
        java.lang.String commonUtilConvertCurrencyTextOrderOvertimeFeeJavaLangString = null;
        java.lang.String orderNote = null;
        java.lang.String workerPhoneNumber = null;
        java.lang.String orderOrderDetailsSizeJavaLangString = null;
        java.lang.String serviceName = null;
        java.lang.String orderWorkingAddressAddress = null;
        java.lang.String commonUtilConvertCurrencyTextOrderTotal = null;
        java.lang.String orderStatusTitle = null;
        java.lang.String commonUtilConvertCurrencyTextOrderEstimatedFeeJavaLangString = null;
        java.lang.String commonUtilConvertCurrencyTextOrderMovingExpense = null;
        java.lang.String orderTypeTitle = null;
        java.util.List<dev.vcgroup.thonowandroidapp.data.model.OrderDetail> orderOrderDetails = null;
        int orderOrderDetailsSize = 0;
        java.lang.String commonUtilConvertCurrencyTextOrderMovingExpenseJavaLangString = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String orderId = null;
        dev.vcgroup.thonowandroidapp.data.model.OrderType orderType = null;
        java.lang.String commonUtilConvertCurrencyTextOrderOvertimeFee = null;
        dev.vcgroup.thonowandroidapp.data.model.Service service = mService;
        double orderOvertimeFee = 0.0;
        java.lang.String commonUtilConvertCurrencyTextOrderTotalJavaLangString = null;
        java.lang.String javaLangStringOrderId = null;
        double orderTotal = 0.0;
        java.lang.String workerDisplayName = null;
        java.lang.String commonUtilConvertCurrencyTextOrderEstimatedFee = null;
        java.util.Date orderWorkingDate = null;
        double orderEstimatedFee = 0.0;
        java.lang.String orderOrderDetailsJavaLangObjectNullJavaLangString0OrderOrderDetailsSizeJavaLangString = null;
        dev.vcgroup.thonowandroidapp.data.model.Address orderWorkingAddress = null;
        boolean orderOrderDetailsJavaLangObjectNull = false;
        dev.vcgroup.thonowandroidapp.data.model.Order order = mOrder;
        java.lang.String orderStatusColor = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (worker != null) {
                    // read worker.phoneNumber
                    workerPhoneNumber = worker.getPhoneNumber();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        if ((dirtyFlags & 0xaL) != 0) {



                if (service != null) {
                    // read service.name
                    serviceName = service.getName();
                }
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (order != null) {
                    // read order.status
                    orderStatus = order.getStatus();
                    // read order.movingExpense
                    orderMovingExpense = order.getMovingExpense();
                    // read order.note
                    orderNote = order.getNote();
                    // read order.orderDetails
                    orderOrderDetails = order.getOrderDetails();
                    // read order.id
                    orderId = order.getId();
                    // read order.type
                    orderType = order.getType();
                    // read order.overtimeFee
                    orderOvertimeFee = order.getOvertimeFee();
                    // read order.total
                    orderTotal = order.getTotal();
                    // read order.workingDate
                    orderWorkingDate = order.getWorkingDate();
                    // read order.estimatedFee
                    orderEstimatedFee = order.getEstimatedFee();
                    // read order.workingAddress
                    orderWorkingAddress = order.getWorkingAddress();
                }


                if (orderStatus != null) {
                    // read order.status.title
                    orderStatusTitle = orderStatus.getTitle();
                    // read order.status.color
                    orderStatusColor = orderStatus.getColor();
                }
                // read CommonUtil.convertCurrencyText(order.movingExpense)
                commonUtilConvertCurrencyTextOrderMovingExpense = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderMovingExpense);
                // read order.orderDetails == null
                orderOrderDetailsJavaLangObjectNull = (orderOrderDetails) == (null);
                // read ("#") + (order.id)
                javaLangStringOrderId = ("#") + (orderId);
                // read CommonUtil.convertCurrencyText(order.overtimeFee)
                commonUtilConvertCurrencyTextOrderOvertimeFee = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderOvertimeFee);
                // read CommonUtil.convertCurrencyText(order.total)
                commonUtilConvertCurrencyTextOrderTotal = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderTotal);
                // read CommonUtil.convertCurrencyText(order.estimatedFee)
                commonUtilConvertCurrencyTextOrderEstimatedFee = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderEstimatedFee);
            if((dirtyFlags & 0xcL) != 0) {
                if(orderOrderDetailsJavaLangObjectNull) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (orderType != null) {
                    // read order.type.title
                    orderTypeTitle = orderType.getTitle();
                }
                if (orderWorkingAddress != null) {
                    // read order.workingAddress.address
                    orderWorkingAddressAddress = orderWorkingAddress.getAddress();
                }


                // read (CommonUtil.convertCurrencyText(order.movingExpense)) + (" đ")
                commonUtilConvertCurrencyTextOrderMovingExpenseJavaLangString = (commonUtilConvertCurrencyTextOrderMovingExpense) + (" đ");
                // read (CommonUtil.convertCurrencyText(order.overtimeFee)) + (" đ")
                commonUtilConvertCurrencyTextOrderOvertimeFeeJavaLangString = (commonUtilConvertCurrencyTextOrderOvertimeFee) + (" đ");
                // read (CommonUtil.convertCurrencyText(order.total)) + (" đ")
                commonUtilConvertCurrencyTextOrderTotalJavaLangString = (commonUtilConvertCurrencyTextOrderTotal) + (" đ");
                // read (CommonUtil.convertCurrencyText(order.estimatedFee)) + (" đ")
                commonUtilConvertCurrencyTextOrderEstimatedFeeJavaLangString = (commonUtilConvertCurrencyTextOrderEstimatedFee) + (" đ");
        }
        // batch finished

        if ((dirtyFlags & 0x10L) != 0) {

                if (orderOrderDetails != null) {
                    // read order.orderDetails.size()
                    orderOrderDetailsSize = orderOrderDetails.size();
                }


                // read (order.orderDetails.size()) + ("")
                orderOrderDetailsSizeJavaLangString = (orderOrderDetailsSize) + ("");
        }

        if ((dirtyFlags & 0xcL) != 0) {

                // read order.orderDetails == null ? "0" : (order.orderDetails.size()) + ("")
                orderOrderDetailsJavaLangObjectNullJavaLangString0OrderOrderDetailsSizeJavaLangString = ((orderOrderDetailsJavaLangObjectNull) ? ("0") : (orderOrderDetailsSizeJavaLangString));
        }
        // batch finished
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, orderStatusTitle);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setBackgroundTintList(this.mboundView1, orderStatusColor);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView10, orderNote);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView11, commonUtilConvertCurrencyTextOrderEstimatedFeeJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView12, commonUtilConvertCurrencyTextOrderMovingExpenseJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView13, commonUtilConvertCurrencyTextOrderOvertimeFeeJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView14, commonUtilConvertCurrencyTextOrderTotalJavaLangString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, javaLangStringOrderId);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, orderTypeTitle);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.formatStartDate(this.mboundView5, orderWorkingDate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, orderWorkingAddressAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, orderOrderDetailsJavaLangObjectNullJavaLangString0OrderOrderDetailsSizeJavaLangString);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, serviceName);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, workerDisplayName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, workerPhoneNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): service
        flag 2 (0x3L): order
        flag 3 (0x4L): null
        flag 4 (0x5L): order.orderDetails == null ? "0" : (order.orderDetails.size()) + ("")
        flag 5 (0x6L): order.orderDetails == null ? "0" : (order.orderDetails.size()) + ("")
    flag mapping end*/
    //end
}