package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BsdConfirmRequestCallWorkerBindingImpl extends BsdConfirmRequestCallWorkerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView3, 8);
        sViewsWithIds.put(R.id.btn_confirm_request_call_back, 9);
        sViewsWithIds.put(R.id.tipNote, 10);
        sViewsWithIds.put(R.id.tv_estimated_fee, 11);
        sViewsWithIds.put(R.id.btnFindWorker, 12);
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    @NonNull
    private final android.widget.TextView mboundView6;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView7;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public BsdConfirmRequestCallWorkerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private BsdConfirmRequestCallWorkerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (com.google.android.material.button.MaterialButton) bindings[9]
            , (com.google.android.material.button.MaterialButton) bindings[12]
            , (android.widget.LinearLayout) bindings[0]
            , (android.widget.ImageView) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (android.widget.TextView) bindings[11]
            );
        this.confirmCallWorkerBottomSheet.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (com.google.android.material.textfield.TextInputEditText) bindings[7];
        this.mboundView7.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemCurrentAddress((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address>) object, fieldId);
            case 1 :
                return onChangeItemSelectedService((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service>) object, fieldId);
            case 2 :
                return onChangeItemCustomer((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User>) object, fieldId);
            case 3 :
                return onChangeItemJobQuatitiy((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 4 :
                return onChangeItemNote((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeItemCurrentOrderType((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemCurrentAddress(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address> ItemCurrentAddress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemSelectedService(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service> ItemSelectedService, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemCustomer(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> ItemCustomer, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemJobQuatitiy(androidx.lifecycle.MutableLiveData<java.lang.Integer> ItemJobQuatitiy, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemNote(androidx.lifecycle.MutableLiveData<java.lang.String> ItemNote, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemCurrentOrderType(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType> ItemCurrentOrderType, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel item = mItem;
        java.lang.Integer itemJobQuatitiyGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxItemJobQuatitiyGetValue = 0;
        java.lang.String stringValueOfItemJobQuatitiy = null;
        boolean itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT = false;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address> itemCurrentAddress = null;
        java.lang.String itemCustomerDisplayName = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service> itemSelectedService = null;
        java.lang.String itemSelectedServiceName = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.User> itemCustomer = null;
        java.lang.String itemCurrentAddressAddress = null;
        java.lang.String itemCustomerPhoneNumber = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> itemJobQuatitiy = null;
        java.lang.String itemNoteGetValue = null;
        dev.vcgroup.thonowandroidapp.data.model.Address itemCurrentAddressGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> itemNote = null;
        dev.vcgroup.thonowandroidapp.data.model.User itemCustomerGetValue = null;
        java.lang.String itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTJavaLangStringTLChJavaLangStringGINhanh = null;
        dev.vcgroup.thonowandroidapp.data.model.OrderType itemCurrentOrderTypeGetValue = null;
        dev.vcgroup.thonowandroidapp.data.model.Service itemSelectedServiceGetValue = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType> itemCurrentOrderType = null;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0xc1L) != 0) {

                    if (item != null) {
                        // read item.currentAddress
                        itemCurrentAddress = item.getCurrentAddress();
                    }
                    updateLiveDataRegistration(0, itemCurrentAddress);


                    if (itemCurrentAddress != null) {
                        // read item.currentAddress.getValue()
                        itemCurrentAddressGetValue = itemCurrentAddress.getValue();
                    }


                    if (itemCurrentAddressGetValue != null) {
                        // read item.currentAddress.getValue().address
                        itemCurrentAddressAddress = itemCurrentAddressGetValue.getAddress();
                    }
            }
            if ((dirtyFlags & 0xc2L) != 0) {

                    if (item != null) {
                        // read item.selectedService
                        itemSelectedService = item.getSelectedService();
                    }
                    updateLiveDataRegistration(1, itemSelectedService);


                    if (itemSelectedService != null) {
                        // read item.selectedService.getValue()
                        itemSelectedServiceGetValue = itemSelectedService.getValue();
                    }


                    if (itemSelectedServiceGetValue != null) {
                        // read item.selectedService.getValue().name
                        itemSelectedServiceName = itemSelectedServiceGetValue.getName();
                    }
            }
            if ((dirtyFlags & 0xc4L) != 0) {

                    if (item != null) {
                        // read item.customer
                        itemCustomer = item.getCustomer();
                    }
                    updateLiveDataRegistration(2, itemCustomer);


                    if (itemCustomer != null) {
                        // read item.customer.getValue()
                        itemCustomerGetValue = itemCustomer.getValue();
                    }


                    if (itemCustomerGetValue != null) {
                        // read item.customer.getValue().displayName
                        itemCustomerDisplayName = itemCustomerGetValue.getDisplayName();
                        // read item.customer.getValue().phoneNumber
                        itemCustomerPhoneNumber = itemCustomerGetValue.getPhoneNumber();
                    }
            }
            if ((dirtyFlags & 0xc8L) != 0) {

                    if (item != null) {
                        // read item.jobQuatitiy
                        itemJobQuatitiy = item.getJobQuatitiy();
                    }
                    updateLiveDataRegistration(3, itemJobQuatitiy);


                    if (itemJobQuatitiy != null) {
                        // read item.jobQuatitiy.getValue()
                        itemJobQuatitiyGetValue = itemJobQuatitiy.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(item.jobQuatitiy.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxItemJobQuatitiyGetValue = androidx.databinding.ViewDataBinding.safeUnbox(itemJobQuatitiyGetValue);


                    // read String.valueOf(androidx.databinding.ViewDataBinding.safeUnbox(item.jobQuatitiy.getValue()))
                    stringValueOfItemJobQuatitiy = java.lang.String.valueOf(androidxDatabindingViewDataBindingSafeUnboxItemJobQuatitiyGetValue);
            }
            if ((dirtyFlags & 0xd0L) != 0) {

                    if (item != null) {
                        // read item.note
                        itemNote = item.getNote();
                    }
                    updateLiveDataRegistration(4, itemNote);


                    if (itemNote != null) {
                        // read item.note.getValue()
                        itemNoteGetValue = itemNote.getValue();
                    }
            }
            if ((dirtyFlags & 0xe0L) != 0) {

                    if (item != null) {
                        // read item.currentOrderType
                        itemCurrentOrderType = item.getCurrentOrderType();
                    }
                    updateLiveDataRegistration(5, itemCurrentOrderType);


                    if (itemCurrentOrderType != null) {
                        // read item.currentOrderType.getValue()
                        itemCurrentOrderTypeGetValue = itemCurrentOrderType.getValue();
                    }


                    if (itemCurrentOrderTypeGetValue != null) {
                        // read item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT)
                        itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT = itemCurrentOrderTypeGetValue.equals(dev.vcgroup.thonowandroidapp.data.model.OrderType.MAKE_AN_APPOINTMENT);
                    }
                if((dirtyFlags & 0xe0L) != 0) {
                    if(itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? "Đặt lịch" : "Gọi nhanh"
                    itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTJavaLangStringTLChJavaLangStringGINhanh = ((itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT) ? ("Đặt lịch") : ("Gọi nhanh"));
            }
        }
        // batch finished
        if ((dirtyFlags & 0xc4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, itemCustomerDisplayName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, itemCustomerPhoneNumber);
        }
        if ((dirtyFlags & 0xc2L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, itemSelectedServiceName);
        }
        if ((dirtyFlags & 0xc8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, stringValueOfItemJobQuatitiy);
        }
        if ((dirtyFlags & 0xe0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTJavaLangStringTLChJavaLangStringGINhanh);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, itemCurrentAddressAddress);
        }
        if ((dirtyFlags & 0xd0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, itemNoteGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item.currentAddress
        flag 1 (0x2L): item.selectedService
        flag 2 (0x3L): item.customer
        flag 3 (0x4L): item.jobQuatitiy
        flag 4 (0x5L): item.note
        flag 5 (0x6L): item.currentOrderType
        flag 6 (0x7L): item
        flag 7 (0x8L): null
        flag 8 (0x9L): item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? "Đặt lịch" : "Gọi nhanh"
        flag 9 (0xaL): item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? "Đặt lịch" : "Gọi nhanh"
    flag mapping end*/
    //end
}