package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCustomToastBindingImpl extends ItemCustomToastBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCustomToastBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemCustomToastBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageButton) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            );
        this.ibnCloseCustomToast.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.txtMessCustomToast.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.toastItem == variableId) {
            setToastItem((dev.vcgroup.thonowandroidapp.util.VassCustomToast.ToastItem) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setToastItem(@Nullable dev.vcgroup.thonowandroidapp.util.VassCustomToast.ToastItem ToastItem) {
        this.mToastItem = ToastItem;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.toastItem);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String toastItemTypePrimaryColor = null;
        dev.vcgroup.thonowandroidapp.util.VassCustomToast.ToastItem toastItem = mToastItem;
        dev.vcgroup.thonowandroidapp.util.VassCustomToast.ToastType toastItemType = null;
        java.lang.String toastItemMsg = null;
        java.lang.String toastItemTypeSecondaryColor = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (toastItem != null) {
                    // read toastItem.type
                    toastItemType = toastItem.getType();
                    // read toastItem.msg
                    toastItemMsg = toastItem.getMsg();
                }


                if (toastItemType != null) {
                    // read toastItem.type.primaryColor
                    toastItemTypePrimaryColor = toastItemType.getPrimaryColor();
                    // read toastItem.type.secondaryColor
                    toastItemTypeSecondaryColor = toastItemType.getSecondaryColor();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setTint(this.ibnCloseCustomToast, toastItemTypePrimaryColor);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setBackgroundTintList(this.mboundView0, toastItemTypeSecondaryColor);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.txtMessCustomToast, toastItemMsg);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): toastItem
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}