
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class OrderDetail$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.OrderDetail>
{

    private dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<OrderDetail$$Parcelable>CREATOR = new Creator<OrderDetail$$Parcelable>() {


        @Override
        public OrderDetail$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new OrderDetail$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public OrderDetail$$Parcelable[] newArray(int size) {
            return new OrderDetail$$Parcelable[size] ;
        }

    }
    ;

    public OrderDetail$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$2) {
        orderDetail$$0 = orderDetail$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(orderDetail$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(orderDetail$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(orderDetail$$1));
            parcel$$1 .writeDouble(orderDetail$$1 .unitPrice);
            parcel$$1 .writeInt(orderDetail$$1 .quantity);
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(orderDetail$$1 .service, parcel$$1);
            parcel$$1 .writeDouble(orderDetail$$1 .subtotal);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.OrderDetail getParcel() {
        return orderDetail$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.OrderDetail read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            orderDetail$$4 = new dev.vcgroup.thonowandroidapp.data.model.OrderDetail();
            identityMap$$1 .put(reservation$$0, orderDetail$$4);
            orderDetail$$4 .unitPrice = parcel$$3 .readDouble();
            orderDetail$$4 .quantity = parcel$$3 .readInt();
            orderDetail$$4 .service = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            orderDetail$$4 .subtotal = parcel$$3 .readDouble();
            dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$3 = orderDetail$$4;
            identityMap$$1 .put(identity$$1, orderDetail$$3);
            return orderDetail$$3;
        }
    }

}
