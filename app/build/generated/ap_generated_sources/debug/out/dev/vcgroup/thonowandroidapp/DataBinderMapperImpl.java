package dev.vcgroup.thonowandroidapp;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import dev.vcgroup.thonowandroidapp.databinding.ActivityAskPermissionBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCallWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCancelOrderBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityChatBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityConfirmationOfPaymentBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityCreateAPaymentBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityEditProfileBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityFavoriteWorkerListBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityFoundWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityIncomingCallBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityJobHistoryBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityMainScreenBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityNewsBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOfferDetailBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOrderDetailBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityOutgoingCallBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityRatingWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySelectServiceTypeBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivitySignInScreenBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityThankYouForOrderBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityTransactionHistoryBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ActivityWelcomeBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdCallWorkerInformationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdChooseCountryCodeBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdConfirmRequestCallWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdInviteFriendDialogBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdPartialFindingWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.BsdTaskFilterBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogAskPermissionBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogChangeProfilePictureBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogChoosePaymentMethodBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogConfirmationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogEnterTipMoneyBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.DialogSampleMessageListBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentCallWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentChooseLocationInMapBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentConservationListBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentCreateAPaymentStepOneBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentDefineMyLocationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentFindingWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentHomePageBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentNotificationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentNotificationListBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentProfileBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentSignInStepOneBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentSignInStepTwoBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.FragmentTaskManagementBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemByCalendarTaskBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftMediaBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageLeftTextBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageMediaBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightMediaBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemChatMessageRightTextBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemConservationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemCountryCodeBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomSnackbarBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomToastBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemCustomWorkerInfowindowBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemFavoriteWorkerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemNewsBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemNotificationBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemOfferBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemOrderDetailServiceBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemRelatedNewsBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemServiceTypeCardviewBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemSlideContainerBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemTaskBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ItemTransactionBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.PartialItemImageSingleMessageBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.TaskByCalendarFragmentBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.TaskByListFragmentBindingImpl;
import dev.vcgroup.thonowandroidapp.databinding.ViewThonowKeyboardBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYASKPERMISSION = 1;

  private static final int LAYOUT_ACTIVITYCALLWORKER = 2;

  private static final int LAYOUT_ACTIVITYCANCELORDER = 3;

  private static final int LAYOUT_ACTIVITYCHAT = 4;

  private static final int LAYOUT_ACTIVITYCONFIRMATIONOFPAYMENT = 5;

  private static final int LAYOUT_ACTIVITYCREATEAPAYMENT = 6;

  private static final int LAYOUT_ACTIVITYEDITPROFILE = 7;

  private static final int LAYOUT_ACTIVITYFAVORITEWORKERLIST = 8;

  private static final int LAYOUT_ACTIVITYFOUNDWORKER = 9;

  private static final int LAYOUT_ACTIVITYINCOMINGCALL = 10;

  private static final int LAYOUT_ACTIVITYJOBHISTORY = 11;

  private static final int LAYOUT_ACTIVITYMAINSCREEN = 12;

  private static final int LAYOUT_ACTIVITYNEWS = 13;

  private static final int LAYOUT_ACTIVITYOFFERDETAIL = 14;

  private static final int LAYOUT_ACTIVITYORDERDETAIL = 15;

  private static final int LAYOUT_ACTIVITYOUTGOINGCALL = 16;

  private static final int LAYOUT_ACTIVITYRATINGWORKER = 17;

  private static final int LAYOUT_ACTIVITYSELECTSERVICE = 18;

  private static final int LAYOUT_ACTIVITYSELECTSERVICETYPE = 19;

  private static final int LAYOUT_ACTIVITYSIGNINSCREEN = 20;

  private static final int LAYOUT_ACTIVITYTHANKYOUFORORDER = 21;

  private static final int LAYOUT_ACTIVITYTRANSACTIONHISTORY = 22;

  private static final int LAYOUT_ACTIVITYWELCOME = 23;

  private static final int LAYOUT_BSDCALLWORKERINFORMATION = 24;

  private static final int LAYOUT_BSDCHOOSECOUNTRYCODE = 25;

  private static final int LAYOUT_BSDCONFIRMREQUESTCALLWORKER = 26;

  private static final int LAYOUT_BSDINVITEFRIENDDIALOG = 27;

  private static final int LAYOUT_BSDPARTIALFINDINGWORKER = 28;

  private static final int LAYOUT_BSDTASKFILTER = 29;

  private static final int LAYOUT_DIALOGASKPERMISSION = 30;

  private static final int LAYOUT_DIALOGCHANGEPROFILEPICTURE = 31;

  private static final int LAYOUT_DIALOGCHOOSEPAYMENTMETHOD = 32;

  private static final int LAYOUT_DIALOGCONFIRMATION = 33;

  private static final int LAYOUT_DIALOGENTERTIPMONEY = 34;

  private static final int LAYOUT_DIALOGSAMPLEMESSAGELIST = 35;

  private static final int LAYOUT_FRAGMENTCALLWORKER = 36;

  private static final int LAYOUT_FRAGMENTCHOOSELOCATIONINMAP = 37;

  private static final int LAYOUT_FRAGMENTCONSERVATIONLIST = 38;

  private static final int LAYOUT_FRAGMENTCREATEAPAYMENTSTEPONE = 39;

  private static final int LAYOUT_FRAGMENTDEFINEMYLOCATION = 40;

  private static final int LAYOUT_FRAGMENTFINDINGWORKER = 41;

  private static final int LAYOUT_FRAGMENTHOMEPAGE = 42;

  private static final int LAYOUT_FRAGMENTNOTIFICATION = 43;

  private static final int LAYOUT_FRAGMENTNOTIFICATIONLIST = 44;

  private static final int LAYOUT_FRAGMENTPROFILE = 45;

  private static final int LAYOUT_FRAGMENTSIGNINSTEPONE = 46;

  private static final int LAYOUT_FRAGMENTSIGNINSTEPTWO = 47;

  private static final int LAYOUT_FRAGMENTTASKMANAGEMENT = 48;

  private static final int LAYOUT_ITEMBYCALENDARTASK = 49;

  private static final int LAYOUT_ITEMCHATMESSAGELEFTMEDIA = 50;

  private static final int LAYOUT_ITEMCHATMESSAGELEFTTEXT = 51;

  private static final int LAYOUT_ITEMCHATMESSAGEMEDIA = 52;

  private static final int LAYOUT_ITEMCHATMESSAGERIGHTMEDIA = 53;

  private static final int LAYOUT_ITEMCHATMESSAGERIGHTTEXT = 54;

  private static final int LAYOUT_ITEMCONSERVATION = 55;

  private static final int LAYOUT_ITEMCOUNTRYCODE = 56;

  private static final int LAYOUT_ITEMCUSTOMSNACKBAR = 57;

  private static final int LAYOUT_ITEMCUSTOMTOAST = 58;

  private static final int LAYOUT_ITEMCUSTOMWORKERINFOWINDOW = 59;

  private static final int LAYOUT_ITEMFAVORITEWORKER = 60;

  private static final int LAYOUT_ITEMNEWS = 61;

  private static final int LAYOUT_ITEMNOTIFICATION = 62;

  private static final int LAYOUT_ITEMOFFER = 63;

  private static final int LAYOUT_ITEMORDERDETAILSERVICE = 64;

  private static final int LAYOUT_ITEMRELATEDNEWS = 65;

  private static final int LAYOUT_ITEMSERVICE = 66;

  private static final int LAYOUT_ITEMSERVICETYPE = 67;

  private static final int LAYOUT_ITEMSERVICETYPECARDVIEW = 68;

  private static final int LAYOUT_ITEMSLIDECONTAINER = 69;

  private static final int LAYOUT_ITEMTASK = 70;

  private static final int LAYOUT_ITEMTRANSACTION = 71;

  private static final int LAYOUT_PARTIALITEMIMAGESINGLEMESSAGE = 72;

  private static final int LAYOUT_TASKBYCALENDARFRAGMENT = 73;

  private static final int LAYOUT_TASKBYLISTFRAGMENT = 74;

  private static final int LAYOUT_VIEWTHONOWKEYBOARD = 75;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(75);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_ask_permission, LAYOUT_ACTIVITYASKPERMISSION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_call_worker, LAYOUT_ACTIVITYCALLWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_cancel_order, LAYOUT_ACTIVITYCANCELORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_chat, LAYOUT_ACTIVITYCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_confirmation_of_payment, LAYOUT_ACTIVITYCONFIRMATIONOFPAYMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_create_a_payment, LAYOUT_ACTIVITYCREATEAPAYMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_edit_profile, LAYOUT_ACTIVITYEDITPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_favorite_worker_list, LAYOUT_ACTIVITYFAVORITEWORKERLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_found_worker, LAYOUT_ACTIVITYFOUNDWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_incoming_call, LAYOUT_ACTIVITYINCOMINGCALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_job_history, LAYOUT_ACTIVITYJOBHISTORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_main_screen, LAYOUT_ACTIVITYMAINSCREEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_news, LAYOUT_ACTIVITYNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_offer_detail, LAYOUT_ACTIVITYOFFERDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_order_detail, LAYOUT_ACTIVITYORDERDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_outgoing_call, LAYOUT_ACTIVITYOUTGOINGCALL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_rating_worker, LAYOUT_ACTIVITYRATINGWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_select_service, LAYOUT_ACTIVITYSELECTSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_select_service_type, LAYOUT_ACTIVITYSELECTSERVICETYPE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_sign_in_screen, LAYOUT_ACTIVITYSIGNINSCREEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_thank_you_for_order, LAYOUT_ACTIVITYTHANKYOUFORORDER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_transaction_history, LAYOUT_ACTIVITYTRANSACTIONHISTORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.activity_welcome, LAYOUT_ACTIVITYWELCOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_call_worker_information, LAYOUT_BSDCALLWORKERINFORMATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_choose_country_code, LAYOUT_BSDCHOOSECOUNTRYCODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_confirm_request_call_worker, LAYOUT_BSDCONFIRMREQUESTCALLWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_invite_friend_dialog, LAYOUT_BSDINVITEFRIENDDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_partial_finding_worker, LAYOUT_BSDPARTIALFINDINGWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.bsd_task_filter, LAYOUT_BSDTASKFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_ask_permission, LAYOUT_DIALOGASKPERMISSION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_change_profile_picture, LAYOUT_DIALOGCHANGEPROFILEPICTURE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_choose_payment_method, LAYOUT_DIALOGCHOOSEPAYMENTMETHOD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_confirmation, LAYOUT_DIALOGCONFIRMATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_enter_tip_money, LAYOUT_DIALOGENTERTIPMONEY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.dialog_sample_message_list, LAYOUT_DIALOGSAMPLEMESSAGELIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_call_worker, LAYOUT_FRAGMENTCALLWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_choose_location_in_map, LAYOUT_FRAGMENTCHOOSELOCATIONINMAP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_conservation_list, LAYOUT_FRAGMENTCONSERVATIONLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_create_a_payment_step_one, LAYOUT_FRAGMENTCREATEAPAYMENTSTEPONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_define_my_location, LAYOUT_FRAGMENTDEFINEMYLOCATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_finding_worker, LAYOUT_FRAGMENTFINDINGWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_home_page, LAYOUT_FRAGMENTHOMEPAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_notification, LAYOUT_FRAGMENTNOTIFICATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_notification_list, LAYOUT_FRAGMENTNOTIFICATIONLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_sign_in_step_one, LAYOUT_FRAGMENTSIGNINSTEPONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_sign_in_step_two, LAYOUT_FRAGMENTSIGNINSTEPTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.fragment_task_management, LAYOUT_FRAGMENTTASKMANAGEMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_by_calendar_task, LAYOUT_ITEMBYCALENDARTASK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_left_media, LAYOUT_ITEMCHATMESSAGELEFTMEDIA);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_left_text, LAYOUT_ITEMCHATMESSAGELEFTTEXT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_media, LAYOUT_ITEMCHATMESSAGEMEDIA);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_right_media, LAYOUT_ITEMCHATMESSAGERIGHTMEDIA);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_right_text, LAYOUT_ITEMCHATMESSAGERIGHTTEXT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_conservation, LAYOUT_ITEMCONSERVATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_country_code, LAYOUT_ITEMCOUNTRYCODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_custom_snackbar, LAYOUT_ITEMCUSTOMSNACKBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_custom_toast, LAYOUT_ITEMCUSTOMTOAST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_custom_worker_infowindow, LAYOUT_ITEMCUSTOMWORKERINFOWINDOW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_favorite_worker, LAYOUT_ITEMFAVORITEWORKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_news, LAYOUT_ITEMNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_notification, LAYOUT_ITEMNOTIFICATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_offer, LAYOUT_ITEMOFFER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_order_detail_service, LAYOUT_ITEMORDERDETAILSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_related_news, LAYOUT_ITEMRELATEDNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_service, LAYOUT_ITEMSERVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_service_type, LAYOUT_ITEMSERVICETYPE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_service_type_cardview, LAYOUT_ITEMSERVICETYPECARDVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_slide_container, LAYOUT_ITEMSLIDECONTAINER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_task, LAYOUT_ITEMTASK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.item_transaction, LAYOUT_ITEMTRANSACTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.partial_item_image_single_message, LAYOUT_PARTIALITEMIMAGESINGLEMESSAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.task_by_calendar_fragment, LAYOUT_TASKBYCALENDARFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.task_by_list_fragment, LAYOUT_TASKBYLISTFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.vcgroup.thonowandroidapp.R.layout.view_thonow_keyboard, LAYOUT_VIEWTHONOWKEYBOARD);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYASKPERMISSION: {
        if ("layout/activity_ask_permission_0".equals(tag)) {
          return new ActivityAskPermissionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_ask_permission is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCALLWORKER: {
        if ("layout/activity_call_worker_0".equals(tag)) {
          return new ActivityCallWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_call_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCANCELORDER: {
        if ("layout/activity_cancel_order_0".equals(tag)) {
          return new ActivityCancelOrderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_cancel_order is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCHAT: {
        if ("layout/activity_chat_0".equals(tag)) {
          return new ActivityChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_chat is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCONFIRMATIONOFPAYMENT: {
        if ("layout/activity_confirmation_of_payment_0".equals(tag)) {
          return new ActivityConfirmationOfPaymentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_confirmation_of_payment is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCREATEAPAYMENT: {
        if ("layout/activity_create_a_payment_0".equals(tag)) {
          return new ActivityCreateAPaymentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_create_a_payment is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYEDITPROFILE: {
        if ("layout/activity_edit_profile_0".equals(tag)) {
          return new ActivityEditProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_edit_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYFAVORITEWORKERLIST: {
        if ("layout/activity_favorite_worker_list_0".equals(tag)) {
          return new ActivityFavoriteWorkerListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_favorite_worker_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYFOUNDWORKER: {
        if ("layout/activity_found_worker_0".equals(tag)) {
          return new ActivityFoundWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_found_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYINCOMINGCALL: {
        if ("layout/activity_incoming_call_0".equals(tag)) {
          return new ActivityIncomingCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_incoming_call is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYJOBHISTORY: {
        if ("layout/activity_job_history_0".equals(tag)) {
          return new ActivityJobHistoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_job_history is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMAINSCREEN: {
        if ("layout/activity_main_screen_0".equals(tag)) {
          return new ActivityMainScreenBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_main_screen is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYNEWS: {
        if ("layout/activity_news_0".equals(tag)) {
          return new ActivityNewsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_news is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYOFFERDETAIL: {
        if ("layout/activity_offer_detail_0".equals(tag)) {
          return new ActivityOfferDetailBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_offer_detail is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYORDERDETAIL: {
        if ("layout/activity_order_detail_0".equals(tag)) {
          return new ActivityOrderDetailBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_order_detail is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYOUTGOINGCALL: {
        if ("layout/activity_outgoing_call_0".equals(tag)) {
          return new ActivityOutgoingCallBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_outgoing_call is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYRATINGWORKER: {
        if ("layout/activity_rating_worker_0".equals(tag)) {
          return new ActivityRatingWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_rating_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSELECTSERVICE: {
        if ("layout/activity_select_service_0".equals(tag)) {
          return new ActivitySelectServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_select_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSELECTSERVICETYPE: {
        if ("layout/activity_select_service_type_0".equals(tag)) {
          return new ActivitySelectServiceTypeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_select_service_type is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSIGNINSCREEN: {
        if ("layout/activity_sign_in_screen_0".equals(tag)) {
          return new ActivitySignInScreenBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_sign_in_screen is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYTHANKYOUFORORDER: {
        if ("layout/activity_thank_you_for_order_0".equals(tag)) {
          return new ActivityThankYouForOrderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_thank_you_for_order is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYTRANSACTIONHISTORY: {
        if ("layout/activity_transaction_history_0".equals(tag)) {
          return new ActivityTransactionHistoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_transaction_history is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYWELCOME: {
        if ("layout/activity_welcome_0".equals(tag)) {
          return new ActivityWelcomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_welcome is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDCALLWORKERINFORMATION: {
        if ("layout/bsd_call_worker_information_0".equals(tag)) {
          return new BsdCallWorkerInformationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_call_worker_information is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDCHOOSECOUNTRYCODE: {
        if ("layout/bsd_choose_country_code_0".equals(tag)) {
          return new BsdChooseCountryCodeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_choose_country_code is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDCONFIRMREQUESTCALLWORKER: {
        if ("layout/bsd_confirm_request_call_worker_0".equals(tag)) {
          return new BsdConfirmRequestCallWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_confirm_request_call_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDINVITEFRIENDDIALOG: {
        if ("layout/bsd_invite_friend_dialog_0".equals(tag)) {
          return new BsdInviteFriendDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_invite_friend_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDPARTIALFINDINGWORKER: {
        if ("layout/bsd_partial_finding_worker_0".equals(tag)) {
          return new BsdPartialFindingWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_partial_finding_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_BSDTASKFILTER: {
        if ("layout/bsd_task_filter_0".equals(tag)) {
          return new BsdTaskFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bsd_task_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGASKPERMISSION: {
        if ("layout/dialog_ask_permission_0".equals(tag)) {
          return new DialogAskPermissionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_ask_permission is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGCHANGEPROFILEPICTURE: {
        if ("layout/dialog_change_profile_picture_0".equals(tag)) {
          return new DialogChangeProfilePictureBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_change_profile_picture is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGCHOOSEPAYMENTMETHOD: {
        if ("layout/dialog_choose_payment_method_0".equals(tag)) {
          return new DialogChoosePaymentMethodBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_choose_payment_method is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGCONFIRMATION: {
        if ("layout/dialog_confirmation_0".equals(tag)) {
          return new DialogConfirmationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_confirmation is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGENTERTIPMONEY: {
        if ("layout/dialog_enter_tip_money_0".equals(tag)) {
          return new DialogEnterTipMoneyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_enter_tip_money is invalid. Received: " + tag);
      }
      case  LAYOUT_DIALOGSAMPLEMESSAGELIST: {
        if ("layout/dialog_sample_message_list_0".equals(tag)) {
          return new DialogSampleMessageListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dialog_sample_message_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCALLWORKER: {
        if ("layout/fragment_call_worker_0".equals(tag)) {
          return new FragmentCallWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_call_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHOOSELOCATIONINMAP: {
        if ("layout/fragment_choose_location_in_map_0".equals(tag)) {
          return new FragmentChooseLocationInMapBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_choose_location_in_map is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONSERVATIONLIST: {
        if ("layout/fragment_conservation_list_0".equals(tag)) {
          return new FragmentConservationListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_conservation_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCREATEAPAYMENTSTEPONE: {
        if ("layout/fragment_create_a_payment_step_one_0".equals(tag)) {
          return new FragmentCreateAPaymentStepOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_create_a_payment_step_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDEFINEMYLOCATION: {
        if ("layout/fragment_define_my_location_0".equals(tag)) {
          return new FragmentDefineMyLocationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_define_my_location is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFINDINGWORKER: {
        if ("layout/fragment_finding_worker_0".equals(tag)) {
          return new FragmentFindingWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_finding_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOMEPAGE: {
        if ("layout/fragment_home_page_0".equals(tag)) {
          return new FragmentHomePageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home_page is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNOTIFICATION: {
        if ("layout/fragment_notification_0".equals(tag)) {
          return new FragmentNotificationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_notification is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNOTIFICATIONLIST: {
        if ("layout/fragment_notification_list_0".equals(tag)) {
          return new FragmentNotificationListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_notification_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILE: {
        if ("layout/fragment_profile_0".equals(tag)) {
          return new FragmentProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNINSTEPONE: {
        if ("layout/fragment_sign_in_step_one_0".equals(tag)) {
          return new FragmentSignInStepOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_in_step_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNINSTEPTWO: {
        if ("layout/fragment_sign_in_step_two_0".equals(tag)) {
          return new FragmentSignInStepTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_in_step_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTASKMANAGEMENT: {
        if ("layout/fragment_task_management_0".equals(tag)) {
          return new FragmentTaskManagementBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_task_management is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMBYCALENDARTASK: {
        if ("layout/item_by_calendar_task_0".equals(tag)) {
          return new ItemByCalendarTaskBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_by_calendar_task is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATMESSAGELEFTMEDIA: {
        if ("layout/item_chat_message_left_media_0".equals(tag)) {
          return new ItemChatMessageLeftMediaBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message_left_media is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ITEMCHATMESSAGELEFTTEXT: {
        if ("layout/item_chat_message_left_text_0".equals(tag)) {
          return new ItemChatMessageLeftTextBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message_left_text is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATMESSAGEMEDIA: {
        if ("layout/item_chat_message_media_0".equals(tag)) {
          return new ItemChatMessageMediaBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message_media is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATMESSAGERIGHTMEDIA: {
        if ("layout/item_chat_message_right_media_0".equals(tag)) {
          return new ItemChatMessageRightMediaBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message_right_media is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATMESSAGERIGHTTEXT: {
        if ("layout/item_chat_message_right_text_0".equals(tag)) {
          return new ItemChatMessageRightTextBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message_right_text is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCONSERVATION: {
        if ("layout/item_conservation_0".equals(tag)) {
          return new ItemConservationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_conservation is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCOUNTRYCODE: {
        if ("layout/item_country_code_0".equals(tag)) {
          return new ItemCountryCodeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_country_code is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCUSTOMSNACKBAR: {
        if ("layout/item_custom_snackbar_0".equals(tag)) {
          return new ItemCustomSnackbarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_custom_snackbar is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCUSTOMTOAST: {
        if ("layout/item_custom_toast_0".equals(tag)) {
          return new ItemCustomToastBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_custom_toast is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCUSTOMWORKERINFOWINDOW: {
        if ("layout/item_custom_worker_infowindow_0".equals(tag)) {
          return new ItemCustomWorkerInfowindowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_custom_worker_infowindow is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFAVORITEWORKER: {
        if ("layout/item_favorite_worker_0".equals(tag)) {
          return new ItemFavoriteWorkerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_favorite_worker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMNEWS: {
        if ("layout/item_news_0".equals(tag)) {
          return new ItemNewsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_news is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMNOTIFICATION: {
        if ("layout/item_notification_0".equals(tag)) {
          return new ItemNotificationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_notification is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMOFFER: {
        if ("layout/item_offer_0".equals(tag)) {
          return new ItemOfferBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_offer is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERDETAILSERVICE: {
        if ("layout/item_order_detail_service_0".equals(tag)) {
          return new ItemOrderDetailServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_detail_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMRELATEDNEWS: {
        if ("layout/item_related_news_0".equals(tag)) {
          return new ItemRelatedNewsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_related_news is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSERVICE: {
        if ("layout/item_service_0".equals(tag)) {
          return new ItemServiceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_service is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSERVICETYPE: {
        if ("layout/item_service_type_0".equals(tag)) {
          return new ItemServiceTypeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_service_type is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSERVICETYPECARDVIEW: {
        if ("layout/item_service_type_cardview_0".equals(tag)) {
          return new ItemServiceTypeCardviewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_service_type_cardview is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSLIDECONTAINER: {
        if ("layout/item_slide_container_0".equals(tag)) {
          return new ItemSlideContainerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_slide_container is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTASK: {
        if ("layout/item_task_0".equals(tag)) {
          return new ItemTaskBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_task is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTRANSACTION: {
        if ("layout/item_transaction_0".equals(tag)) {
          return new ItemTransactionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_transaction is invalid. Received: " + tag);
      }
      case  LAYOUT_PARTIALITEMIMAGESINGLEMESSAGE: {
        if ("layout/partial_item_image_single_message_0".equals(tag)) {
          return new PartialItemImageSingleMessageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for partial_item_image_single_message is invalid. Received: " + tag);
      }
      case  LAYOUT_TASKBYCALENDARFRAGMENT: {
        if ("layout/task_by_calendar_fragment_0".equals(tag)) {
          return new TaskByCalendarFragmentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for task_by_calendar_fragment is invalid. Received: " + tag);
      }
      case  LAYOUT_TASKBYLISTFRAGMENT: {
        if ("layout/task_by_list_fragment_0".equals(tag)) {
          return new TaskByListFragmentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for task_by_list_fragment is invalid. Received: " + tag);
      }
      case  LAYOUT_VIEWTHONOWKEYBOARD: {
        if ("layout/view_thonow_keyboard_0".equals(tag)) {
          return new ViewThonowKeyboardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for view_thonow_keyboard is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(2);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    result.add(new gun0912.tedimagepicker.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(37);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "album");
      sKeys.put(2, "background");
      sKeys.put(3, "buttonBackground");
      sKeys.put(4, "buttonDrawableOnly");
      sKeys.put(5, "buttonGravity");
      sKeys.put(6, "buttonText");
      sKeys.put(7, "buttonTextColor");
      sKeys.put(8, "handlers");
      sKeys.put(9, "imageCountFormat");
      sKeys.put(10, "isAlbumOpened");
      sKeys.put(11, "isOpened");
      sKeys.put(12, "isSelected");
      sKeys.put(13, "item");
      sKeys.put(14, "itemList");
      sKeys.put(15, "items");
      sKeys.put(16, "media");
      sKeys.put(17, "mediaCountText");
      sKeys.put(18, "message");
      sKeys.put(19, "notification");
      sKeys.put(20, "order");
      sKeys.put(21, "orderDetail");
      sKeys.put(22, "selectType");
      sKeys.put(23, "selectedAlbum");
      sKeys.put(24, "selectedNumber");
      sKeys.put(25, "service");
      sKeys.put(26, "serviceType");
      sKeys.put(27, "showButton");
      sKeys.put(28, "showZoom");
      sKeys.put(29, "signInViewModel");
      sKeys.put(30, "snackBarItem");
      sKeys.put(31, "text");
      sKeys.put(32, "textColor");
      sKeys.put(33, "toastItem");
      sKeys.put(34, "transaction");
      sKeys.put(35, "uri");
      sKeys.put(36, "worker");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(75);

    static {
      sKeys.put("layout/activity_ask_permission_0", dev.vcgroup.thonowandroidapp.R.layout.activity_ask_permission);
      sKeys.put("layout/activity_call_worker_0", dev.vcgroup.thonowandroidapp.R.layout.activity_call_worker);
      sKeys.put("layout/activity_cancel_order_0", dev.vcgroup.thonowandroidapp.R.layout.activity_cancel_order);
      sKeys.put("layout/activity_chat_0", dev.vcgroup.thonowandroidapp.R.layout.activity_chat);
      sKeys.put("layout/activity_confirmation_of_payment_0", dev.vcgroup.thonowandroidapp.R.layout.activity_confirmation_of_payment);
      sKeys.put("layout/activity_create_a_payment_0", dev.vcgroup.thonowandroidapp.R.layout.activity_create_a_payment);
      sKeys.put("layout/activity_edit_profile_0", dev.vcgroup.thonowandroidapp.R.layout.activity_edit_profile);
      sKeys.put("layout/activity_favorite_worker_list_0", dev.vcgroup.thonowandroidapp.R.layout.activity_favorite_worker_list);
      sKeys.put("layout/activity_found_worker_0", dev.vcgroup.thonowandroidapp.R.layout.activity_found_worker);
      sKeys.put("layout/activity_incoming_call_0", dev.vcgroup.thonowandroidapp.R.layout.activity_incoming_call);
      sKeys.put("layout/activity_job_history_0", dev.vcgroup.thonowandroidapp.R.layout.activity_job_history);
      sKeys.put("layout/activity_main_screen_0", dev.vcgroup.thonowandroidapp.R.layout.activity_main_screen);
      sKeys.put("layout/activity_news_0", dev.vcgroup.thonowandroidapp.R.layout.activity_news);
      sKeys.put("layout/activity_offer_detail_0", dev.vcgroup.thonowandroidapp.R.layout.activity_offer_detail);
      sKeys.put("layout/activity_order_detail_0", dev.vcgroup.thonowandroidapp.R.layout.activity_order_detail);
      sKeys.put("layout/activity_outgoing_call_0", dev.vcgroup.thonowandroidapp.R.layout.activity_outgoing_call);
      sKeys.put("layout/activity_rating_worker_0", dev.vcgroup.thonowandroidapp.R.layout.activity_rating_worker);
      sKeys.put("layout/activity_select_service_0", dev.vcgroup.thonowandroidapp.R.layout.activity_select_service);
      sKeys.put("layout/activity_select_service_type_0", dev.vcgroup.thonowandroidapp.R.layout.activity_select_service_type);
      sKeys.put("layout/activity_sign_in_screen_0", dev.vcgroup.thonowandroidapp.R.layout.activity_sign_in_screen);
      sKeys.put("layout/activity_thank_you_for_order_0", dev.vcgroup.thonowandroidapp.R.layout.activity_thank_you_for_order);
      sKeys.put("layout/activity_transaction_history_0", dev.vcgroup.thonowandroidapp.R.layout.activity_transaction_history);
      sKeys.put("layout/activity_welcome_0", dev.vcgroup.thonowandroidapp.R.layout.activity_welcome);
      sKeys.put("layout/bsd_call_worker_information_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_call_worker_information);
      sKeys.put("layout/bsd_choose_country_code_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_choose_country_code);
      sKeys.put("layout/bsd_confirm_request_call_worker_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_confirm_request_call_worker);
      sKeys.put("layout/bsd_invite_friend_dialog_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_invite_friend_dialog);
      sKeys.put("layout/bsd_partial_finding_worker_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_partial_finding_worker);
      sKeys.put("layout/bsd_task_filter_0", dev.vcgroup.thonowandroidapp.R.layout.bsd_task_filter);
      sKeys.put("layout/dialog_ask_permission_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_ask_permission);
      sKeys.put("layout/dialog_change_profile_picture_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_change_profile_picture);
      sKeys.put("layout/dialog_choose_payment_method_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_choose_payment_method);
      sKeys.put("layout/dialog_confirmation_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_confirmation);
      sKeys.put("layout/dialog_enter_tip_money_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_enter_tip_money);
      sKeys.put("layout/dialog_sample_message_list_0", dev.vcgroup.thonowandroidapp.R.layout.dialog_sample_message_list);
      sKeys.put("layout/fragment_call_worker_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_call_worker);
      sKeys.put("layout/fragment_choose_location_in_map_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_choose_location_in_map);
      sKeys.put("layout/fragment_conservation_list_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_conservation_list);
      sKeys.put("layout/fragment_create_a_payment_step_one_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_create_a_payment_step_one);
      sKeys.put("layout/fragment_define_my_location_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_define_my_location);
      sKeys.put("layout/fragment_finding_worker_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_finding_worker);
      sKeys.put("layout/fragment_home_page_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_home_page);
      sKeys.put("layout/fragment_notification_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_notification);
      sKeys.put("layout/fragment_notification_list_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_notification_list);
      sKeys.put("layout/fragment_profile_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_profile);
      sKeys.put("layout/fragment_sign_in_step_one_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_sign_in_step_one);
      sKeys.put("layout/fragment_sign_in_step_two_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_sign_in_step_two);
      sKeys.put("layout/fragment_task_management_0", dev.vcgroup.thonowandroidapp.R.layout.fragment_task_management);
      sKeys.put("layout/item_by_calendar_task_0", dev.vcgroup.thonowandroidapp.R.layout.item_by_calendar_task);
      sKeys.put("layout/item_chat_message_left_media_0", dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_left_media);
      sKeys.put("layout/item_chat_message_left_text_0", dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_left_text);
      sKeys.put("layout/item_chat_message_media_0", dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_media);
      sKeys.put("layout/item_chat_message_right_media_0", dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_right_media);
      sKeys.put("layout/item_chat_message_right_text_0", dev.vcgroup.thonowandroidapp.R.layout.item_chat_message_right_text);
      sKeys.put("layout/item_conservation_0", dev.vcgroup.thonowandroidapp.R.layout.item_conservation);
      sKeys.put("layout/item_country_code_0", dev.vcgroup.thonowandroidapp.R.layout.item_country_code);
      sKeys.put("layout/item_custom_snackbar_0", dev.vcgroup.thonowandroidapp.R.layout.item_custom_snackbar);
      sKeys.put("layout/item_custom_toast_0", dev.vcgroup.thonowandroidapp.R.layout.item_custom_toast);
      sKeys.put("layout/item_custom_worker_infowindow_0", dev.vcgroup.thonowandroidapp.R.layout.item_custom_worker_infowindow);
      sKeys.put("layout/item_favorite_worker_0", dev.vcgroup.thonowandroidapp.R.layout.item_favorite_worker);
      sKeys.put("layout/item_news_0", dev.vcgroup.thonowandroidapp.R.layout.item_news);
      sKeys.put("layout/item_notification_0", dev.vcgroup.thonowandroidapp.R.layout.item_notification);
      sKeys.put("layout/item_offer_0", dev.vcgroup.thonowandroidapp.R.layout.item_offer);
      sKeys.put("layout/item_order_detail_service_0", dev.vcgroup.thonowandroidapp.R.layout.item_order_detail_service);
      sKeys.put("layout/item_related_news_0", dev.vcgroup.thonowandroidapp.R.layout.item_related_news);
      sKeys.put("layout/item_service_0", dev.vcgroup.thonowandroidapp.R.layout.item_service);
      sKeys.put("layout/item_service_type_0", dev.vcgroup.thonowandroidapp.R.layout.item_service_type);
      sKeys.put("layout/item_service_type_cardview_0", dev.vcgroup.thonowandroidapp.R.layout.item_service_type_cardview);
      sKeys.put("layout/item_slide_container_0", dev.vcgroup.thonowandroidapp.R.layout.item_slide_container);
      sKeys.put("layout/item_task_0", dev.vcgroup.thonowandroidapp.R.layout.item_task);
      sKeys.put("layout/item_transaction_0", dev.vcgroup.thonowandroidapp.R.layout.item_transaction);
      sKeys.put("layout/partial_item_image_single_message_0", dev.vcgroup.thonowandroidapp.R.layout.partial_item_image_single_message);
      sKeys.put("layout/task_by_calendar_fragment_0", dev.vcgroup.thonowandroidapp.R.layout.task_by_calendar_fragment);
      sKeys.put("layout/task_by_list_fragment_0", dev.vcgroup.thonowandroidapp.R.layout.task_by_list_fragment);
      sKeys.put("layout/view_thonow_keyboard_0", dev.vcgroup.thonowandroidapp.R.layout.view_thonow_keyboard);
    }
  }
}
