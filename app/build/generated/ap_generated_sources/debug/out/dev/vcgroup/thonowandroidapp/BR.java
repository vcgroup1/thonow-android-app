package dev.vcgroup.thonowandroidapp;

public class BR {
  public static final int _all = 0;

  public static final int album = 1;

  public static final int background = 2;

  public static final int buttonBackground = 3;

  public static final int buttonDrawableOnly = 4;

  public static final int buttonGravity = 5;

  public static final int buttonText = 6;

  public static final int buttonTextColor = 7;

  public static final int handlers = 8;

  public static final int imageCountFormat = 9;

  public static final int isAlbumOpened = 10;

  public static final int isOpened = 11;

  public static final int isSelected = 12;

  public static final int item = 13;

  public static final int itemList = 14;

  public static final int items = 15;

  public static final int media = 16;

  public static final int mediaCountText = 17;

  public static final int message = 18;

  public static final int notification = 19;

  public static final int order = 20;

  public static final int orderDetail = 21;

  public static final int selectType = 22;

  public static final int selectedAlbum = 23;

  public static final int selectedNumber = 24;

  public static final int service = 25;

  public static final int serviceType = 26;

  public static final int showButton = 27;

  public static final int showZoom = 28;

  public static final int signInViewModel = 29;

  public static final int snackBarItem = 30;

  public static final int text = 31;

  public static final int textColor = 32;

  public static final int toastItem = 33;

  public static final int transaction = 34;

  public static final int uri = 35;

  public static final int worker = 36;
}
