
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class OrderDetailPayload$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload>
{

    private dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<OrderDetailPayload$$Parcelable>CREATOR = new Creator<OrderDetailPayload$$Parcelable>() {


        @Override
        public OrderDetailPayload$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new OrderDetailPayload$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public OrderDetailPayload$$Parcelable[] newArray(int size) {
            return new OrderDetailPayload$$Parcelable[size] ;
        }

    }
    ;

    public OrderDetailPayload$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$2) {
        orderDetailPayload$$0 = orderDetailPayload$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(orderDetailPayload$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(orderDetailPayload$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(orderDetailPayload$$1));
            parcel$$1 .writeDouble(orderDetailPayload$$1 .unitPrice);
            parcel$$1 .writeInt(orderDetailPayload$$1 .quantity);
            parcel$$1 .writeString(orderDetailPayload$$1 .service);
            parcel$$1 .writeDouble(orderDetailPayload$$1 .subtotal);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload getParcel() {
        return orderDetailPayload$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            orderDetailPayload$$4 = new dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload();
            identityMap$$1 .put(reservation$$0, orderDetailPayload$$4);
            orderDetailPayload$$4 .unitPrice = parcel$$3 .readDouble();
            orderDetailPayload$$4 .quantity = parcel$$3 .readInt();
            orderDetailPayload$$4 .service = parcel$$3 .readString();
            orderDetailPayload$$4 .subtotal = parcel$$3 .readDouble();
            dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$3 = orderDetailPayload$$4;
            identityMap$$1 .put(identity$$1, orderDetailPayload$$3);
            return orderDetailPayload$$3;
        }
    }

}
