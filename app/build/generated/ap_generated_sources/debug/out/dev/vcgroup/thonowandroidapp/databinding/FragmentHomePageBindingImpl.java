package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentHomePageBindingImpl extends FragmentHomePageBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageSliderSection, 2);
        sViewsWithIds.put(R.id.serviceTypeSection, 3);
        sViewsWithIds.put(R.id.offerSection, 4);
        sViewsWithIds.put(R.id.newsSection, 5);
        sViewsWithIds.put(R.id.appbar, 7);
        sViewsWithIds.put(R.id.aihToolbarTransparent, 8);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @Nullable
    private final dev.vcgroup.thonowandroidapp.databinding.PartialHotActionBinding mboundView11;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentHomePageBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private FragmentHomePageBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.Toolbar) bindings[8]
            , (com.google.android.material.appbar.AppBarLayout) bindings[7]
            , (bindings[2] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialImageSliderBinding.bind((android.view.View) bindings[2]) : null
            , (bindings[5] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialNewsSectionBinding.bind((android.view.View) bindings[5]) : null
            , (bindings[4] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialHomepageOfferSectionBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[3] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialServiceTypeBinding.bind((android.view.View) bindings[3]) : null
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView11 = (bindings[6] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialHotActionBinding.bind((android.view.View) bindings[6]) : null;
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}