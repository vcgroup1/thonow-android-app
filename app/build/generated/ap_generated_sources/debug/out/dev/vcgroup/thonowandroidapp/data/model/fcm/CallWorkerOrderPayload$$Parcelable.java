
package dev.vcgroup.thonowandroidapp.data.model.fcm;

import java.util.ArrayList;
import java.util.Date;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import dev.vcgroup.thonowandroidapp.data.model.Address;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class CallWorkerOrderPayload$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload>
{

    private dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload callWorkerOrderPayload$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<CallWorkerOrderPayload$$Parcelable>CREATOR = new Creator<CallWorkerOrderPayload$$Parcelable>() {


        @Override
        public CallWorkerOrderPayload$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new CallWorkerOrderPayload$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public CallWorkerOrderPayload$$Parcelable[] newArray(int size) {
            return new CallWorkerOrderPayload$$Parcelable[size] ;
        }

    }
    ;

    public CallWorkerOrderPayload$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload callWorkerOrderPayload$$2) {
        callWorkerOrderPayload$$0 = callWorkerOrderPayload$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(callWorkerOrderPayload$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload callWorkerOrderPayload$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(callWorkerOrderPayload$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(callWorkerOrderPayload$$1));
            parcel$$1 .writeString(callWorkerOrderPayload$$1 .note);
            if (callWorkerOrderPayload$$1 .orderDetails == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(callWorkerOrderPayload$$1 .orderDetails.size());
                for (dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$0 : callWorkerOrderPayload$$1 .orderDetails) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetail$$Parcelable.write(orderDetail$$0, parcel$$1, flags$$0, identityMap$$0);
                }
            }
            parcel$$1 .writeSerializable(callWorkerOrderPayload$$1 .workingDate);
            dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.write(callWorkerOrderPayload$$1 .workingAddress, parcel$$1, flags$$0, identityMap$$0);
            dev.vcgroup.thonowandroidapp.data.model.OrderType orderType$$0 = callWorkerOrderPayload$$1 .type;
            parcel$$1 .writeString(((orderType$$0 == null)?null:orderType$$0 .name()));
            parcel$$1 .writeDouble(callWorkerOrderPayload$$1 .estimatedFee);
            parcel$$1 .writeString(callWorkerOrderPayload$$1 .customer);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload getParcel() {
        return callWorkerOrderPayload$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload callWorkerOrderPayload$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            callWorkerOrderPayload$$4 = new dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload();
            identityMap$$1 .put(reservation$$0, callWorkerOrderPayload$$4);
            callWorkerOrderPayload$$4 .note = parcel$$3 .readString();
            int int$$0 = parcel$$3 .readInt();
            ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetail> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetail>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$1 = dev.vcgroup.thonowandroidapp.data.model.OrderDetail$$Parcelable.read(parcel$$3, identityMap$$1);
                    list$$0 .add(orderDetail$$1);
                }
            }
            callWorkerOrderPayload$$4 .orderDetails = list$$0;
            callWorkerOrderPayload$$4 .workingDate = ((Date) parcel$$3 .readSerializable());
            Address address$$0 = dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.read(parcel$$3, identityMap$$1);
            callWorkerOrderPayload$$4 .workingAddress = address$$0;
            String orderType$$1 = parcel$$3 .readString();
            callWorkerOrderPayload$$4 .type = ((orderType$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.OrderType.class, orderType$$1));
            callWorkerOrderPayload$$4 .estimatedFee = parcel$$3 .readDouble();
            callWorkerOrderPayload$$4 .customer = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.fcm.CallWorkerOrderPayload callWorkerOrderPayload$$3 = callWorkerOrderPayload$$4;
            identityMap$$1 .put(identity$$1, callWorkerOrderPayload$$3);
            return callWorkerOrderPayload$$3;
        }
    }

}
