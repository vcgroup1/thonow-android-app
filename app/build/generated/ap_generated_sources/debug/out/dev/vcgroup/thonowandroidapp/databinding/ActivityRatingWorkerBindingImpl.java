package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityRatingWorkerBindingImpl extends ActivityRatingWorkerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.view8, 4);
        sViewsWithIds.put(R.id.ratingBar, 5);
        sViewsWithIds.put(R.id.frameLayout, 6);
        sViewsWithIds.put(R.id.tipReview, 7);
        sViewsWithIds.put(R.id.textView18, 8);
        sViewsWithIds.put(R.id.btn_ignore_add_favorite_worker, 9);
        sViewsWithIds.put(R.id.btn_add_favorite_worker, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityRatingWorkerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private ActivityRatingWorkerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[10]
            , (com.google.android.material.button.MaterialButton) bindings[9]
            , (android.widget.FrameLayout) bindings[6]
            , (android.widget.RatingBar) bindings[5]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            , (android.view.View) bindings[4]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.roundedImageView4.setTag(null);
        this.textView16.setTag(null);
        this.textView17.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        java.util.List<com.google.firebase.firestore.DocumentReference> workerServiceTypeList = null;
        java.lang.String workerDisplayName = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;

        if ((dirtyFlags & 0x3L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.serviceTypeList
                    workerServiceTypeList = worker.getServiceTypeList();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.roundedImageView4, workerPhotoUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView16, workerDisplayName);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setWorkerServiceTypeText(this.textView17, workerServiceTypeList);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}