package dev.vcgroup.thonowandroidapp.data.local.database.dao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification;
import dev.vcgroup.thonowandroidapp.util.DateConverter;
import java.lang.Class;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class NotificationDao_Impl implements NotificationDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<FCMRemoteNotification> __insertionAdapterOfFCMRemoteNotification;

  private final EntityDeletionOrUpdateAdapter<FCMRemoteNotification> __deletionAdapterOfFCMRemoteNotification;

  public NotificationDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfFCMRemoteNotification = new EntityInsertionAdapter<FCMRemoteNotification>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `TBL_NOTIFICATION` (`timestamp`,`title`,`text`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FCMRemoteNotification value) {
        final Long _tmp;
        _tmp = DateConverter.fromDate(value.getTimestamp());
        if (_tmp == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, _tmp);
        }
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getText() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getText());
        }
      }
    };
    this.__deletionAdapterOfFCMRemoteNotification = new EntityDeletionOrUpdateAdapter<FCMRemoteNotification>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `TBL_NOTIFICATION` WHERE `timestamp` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FCMRemoteNotification value) {
        final Long _tmp;
        _tmp = DateConverter.fromDate(value.getTimestamp());
        if (_tmp == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, _tmp);
        }
      }
    };
  }

  @Override
  public Long insert(final FCMRemoteNotification notification) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfFCMRemoteNotification.insertAndReturnId(notification);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final FCMRemoteNotification notification) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfFCMRemoteNotification.handle(notification);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<FCMRemoteNotification> getAll() {
    final String _sql = "SELECT * FROM TBL_NOTIFICATION";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfTimestamp = CursorUtil.getColumnIndexOrThrow(_cursor, "timestamp");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfText = CursorUtil.getColumnIndexOrThrow(_cursor, "text");
      final List<FCMRemoteNotification> _result = new ArrayList<FCMRemoteNotification>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final FCMRemoteNotification _item;
        final Date _tmpTimestamp;
        final Long _tmp;
        if (_cursor.isNull(_cursorIndexOfTimestamp)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getLong(_cursorIndexOfTimestamp);
        }
        _tmpTimestamp = DateConverter.toDate(_tmp);
        final String _tmpTitle;
        if (_cursor.isNull(_cursorIndexOfTitle)) {
          _tmpTitle = null;
        } else {
          _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        }
        final String _tmpText;
        if (_cursor.isNull(_cursorIndexOfText)) {
          _tmpText = null;
        } else {
          _tmpText = _cursor.getString(_cursorIndexOfText);
        }
        _item = new FCMRemoteNotification(_tmpTimestamp,_tmpTitle,_tmpText);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
