package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemSlideContainerBindingImpl extends ItemSlideContainerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView8, 4);
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemSlideContainerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemSlideContainerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[4]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.ivSlideImage.setTag(null);
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvSliderFeedback.setTag(null);
        this.tvSliderPerson.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.homepage.adapter.HomeSliderAdapter.SliderItem) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.homepage.adapter.HomeSliderAdapter.SliderItem Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int itemImage = 0;
        dev.vcgroup.thonowandroidapp.ui.homepage.adapter.HomeSliderAdapter.SliderItem item = mItem;
        java.lang.String itemFeedback = null;
        java.lang.String itemOwner = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (item != null) {
                    // read item.image
                    itemImage = item.getImage();
                    // read item.feedback
                    itemFeedback = item.getFeedback();
                    // read item.owner
                    itemOwner = item.getOwner();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadDrawable(this.ivSlideImage, itemImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSliderFeedback, itemFeedback);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSliderPerson, itemOwner);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}