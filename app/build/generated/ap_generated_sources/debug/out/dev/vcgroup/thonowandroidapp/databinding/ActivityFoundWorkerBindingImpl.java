package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityFoundWorkerBindingImpl extends ActivityFoundWorkerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 6);
        sViewsWithIds.put(R.id.btn_go_to_task_management, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityFoundWorkerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ActivityFoundWorkerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.roundedImageView3.setTag(null);
        this.tvJobQuantity.setTag(null);
        this.tvService.setTag(null);
        this.tvWorkerName.setTag(null);
        this.tvWorkerNumberPhone.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else if (BR.order == variableId) {
            setOrder((dev.vcgroup.thonowandroidapp.data.model.Order) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }
    public void setOrder(@Nullable dev.vcgroup.thonowandroidapp.data.model.Order Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        java.lang.String orderOrderDetailsSizeJavaLangStringViC = null;
        java.lang.String workerPhoneNumber = null;
        java.lang.String workerDisplayName = null;
        java.util.List<dev.vcgroup.thonowandroidapp.data.model.OrderDetail> orderOrderDetails = null;
        int orderOrderDetailsSize = 0;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        dev.vcgroup.thonowandroidapp.data.model.Order order = mOrder;

        if ((dirtyFlags & 0x5L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.phoneNumber
                    workerPhoneNumber = worker.getPhoneNumber();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (order != null) {
                    // read order.orderDetails
                    orderOrderDetails = order.getOrderDetails();
                }


                if (orderOrderDetails != null) {
                    // read order.orderDetails.size()
                    orderOrderDetailsSize = orderOrderDetails.size();
                }


                // read (order.orderDetails.size()) + (" việc")
                orderOrderDetailsSizeJavaLangStringViC = (orderOrderDetailsSize) + (" việc");
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.roundedImageView3, workerPhotoUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvWorkerName, workerDisplayName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvWorkerNumberPhone, workerPhoneNumber);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvJobQuantity, orderOrderDetailsSizeJavaLangStringViC);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setOrderServiceList(this.tvService, orderOrderDetails);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): order
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}