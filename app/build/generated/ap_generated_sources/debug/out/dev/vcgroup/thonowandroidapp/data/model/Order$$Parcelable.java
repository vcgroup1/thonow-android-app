
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Order$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.Order>
{

    private dev.vcgroup.thonowandroidapp.data.model.Order order$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Order$$Parcelable>CREATOR = new Creator<Order$$Parcelable>() {


        @Override
        public Order$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Order$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Order$$Parcelable[] newArray(int size) {
            return new Order$$Parcelable[size] ;
        }

    }
    ;

    public Order$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.Order order$$2) {
        order$$0 = order$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(order$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.Order order$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(order$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(order$$1));
            parcel$$1 .writeString(order$$1 .note);
            parcel$$1 .writeDouble(order$$1 .movingExpense);
            if (order$$1 .images == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(order$$1 .images.size());
                for (java.lang.String string$$0 : order$$1 .images) {
                    parcel$$1 .writeString(string$$0);
                }
            }
            dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.write(order$$1 .workingAddress, parcel$$1, flags$$0, identityMap$$0);
            if (order$$1 .materialCosts == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(order$$1 .materialCosts.size());
                for (dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$0 : order$$1 .materialCosts) {
                    dev.vcgroup.thonowandroidapp.data.model.MaterialCost$$Parcelable.write(materialCost$$0, parcel$$1, flags$$0, identityMap$$0);
                }
            }
            dev.vcgroup.thonowandroidapp.data.model.OrderType orderType$$0 = order$$1 .type;
            parcel$$1 .writeString(((orderType$$0 == null)?null:orderType$$0 .name()));
            dev.vcgroup.thonowandroidapp.data.model.Feedback$$Parcelable.write(order$$1 .feedback, parcel$$1, flags$$0, identityMap$$0);
            if (order$$1 .orderDetails == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(order$$1 .orderDetails.size());
                for (dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$0 : order$$1 .orderDetails) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetail$$Parcelable.write(orderDetail$$0, parcel$$1, flags$$0, identityMap$$0);
                }
            }
            parcel$$1 .writeSerializable(order$$1 .workingDate);
            parcel$$1 .writeDouble(order$$1 .total);
            parcel$$1 .writeDouble(order$$1 .otherExpense);
            parcel$$1 .writeSerializable(order$$1 .finishDate);
            parcel$$1 .writeDouble(order$$1 .tip);
            parcel$$1 .writeString(order$$1 .id);
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(order$$1 .worker, parcel$$1);
            parcel$$1 .writeDouble(order$$1 .estimatedFee);
            parcel$$1 .writeDouble(order$$1 .overtimeFee);
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(order$$1 .customer, parcel$$1);
            dev.vcgroup.thonowandroidapp.data.model.OrderStatus orderStatus$$0 = order$$1 .status;
            parcel$$1 .writeString(((orderStatus$$0 == null)?null:orderStatus$$0 .name()));
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.Order getParcel() {
        return order$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.Order read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.Order order$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            order$$4 = new dev.vcgroup.thonowandroidapp.data.model.Order();
            identityMap$$1 .put(reservation$$0, order$$4);
            order$$4 .note = parcel$$3 .readString();
            order$$4 .movingExpense = parcel$$3 .readDouble();
            int int$$0 = parcel$$3 .readInt();
            java.util.ArrayList<java.lang.String> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new java.util.ArrayList<java.lang.String>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    list$$0 .add(parcel$$3 .readString());
                }
            }
            order$$4 .images = list$$0;
            Address address$$0 = dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.read(parcel$$3, identityMap$$1);
            order$$4 .workingAddress = address$$0;
            int int$$2 = parcel$$3 .readInt();
            java.util.ArrayList<dev.vcgroup.thonowandroidapp.data.model.MaterialCost> list$$1;
            if (int$$2 < 0) {
                list$$1 = null;
            } else {
                list$$1 = new java.util.ArrayList<dev.vcgroup.thonowandroidapp.data.model.MaterialCost>(int$$2);
                for (int int$$3 = 0; (int$$3 <int$$2); int$$3 ++) {
                    dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$1 = dev.vcgroup.thonowandroidapp.data.model.MaterialCost$$Parcelable.read(parcel$$3, identityMap$$1);
                    list$$1 .add(materialCost$$1);
                }
            }
            order$$4 .materialCosts = list$$1;
            java.lang.String orderType$$1 = parcel$$3 .readString();
            order$$4 .type = ((orderType$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.OrderType.class, orderType$$1));
            Feedback feedback$$0 = dev.vcgroup.thonowandroidapp.data.model.Feedback$$Parcelable.read(parcel$$3, identityMap$$1);
            order$$4 .feedback = feedback$$0;
            int int$$4 = parcel$$3 .readInt();
            java.util.ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetail> list$$2;
            if (int$$4 < 0) {
                list$$2 = null;
            } else {
                list$$2 = new java.util.ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetail>(int$$4);
                for (int int$$5 = 0; (int$$5 <int$$4); int$$5 ++) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetail orderDetail$$1 = dev.vcgroup.thonowandroidapp.data.model.OrderDetail$$Parcelable.read(parcel$$3, identityMap$$1);
                    list$$2 .add(orderDetail$$1);
                }
            }
            order$$4 .orderDetails = list$$2;
            order$$4 .workingDate = ((java.util.Date) parcel$$3 .readSerializable());
            order$$4 .total = parcel$$3 .readDouble();
            order$$4 .otherExpense = parcel$$3 .readDouble();
            order$$4 .finishDate = ((java.util.Date) parcel$$3 .readSerializable());
            order$$4 .tip = parcel$$3 .readDouble();
            order$$4 .id = parcel$$3 .readString();
            order$$4 .worker = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            order$$4 .estimatedFee = parcel$$3 .readDouble();
            order$$4 .overtimeFee = parcel$$3 .readDouble();
            order$$4 .customer = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            java.lang.String orderStatus$$1 = parcel$$3 .readString();
            order$$4 .status = ((orderStatus$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.OrderStatus.class, orderStatus$$1));
            dev.vcgroup.thonowandroidapp.data.model.Order order$$3 = order$$4;
            identityMap$$1 .put(identity$$1, order$$3);
            return order$$3;
        }
    }

}
