
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Worker$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.Worker>
{

    private dev.vcgroup.thonowandroidapp.data.model.Worker worker$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Worker$$Parcelable>CREATOR = new Creator<Worker$$Parcelable>() {


        @Override
        public Worker$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Worker$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Worker$$Parcelable[] newArray(int size) {
            return new Worker$$Parcelable[size] ;
        }

    }
    ;

    public Worker$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.Worker worker$$2) {
        worker$$0 = worker$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(worker$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.Worker worker$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(worker$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(worker$$1));
            parcel$$1 .writeString(worker$$1 .photoUrl);
            parcel$$1 .writeString(worker$$1 .phoneNumber);
            parcel$$1 .writeString(worker$$1 .displayName);
            parcel$$1 .writeFloat(worker$$1 .rating);
            parcel$$1 .writeString(worker$$1 .promoCode);
            parcel$$1 .writeInt((worker$$1 .isOnline? 1 : 0));
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverterList().toParcel(worker$$1 .serviceTypeList, parcel$$1);
            parcel$$1 .writeString(worker$$1 .id);
            parcel$$1 .writeString(worker$$1 .email);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.Worker getParcel() {
        return worker$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.Worker read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.Worker worker$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            worker$$4 = new dev.vcgroup.thonowandroidapp.data.model.Worker();
            identityMap$$1 .put(reservation$$0, worker$$4);
            worker$$4 .photoUrl = parcel$$3 .readString();
            worker$$4 .phoneNumber = parcel$$3 .readString();
            worker$$4 .displayName = parcel$$3 .readString();
            worker$$4 .rating = parcel$$3 .readFloat();
            worker$$4 .promoCode = parcel$$3 .readString();
            worker$$4 .isOnline = (parcel$$3 .readInt() == 1);
            worker$$4 .serviceTypeList = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverterList().fromParcel(parcel$$3);
            worker$$4 .id = parcel$$3 .readString();
            worker$$4 .email = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.Worker worker$$3 = worker$$4;
            identityMap$$1 .put(identity$$1, worker$$3);
            return worker$$3;
        }
    }

}
