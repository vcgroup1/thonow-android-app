package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemNotificationBindingImpl extends ItemNotificationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.chat_conservation_photo, 4);
        sViewsWithIds.put(R.id.gr_name_last_mess_section, 5);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemNotificationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ItemNotificationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[1]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[4]
            , (android.widget.LinearLayout) bindings[5]
            );
        this.chatConservationLastMessage.setTag(null);
        this.chatConservationLastStatus.setTag(null);
        this.chatConservationName.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.notification == variableId) {
            setNotification((dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setNotification(@Nullable dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification Notification) {
        this.mNotification = Notification;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.notification);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.data.model.fcm.FCMRemoteNotification notification = mNotification;
        java.lang.String notificationTitle = null;
        java.lang.String notificationText = null;
        java.util.Date notificationTimestamp = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (notification != null) {
                    // read notification.title
                    notificationTitle = notification.getTitle();
                    // read notification.text
                    notificationText = notification.getText();
                    // read notification.timestamp
                    notificationTimestamp = notification.getTimestamp();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.chatConservationLastMessage, notificationText);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setDateGap(this.chatConservationLastStatus, notificationTimestamp);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.chatConservationName, notificationTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): notification
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}