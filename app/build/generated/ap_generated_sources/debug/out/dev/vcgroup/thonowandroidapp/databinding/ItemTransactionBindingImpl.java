package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTransactionBindingImpl extends ItemTransactionBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.specialty_icon, 4);
        sViewsWithIds.put(R.id.gr_name_last_mess_section, 5);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemTransactionBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ItemTransactionBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[1]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.ImageView) bindings[4]
            );
        this.chatConservationLastMessage.setTag(null);
        this.chatConservationLastStatus.setTag(null);
        this.chatConservationName.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.transaction == variableId) {
            setTransaction((dev.vcgroup.thonowandroidapp.data.model.Transaction) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setTransaction(@Nullable dev.vcgroup.thonowandroidapp.data.model.Transaction Transaction) {
        this.mTransaction = Transaction;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.transaction);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String javaLangStringThanhToNHoNTransactionOrderId = null;
        java.lang.String commonUtilConvertCurrencyTextTransactionAmount = null;
        java.lang.String transactionOrderId = null;
        com.google.firebase.firestore.DocumentReference transactionOrder = null;
        java.util.Date transactionTimestamp = null;
        java.lang.String commonUtilConvertCurrencyTextTransactionAmountJavaLangString = null;
        dev.vcgroup.thonowandroidapp.data.model.Transaction transaction = mTransaction;
        double transactionAmount = 0.0;

        if ((dirtyFlags & 0x3L) != 0) {



                if (transaction != null) {
                    // read transaction.order
                    transactionOrder = transaction.getOrder();
                    // read transaction.timestamp
                    transactionTimestamp = transaction.getTimestamp();
                    // read transaction.amount
                    transactionAmount = transaction.getAmount();
                }


                if (transactionOrder != null) {
                    // read transaction.order.id
                    transactionOrderId = transactionOrder.getId();
                }
                // read CommonUtil.convertCurrencyText(transaction.amount)
                commonUtilConvertCurrencyTextTransactionAmount = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(transactionAmount);


                // read ("Thanh toán hoá đơn ") + (transaction.order.id)
                javaLangStringThanhToNHoNTransactionOrderId = ("Thanh toán hoá đơn ") + (transactionOrderId);
                // read (CommonUtil.convertCurrencyText(transaction.amount)) + (" đ")
                commonUtilConvertCurrencyTextTransactionAmountJavaLangString = (commonUtilConvertCurrencyTextTransactionAmount) + (" đ");
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.chatConservationLastMessage, javaLangStringThanhToNHoNTransactionOrderId);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.formatStartDate(this.chatConservationLastStatus, transactionTimestamp);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.chatConservationName, commonUtilConvertCurrencyTextTransactionAmountJavaLangString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): transaction
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}