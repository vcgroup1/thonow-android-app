package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityCreateAPaymentBindingImpl extends ActivityCreateAPaymentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.top_app_bar, 4);
        sViewsWithIds.put(R.id.btn_zalo_pay, 5);
        sViewsWithIds.put(R.id.rd_btn_zalo_pay, 6);
        sViewsWithIds.put(R.id.btn_momo, 7);
        sViewsWithIds.put(R.id.rd_btn_momo, 8);
        sViewsWithIds.put(R.id.btn_vnpay, 9);
        sViewsWithIds.put(R.id.rd_btn_vnpay, 10);
        sViewsWithIds.put(R.id.wv_vnpay, 11);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityCreateAPaymentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private ActivityCreateAPaymentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[5]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[8]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[10]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[6]
            , (bindings[4] != null) ? dev.vcgroup.thonowandroidapp.databinding.PartialToolbarBinding.bind((android.view.View) bindings[4]) : null
            , (android.webkit.WebView) bindings[11]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String transactionMethodMOMOTitle = null;
        java.lang.String transactionMethodZALOPAYTitle = null;
        java.lang.String transactionMethodVNPAYTitle = null;

        if ((dirtyFlags & 0x1L) != 0) {

                // read TransactionMethod.MOMO.title
                transactionMethodMOMOTitle = dev.vcgroup.thonowandroidapp.data.model.TransactionMethod.MOMO.getTitle();
                // read TransactionMethod.ZALOPAY.title
                transactionMethodZALOPAYTitle = dev.vcgroup.thonowandroidapp.data.model.TransactionMethod.ZALOPAY.getTitle();
                // read TransactionMethod.VNPAY.title
                transactionMethodVNPAYTitle = dev.vcgroup.thonowandroidapp.data.model.TransactionMethod.VNPAY.getTitle();
        }
        // batch finished
        if ((dirtyFlags & 0x1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, transactionMethodZALOPAYTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, transactionMethodMOMOTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, transactionMethodVNPAYTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}