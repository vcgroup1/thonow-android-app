
package dev.vcgroup.thonowandroidapp.data.model.chat;

import java.util.Date;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Message$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.chat.Message>
{

    private dev.vcgroup.thonowandroidapp.data.model.chat.Message message$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Message$$Parcelable>CREATOR = new Creator<Message$$Parcelable>() {


        @Override
        public Message$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Message$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Message$$Parcelable[] newArray(int size) {
            return new Message$$Parcelable[size] ;
        }

    }
    ;

    public Message$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.chat.Message message$$2) {
        message$$0 = message$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(message$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.chat.Message message$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(message$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(message$$1));
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(message$$1 .from, parcel$$1);
            parcel$$1 .writeString(message$$1 .id);
            dev.vcgroup.thonowandroidapp.data.model.chat.MessageType messageType$$0 = message$$1 .type;
            parcel$$1 .writeString(((messageType$$0 == null)?null:messageType$$0 .name()));
            parcel$$1 .writeString(message$$1 .content);
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(message$$1 .order, parcel$$1);
            parcel$$1 .writeSerializable(message$$1 .timestamp);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.chat.Message getParcel() {
        return message$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.chat.Message read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.chat.Message message$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            message$$4 = new dev.vcgroup.thonowandroidapp.data.model.chat.Message();
            identityMap$$1 .put(reservation$$0, message$$4);
            message$$4 .from = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            message$$4 .id = parcel$$3 .readString();
            String messageType$$1 = parcel$$3 .readString();
            message$$4 .type = ((messageType$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.chat.MessageType.class, messageType$$1));
            message$$4 .content = parcel$$3 .readString();
            message$$4 .order = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            message$$4 .timestamp = ((Date) parcel$$3 .readSerializable());
            dev.vcgroup.thonowandroidapp.data.model.chat.Message message$$3 = message$$4;
            identityMap$$1 .put(identity$$1, message$$3);
            return message$$3;
        }
    }

}
