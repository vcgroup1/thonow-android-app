package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemConservationBindingImpl extends ItemConservationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.gr_name_last_mess_section, 5);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemConservationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ItemConservationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[5]
            );
        this.chatConservationLastMessage.setTag(null);
        this.chatConservationLastStatus.setTag(null);
        this.chatConservationName.setTag(null);
        this.chatConservationPhoto.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.message == variableId) {
            setMessage((dev.vcgroup.thonowandroidapp.data.model.chat.Message) variable);
        }
        else if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMessage(@Nullable dev.vcgroup.thonowandroidapp.data.model.chat.Message Message) {
        this.mMessage = Message;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.message);
        super.requestRebind();
    }
    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        java.util.Date messageTimestamp = null;
        java.lang.String workerDisplayNameEmptyWorkerPhoneNumberWorkerDisplayName = null;
        java.lang.String workerPhoneNumber = null;
        dev.vcgroup.thonowandroidapp.data.model.chat.Message message = mMessage;
        java.lang.String workerDisplayName = null;
        boolean workerDisplayNameEmpty = false;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String messageContent = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (message != null) {
                    // read message.timestamp
                    messageTimestamp = message.getTimestamp();
                    // read message.content
                    messageContent = message.getContent();
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }


                if (workerDisplayName != null) {
                    // read worker.displayName.empty
                    workerDisplayNameEmpty = workerDisplayName.isEmpty();
                }
            if((dirtyFlags & 0x6L) != 0) {
                if(workerDisplayNameEmpty) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x10L) != 0) {

                if (worker != null) {
                    // read worker.phoneNumber
                    workerPhoneNumber = worker.getPhoneNumber();
                }
        }

        if ((dirtyFlags & 0x6L) != 0) {

                // read worker.displayName.empty ? worker.phoneNumber : worker.displayName
                workerDisplayNameEmptyWorkerPhoneNumberWorkerDisplayName = ((workerDisplayNameEmpty) ? (workerPhoneNumber) : (workerDisplayName));
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.formatLastMessageContent(this.chatConservationLastMessage, messageContent);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setDateGap(this.chatConservationLastStatus, messageTimestamp);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.chatConservationName, workerDisplayNameEmptyWorkerPhoneNumberWorkerDisplayName);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.chatConservationPhoto, workerPhotoUrl);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): message
        flag 1 (0x2L): worker
        flag 2 (0x3L): null
        flag 3 (0x4L): worker.displayName.empty ? worker.phoneNumber : worker.displayName
        flag 4 (0x5L): worker.displayName.empty ? worker.phoneNumber : worker.displayName
    flag mapping end*/
    //end
}