
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class ApiError$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.ApiError>
{

    private dev.vcgroup.thonowandroidapp.data.model.ApiError apiError$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<ApiError$$Parcelable>CREATOR = new Creator<ApiError$$Parcelable>() {


        @Override
        public ApiError$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new ApiError$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public ApiError$$Parcelable[] newArray(int size) {
            return new ApiError$$Parcelable[size] ;
        }

    }
    ;

    public ApiError$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.ApiError apiError$$2) {
        apiError$$0 = apiError$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(apiError$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.ApiError apiError$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(apiError$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(apiError$$1));
            parcel$$1 .writeString(apiError$$1 .message);
            parcel$$1 .writeInt(apiError$$1 .statusCode);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.ApiError getParcel() {
        return apiError$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.ApiError read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.ApiError apiError$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            apiError$$4 = new dev.vcgroup.thonowandroidapp.data.model.ApiError();
            identityMap$$1 .put(reservation$$0, apiError$$4);
            apiError$$4 .message = parcel$$3 .readString();
            apiError$$4 .statusCode = parcel$$3 .readInt();
            dev.vcgroup.thonowandroidapp.data.model.ApiError apiError$$3 = apiError$$4;
            identityMap$$1 .put(identity$$1, apiError$$3);
            return apiError$$3;
        }
    }

}
