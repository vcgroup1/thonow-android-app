
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class ServiceType$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.ServiceType>
{

    private dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<ServiceType$$Parcelable>CREATOR = new Creator<ServiceType$$Parcelable>() {


        @Override
        public ServiceType$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new ServiceType$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public ServiceType$$Parcelable[] newArray(int size) {
            return new ServiceType$$Parcelable[size] ;
        }

    }
    ;

    public ServiceType$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType$$2) {
        serviceType$$0 = serviceType$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(serviceType$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(serviceType$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(serviceType$$1));
            parcel$$1 .writeString(serviceType$$1 .name);
            parcel$$1 .writeString(serviceType$$1 .drawableName);
            parcel$$1 .writeString(serviceType$$1 .id);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.ServiceType getParcel() {
        return serviceType$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.ServiceType read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            serviceType$$4 = new dev.vcgroup.thonowandroidapp.data.model.ServiceType();
            identityMap$$1 .put(reservation$$0, serviceType$$4);
            serviceType$$4 .name = parcel$$3 .readString();
            serviceType$$4 .drawableName = parcel$$3 .readString();
            serviceType$$4 .id = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType$$3 = serviceType$$4;
            identityMap$$1 .put(identity$$1, serviceType$$3);
            return serviceType$$3;
        }
    }

}
