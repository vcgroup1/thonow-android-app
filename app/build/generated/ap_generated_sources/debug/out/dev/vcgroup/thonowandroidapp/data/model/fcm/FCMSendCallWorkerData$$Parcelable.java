
package dev.vcgroup.thonowandroidapp.data.model.fcm;

import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import dev.vcgroup.thonowandroidapp.data.model.OrderPayload;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class FCMSendCallWorkerData$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData>
{

    private dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData fCMSendCallWorkerData$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<FCMSendCallWorkerData$$Parcelable>CREATOR = new Creator<FCMSendCallWorkerData$$Parcelable>() {


        @Override
        public FCMSendCallWorkerData$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new FCMSendCallWorkerData$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public FCMSendCallWorkerData$$Parcelable[] newArray(int size) {
            return new FCMSendCallWorkerData$$Parcelable[size] ;
        }

    }
    ;

    public FCMSendCallWorkerData$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData fCMSendCallWorkerData$$2) {
        fCMSendCallWorkerData$$0 = fCMSendCallWorkerData$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(fCMSendCallWorkerData$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData fCMSendCallWorkerData$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(fCMSendCallWorkerData$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(fCMSendCallWorkerData$$1));
            dev.vcgroup.thonowandroidapp.data.model.OrderPayload$$Parcelable.write(fCMSendCallWorkerData$$1 .data, parcel$$1, flags$$0, identityMap$$0);
            if (fCMSendCallWorkerData$$1 .registration_ids == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(fCMSendCallWorkerData$$1 .registration_ids.size());
                for (java.lang.String string$$0 : fCMSendCallWorkerData$$1 .registration_ids) {
                    parcel$$1 .writeString(string$$0);
                }
            }
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData getParcel() {
        return fCMSendCallWorkerData$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData fCMSendCallWorkerData$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            fCMSendCallWorkerData$$4 = new dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData();
            identityMap$$1 .put(reservation$$0, fCMSendCallWorkerData$$4);
            OrderPayload orderPayload$$0 = dev.vcgroup.thonowandroidapp.data.model.OrderPayload$$Parcelable.read(parcel$$3, identityMap$$1);
            fCMSendCallWorkerData$$4 .data = orderPayload$$0;
            int int$$0 = parcel$$3 .readInt();
            ArrayList<java.lang.String> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new ArrayList<java.lang.String>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    list$$0 .add(parcel$$3 .readString());
                }
            }
            fCMSendCallWorkerData$$4 .registration_ids = list$$0;
            dev.vcgroup.thonowandroidapp.data.model.fcm.FCMSendCallWorkerData fCMSendCallWorkerData$$3 = fCMSendCallWorkerData$$4;
            identityMap$$1 .put(identity$$1, fCMSendCallWorkerData$$3);
            return fCMSendCallWorkerData$$3;
        }
    }

}
