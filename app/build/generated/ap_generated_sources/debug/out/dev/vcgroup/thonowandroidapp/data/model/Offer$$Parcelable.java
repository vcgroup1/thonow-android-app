
package dev.vcgroup.thonowandroidapp.data.model;

import java.util.Date;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Offer$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.Offer>
{

    private dev.vcgroup.thonowandroidapp.data.model.Offer offer$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Offer$$Parcelable>CREATOR = new Creator<Offer$$Parcelable>() {


        @Override
        public Offer$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Offer$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Offer$$Parcelable[] newArray(int size) {
            return new Offer$$Parcelable[size] ;
        }

    }
    ;

    public Offer$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.Offer offer$$2) {
        offer$$0 = offer$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(offer$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.Offer offer$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(offer$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(offer$$1));
            parcel$$1 .writeSerializable(offer$$1 .createdAt);
            parcel$$1 .writeString(offer$$1 .thumbnail);
            parcel$$1 .writeString(offer$$1 .code);
            parcel$$1 .writeString(offer$$1 .subtitle);
            parcel$$1 .writeString(offer$$1 .id);
            parcel$$1 .writeString(offer$$1 .title);
            parcel$$1 .writeString(offer$$1 .content);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.Offer getParcel() {
        return offer$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.Offer read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.Offer offer$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            offer$$4 = new dev.vcgroup.thonowandroidapp.data.model.Offer();
            identityMap$$1 .put(reservation$$0, offer$$4);
            offer$$4 .createdAt = ((Date) parcel$$3 .readSerializable());
            offer$$4 .thumbnail = parcel$$3 .readString();
            offer$$4 .code = parcel$$3 .readString();
            offer$$4 .subtitle = parcel$$3 .readString();
            offer$$4 .id = parcel$$3 .readString();
            offer$$4 .title = parcel$$3 .readString();
            offer$$4 .content = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.Offer offer$$3 = offer$$4;
            identityMap$$1 .put(identity$$1, offer$$3);
            return offer$$3;
        }
    }

}
