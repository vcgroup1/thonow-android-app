package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTaskBindingImpl extends ItemTaskBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemTaskBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemTaskBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.card.MaterialCardView) bindings[0]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[2]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[5]
            );
        this.cardview.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.specialtyIcon.setTag(null);
        this.textView10.setTag(null);
        this.textView11.setTag(null);
        this.textView12.setTag(null);
        this.textView13.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.serviceType == variableId) {
            setServiceType((dev.vcgroup.thonowandroidapp.data.model.ServiceType) variable);
        }
        else if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else if (BR.order == variableId) {
            setOrder((dev.vcgroup.thonowandroidapp.data.model.Order) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setServiceType(@Nullable dev.vcgroup.thonowandroidapp.data.model.ServiceType ServiceType) {
        this.mServiceType = ServiceType;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.serviceType);
        super.requestRebind();
    }
    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }
    public void setOrder(@Nullable dev.vcgroup.thonowandroidapp.data.model.Order Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.data.model.OrderStatus orderStatus = null;
        java.lang.String serviceTypeDrawableName = null;
        java.lang.String javaLangStringThWorkerDisplayName = null;
        dev.vcgroup.thonowandroidapp.data.model.ServiceType serviceType = mServiceType;
        java.lang.String serviceTypeName = null;
        java.lang.String javaLangStringDChVServiceTypeName = null;
        java.lang.String workerDisplayName = null;
        java.util.Date orderWorkingDate = null;
        java.lang.String orderStatusTitle = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String orderId = null;
        dev.vcgroup.thonowandroidapp.data.model.Order order = mOrder;
        java.lang.String javaLangStringMNHNgOrderId = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (serviceType != null) {
                    // read serviceType.drawableName
                    serviceTypeDrawableName = serviceType.getDrawableName();
                    // read serviceType.name
                    serviceTypeName = serviceType.getName();
                }


                // read ("Dịch vụ: ") + (serviceType.name)
                javaLangStringDChVServiceTypeName = ("Dịch vụ: ") + (serviceTypeName);
        }
        if ((dirtyFlags & 0xaL) != 0) {



                if (worker != null) {
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }


                // read ("Thợ: ") + (worker.displayName)
                javaLangStringThWorkerDisplayName = ("Thợ: ") + (workerDisplayName);
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (order != null) {
                    // read order.status
                    orderStatus = order.getStatus();
                    // read order.workingDate
                    orderWorkingDate = order.getWorkingDate();
                    // read order.id
                    orderId = order.getId();
                }


                if (orderStatus != null) {
                    // read order.status.title
                    orderStatusTitle = orderStatus.getTitle();
                }
                // read ("Mã đơn hàng: ") + (order.id)
                javaLangStringMNHNgOrderId = ("Mã đơn hàng: ") + (orderId);
        }
        // batch finished
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, javaLangStringThWorkerDisplayName);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadDrawableFromName(this.specialtyIcon, serviceTypeDrawableName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView10, javaLangStringDChVServiceTypeName);
        }
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView11, orderStatusTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView12, javaLangStringMNHNgOrderId);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.formatStartDate2(this.textView13, orderWorkingDate);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): serviceType
        flag 1 (0x2L): worker
        flag 2 (0x3L): order
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}