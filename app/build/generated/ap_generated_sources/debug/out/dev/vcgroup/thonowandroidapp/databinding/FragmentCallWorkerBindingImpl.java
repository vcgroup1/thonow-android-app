package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCallWorkerBindingImpl extends FragmentCallWorkerBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(5);
        sIncludes.setIncludes(0, 
            new String[] {"bsd_call_worker_information", "bsd_confirm_request_call_worker"},
            new int[] {1, 2},
            new int[] {dev.vcgroup.thonowandroidapp.R.layout.bsd_call_worker_information,
                dev.vcgroup.thonowandroidapp.R.layout.bsd_confirm_request_call_worker});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.map, 3);
        sViewsWithIds.put(R.id.btn_extend_scope, 4);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCallWorkerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private FragmentCallWorkerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.TextView) bindings[4]
            , (dev.vcgroup.thonowandroidapp.databinding.BsdConfirmRequestCallWorkerBinding) bindings[2]
            , (dev.vcgroup.thonowandroidapp.databinding.BsdCallWorkerInformationBinding) bindings[1]
            , (androidx.fragment.app.FragmentContainerView) bindings[3]
            );
        setContainedBinding(this.callWorkerConfirmInformation);
        setContainedBinding(this.callWorkerInformation);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        callWorkerInformation.invalidateAll();
        callWorkerConfirmInformation.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (callWorkerInformation.hasPendingBindings()) {
            return true;
        }
        if (callWorkerConfirmInformation.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        callWorkerInformation.setLifecycleOwner(lifecycleOwner);
        callWorkerConfirmInformation.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeCallWorkerConfirmInformation((dev.vcgroup.thonowandroidapp.databinding.BsdConfirmRequestCallWorkerBinding) object, fieldId);
            case 1 :
                return onChangeCallWorkerInformation((dev.vcgroup.thonowandroidapp.databinding.BsdCallWorkerInformationBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeCallWorkerConfirmInformation(dev.vcgroup.thonowandroidapp.databinding.BsdConfirmRequestCallWorkerBinding CallWorkerConfirmInformation, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeCallWorkerInformation(dev.vcgroup.thonowandroidapp.databinding.BsdCallWorkerInformationBinding CallWorkerInformation, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel item = mItem;

        if ((dirtyFlags & 0xcL) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            this.callWorkerConfirmInformation.setItem(item);
            this.callWorkerInformation.setItem(item);
        }
        executeBindingsOn(callWorkerInformation);
        executeBindingsOn(callWorkerConfirmInformation);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): callWorkerConfirmInformation
        flag 1 (0x2L): callWorkerInformation
        flag 2 (0x3L): item
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}