package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityChatBindingImpl extends ActivityChatBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appbar, 3);
        sViewsWithIds.put(R.id.mToolbar, 4);
        sViewsWithIds.put(R.id.rvChatList, 5);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityChatBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ActivityChatBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.appbar.AppBarLayout) bindings[3]
            , (dev.vcgroup.thonowandroidapp.util.keyboard.VCEmoticonsKeyBoard) bindings[0]
            , (androidx.appcompat.widget.Toolbar) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.chatKeyboard.setTag(null);
        this.textMessageItemAvatar.setTag(null);
        this.tvTitleToolbar.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String workerDisplayName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.textMessageItemAvatar, workerPhotoUrl);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTitleToolbar, workerDisplayName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}