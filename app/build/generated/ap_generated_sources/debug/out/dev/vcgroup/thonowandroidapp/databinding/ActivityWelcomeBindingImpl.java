package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityWelcomeBindingImpl extends ActivityWelcomeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.logo, 3);
        sViewsWithIds.put(R.id.linearLayout2, 4);
        sViewsWithIds.put(R.id.btnGoToLogin, 5);
        sViewsWithIds.put(R.id.tvPrefix, 6);
        sViewsWithIds.put(R.id.tipAddress, 7);
        sViewsWithIds.put(R.id.btnConnectSocialMethod, 8);
        sViewsWithIds.put(R.id.social_connect_method_section, 9);
        sViewsWithIds.put(R.id.facebook_login_btn, 10);
        sViewsWithIds.put(R.id.btnGoogleSignIn, 11);
        sViewsWithIds.put(R.id.btnFacebookSignIn, 12);
    }
    // views
    // variables
    // values
    // listeners
    private OnClickListenerImpl mHandlersShowChangeLanguageAndroidViewViewOnClickListener;
    private OnClickListenerImpl1 mHandlersOnLoginClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public ActivityWelcomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private ActivityWelcomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[8]
            , (com.google.android.material.button.MaterialButton) bindings[12]
            , (android.widget.FrameLayout) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[11]
            , (com.facebook.login.widget.LoginButton) bindings[10]
            , (android.widget.LinearLayout) bindings[2]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.LinearLayout) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            , (android.widget.TextView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            );
        this.goToPhoneRegister.setTag(null);
        this.ivbtnLanguage.setTag(null);
        this.welcomeContainer.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.handlers == variableId) {
            setHandlers((dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setHandlers(@Nullable dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity Handlers) {
        this.mHandlers = Handlers;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.handlers);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener handlersShowChangeLanguageAndroidViewViewOnClickListener = null;
        dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity handlers = mHandlers;
        android.view.View.OnClickListener handlersOnLoginClickAndroidViewViewOnClickListener = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (handlers != null) {
                    // read handlers::showChangeLanguage
                    handlersShowChangeLanguageAndroidViewViewOnClickListener = (((mHandlersShowChangeLanguageAndroidViewViewOnClickListener == null) ? (mHandlersShowChangeLanguageAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mHandlersShowChangeLanguageAndroidViewViewOnClickListener).setValue(handlers));
                    // read handlers::onLoginClick
                    handlersOnLoginClickAndroidViewViewOnClickListener = (((mHandlersOnLoginClickAndroidViewViewOnClickListener == null) ? (mHandlersOnLoginClickAndroidViewViewOnClickListener = new OnClickListenerImpl1()) : mHandlersOnLoginClickAndroidViewViewOnClickListener).setValue(handlers));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.goToPhoneRegister.setOnClickListener(handlersOnLoginClickAndroidViewViewOnClickListener);
            this.ivbtnLanguage.setOnClickListener(handlersShowChangeLanguageAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity value;
        public OnClickListenerImpl setValue(dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.showChangeLanguage(arg0); 
        }
    }
    public static class OnClickListenerImpl1 implements android.view.View.OnClickListener{
        private dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity value;
        public OnClickListenerImpl1 setValue(dev.vcgroup.thonowandroidapp.ui.welcomescreen.WelcomeActivity value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onLoginClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): handlers
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}