
package dev.vcgroup.thonowandroidapp.data.model;

import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class OrderPayload$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.OrderPayload>
{

    private dev.vcgroup.thonowandroidapp.data.model.OrderPayload orderPayload$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<OrderPayload$$Parcelable>CREATOR = new Creator<OrderPayload$$Parcelable>() {


        @Override
        public OrderPayload$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new OrderPayload$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public OrderPayload$$Parcelable[] newArray(int size) {
            return new OrderPayload$$Parcelable[size] ;
        }

    }
    ;

    public OrderPayload$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.OrderPayload orderPayload$$2) {
        orderPayload$$0 = orderPayload$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(orderPayload$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.OrderPayload orderPayload$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(orderPayload$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(orderPayload$$1));
            parcel$$1 .writeString(orderPayload$$1 .note);
            if (orderPayload$$1 .orderDetails == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(orderPayload$$1 .orderDetails.size());
                for (dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$0 : orderPayload$$1 .orderDetails) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload$$Parcelable.write(orderDetailPayload$$0, parcel$$1, flags$$0, identityMap$$0);
                }
            }
            parcel$$1 .writeLong(orderPayload$$1 .workingDate);
            dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.write(orderPayload$$1 .workingAddress, parcel$$1, flags$$0, identityMap$$0);
            parcel$$1 .writeString(orderPayload$$1 .id);
            dev.vcgroup.thonowandroidapp.data.model.OrderType orderType$$0 = orderPayload$$1 .type;
            parcel$$1 .writeString(((orderType$$0 == null)?null:orderType$$0 .name()));
            parcel$$1 .writeDouble(orderPayload$$1 .estimatedFee);
            parcel$$1 .writeDouble(orderPayload$$1 .overtimeFee);
            parcel$$1 .writeString(orderPayload$$1 .customer);
            dev.vcgroup.thonowandroidapp.data.model.OrderStatus orderStatus$$0 = orderPayload$$1 .status;
            parcel$$1 .writeString(((orderStatus$$0 == null)?null:orderStatus$$0 .name()));
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.OrderPayload getParcel() {
        return orderPayload$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.OrderPayload read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.OrderPayload orderPayload$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            orderPayload$$4 = new dev.vcgroup.thonowandroidapp.data.model.OrderPayload();
            identityMap$$1 .put(reservation$$0, orderPayload$$4);
            orderPayload$$4 .note = parcel$$3 .readString();
            int int$$0 = parcel$$3 .readInt();
            ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new ArrayList<dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload orderDetailPayload$$1 = dev.vcgroup.thonowandroidapp.data.model.OrderDetailPayload$$Parcelable.read(parcel$$3, identityMap$$1);
                    list$$0 .add(orderDetailPayload$$1);
                }
            }
            orderPayload$$4 .orderDetails = list$$0;
            orderPayload$$4 .workingDate = parcel$$3 .readLong();
            Address address$$0 = dev.vcgroup.thonowandroidapp.data.model.Address$$Parcelable.read(parcel$$3, identityMap$$1);
            orderPayload$$4 .workingAddress = address$$0;
            orderPayload$$4 .id = parcel$$3 .readString();
            String orderType$$1 = parcel$$3 .readString();
            orderPayload$$4 .type = ((orderType$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.OrderType.class, orderType$$1));
            orderPayload$$4 .estimatedFee = parcel$$3 .readDouble();
            orderPayload$$4 .overtimeFee = parcel$$3 .readDouble();
            orderPayload$$4 .customer = parcel$$3 .readString();
            String orderStatus$$1 = parcel$$3 .readString();
            orderPayload$$4 .status = ((orderStatus$$1 == null)?null:Enum.valueOf(dev.vcgroup.thonowandroidapp.data.model.OrderStatus.class, orderStatus$$1));
            dev.vcgroup.thonowandroidapp.data.model.OrderPayload orderPayload$$3 = orderPayload$$4;
            identityMap$$1 .put(identity$$1, orderPayload$$3);
            return orderPayload$$3;
        }
    }

}
