
package dev.vcgroup.thonowandroidapp.data.model;

import java.util.Date;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class News$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.News>
{

    private dev.vcgroup.thonowandroidapp.data.model.News news$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<News$$Parcelable>CREATOR = new Creator<News$$Parcelable>() {


        @Override
        public News$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new News$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public News$$Parcelable[] newArray(int size) {
            return new News$$Parcelable[size] ;
        }

    }
    ;

    public News$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.News news$$2) {
        news$$0 = news$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(news$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.News news$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(news$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(news$$1));
            parcel$$1 .writeSerializable(news$$1 .createdAt);
            parcel$$1 .writeString(news$$1 .thumbnail);
            parcel$$1 .writeString(news$$1 .id);
            parcel$$1 .writeString(news$$1 .title);
            parcel$$1 .writeString(news$$1 .content);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.News getParcel() {
        return news$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.News read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.News news$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            news$$4 = new dev.vcgroup.thonowandroidapp.data.model.News();
            identityMap$$1 .put(reservation$$0, news$$4);
            news$$4 .createdAt = ((Date) parcel$$3 .readSerializable());
            news$$4 .thumbnail = parcel$$3 .readString();
            news$$4 .id = parcel$$3 .readString();
            news$$4 .title = parcel$$3 .readString();
            news$$4 .content = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.News news$$3 = news$$4;
            identityMap$$1 .put(identity$$1, news$$3);
            return news$$3;
        }
    }

}
