package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityConfirmationOfPaymentBindingImpl extends ActivityConfirmationOfPaymentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 9);
        sViewsWithIds.put(R.id.linearLayout5, 10);
        sViewsWithIds.put(R.id.linearLayout6, 11);
        sViewsWithIds.put(R.id.view7, 12);
        sViewsWithIds.put(R.id.tableLayout, 13);
        sViewsWithIds.put(R.id.btn_tip_money, 14);
        sViewsWithIds.put(R.id.payment_method, 15);
        sViewsWithIds.put(R.id.btn_make_a_payment, 16);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView5;
    @NonNull
    private final android.widget.TextView mboundView6;
    @NonNull
    private final android.widget.TextView mboundView7;
    @NonNull
    private final android.widget.TextView mboundView8;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityConfirmationOfPaymentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private ActivityConfirmationOfPaymentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[16]
            , (android.widget.TextView) bindings[14]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.TextView) bindings[15]
            , (android.widget.RatingBar) bindings[4]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            , (android.widget.TableLayout) bindings[13]
            , (android.widget.TextView) bindings[9]
            , (android.view.View) bindings[12]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (android.widget.TextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.TextView) bindings[8];
        this.mboundView8.setTag(null);
        this.ratingBar.setTag(null);
        this.roundedImageView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.worker == variableId) {
            setWorker((dev.vcgroup.thonowandroidapp.data.model.Worker) variable);
        }
        else if (BR.service == variableId) {
            setService((dev.vcgroup.thonowandroidapp.data.model.Service) variable);
        }
        else if (BR.order == variableId) {
            setOrder((dev.vcgroup.thonowandroidapp.data.model.Order) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setWorker(@Nullable dev.vcgroup.thonowandroidapp.data.model.Worker Worker) {
        this.mWorker = Worker;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.worker);
        super.requestRebind();
    }
    public void setService(@Nullable dev.vcgroup.thonowandroidapp.data.model.Service Service) {
        this.mService = Service;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.service);
        super.requestRebind();
    }
    public void setOrder(@Nullable dev.vcgroup.thonowandroidapp.data.model.Order Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String workerPhotoUrl = null;
        java.lang.String workerPhoneNumber = null;
        java.lang.String serviceName = null;
        java.lang.String orderWorkingAddressAddress = null;
        java.lang.String commonUtilConvertCurrencyTextOrderTotal = null;
        java.util.List<dev.vcgroup.thonowandroidapp.data.model.OrderDetail> orderOrderDetails = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker worker = mWorker;
        java.lang.String commonUtilConvertCurrencyTextOrderTotalJavaLangString = null;
        float workerRating = 0f;
        dev.vcgroup.thonowandroidapp.data.model.Service service = mService;
        java.util.List<com.google.firebase.firestore.DocumentReference> workerServiceTypeList = null;
        double orderTotal = 0.0;
        java.lang.String workerDisplayName = null;
        dev.vcgroup.thonowandroidapp.data.model.Address orderWorkingAddress = null;
        dev.vcgroup.thonowandroidapp.data.model.Order order = mOrder;

        if ((dirtyFlags & 0x9L) != 0) {



                if (worker != null) {
                    // read worker.photoUrl
                    workerPhotoUrl = worker.getPhotoUrl();
                    // read worker.phoneNumber
                    workerPhoneNumber = worker.getPhoneNumber();
                    // read worker.rating
                    workerRating = worker.getRating();
                    // read worker.serviceTypeList
                    workerServiceTypeList = worker.getServiceTypeList();
                    // read worker.displayName
                    workerDisplayName = worker.getDisplayName();
                }
        }
        if ((dirtyFlags & 0xaL) != 0) {



                if (service != null) {
                    // read service.name
                    serviceName = service.getName();
                }
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (order != null) {
                    // read order.orderDetails
                    orderOrderDetails = order.getOrderDetails();
                    // read order.total
                    orderTotal = order.getTotal();
                    // read order.workingAddress
                    orderWorkingAddress = order.getWorkingAddress();
                }


                // read CommonUtil.convertCurrencyText(order.total)
                commonUtilConvertCurrencyTextOrderTotal = dev.vcgroup.thonowandroidapp.util.CommonUtil.convertCurrencyText(orderTotal);
                if (orderWorkingAddress != null) {
                    // read order.workingAddress.address
                    orderWorkingAddressAddress = orderWorkingAddress.getAddress();
                }


                // read (CommonUtil.convertCurrencyText(order.total)) + ("đ")
                commonUtilConvertCurrencyTextOrderTotalJavaLangString = (commonUtilConvertCurrencyTextOrderTotal) + ("đ");
        }
        // batch finished
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, workerDisplayName);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setWorkerServiceTypeText(this.mboundView3, workerServiceTypeList);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, workerPhoneNumber);
            androidx.databinding.adapters.RatingBarBindingAdapter.setRating(this.ratingBar, workerRating);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadImage(this.roundedImageView5, workerPhotoUrl);
        }
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.setOrderServiceList(this.mboundView6, orderOrderDetails);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, orderWorkingAddressAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, commonUtilConvertCurrencyTextOrderTotalJavaLangString);
        }
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, serviceName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): worker
        flag 1 (0x2L): service
        flag 2 (0x3L): order
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}