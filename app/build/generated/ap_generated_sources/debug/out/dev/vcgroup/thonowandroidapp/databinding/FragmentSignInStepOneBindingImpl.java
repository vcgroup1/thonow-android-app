package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSignInStepOneBindingImpl extends FragmentSignInStepOneBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.linearLayout2, 6);
        sViewsWithIds.put(R.id.btnGoToLogin, 7);
        sViewsWithIds.put(R.id.drop_down_country, 8);
        sViewsWithIds.put(R.id.btnConnectSocialMethod, 9);
        sViewsWithIds.put(R.id.view, 10);
        sViewsWithIds.put(R.id.btnResendCode, 11);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final android.widget.ImageView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of signInViewModel.phoneNumber.getValue()
            //         is signInViewModel.phoneNumber.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // signInViewModel.phoneNumber
            androidx.lifecycle.MutableLiveData<java.lang.String> signInViewModelPhoneNumber = null;
            // signInViewModel.phoneNumber != null
            boolean signInViewModelPhoneNumberJavaLangObjectNull = false;
            // signInViewModel.phoneNumber.getValue()
            java.lang.String signInViewModelPhoneNumberGetValue = null;
            // signInViewModel != null
            boolean signInViewModelJavaLangObjectNull = false;
            // signInViewModel
            dev.vcgroup.thonowandroidapp.ui.signinscreen.SignInViewModel signInViewModel = mSignInViewModel;



            signInViewModelJavaLangObjectNull = (signInViewModel) != (null);
            if (signInViewModelJavaLangObjectNull) {


                signInViewModelPhoneNumber = signInViewModel.getPhoneNumber();

                signInViewModelPhoneNumberJavaLangObjectNull = (signInViewModelPhoneNumber) != (null);
                if (signInViewModelPhoneNumberJavaLangObjectNull) {




                    signInViewModelPhoneNumber.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentSignInStepOneBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentSignInStepOneBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.TextView) bindings[9]
            , (android.widget.FrameLayout) bindings[7]
            , (com.kusu.loadingbutton.LoadingButton) bindings[11]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.LinearLayout) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[1]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[10]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.ImageView) bindings[3];
        this.mboundView3.setTag(null);
        this.tipAddress.setTag(null);
        this.tvAcceptTerms.setTag(null);
        this.tvPrefix.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.signInViewModel == variableId) {
            setSignInViewModel((dev.vcgroup.thonowandroidapp.ui.signinscreen.SignInViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setSignInViewModel(@Nullable dev.vcgroup.thonowandroidapp.ui.signinscreen.SignInViewModel SignInViewModel) {
        this.mSignInViewModel = SignInViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.signInViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeSignInViewModelPhoneNumber((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeSignInViewModelErrPhoneNumber((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeSignInViewModelSelectedCountryCode((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeSignInViewModelPhoneNumber(androidx.lifecycle.MutableLiveData<java.lang.String> SignInViewModelPhoneNumber, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignInViewModelErrPhoneNumber(androidx.databinding.ObservableField<java.lang.String> SignInViewModelErrPhoneNumber, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeSignInViewModelSelectedCountryCode(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo> SignInViewModelSelectedCountryCode, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.data.model.CountryInfo signInViewModelSelectedCountryCodeGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> signInViewModelPhoneNumber = null;
        java.lang.String signInViewModelPhoneNumberGetValue = null;
        java.lang.String charSignInViewModelSelectedCountryCodeCallingCode = null;
        java.lang.String signInViewModelErrPhoneNumberGet = null;
        java.lang.String signInViewModelSelectedCountryCodeAlp2Code = null;
        androidx.databinding.ObservableField<java.lang.String> signInViewModelErrPhoneNumber = null;
        android.text.Spanned signInViewModelAcceptTerms = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.CountryInfo> signInViewModelSelectedCountryCode = null;
        java.lang.String signInViewModelSelectedCountryCodeCallingCode = null;
        dev.vcgroup.thonowandroidapp.ui.signinscreen.SignInViewModel signInViewModel = mSignInViewModel;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (signInViewModel != null) {
                        // read signInViewModel.phoneNumber
                        signInViewModelPhoneNumber = signInViewModel.getPhoneNumber();
                    }
                    updateLiveDataRegistration(0, signInViewModelPhoneNumber);


                    if (signInViewModelPhoneNumber != null) {
                        // read signInViewModel.phoneNumber.getValue()
                        signInViewModelPhoneNumberGetValue = signInViewModelPhoneNumber.getValue();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (signInViewModel != null) {
                        // read signInViewModel.errPhoneNumber
                        signInViewModelErrPhoneNumber = signInViewModel.errPhoneNumber;
                    }
                    updateRegistration(1, signInViewModelErrPhoneNumber);


                    if (signInViewModelErrPhoneNumber != null) {
                        // read signInViewModel.errPhoneNumber.get()
                        signInViewModelErrPhoneNumberGet = signInViewModelErrPhoneNumber.get();
                    }
            }
            if ((dirtyFlags & 0x18L) != 0) {

                    if (signInViewModel != null) {
                        // read signInViewModel.acceptTerms
                        signInViewModelAcceptTerms = signInViewModel.acceptTerms;
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (signInViewModel != null) {
                        // read signInViewModel.selectedCountryCode
                        signInViewModelSelectedCountryCode = signInViewModel.selectedCountryCode;
                    }
                    updateLiveDataRegistration(2, signInViewModelSelectedCountryCode);


                    if (signInViewModelSelectedCountryCode != null) {
                        // read signInViewModel.selectedCountryCode.getValue()
                        signInViewModelSelectedCountryCodeGetValue = signInViewModelSelectedCountryCode.getValue();
                    }


                    if (signInViewModelSelectedCountryCodeGetValue != null) {
                        // read signInViewModel.selectedCountryCode.getValue().alp2Code
                        signInViewModelSelectedCountryCodeAlp2Code = signInViewModelSelectedCountryCodeGetValue.getAlp2Code();
                        // read signInViewModel.selectedCountryCode.getValue().callingCode
                        signInViewModelSelectedCountryCodeCallingCode = signInViewModelSelectedCountryCodeGetValue.getCallingCode();
                    }


                    // read ('+') + (signInViewModel.selectedCountryCode.getValue().callingCode)
                    charSignInViewModelSelectedCountryCodeCallingCode = ('+') + (signInViewModelSelectedCountryCodeCallingCode);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, signInViewModelPhoneNumberGetValue);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.loadCountryIcon(this.mboundView3, signInViewModelSelectedCountryCodeAlp2Code);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPrefix, charSignInViewModelSelectedCountryCodeCallingCode);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.tipAddress.setError(signInViewModelErrPhoneNumberGet);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAcceptTerms, signInViewModelAcceptTerms);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): signInViewModel.phoneNumber
        flag 1 (0x2L): signInViewModel.errPhoneNumber
        flag 2 (0x3L): signInViewModel.selectedCountryCode
        flag 3 (0x4L): signInViewModel
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}