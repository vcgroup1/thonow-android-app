
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class MaterialCost$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.MaterialCost>
{

    private dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<MaterialCost$$Parcelable>CREATOR = new Creator<MaterialCost$$Parcelable>() {


        @Override
        public MaterialCost$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new MaterialCost$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public MaterialCost$$Parcelable[] newArray(int size) {
            return new MaterialCost$$Parcelable[size] ;
        }

    }
    ;

    public MaterialCost$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$2) {
        materialCost$$0 = materialCost$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(materialCost$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(materialCost$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(materialCost$$1));
            parcel$$1 .writeDouble(materialCost$$1 .price);
            parcel$$1 .writeString(materialCost$$1 .name);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.MaterialCost getParcel() {
        return materialCost$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.MaterialCost read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            materialCost$$4 = new dev.vcgroup.thonowandroidapp.data.model.MaterialCost();
            identityMap$$1 .put(reservation$$0, materialCost$$4);
            materialCost$$4 .price = parcel$$3 .readDouble();
            materialCost$$4 .name = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.MaterialCost materialCost$$3 = materialCost$$4;
            identityMap$$1 .put(identity$$1, materialCost$$3);
            return materialCost$$3;
        }
    }

}
