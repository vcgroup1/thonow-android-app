
package dev.vcgroup.thonowandroidapp.data.model;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated("org.parceler.ParcelAnnotationProcessor")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Service$$Parcelable
    implements Parcelable, ParcelWrapper<dev.vcgroup.thonowandroidapp.data.model.Service>
{

    private dev.vcgroup.thonowandroidapp.data.model.Service service$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Service$$Parcelable>CREATOR = new Creator<Service$$Parcelable>() {


        @Override
        public Service$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Service$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Service$$Parcelable[] newArray(int size) {
            return new Service$$Parcelable[size] ;
        }

    }
    ;

    public Service$$Parcelable(dev.vcgroup.thonowandroidapp.data.model.Service service$$2) {
        service$$0 = service$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(service$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(dev.vcgroup.thonowandroidapp.data.model.Service service$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(service$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(service$$1));
            new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().toParcel(service$$1 .serviceType, parcel$$1);
            parcel$$1 .writeString(service$$1 .unit);
            parcel$$1 .writeInt(service$$1 .warrantyPeriod);
            parcel$$1 .writeDouble(service$$1 .price);
            parcel$$1 .writeString(service$$1 .name);
            parcel$$1 .writeString(service$$1 .id);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public dev.vcgroup.thonowandroidapp.data.model.Service getParcel() {
        return service$$0;
    }

    public static dev.vcgroup.thonowandroidapp.data.model.Service read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            dev.vcgroup.thonowandroidapp.data.model.Service service$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            service$$4 = new dev.vcgroup.thonowandroidapp.data.model.Service();
            identityMap$$1 .put(reservation$$0, service$$4);
            service$$4 .serviceType = new dev.vcgroup.thonowandroidapp.data.model.DocumentReferenceConverter().fromParcel(parcel$$3);
            service$$4 .unit = parcel$$3 .readString();
            service$$4 .warrantyPeriod = parcel$$3 .readInt();
            service$$4 .price = parcel$$3 .readDouble();
            service$$4 .name = parcel$$3 .readString();
            service$$4 .id = parcel$$3 .readString();
            dev.vcgroup.thonowandroidapp.data.model.Service service$$3 = service$$4;
            identityMap$$1 .put(identity$$1, service$$3);
            return service$$3;
        }
    }

}
