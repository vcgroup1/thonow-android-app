package dev.vcgroup.thonowandroidapp.databinding;
import dev.vcgroup.thonowandroidapp.R;
import dev.vcgroup.thonowandroidapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BsdCallWorkerInformationBindingImpl extends BsdCallWorkerInformationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView3, 11);
        sViewsWithIds.put(R.id.call_worker_form, 12);
        sViewsWithIds.put(R.id.tipAddress, 13);
        sViewsWithIds.put(R.id.btn_pick_location, 14);
        sViewsWithIds.put(R.id.btn_select_service, 15);
        sViewsWithIds.put(R.id.btn_select_favorite_worker, 16);
        sViewsWithIds.put(R.id.rb_quick_call, 17);
        sViewsWithIds.put(R.id.rb_make_an_appointment, 18);
        sViewsWithIds.put(R.id.tipStartDate, 19);
        sViewsWithIds.put(R.id.tipStartTime, 20);
        sViewsWithIds.put(R.id.btn_call_worker_next, 21);
    }
    // views
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView1;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView10;
    @NonNull
    private final com.google.android.material.textfield.TextInputLayout mboundView3;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener tipJobQuantityandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of ("") + (item.jobQuatitiy.getValue())
            //         is item.jobQuatitiy.setValue((java.lang.Integer) androidx.databinding.ViewDataBinding.parse(callbackArg_0, item.jobQuatitiy.getValue()))
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(tipJobQuantity);
            // localize variables for thread safety
            // item.jobQuatitiy
            androidx.lifecycle.MutableLiveData<java.lang.Integer> itemJobQuatitiy = null;
            // item
            dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel item = mItem;
            // androidx.databinding.ViewDataBinding.parse(callbackArg_0, item.jobQuatitiy.getValue())
            int androidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue = 0;
            // item != null
            boolean itemJavaLangObjectNull = false;
            // item.jobQuatitiy.getValue()
            java.lang.Integer itemJobQuatitiyGetValue = null;
            // ("") + (item.jobQuatitiy.getValue())
            java.lang.String javaLangStringItemJobQuatitiy = null;
            // (java.lang.Integer) androidx.databinding.ViewDataBinding.parse(callbackArg_0, item.jobQuatitiy.getValue())
            java.lang.Integer javaLangIntegerAndroidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue = null;
            // item.jobQuatitiy != null
            boolean itemJobQuatitiyJavaLangObjectNull = false;



            itemJavaLangObjectNull = (item) != (null);
            if (itemJavaLangObjectNull) {


                itemJobQuatitiy = item.getJobQuatitiy();

                itemJobQuatitiyJavaLangObjectNull = (itemJobQuatitiy) != (null);
                if (itemJobQuatitiyJavaLangObjectNull) {






                    itemJobQuatitiyGetValue = itemJobQuatitiy.getValue();

                    androidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue = androidx.databinding.ViewDataBinding.parse(callbackArg_0, itemJobQuatitiyGetValue);

                    javaLangIntegerAndroidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue = ((java.lang.Integer) (androidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue));

                    itemJobQuatitiy.setValue(javaLangIntegerAndroidxDatabindingViewDataBindingParseCallbackArg0ItemJobQuatitiyGetValue);
                }
            }
        }
    };

    public BsdCallWorkerInformationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private BsdCallWorkerInformationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (com.google.android.material.button.MaterialButton) bindings[21]
            , (android.widget.LinearLayout) bindings[14]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[0]
            , (android.widget.LinearLayout) bindings[12]
            , (android.widget.ImageView) bindings[11]
            , (android.widget.LinearLayout) bindings[8]
            , (android.view.View) bindings[7]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[18]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[17]
            , (android.widget.RadioGroup) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.google.android.material.textfield.TextInputEditText) bindings[5]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[20]
            );
        this.callWorkerBottomSheet.setTag(null);
        this.makeAnAppointmentPart.setTag(null);
        this.makeAnAppointmentPartBreakLine.setTag(null);
        this.mboundView1 = (com.google.android.material.textfield.TextInputEditText) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (com.google.android.material.textfield.TextInputEditText) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView9 = (com.google.android.material.textfield.TextInputEditText) bindings[9];
        this.mboundView9.setTag(null);
        this.rgOrderType.setTag(null);
        this.tipFavoriteWorker.setTag(null);
        this.tipJobQuantity.setTag(null);
        this.tipService.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.item == variableId) {
            setItem((dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItem(@Nullable dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel Item) {
        this.mItem = Item;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.item);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemCurrentAddress((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address>) object, fieldId);
            case 1 :
                return onChangeItemJobQuatitiy((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 2 :
                return onChangeItemCurrentOrderType((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType>) object, fieldId);
            case 3 :
                return onChangeItemStartDate((androidx.lifecycle.MutableLiveData<java.util.Calendar>) object, fieldId);
            case 4 :
                return onChangeItemFavoriteWorker((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Worker>) object, fieldId);
            case 5 :
                return onChangeItemSelectedService((androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemCurrentAddress(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address> ItemCurrentAddress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemJobQuatitiy(androidx.lifecycle.MutableLiveData<java.lang.Integer> ItemJobQuatitiy, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemCurrentOrderType(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType> ItemCurrentOrderType, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemStartDate(androidx.lifecycle.MutableLiveData<java.util.Calendar> ItemStartDate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemFavoriteWorker(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Worker> ItemFavoriteWorker, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemSelectedService(androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service> ItemSelectedService, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        dev.vcgroup.thonowandroidapp.ui.callworker.viewmodel.CallWorkerViewModel item = mItem;
        java.lang.Integer itemJobQuatitiyGetValue = null;
        boolean itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT = false;
        java.lang.String itemFavoriteWorkerJavaLangObjectNullItemFavoriteWorkerDisplayNameJavaLangString = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Address> itemCurrentAddress = null;
        java.lang.String itemSelectedServiceName = null;
        dev.vcgroup.thonowandroidapp.data.model.Worker itemFavoriteWorkerGetValue = null;
        android.view.View.OnClickListener itemOnStartTimeClickListener = null;
        java.lang.String itemCurrentAddressAddress = null;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> itemJobQuatitiy = null;
        int itemCurrentOrderTypeId = 0;
        java.util.Calendar itemStartDateGetValue = null;
        boolean itemFavoriteWorkerJavaLangObjectNull = false;
        dev.vcgroup.thonowandroidapp.data.model.Address itemCurrentAddressGetValue = null;
        dev.vcgroup.thonowandroidapp.data.model.OrderType itemCurrentOrderTypeGetValue = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.OrderType> itemCurrentOrderType = null;
        androidx.lifecycle.MutableLiveData<java.util.Calendar> itemStartDate = null;
        java.lang.String itemFavoriteWorkerDisplayName = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Worker> itemFavoriteWorker = null;
        android.widget.RadioGroup.OnCheckedChangeListener itemRgCheckedChangeListener = null;
        java.lang.String javaLangStringItemJobQuatitiy = null;
        androidx.lifecycle.MutableLiveData<dev.vcgroup.thonowandroidapp.data.model.Service> itemSelectedService = null;
        int itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTViewVISIBLEViewGONE = 0;
        android.view.View.OnClickListener itemOnStartDateClickListener = null;
        java.lang.String itemSelectedServiceUnit = null;
        dev.vcgroup.thonowandroidapp.data.model.Service itemSelectedServiceGetValue = null;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0xc1L) != 0) {

                    if (item != null) {
                        // read item.currentAddress
                        itemCurrentAddress = item.getCurrentAddress();
                    }
                    updateLiveDataRegistration(0, itemCurrentAddress);


                    if (itemCurrentAddress != null) {
                        // read item.currentAddress.getValue()
                        itemCurrentAddressGetValue = itemCurrentAddress.getValue();
                    }


                    if (itemCurrentAddressGetValue != null) {
                        // read item.currentAddress.getValue().address
                        itemCurrentAddressAddress = itemCurrentAddressGetValue.getAddress();
                    }
            }
            if ((dirtyFlags & 0xc0L) != 0) {

                    if (item != null) {
                        // read item.onStartTimeClickListener
                        itemOnStartTimeClickListener = item.onStartTimeClickListener;
                        // read item.currentOrderTypeId
                        itemCurrentOrderTypeId = item.getCurrentOrderTypeId();
                        // read item.rgCheckedChangeListener
                        itemRgCheckedChangeListener = item.rgCheckedChangeListener;
                        // read item.onStartDateClickListener
                        itemOnStartDateClickListener = item.onStartDateClickListener;
                    }
            }
            if ((dirtyFlags & 0xc2L) != 0) {

                    if (item != null) {
                        // read item.jobQuatitiy
                        itemJobQuatitiy = item.getJobQuatitiy();
                    }
                    updateLiveDataRegistration(1, itemJobQuatitiy);


                    if (itemJobQuatitiy != null) {
                        // read item.jobQuatitiy.getValue()
                        itemJobQuatitiyGetValue = itemJobQuatitiy.getValue();
                    }


                    // read ("") + (item.jobQuatitiy.getValue())
                    javaLangStringItemJobQuatitiy = ("") + (itemJobQuatitiyGetValue);
            }
            if ((dirtyFlags & 0xc4L) != 0) {

                    if (item != null) {
                        // read item.currentOrderType
                        itemCurrentOrderType = item.getCurrentOrderType();
                    }
                    updateLiveDataRegistration(2, itemCurrentOrderType);


                    if (itemCurrentOrderType != null) {
                        // read item.currentOrderType.getValue()
                        itemCurrentOrderTypeGetValue = itemCurrentOrderType.getValue();
                    }


                    if (itemCurrentOrderTypeGetValue != null) {
                        // read item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT)
                        itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT = itemCurrentOrderTypeGetValue.equals(dev.vcgroup.thonowandroidapp.data.model.OrderType.MAKE_AN_APPOINTMENT);
                    }
                if((dirtyFlags & 0xc4L) != 0) {
                    if(itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? View.VISIBLE : View.GONE
                    itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTViewVISIBLEViewGONE = ((itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENT) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xc8L) != 0) {

                    if (item != null) {
                        // read item.startDate
                        itemStartDate = item.getStartDate();
                    }
                    updateLiveDataRegistration(3, itemStartDate);


                    if (itemStartDate != null) {
                        // read item.startDate.getValue()
                        itemStartDateGetValue = itemStartDate.getValue();
                    }
            }
            if ((dirtyFlags & 0xd0L) != 0) {

                    if (item != null) {
                        // read item.favoriteWorker
                        itemFavoriteWorker = item.getFavoriteWorker();
                    }
                    updateLiveDataRegistration(4, itemFavoriteWorker);


                    if (itemFavoriteWorker != null) {
                        // read item.favoriteWorker.getValue()
                        itemFavoriteWorkerGetValue = itemFavoriteWorker.getValue();
                    }


                    // read item.favoriteWorker.getValue() != null
                    itemFavoriteWorkerJavaLangObjectNull = (itemFavoriteWorkerGetValue) != (null);
                if((dirtyFlags & 0xd0L) != 0) {
                    if(itemFavoriteWorkerJavaLangObjectNull) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
            }
            if ((dirtyFlags & 0xe0L) != 0) {

                    if (item != null) {
                        // read item.selectedService
                        itemSelectedService = item.getSelectedService();
                    }
                    updateLiveDataRegistration(5, itemSelectedService);


                    if (itemSelectedService != null) {
                        // read item.selectedService.getValue()
                        itemSelectedServiceGetValue = itemSelectedService.getValue();
                    }


                    if (itemSelectedServiceGetValue != null) {
                        // read item.selectedService.getValue().name
                        itemSelectedServiceName = itemSelectedServiceGetValue.getName();
                        // read item.selectedService.getValue().unit
                        itemSelectedServiceUnit = itemSelectedServiceGetValue.getUnit();
                    }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x200L) != 0) {

                if (itemFavoriteWorkerGetValue != null) {
                    // read item.favoriteWorker.getValue().displayName
                    itemFavoriteWorkerDisplayName = itemFavoriteWorkerGetValue.getDisplayName();
                }
        }

        if ((dirtyFlags & 0xd0L) != 0) {

                // read item.favoriteWorker.getValue() != null ? item.favoriteWorker.getValue().displayName : ""
                itemFavoriteWorkerJavaLangObjectNullItemFavoriteWorkerDisplayNameJavaLangString = ((itemFavoriteWorkerJavaLangObjectNull) ? (itemFavoriteWorkerDisplayName) : (""));
        }
        // batch finished
        if ((dirtyFlags & 0xc4L) != 0) {
            // api target 1

            this.makeAnAppointmentPart.setVisibility(itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTViewVISIBLEViewGONE);
            this.makeAnAppointmentPartBreakLine.setVisibility(itemCurrentOrderTypeEqualsOrderTypeMAKEANAPPOINTMENTViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, itemCurrentAddressAddress);
        }
        if ((dirtyFlags & 0xc8L) != 0) {
            // api target 1

            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.convertHourToString(this.mboundView10, itemStartDateGetValue);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.BindingAdapterUtil.convertDateToString(this.mboundView9, itemStartDateGetValue);
        }
        if ((dirtyFlags & 0xc0L) != 0) {
            // api target 1

            this.mboundView10.setOnClickListener(itemOnStartTimeClickListener);
            this.mboundView9.setOnClickListener(itemOnStartDateClickListener);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.RadioGroupBindingAdapter.setCheckedButton(this.rgOrderType, itemCurrentOrderTypeId);
            dev.vcgroup.thonowandroidapp.util.databinding.adapters.RadioGroupBindingAdapter.setListeners(this.rgOrderType, itemRgCheckedChangeListener, (androidx.databinding.InverseBindingListener)null);
        }
        if ((dirtyFlags & 0xe0L) != 0) {
            // api target 1

            this.mboundView3.setSuffixText(itemSelectedServiceUnit);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tipService, itemSelectedServiceName);
        }
        if ((dirtyFlags & 0xd0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tipFavoriteWorker, itemFavoriteWorkerJavaLangObjectNullItemFavoriteWorkerDisplayNameJavaLangString);
        }
        if ((dirtyFlags & 0xc2L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tipJobQuantity, javaLangStringItemJobQuatitiy);
        }
        if ((dirtyFlags & 0x80L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.tipJobQuantity, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, tipJobQuantityandroidTextAttrChanged);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): item.currentAddress
        flag 1 (0x2L): item.jobQuatitiy
        flag 2 (0x3L): item.currentOrderType
        flag 3 (0x4L): item.startDate
        flag 4 (0x5L): item.favoriteWorker
        flag 5 (0x6L): item.selectedService
        flag 6 (0x7L): item
        flag 7 (0x8L): null
        flag 8 (0x9L): item.favoriteWorker.getValue() != null ? item.favoriteWorker.getValue().displayName : ""
        flag 9 (0xaL): item.favoriteWorker.getValue() != null ? item.favoriteWorker.getValue().displayName : ""
        flag 10 (0xbL): item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): item.currentOrderType.getValue().equals(OrderType.MAKE_AN_APPOINTMENT) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}